<?php
 namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Auth;
use DB;
use Exception;
use App\User;
use Log;

class SocialAuthGoogleController extends Controller
{
   public function __construct()
    {
       $user = \Auth::user();
    }
 
  public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }

 
 
 function createUser($getInfo, $provider){
 $user = User::where('provider_id', $getInfo->id)->first();
 
 if (!$user) {
       // check for email exist
                $userExist = User::where('email', $getInfo->email)->first();
                
                if($userExist){
                    User::where('id',$userExist->id)->update(['provider' => 'google',
                                                        'provider_id' => $getInfo->id]);
                    return $userExist;
                } else{
                    $user = new User;
                    $password = 'asd12345';
         $user->first_name  = $getInfo->name;
         $user->email    = $getInfo->email;
         $user->role_id = 2;
         $user->avatar = $getInfo->avatar_original;
         $user->status = '1';
         $user->is_seen = '0';
         $user->provider = 'google';
         $user->provider_id = $getInfo->id;
         $user->password = \Hash::make($password);
     $user->save();


$user = User::where('email',$getInfo->email)->first();

					DB::table('address_book')->insert(
						array(
							'entry_firstname' => $user->first_name ?? '',
							'entry_lastname' => $user->last_name ?? '',
							'customers_id' => $user->id,
							'entry_country_id' => 99,
							'user_id' => $user->id

						));			

   return $user;
                }

   }
   else{
       return $user;
   }
 }
 
 
 public function callback($provider = 'google')
 {
   $getInfo = Socialite::driver('google')->user();
    
   $user = $this->createUser($getInfo, 'google');
  
   auth()->login($user);
   
   return redirect()->to('/');
 }
 

}
