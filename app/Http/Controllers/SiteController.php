<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Core\Products;
use App\Models\Core\ProductsBrand;
use App\Models\Core\Categories;
// use App\Models\Core\Address;
use App\Models\Core\Order;
// use App\Models\Core\OrderItem;
use App\Models\Core\Reviews;
// use App\Wishlist;
use App\Models\Core\Setting;
use App\Models\Core\Career;
// use App\Models\Core\Query;
// use App\Models\Core\Faq;
// use App\Models\Core\Newsletter;
// use App\Models\Core\Help;
use App\User;
use Log;
use DB;
use Auth;
use Mail;
use StdClass;


class SiteController extends Controller
{
  public function getIndex(){
    $companyInfo = new StdClass;
    $address = "";
    $res = Setting::where('name','contact_us_email')->first();
    $companyInfo->email = $res->value;
    $res = Setting::where('name','phone_no')->first();
    $companyInfo->contact_no = $res->value;
    $res = Setting::where('name','address')->first();
    $address = $address + $res->value;
    $res = Setting::where('name','state')->first();
    $address = $address + $res->value;
    $res = Setting::where('name','city')->first();
    $address = $address + $res->value;
    $res = Setting::where('name','zip')->first();
    $address = $address + $res->value;
    $companyInfo->address = $address;


    return view('homeglare2.index', compact('companyInfo'));
  }

  public function getProductDetail(Request $request,$slug){
    $product = Product::where('slug',$slug)->first();
    return view('homeglare2.product-detail',compact('product'));
  }

    // get category wise product
  public function getCategoryWiseProduct($slug, Request $request)
  {
      // code...
  }
    //Category Wise Products Selection
  public function getCategoryProducts($id,Request $request){
    $checkId = Category::where('slug',$id)->first();
    $subcat = $checkId->id;

    $keyword = $request->product_keyword;
    $max_price = Product::where('status','Active')->max("sell_price");

    $cartproducts = array();
    $color = array();
    $brand = array();
    $material = array();
    $size = array();
    $quantity = array();
    $cnt = 0;
    if(session()->get('cart') != null){
      foreach(session()->get('cart') as $cart){
        $cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
        $quantity[$cnt] = $cart->quantity;
        $cnt++;
      }
    }
    else{
      $cartproducts = null;
      $quantity = 0;
    }

    $lastproduct = Product::where('status','Active')->orderBy('created_at','desc')->first();


    $maincategory = Category::where('category_id',0)->where('status','Active')->get();

    foreach($maincategory as $row){
      $count=0;
      $count=Product::where('category_id',$row->id)->where('status','Active')->count();
      $category = Category::where('category_id',$row->id)->where('status','Active')->get();
      if($category){
        foreach($category as $col){
          $count+=Product::where('category_id',$col->id)->where('status','Active')->count();
          $subcategory = Category::where('category_id',$col->id)->where('status','Active')->get();
          if($subcategory){
            foreach($subcategory as $set){
              $count+=Product::where('category_id',$set->id)->where('status','Active')->count();
            }
          }
        }

      }
    }

    $searched_products = array();
    $cat_ids = array();

    $total = array();
    $i=0;
    $sec = Category::where('slug',$id)->where('status','Active')->first();
    $cat_ids[0] = Category::where('id',$sec->id)->where('status','Active')->first();

    $cid = 1;
    $cnt_pro = 0;
    $count_catpro = 0;
    $idss = Category::where('category_id',$sec->id)->where('status','Active')->get();
    if($idss != null){
      foreach($idss as $ids){
        $count_catpro = 0;
        $cat_ids[$cid++] = $ids;
        $idsss = Category::where('category_id',$ids->id)->where('status','Active')->get();
        $count_catpro+=Product::where('category_id',$ids->id)->where('status','Active')->count();
        if($idsss != null){
          foreach($idsss as $ds){
            $cat_ids[$cid++] = $ds;
            $count_catpro+=Product::where('category_id',$ds->id)->where('status','Active')->count();
          }
          if($count==0)
            $total[$i++] = 0;
          else
            $total[$i++] = $count_catpro;
        }
      }
    }

    // foreach ($total as $t){
    //   echo $t." / ";
    // }

    for($j=0 ; $j<$cid ; $j++){
      $products = Product::where('category_id',$cat_ids[$j]->id)->where('status','Active')->get();
      if($products != null){
        foreach($products as $pro){
          $searched_products[$cnt_pro] = $pro;
          $color[$cnt_pro] = $pro->product_color;
          $brand[$cnt_pro] = $pro->product_brand;
          $material[$cnt_pro] = $pro->product_material;
          $cnt_pro++;
        }
      }
    }


    $color = array_filter($color);
    $color = array_unique($color);

    $brand = array_filter($brand);
    $brand = array_unique($brand);

    $material = array_filter($material);
    $material = array_unique($material);

    shuffle($searched_products);

    return view('homeglare2.categorywise-products',compact('maincategory','searched_products','cat_ids','total','lastproduct','max_price','cartproducts','quantity','id','color','brand','material'));
  }

  public function getSearchedProducts(){
   return view('homeglare2.searched-products');
 }

 public function getCart(){
   return view('homeglare2.cart');
 }

 public function getWishlist($id){
  $wishlist = Wishlist::where('user_id',$id)->get();

  return view('homeglare2.wishlist',compact('wishlist'));
}

public function getCheckoutPage(){
  $productDetails = array();
  $totalAmount = 0;
  $price = 0;
  if (session()->get('cart')) {
    foreach (session()->get('cart') as $value) {
      $product_id = explode('_',$value->product_id)[0];
      
      if($value->type == 'sample'){

        $product = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','sample')->first();
        $price = $product->products_price ?? '';
      }
      elseif($value->type == 'normal'){
        $product = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','quantity_based')->first();
        if(isset($product)){
          if($value->quantity >= $product->products_quantity){

            $price = $product->products_price;
          }
          else{
            $product = Products::where('products_id',$product_id)->first();
            $price = $product->products_price ?? '';
          }
        }
        else{
          $product = Products::where('products_id',$product_id)->first();
          $price = $product->products_price;
        }
      }
      
      $product['quantity'] = $value->quantity;
      $product['total_price'] = $price * $value->quantity;
      $totalAmount += $product['total_price'];
      array_push($productDetails, $product);
    }
  } else {
    return redirect('/');
  }

  $success ='';
  $address = Address::where('user_id', Auth::user()->id)->first();
  $allAddress = Address::where('user_id', Auth::user()->id)->where('id','!=',$address->id)->get();

      // Log::info($address);
  return view('homeglare2.checkout', compact('productDetails', 'totalAmount', 'success', 'address', 'allAddress'));
}


public function orderProduct(Request $request){
 $admin = new StdClass;
    //   $admin->email = 'jyotikasethi3007@gmail.com';
 $admin->email = 'support@homeglare.com';
 $admin->name = 'Homeglare';
 
 $data = $request->all();

  // var_dump($data);
  // die;
 $products = $request->session()->get('cart');  
 $orders_id = "";
 $shipping_method = 2;
 $order_status_id = 1;
 $shipping_type = 1;
 $total = 0;
 $shipping_method = $data['shipping_method'] ?? 'courier';
 $totalAmount = $data['totalAmount'];
 $total_tax = $data['gst_tax'] ?? '';
 $coupon_amount = $data['coupon_amount'] ?? '';
 $coupon_code = $data['coupon_code'] ?? '';
 $user_order = "";
 
 if(!empty($data['user_gst'])){
  $user = DB::table('users')->where('id',$data['uid'])->update(array(
    'gst' => $data['user_gst'],
  ));
        // $totalAmount = $totalAmount-$total_tax;
        // $total_tax = 0;
}

if($shipping_method == 'courier'){
  $shipping_method = 5;
  $shipping_type = 'courier';
}
else{
  
  $shipping_type = 'transport';
}

if($data['payment_method'] == 'prepaid'){
  $order_status_id = 1;
}

$user = User::where('id',$data['uid'])->first();  

$last_order = DB::table('orders')->get()->last();

if(empty($last_order)){
  $last_order = new StdClass;
  $last_order->orders_id = 0;
}

if(!isset($data['ship_to_different_address'])){

  $orders_id = DB::table('orders')->insertGetId(
    [ 'orders_id' => $last_order->orders_id+1,
    'customers_id' => $data['uid'] ?? '',
    'customers_name'  => $data['billing_name'] ?? '',
    'customers_street_address' => $data['billing_street_address'] ?? '',
    'customers_city' => $data['billing_city'] ?? '',
    'customers_postcode'  => $data['billing_postcode'] ?? '',
    'customers_state' => $data['billing_state'] ?? '',
    'customers_country'  =>  $data['billing_country'] ?? '',
    'customers_telephone' => $data['billing_phone'] ?? '',
    'email'  => $user->email,

    'delivery_name'  =>  $data['billing_name'] ?? '',
    'delivery_street_address' => $data['billing_street_address'] ?? '',
    'delivery_city' => $data['billing_city'] ?? '',
    'delivery_postcode'  =>  $data['billing_postcode'] ?? '',
    'delivery_state' => $data['billing_state'] ?? '',
    'delivery_country'  => $data['billing_country'] ?? '',
    'billing_name'  => $data['billing_name'] ?? '',
    'billing_street_address' => $data['billing_street_address'] ?? '',
    'billing_city' => $data['billing_city'] ?? '',
    'billing_postcode'  => $data['billing_postcode'] ?? '',
    'billing_state' => $data['billing_state'] ?? '',
    'billing_country'  =>  $data['billing_country'] ?? '',

    'payment_method'  =>  $data['payment_method'] ?? '',
    'last_modified' => date('Y-m-d H:i:s'),
    'date_purchased'  => date('Y-m-d H:i:s'),
    'order_price'  => $totalAmount,
    'shipping_method'  =>  $shipping_method,
    'shipping_type'  =>  $shipping_type,
    'currency'  =>  'INR',
    'currency_value' => 1,
    'order_information' => json_encode($products),
    'ordered_source'    =>   '1',
    'delivery_phone'  =>   $data['billing_phone'] ?? '',
    'billing_phone'   =>   $data['billing_phone'] ?? '',

    'coupon_code' => $coupon_code,
    'coupon_amount' => $coupon_amount,
    'total_tax' => $total_tax,

    'transportation_name'   =>   $data['transportation_name'] ?? '',
    'transportation_email'   =>   $data['transportation_email'] ?? '',
    'transportation_mobile'   =>   $data['transportation_mobile'] ?? '',
    'transportation_address'   =>   $data['transportation_address'] ?? ''    ]);
}
else{

  $orders_id = DB::table('orders')->insertGetId(
    [  'orders_id' => $last_order->orders_id+1,
    'customers_id' => $data['uid'] ?? '',
    'customers_name'  => $data['delivery_name'] ?? '',
    'customers_street_address' => $data['delivery_street_address'] ?? '',
    'customers_city' => $data['delivery_city'] ?? '',
    'customers_postcode'  => $data['delivery_postcode'] ?? '',
    'customers_state' => $data['delivery_state'] ?? '',
    'customers_country'  =>  $data['delivery_country'] ?? '',
    'customers_telephone' => $data['deliver_phone'] ?? '',
    'email'  => $user->email,

    'delivery_name'  =>  $data['delivery_name'] ?? '',
    'delivery_street_address' => $data['delivery_street_address'] ?? '',
    'delivery_city' => $data['delivery_city'] ?? '',
    'delivery_postcode'  =>  $data['delivery_postcode'] ?? '',
    'delivery_state' => $data['delivery_state'] ?? '',
    'delivery_country'  => $data['delivery_country'] ?? '',
    'billing_name'  => $data['billing_name'] ?? '',
    'billing_street_address' => $data['billing_street_address'] ?? '',
    'billing_city' => $data['billing_city'] ?? '',
    'billing_postcode'  => $data['billing_postcode'] ?? '',
    'billing_state' => $data['billing_state'] ?? '',
    'billing_country'  =>  $data['billing_country'] ?? '',

    'payment_method'  =>  $data['payment_method'] ?? '',
    'last_modified' => date('Y-m-d H:i:s'),
    'date_purchased'  => date('Y-m-d H:i:s'),
    'order_price'  => $totalAmount,
    'shipping_method'  =>  $shipping_method,
    'shipping_type'  =>  $shipping_type,
    'currency'  =>  'INR',
    'currency_value' => 1,
    'order_information' => json_encode($products),
    'ordered_source'    =>   '1',
    'delivery_phone'  =>   $data['delivery_phone'] ?? '',
    'billing_phone'   =>   $data['billing_phone'] ?? '',

    'coupon_code' => $coupon_code,
    'coupon_amount' => $coupon_amount,
    'total_tax' => $total_tax,

    'transportation_name'   =>   $data['transportation_name'] ?? '',
    'transportation_email'   =>   $data['transportation_email'] ?? '',
    'transportation_mobile'   =>   $data['transportation_mobile'] ?? '',
    'transportation_address'   =>   $data['transportation_address'] ?? ''
  ]);
}

$last_id = DB::table('orders_status_history')->get()->last();
if(empty($last_id)){
  $last_id = new StdClass;
  $last_id->orders_status_history_id = 0;
}

$user_order = DB::table('orders')->where('customers_id',Auth::user()->id)->get()->last();

$orders_history_id = DB::table('orders_status_history')->insertGetId(
  [  'orders_id'  => $user_order->orders_id,
  'orders_status_id' => $order_status_id,
  'orders_status_history_id' => $last_id->orders_status_history_id+1,
  'date_added'  => date('Y-m-d H:i:s'),
  'customer_notified' =>'1',
  'comments'  =>  " "
]);

foreach(session()->get('cart') as $cart){

  $product_id = explode('_',$cart->product_id)[0];
  $price = 0;

  if($cart->type == 'sample'){

    $product = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','sample')->first();
    $price = $product->products_price ?? '';
  }
  elseif($cart->type == 'normal'){
    $product = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','quantity_based')->first();
    if(isset($product)){
      if($cart->quantity >= $product->products_quantity){

        $price = $product->products_price;
      }
      else{
        $product = Products::where('products_id',$product_id)->first();
        $price = $product->products_price ?? '';
      }
    }
    else{
      $product = Products::where('products_id',$product_id)->first();
      $price = $product->products_price;
    }
  }

  elseif($cart->type == 'carton'){
    $product = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','carton')->first();
    if(isset($product)){
      if($cart->quantity >= $product->products_quantity){

        $price = $product->products_price;
      }
      else{
        $product = Products::where('products_id',$product_id)->first();
        $price = $product->products_price ?? '';
      }
    }
    else{
      $product = Products::where('products_id',$product_id)->first();
      $price = $product->products_price;
    }
  }

  $product = Products::leftjoin('products_description','products_description.products_id','products.products_id')->where('products.products_id',$cart->product_id)->select('products.*','products_description.products_name')->first();


  $final_price = $price*$cart->quantity;

  $orders_products_last = DB::table('orders_products')->get()->last();
  if(empty($orders_products_last )){
    $orders_products_last  = new StdClass;
    $orders_products_last->orders_products_id=0;
  }

  $orders_products_id = DB::table('orders_products')->insertGetId(
    [
      'orders_products_id' => $orders_products_last->orders_products_id+1,
      'orders_id'     =>   $user_order->orders_id,
      'products_id'     =>   $product_id,
      'products_name'   =>   $product['products_name'],
      'products_price'  =>   $price,
      'final_price'     =>   $final_price,
      'products_quantity' =>   $cart->quantity,
      'customers_id' => $data['uid'] ?? '',
    ]);


  $update_quantity = Products::where('products_id',$product_id)->decrement('products_quantity',$cart->quantity);


}


if($data['payment_method'] == 'cash_on_delivery'){
  session()->forget('cart');
  session()->forget('category');
  session()->forget('coupon');


  try {
    $user = Auth::user();

    $total = $totalAmount;

    $orders_id = $user_order->orders_id;

    $orders = DB::table('orders_products')->where('orders_id',$orders_id)->get();
    $name = "";
    $name1 = $user->first_name ?? ' ';
    $name2 = $user->last_name ?? ' ';
    $name = $name1." ".$name2;
    $mobile = $data['billing_phone'];
    
    
    
    Mail::send('mail.orderInvoice', ['user' => $user, 'orders' => $orders, 'totalAmount' => $total, 'orders_id' => $orders_id, 'user_type' => 'User'],
     function ($n) use ($user) {
       $n->from('sales@homeglare.com', 'Homeglare' );

       $n->to($user->email, $user->first_name)->subject('Order Invoice');
     });
    
    Mail::send('mail.orderInvoice', ['user' => $user, 'orders' => $orders, 'totalAmount' => $total, 'orders_id' => $orders_id, 'user_type' => 'Admin'],
     function ($n) use ($admin) {
       $n->from('sales@homeglare.com', 'Homeglare' );

       $n->to($admin->email, $admin->name)->subject('New Order Booked');
     });
    
    $message ="Your%20order%20has%20been%20booked%20successfully%20of%20Rs%2E%20".$total."%20at%20HOMEGLARE%2E%20Thank%20you%20for%20your%20order%2E";
    
    $url = "http://smpp.webtechsolution.co/http-tokenkeyapi.php?authentic-key=3934486f6d65676c6172653030313531391585654410&senderid=HOMGLR&route=1&number=$mobile&message=$message";
    
    $c = curl_init();
    curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($c,CURLOPT_HTTPGET ,1);
    
    curl_setopt($c, CURLOPT_URL, $url);
    $contents = curl_exec($c);
    if (curl_errno($c)) {
     echo 'Curl error: ' . curl_error($c);
   }else{
     curl_close($c);
   }
   
   
   
 }
 catch (Exception $e){

 }

 $success = "Order Booked Successfully";
 $orderItemDetails = DB::table('orders_products')
 ->where('orders_id',$user_order->orders_id)
 ->orderBy('orders_products_id','DESC')
 ->get();
 $order_tran = $user_order;
 $orderId = $user_order->orders_id;

$deduct_amount = DB::table('orders_products')->where('orders_id',$orderId)->where('orders_action','cancelled')->orwhere('orders_action','returned')->sum('cancelled_amount');


 return view('homeglare2.ordered-item-details',compact('orderItemDetails','order_tran','orderId','deduct_amount'));
}

else{
  $amount =  $totalAmount;
  $phone = $data['billing_phone'];
  $user = Auth::user();

  return view('homeglare.event',compact('phone','user','amount'));
}

}



public function getAboutUs(){
  $aboutCompany = Setting::all();
  return view('homeglare2.about-us', compact('aboutCompany'));
}

public function getContactUs(){
  $companyInfo = new StdClass;
  $address = "";
  $res = Setting::where('name','contact_us_email')->first();
  $companyInfo->support_email = $res->value;
  $res = Setting::where('name','phone_no')->first();
  $companyInfo->contact_no = $res->value;
  $res = Setting::where('name','address')->first();
  $address = $address.$res->value.", ";
  $res = Setting::where('name','state')->first();
  $address = $address.$res->value.", ";
  $res = Setting::where('name','city')->first();
  $address = $address.$res->value.", ";
  $res = Setting::where('name','zip')->first();
  $address = $address.$res->value;
  $companyInfo->address = $address;

  return view('homeglare2.contact', compact('companyInfo'));
}

public function getMyAccount(){
  $user = Auth::user();
  $addresses = "";
  $orderDetails = Order::where('user_id', $user->id)->get();
  $address = Address::where('user_id', $user->id)->first();
  if(!empty($address))
    $addresses = Address::where('user_id', $user->id)->where('id','!=',$address->id)->get();
  $help = Help::where('user_id', $user->id)->get();
  return view('homeglare2.my-account', compact('user', 'orderDetails', 'address', 'help','addresses'));
}

public function getFaq(){
  $faq = Faq::where('status', 'active')->orderBy('created_at', 'desc')->get();
  return view('homeglare2.faq', compact('faq'));
}

public function getTermsandCondition(){
 return view('homeglare2.terms');
}

public function getPrivacyPolicy(){
  $companyInfo = Setting::all();
  return view('homeglare2.privacy-policy', compact('companyInfo'));
}

public function getReturnPolicy(){
  $companyInfo = Setting::all();
  return view('homeglare2.return-policy', compact('companyInfo'));
}

public function getCookiePolicy(){
  $companyInfo = Setting::all();
  return view('homeglare2.cookies', compact('companyInfo'));
}

public function getDisclaimer(){
  $companyInfo = Setting::all();
  return view('homeglare2.disclaimer', compact('companyInfo'));
}
    // giveProductRating
public function giveProductRating(Request $request, $id)
{
  $data = $request->all();
  // var_dump($data);

  $review = new Reviews;
  $review->products_id = $id;
  $review->customers_name = $data['customers_name'];
  $review->reviews_rating = $data['reviews_rating'];
  $review->reviews_status = 1;
  $review->reviews_read = 1;

  $review->save();

  $review = Reviews::orderBy('created_at','DESC')->first();

  DB::table('reviews_description')->insert(
    array(
      'review_id' => $review->reviews_id,
      'language_id' => 1,
      'reviews_text' => $data['review']
    ));

  // $data[]
  // Reviews::create($request->toArray());

  return redirect()->back()->with('review-success', 'Thanks for your feedback');
}

    // orderedItemDetails
public function orderedItemDetails($orderId)
{
      // $orderItemDetails = OrderItem::where('order_id', $orderId)->get();
  $orderItemDetails = DB::table('orders_products')
  ->where('orders_id',$orderId)
  ->orderBy('orders_products_id','DESC')
  ->get();
  $order_tran = Order::where('orders_id',$orderId)->first();
  $deduct_amount = DB::table('orders_products')->where('orders_id',$orderId)->where('orders_action','cancelled')->orwhere('orders_action','returned')->sum('products_price');

  return view('homeglare2.ordered-item-details', compact('orderItemDetails','order_tran','orderId','deduct_amount'));
}


    // orderedItemDetails
public function orderedItemTrack($orderId)
{
  $orderItemTrack = array('order_placed' => false, 'processing' => false, 'preparing' => false, 'shipped' => false, 'completed' => false);

  $orderDetails = DB::table('orders_status_history')->leftJoin('orders_status_description','orders_status_history.orders_status_id','orders_status_description.orders_status_id')->where('orders_status_history.orders_id',$orderId)->first();

  if ($orderDetails) {

    switch ($orderDetails->orders_status_name) {
      case 'Pending':
      $orderItemTrack['order_placed'] =true;
      break;
      case 'Processing':
      $orderItemTrack['order_placed'] =true;
      $orderItemTrack['processing'] =true;
      break;
      case 'Preparing':
      $orderItemTrack['order_placed'] =true;
      $orderItemTrack['processing'] =true;
      $orderItemTrack['preparing'] =true;
      break;
      case 'Shipped':
      $orderItemTrack['order_placed'] =true;
      $orderItemTrack['processing'] =true;
      $orderItemTrack['preparing'] =true;
      $orderItemTrack['shipped'] =true;
      break;
      case 'Completed':
      $orderItemTrack['order_placed'] =true;
      $orderItemTrack['processing'] =true;
      $orderItemTrack['preparing'] =true;
      $orderItemTrack['shipped'] =true;
      $orderItemTrack['completed'] =true;
      break;
      default:
            // code...
      break;
    }

  }
      // Log::info($orderItemTrack);
  return view('homeglare2.ordered-item-track', compact('orderItemTrack'));
}
    // globalSearchProduct
public function globalSearchProduct(Request $request)
{

  $page = $request->page ?? '0';
  if($page>0){
    $page--;
  }

  $id="all";
  $searched_products = Products::leftjoin('products_description','products_description.products_id','products.products_id')
  ->leftjoin('images','images.id','products.products_image')
  ->leftjoin('image_categories','image_categories.image_id','images.id')
  ->where('image_categories.image_type','ACTUAL')
  ->where('products.products_status','1')
  ->where('products_description.products_name','LIKE','%'.$request->product_keyword.'%')
  ->orderBy('products_description.products_name','ASC')
  ->select('products.*','products_description.*','images.id','image_categories.path')
  ->paginate(52);

  $keyword  = $request->product_keyword;
  
  $brands = ProductsBrand::where('status','1')->orderBy('brand_name','ASC')->get();
  
  $cat_ids = Categories::leftjoin('categories_description','categories_description.categories_id','categories.categories_id')
  ->where('categories.parent_id',0)->where('categories.categories_status','1')->get();
  
  return view('homeglare2.product', compact('searched_products','keyword','id','brands','page','cat_ids'));
}


    /*
     * share snap on social site
     */
    public function shareLinkOnSocialSite($siteKey , $slug_url, $product_id = null, $user_badge = null, Request $request)
    {
     $productDetails = Product::where('slug', $slug_url)->first();
     if ($productDetails) {

       $shareUrl = env('APP_URL').'/product-detail/'.$productDetails->slug;
       $redirectBackurl = env('APP_URL').'/SharedLink/callback/fb?product_id='.$productDetails->id;
       $imageUrl =  env('APP_URL').'/'.$productDetails->image2;
       $text = $productDetails->long_descriptions;
       switch($siteKey)
       {
         case 'fb':
            // $this->increamentSocialShare($review_badge,"facebook");
             // $fbAppId = 745670775803120;
         $fbUrl = 'https://www.facebook.com/dialog/share?app_id='.env('FACEBOOK_APP_ID').'&display=page'.'&href='.urlencode($shareUrl).'&redirect_uri='.urlencode($redirectBackurl);
         return redirect()->intended($fbUrl);

         break;

         case 'tw':
                 // $this->increamentSocialShare($review_badge,"twitter");
         $twUrl="https://twitter.com/intent/tweet?url=".urlencode($shareUrl)."&text=".urlencode($text);
         return redirect()->intended($twUrl);
         break;
         case 'pi':
              // $this->increamentSocialShare($review_badge,"pinterest");
         $piUrl ="https://www.pinterest.com/pin/create/button/?url=".urlencode($shareUrl)."&media=".$imageUrl."&description=".urlencode($text);
         return redirect()->intended($piUrl);
         break;
       }
     }else {
       return redirect('/');
     }
   }

       /*
        * callback share review on social site
        */
       public function SharedLinkCallback($siteKey,Request $request)
       {
          // Log::info($request);
          // Log::info($request->input('snap_badge'));
        $productDetails = Product::where('id', $request->input('product_id'))->first();
        if ($productDetails) {
          switch($siteKey)
          {
            case 'fb':
            $redirectBackurl = $_ENV['APP_URL'].'/product-detail/'.$productDetails->slug;
            return redirect()->intended($redirectBackurl) ;
            break;
          }
        }else {
          return redirect('/');
        }
      }
      // contactUs
      public function contactUs(Request $request)
      {
        $company = Setting::where('name','contact_us_email')->first();
        $request['send_email'] = $company->value ?? 'support@homeglare.com';
        $request['response_status'] = 'awating';
        
        $query = DB::table('contact_query')->insert(
          array(
            'contact_name' => $request->contact_name,
            'contact_email' => $request->contact_email,
            'contact_subject' => $request->contact_subject,
            'contact_message' => $request->contact_message
          ));
        if($query){
          try {
            
            Mail::send('mail.contact', ['request' => $request],
             function ($m) use ($request) {
               $m->from('noreply@homeglare.com', 'Homeglare' );

               $m->to($request['send_email'], 'Homeglare')->subject('Welcome to Homeglare');
             });

          }
          catch(Exception $e)
          {

          }
        }
        return redirect()->back()->with('status', 'Thank you for contacting us!');
      }
      // Newsletter
      public function createNewsletter(Request $request)
      {
       
       $user = Auth::user();
       
       if ($request->has('email')) {
        $newsletter = DB::table('customers')->where('user_id',$user->id)->first();
        
        if($newsletter){
          if($newsletter->customers_newsletter == $request->email){
            return redirect()->back()->with('newslatter', 'Already Registered. Thank You"');
          }
          else{
            $updateNewsletter = DB::table('customers')->update(array(
              'customers_newsletter' => $request->email,
            ));
            return redirect()->back()->with('newslatter', 'Thank you. We have update your old email with new one.');
          }
        }
        else {
          $newsCreate =  DB::table('customers')->insert(array(
            'customers_newsletter' => $request->email,
            'user_id' => $user->id,
            'customers_id' => $user->id,
          ));
          
          
          $admin = new StdClass;
          $admin->email = 'support@homeglare.com';
          $admin->name = 'User';
          
          
          $email = $request->input('email'); 
          Mail::send('mail.newsletter', ['email' => $email],
           function ($m) use ($admin) {
             $m->from('support@homeglare.com', env('APP_NAME') );

             $m->to($admin->email, $admin->name)->subject('Newsletter Subscription.');
           });

          return redirect()->back()->with('newslatter', 'Thank you .We will reply you soon');
        }
      }else {
        return redirect()->back()->with('newslatter', 'First Enter Your Email');
      }
    }
    
    
    public function getCareer(){
      return view('homeglare2.career');
    }

public function postCareer(Request $request){
  $data = $request->all();

  $newData = new Career;

  $newData->name  = $data['name'] ?? '';
  $newData->email  = $data['email'] ?? '';
  $newData->role  = $data['role'] ?? '';
  $newData->skills  = $data['skills'] ?? '';
  $newData->experience  = $data['experience'] ?? '';
  $newData->message  = $data['message'] ?? '';
  $newData->save();

      return redirect()->back()->with('careerSuccess','1') ;
    }

    public function getComplaint(){
      return view('homeglare2.complaint');
    }

    public function getFeedback(){
      return view('homeglare2.feedback');
    }
    
    public function postFeedback(Request $request){
      $data = $request->all();
      
      $store = DB::table('customers_feedback')->insert(
        array(
          'customers_id' => $data['customers_id'],
          'customers_name' => $data['customers_name'],
          'customers_email' => $data['customers_email'],
          'customers_feedback' => $data['customers_feedback'],
          'customers_rating' => $data['customers_rating'],
          'review_type' => 'feedback',
        ));
      
      $email = $data['customers_email'];
      $name = $data['customers_name'];
      $feedback = $data['customers_feedback'];
      
      $user = User::where('email',$email)->first();
      
      if(!$user){
        $user = new StdClass;
        $user->email = $email;
        $user->first_name = $name;
      }
      
      
      Mail::send('mail.customerFeedback', ['name' => $name, 'email' => $email, 'feedback' => $feedback, 'review_type' => 'Feedback'],
       function ($m) use ($user){
         $m->from('sales@homeglare.com', 'Homeglare' );
         
         $m->to($user->email, $user->first_name)->subject('Feedback Successfully Submitted');
       });
      
      $admin = new StdClass;
      $admin->email = 'support@homeglare.com';
      $admin->name = 'Homeglare';
      
      
      Mail::send('mail.adminFeedback', ['name' => $name, 'email' => $email, 'feedback' => $feedback, 'review_type' => 'Feedback', ],
       function ($m) use ($admin) {
         $m->from('sales@homeglare.com', 'Homeglare' );

         $m->to($admin->email, $admin->name)->subject("Customer's Feedback");
       });

      return redirect()->back()->with('message','Thank you for your feedback.');
    }
    
    public function postComplaint(Request $request){
      $data = $request->all();
      
      $store = DB::table('customers_feedback')->insert(
        array(
          'customers_id' => $data['customers_id'],
          'customers_name' => $data['customers_name'],
          'customers_email' => $data['customers_email'],
          'customers_feedback' => $data['customers_feedback'],
          'customers_rating' => '0',
          'review_type' => 'complaint',
        ));
      
      $email = $data['customers_email'];
      $name = $data['customers_name'];
      $feedback = $data['customers_feedback'];
      
      $user = User::where('email',$email)->first();
      
      if(!$user){
        $user = new StdClass;
        $user->email = $email;
        $user->first_name = $name;
      }
      
      
      Mail::send('mail.customerFeedback', ['name' => $name, 'email' => $email, 'feedback' => $feedback, 'review_type' => 'complaint'],
       function ($m) use ($user){
         $m->from('sales@homeglare.com', 'Homeglare' );
         
         $m->to($user->email, $user->first_name)->subject('Complaint Successfully Submitted');
       });
      
      $admin = new StdClass;
      $admin->email = 'support@homeglare.com';
      $admin->name = 'Homeglare';
      
      Mail::send('mail.adminFeedback', ['name' => $name, 'email' => $email, 'feedback' => $feedback, 'review_type' => 'complaint', ],
       function ($m) use ($admin) {
         $m->from('sales@homeglare.com', 'Homeglare' );

         $m->to($admin->email, $admin->name)->subject("Customer's Complaint");
       });

      return redirect()->back()->with('message','Our team will contact you soon.');
    }


    public function orderAction($orderId, $action, Request $request){
      $data = $request->all();

    //   var_dump($data); die;
      if(empty($data)){
      return redirect()->back()->with('requestCancel','2');
      }
      $admin = new StdClass;
      
       $order = DB::table('orders')->where('orders_id',$orderId)->first();

      $admin->email = 'support@homeglare.com';
      $admin->name = 'Homeglare';
      if(!empty($data['product']) && $data['product'] == 'on'){
          $products = DB::table('orders_products')->where('orders_id',$orderId)->get();
          if(!empty($products)){
            $i=0;
            foreach($products as $pro){
              $data['products'][$i++] = $pro->products_id;
            }
          }
          $orderUpdate = DB::table('orders')->where('orders_id',$orderId)->update(array(
            'order_action' => $action
          ));
          $productUpdate = DB::table('orders_products')->where('orders_id',$orderId)->update(array(
            'orders_action' => 'cancel'
          ));
      }
      
      else{
        foreach($data['products'] as $pro){
          $productUpdate = DB::table('orders_products')->where('orders_products_id',$pro)->update(array(
            'orders_action' => 'cancel'
          ));

        }
      }


      
      $products = DB::table('orders_products')->leftjoin('products_description','products_description.products_id','orders_products.products_id')->where('orders_products.orders_id',$orderId)->whereIn('orders_products.products_id',$data['products'])->select('products_description.products_name','orders_products.*')->get();

      $user = Auth::user();

      $subject = ucfirst($action)." Order";


    //   Mail::send('mail.orderActionUser', ['action' => $action, "products" => $products, 'orderId' => $orderId, 'order' => $order],
    //   function ($n) use ($user) {
    //      $n->from('sales@homeglare.com', 'Homeglare' );

    //      $n->to($user->email, $user->first_name)->subject('Order Return/Cancel');
    //   });
      
    //   Mail::send('mail.orderAction', ['action' => $action, "products" => $products, 'orderId' => $orderId, 'order' => $order, 'user' => $user],
    //   function ($n) use ($admin) {
    //      $n->from('sales@homeglare.com', 'Homeglare' );

    //      $n->to($admin->email, $admin->name)->subject('Order Return/Cancel');
    //   });

      return redirect()->back()->with('requestCancel','1');
    }



    public function orderedProducts($orderId){
 $orderItemDetails = DB::table('orders_products')
 ->where('orders_id',$orderId)
 ->orderBy('orders_products_id','DESC')
 ->get();
 $order_tran = DB::table('orders')->where('orders_id',$orderId)->first();;
 $orderId = $orderId;
 $deduct_amount = DB::table('orders_products')->where('orders_id',$orderId)->where('orders_action','cancelled')->orwhere('orders_action','returned')->sum('cancelled_amount');


 return view('homeglare2.ordered-products',compact('orderItemDetails','order_tran','orderId','deduct_amount'));
    }
    
    
  }
