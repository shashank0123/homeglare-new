<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Core\Products;
use App\Models\Core\Order;
use App\User;
use DB,Mail,Auth;

class PayController extends Controller
{

   // public function index()
   // {
   //      return view('homeglare.event');
   // }

 public function pay(Request $request){


   $api = new \Instamojo\Instamojo(
     '727a0973ed682283b9a32b0bad041663',
     '9f28dde74290a0157f916f3abfd9a186',
     'https://www.instamojo.com/api/1.1/'
   );

   
   try {
    $response = $api->paymentRequestCreate(array(
      "purpose" => "Product Purchase",
      "amount" => $request->amount,
      "buyer_name" => "$request->name",
      "send_email" => true,
      "email" => "$request->email",
      "phone" => "$request->mobile_number",
      "redirect_url" => "http://homeglare.com/pay-success"
    ));


    header('Location: ' . $response['longurl']);
    exit();
  }catch (Exception $e) {
    print('Error: ' . $e->getMessage());
  }
}

public function success(Request $request){

  $message = 'Something Went Wrong';

  $data = $request->all();
  $data = $data['payment_request_id'];
  $orders = "";
  try {

   $api = new \Instamojo\Instamojo(
     '727a0973ed682283b9a32b0bad041663',
     '9f28dde74290a0157f916f3abfd9a186',
     'https://www.instamojo.com/api/1.1/'
   );

   $response = $api->paymentRequestStatus(request('payment_request_id'));


   if( !isset($response['payments'][0]['status']) ) {
     return redirect()->back()->with('message',$message);
   } else if($response['payments'][0]['status'] != 'Credit') {
     return redirect()->back()->with('message',$message); 
   } 
   else if($response['payments'][0]['status'] == 'Credit'){

     $email = $response['email'];

     $user = User::where('email',$email)->first();


     $payment = Order::where('customers_id',$user->id)->orderBy('orders_id','DESC')->first();

     $txn_id = $response['payments'][0]['payment_id'];

     DB::table('orders')->where('orders_id',$payment->orders_id)->update(
       array(
         'transaction_id' => $txn_id,
       )
     );

     $orders_history_id = DB::table('orders_status_history')->where('orders_id', $payment->orders_id)->update(
      [  
        'orders_status_id' => '1',
      ]);
    
     $message = 'Payment success';
     $orders = $payment;

     session()->forget('cart');
     session()->forget('category');
     session()->forget('coupon');

     if($orders){

      try {
        $user = Auth::user();
        $orders_id = $orders->orders_id;
        $total = $orders->order_price;
        $name = "";
        $name1 = $user->first_name ?? ' ';
        $name2 = $user->last_name ?? ' ';
        $name = $name1." ".$name2;

        $mobile = $orders->billing_phone;

        $orders = DB::table('orders_products')->where('orders_id',$orders_id)->get();

        $user->admin_email = 'supporthomeglare.com';
        $user->admin_name = 'Homeglare';

        Mail::send('mail.orderInvoice', ['user' => $user, 'orders' => $orders, 'totalAmount' => $total, 'orders_id' => $orders_id, 'user_type' => 'User'],
         function ($m) use ($user) {
           $m->from('sales@homeglare.com', 'Homeglare' );

           $m->to($user->email, $user->first_name)->subject('Order Invoice');
         });

        Mail::send('mail.orderInvoice', ['user' => $user, 'orders' => $orders, 'totalAmount' => $total, 'orders_id' => $orders_id, 'user_type' => 'Admin'],
         function ($m) use ($user) {
           $m->from('sales@homeglare.com', 'Homeglare' );

           $m->to($user->admin_email, $user->admin_name)->subject('New Order Booked');
         });


        $message ="Your%20order%20has%20been%20booked%20successfully%20of%20Rs%2E%20".$total."%20at%20HOMEGLARE%2E%20Thank%20you%20for%20your%20order%2E";

        $url = "http://smpp.webtechsolution.co/http-tokenkeyapi.php?authentic-key=3934486f6d65676c6172653030313531391585654410&senderid=HOMGLR&route=1&number=$mobile&message=$message";

        $c = curl_init();
        curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($c,CURLOPT_HTTPGET ,1);

        curl_setopt($c, CURLOPT_URL, $url);
        $contents = curl_exec($c);
        if (curl_errno($c)) {
         echo 'Curl error: ' . curl_error($c);
       }else{
         curl_close($c);
       }      

     }
     catch (Exception $e){

     }
   }

   $success = "Order Booked Successfully";
   $orderItemDetails = DB::table('orders_products')
   ->where('orders_id',$orders->orders_id)
   ->orderBy('orders_products_id','DESC')
   ->get();
   $order_tran = $orders;
   $orderId = $orders->orders_id;

   return view('homeglare2.ordered-item-details',compact('orderItemDetails','order_tran','orderId'));


   return view('homeglare2.ordersuccess')->with('message',$message);

 }
}catch (\Exception $e) {
 return redirect()->back()->with('message',$message);
}

}
}