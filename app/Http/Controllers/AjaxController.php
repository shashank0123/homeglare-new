<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Wishlist;
// use App\Cart;
use App\Models\Core\Coupon;
use App\Models\Core\Products;
use App\Models\Core\ProductsBrand;
use App\Models\Core\Categories;
// use App\Models\Core\Newsletter;
use App\User;
// use App\Models\Core\Address;
use DB;
use Auth;
use Hash;
use StdClass;
use Log;


class AjaxController extends Controller
{
    public function addToCart(Request $request){

        $cart = new StdClass;

        $cartItem = 0;
        $tax = 0;

        $unitprice = 0;
        

        $category = $request->category ?? 'moq';

        $product_id = $request->id;
        $quantity = $request->quantity_moq ?? '1';
        
        $check_product = DB::table('products_price')->where('products_id',$product_id)->get();
        $type = $request->type;

        // var_dump($type);die;

        $product = DB::table('products')->where('products_id',$product_id)->first();
        $check_quantity = $product->products_quantity;

        $tax = $product->tax_percent ?? 0;
        $product_price = $product->products_price;
        
        
        if(count($check_product)>0 && $type == 'normal'){

            if($category == 'moq'){
                $check_product = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','quantity_based')->first();
                if($quantity==1){
                    $quantity = $check_product->products_quantity;
                }
                $product_price = $check_product->products_price;
            }
        }
        else if(count($check_product)>0 && $type == 'carton'){
            if($category == 'carton'){
                $check_carton = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','carton')->first();
                $quantity = $request->quantity_carton ?? '1';
                $product_price = $check_carton->products_price ?? $product->products_price;
                $quantity = $check_carton->products_quantity * $quantity;
            }
        }
        
        if($type == 'sample'){

            $check_product = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','sample')->first();
            if($check_product){
                $quantity = $check_product->products_quantity;
                $product_price = $check_product->products_price;
            }
        }
        
        $pro_id = $product_id."_".$type;
        if($type == 'sample'){
            $quantity = 1;
        }        

        if (session()->get('cart') != null){
            $cart = session()->get('cart');
        }
        else {
            $cart = array();
        }

        if (isset($cart[$pro_id]->quantity) && $type=='carton' && $category=='carton' ){

            $qty = $cart[$pro_id]->quantity + $quantity;
            if($check_quantity<$qty){
                return 0;
            }
            $cart[$pro_id]->quantity = $cart[$pro_id]->quantity + $quantity;
            $cart[$pro_id]->total = ($cart[$pro_id]->unit_price+(($cart[$pro_id]->tax * $cart[$pro_id]->unit_price)/100))*$cart[$pro_id]->quantity;
        }
        elseif(isset($cart[$pro_id]->quantity) && $type=='normal' && $category=='moq'){

            $qty = $cart[$pro_id]->quantity + $quantity;
            if($check_quantity<$qty){
                return 0;
            }

            $cart[$pro_id]->quantity = $cart[$pro_id]->quantity + $quantity;       
            $cart[$pro_id]->total = ($cart[$pro_id]->unit_price+(($cart[$pro_id]->tax * $cart[$pro_id]->unit_price)/100))*$cart[$pro_id]->quantity     ;
        }
        else{

            $item = new StdClass;
            if($check_quantity>=$quantity){
                $item->product_id = $product_id."_".$type;
                $item->quantity = $quantity;
                $item->type = $type;
                $item->category = $category;
                $item->unit_price = $product_price;
                $item->tax = $tax;
                $item->total = ($product_price+(($tax * $product_price)/100))*$quantity;
                $cart[$pro_id] = $item;
                $unitprice = ceil($product_price+(($tax * $product_price)/100));
            }
            else{
                return 0;
            }


        }

        $request->session()->put('cart',$cart);

        $selected_category = new StdClass;
        if($type == 'sample'){
            $selected_category = 'sample';
        }
        elseif($category == 'moq'){
            $selected_category = 'moq';
        }
        else{                
            $selected_category = 'carton';
        }
        $request->session()->put('category',$selected_category);


        foreach(session()->get('cart') as $getCart){
            $cartItem++;
        }

        if(!$cart){
            echo "Something Went Worng";
        }


        return response()->json(['status' => 200, 'message' => 'data retrieved','cartItem' => $cartItem , 'unitprice' => $unitprice]);
    }

    public function addToWishlist(Request $request){
        $product_id = $request->id;
        $user_id = $request->userId;
        $dated = date('Y-m-d H:i:s');
        $response = new StdCLass;
        $response->wishItem = 0;
        $check = DB::table('liked_products')->where('liked_customers_id',$user_id)->where('liked_products_id',$product_id)->first();
        if($check){

        }
        else{
            DB::table('liked_products')->insert(
                array(
                    'liked_customers_id' => $user_id,
                    'liked_products_id' => $product_id,
                    'date_liked' => $dated
                ));
        }

        $response->wishItem = DB::table('liked_products')->where('liked_customers_id',$user_id)->count();

        return response()->json($response);

    }



    public function submitNewsletter(Request $request)
    {
        $response = new StdClass;
        $response->status = 200;
        $response->message = 'Something went wrong';
        $newsletter = new Newsletter;
        $checkemail = Newsletter::where('email',$request->email)->first();

        if($checkemail){
            $response->message = "Already Registered. Thank You";
            return response()->json($response);
        }
        else{
            if($request->email == null){
                $response->message = 'First Enter Your Email';
                $response->email = $request->email;
                return response()->json($response);
            }
            else{
                $newsletter->email = $request->email;
                $newsletter->save();
                if($newsletter){
                    $response->message ="Thank you .We will reply you soon";
                    return response()->json($response);
                }
                else{
                    return response()->json($response);
                }
            }
        }
    }

    public function deleteWishlistItem(Request $request){
        $flag = 0;
        $response = new StdClass;
        $response->flag = 0;
        $response->wishItem=0;
        // $wishlist = Wishlist::where('id',$request->id)->first();
        $wishlist = DB::table('liked_products')->where('like_id',$request->id)->delete();

        $cartItem = 0;

        // $response->wishItem = Wishlist::where('user_id',$request->user_id)->count();
        $response->wishItem = DB::table('liked_products')->where('liked_customers_id',$request->user_id)->count();

        return response()->json($response);
    }

    //To show session data on Mini Cart
    public function showMiniCart(){
        $cartproducts = array();
        $quantity = array();
        $prices = array();
        $pro_id = array();
        $i = 0;
        $bill = 0;

        // var_dump(session()->get('cart'));
        // die;
        if(session()->get('cart') != null){
            foreach(session()->get('cart') as $cart){

                $product_id = explode('_',$cart->product_id)[0];
                // $cartproducts[$i] = Products::where('products_id',$cart->product_id)->first();
                $cartproducts[$i] = Products::leftjoin('products_description','products_description.products_id','products.products_id')
                ->leftjoin('images','images.id','products.products_image')
                ->leftjoin('image_categories','image_categories.image_id','images.id')
                ->where('image_categories.image_type','ACTUAL')
                ->where('products.products_id',$product_id)
                ->orderBy('products.created_at','DESC')
                ->select('products.*','products_description.*','images.id','image_categories.path')
                ->first();
                $quantity[$i] = $cart->quantity;
                $pro_id[$i] = $cart->product_id;
                $i++;
            }
            $j=0;
            foreach(session()->get('cart') as $cart){
                $quantity[$j];
                $j++;
            }

            foreach(session()->get('cart') as $pro){
                $product_id = explode('_',$pro->product_id)[0];
                $product = ""; $price = 0;
                if($pro->type == 'sample'){

                    $product = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','sample')->first();
                    $price = $product->products_price ?? '';
                }
                elseif($pro->type == 'normal'){
                    $product = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','quantity_based')->first();
                    if(isset($product)){
                        if($pro->quantity >= $product->products_quantity){

                            $price = $product->products_price;
                        }
                        else{
                            $product = Products::where('products_id',$product_id)->first();
                            $price = $product->products_price ?? '';
                        }
                    }
                    else{
                        $product = Products::where('products_id',$product_id)->first();
                        $price = $product->products_price;
                    }
                }
                elseif($pro->type == 'carton'){
                    $product = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','carton')->first();
                    if(isset($product)){
                        if($pro->quantity >= $product->products_quantity){

                            $price = $product->products_price;
                        }
                        else{
                            $product = Products::where('products_id',$product_id)->first();
                            $price = $product->products_price ?? '';
                        }
                    }
                    else{
                        $product = Products::where('products_id',$product_id)->first();
                        $price = $product->products_price;
                    }
                }
                $prices[] = $price;
                $bill = $bill + $pro->quantity*$price;
            }
        }

        return view('homeglare2.minicart',compact('cartproducts','quantity','bill','prices','pro_id'))->render();
        
    }

    public function deleteSessionData(Request $request)
    {
        // var_dump(session()->get('cart')); die;
        $flag = 1 ;
        $bill = 0;
        $cart = session()->get('cart');
        $cartItem = 0;
        $total = 0;
        $status = 1;
        $decimal_bill = 0;
        $price = 0;
        $total_tax = 0;
        $id = $request->id;
        $cart[$id] = null;
        $cart = array_filter($cart);
        if(count($cart)==0){
            session()->forget('category');
            session()->forget('coupon');
            session()->forget('cart');
            $status = 0;
        }
        $request->session()->put('cart',$cart);


        foreach(session()->get('cart') as $pro){


            $product_id = explode('_',$pro->product_id)[0];


            if($pro->type == 'sample'){

                $product = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','sample')->first();
                $price = $product->products_price ?? '';
            }
            elseif($pro->type == 'normal'){
                $product = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','quantity_based')->first();
                if(isset($product)){

                    if($pro->quantity >= $product->products_quantity){
                        $price = $product->products_price;
                    }
                    else{
                        $product = Products::where('products_id',$product_id)->first();
                        $price = $product->products_price ?? '';
                    }
                }
                else{
                    $product = Products::where('products_id',$product_id)->first();
                    $price = $product->products_price ?? '';
                    $fix_quant = 1;
                }
            }


            $product = DB::table('products')->where('products_id',$product_id)->first();
            $total_tax = $total_tax+(($product->tax_percent*$price)/100)*$pro->quantity;
            // $total_price = $total_price + $pro->quantity*$price;
            $total_tax = number_format($total_tax, 2, '.', ',');

            $cartItem++;
            // $product = Products::where('products_id',$pro->product_id)->first();

                // $total_tax = $total_tax + $pro->tax;
            $total = $total + $pro->quantity*$price;
                // $bill = $bill + $pro->quantity*$price + $pro->tax;
        }
        $bill = $total + $total_tax;
        $decimal_bill = $bill;
        $bill = ceil($bill);
        if($flag == 1)
            return response()->json(['status' => 200, 'message' => 'Data Deleted Successfully','bill' => $bill, 'cartItem' => $cartItem, 'total_tax' => $total_tax, 'total' => $total, 'decimal_bill' => $decimal_bill, 'status' => $status]);
        else
            return response()->json(['status' => 201, 'message' => 'Something Went Wrong']);
    }

    public function updateCart(Request $request)
    {
        $flag = 1 ;
        $bill = 0;
        $total = 0;
        $total_tax = 0;
        $unitprice = $request->unit_price;
        $total_price = 0;
        $product_id = explode('_',$request->id)[0];
        $tax = $request->tax ?? 0;
        $action = $request->action;
        $fix_quant = $request->fix_quant;
        $decimal_bill = 0;

        $product_tax = 0;

        $item = Products::where('products_id',$request->id)->first();
        $check_quantity = $item->products_quantity;
        $cart = session()->get('cart');
        if($action == 'plus'){
            $qty = $cart[$request->id]->quantity + $fix_quant;
            if($check_quantity<$qty){
                return response()->json(['status' => 0]);
            }
            $cart[$request->id]->quantity = $cart[$request->id]->quantity+$fix_quant;
            $product_tax = ($tax*$unitprice*$cart[$request->id]->quantity)/100;
            $product_tax = number_format($product_tax, 2, '.', ',');
        }
        else{
         $cart[$request->id]->quantity = $cart[$request->id]->quantity-$fix_quant;
         $product_tax = ($tax*$unitprice*$cart[$request->id]->quantity)/100;
         $product_tax = number_format($product_tax, 2, '.', ',');
     }


     $total = $cart[$request->id]->quantity * $unitprice;
     $cart = array_filter($cart);
     $request->session()->put('cart',$cart);

     foreach(session()->get('cart') as $pro){
        $price = 0;
        $product_id = explode('_',$pro->product_id)[0];

        if($pro->type == 'sample'){

            $product = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','sample')->first();
            $price = $product->products_price ?? '';
        }
        elseif($pro->type == 'normal' ){
            $product = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','quantity_based')->first();
            if(isset($product)){
                if($pro->quantity >= $product->products_quantity){

                    $price = $product->products_price;
                }
                else{
                    $product = Products::where('products_id',$product_id)->first();
                    $price = $product->products_price ?? '';
                }
            }
            else{
                $product = Products::where('products_id',$product_id)->first();
                $price = $product->products_price ?? '';
            }
        }
        elseif($pro->type == 'carton'){
            $product = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','carton')->first();
            if(isset($product)){
                $price = $product->products_price;
            }
            else{
                $product = Products::where('products_id',$product_id)->first();
                $price = $product->products_price ?? '';
            }
        }

        $product = DB::table('products')->where('products_id',$product_id)->first();
        $total_tax = $total_tax+((($tax*$price)/100)*$pro->quantity);
        $total_price = $total_price + $pro->quantity*$price;
        $bill = $bill + $pro->quantity*$price + (($tax*$price)/100)*$pro->quantity;
    }
    $total_tax = number_format($total_tax, 2, '.', ',');
    $decimal_bill = $bill;
    $bill = ceil($bill);


    if($flag == 1)
        return response()->json(['status' => 200, 'message' => 'Data Deleted Successfully', 'bill' => $bill, 'decimal_bill'=> $decimal_bill, 'total_price' => $total_price, 'total' => $total, 'unitprice' => $unitprice, 'product_tax' => $product_tax,'total_tax' => $total_tax, 'status' => '1']);
    else
        return response()->json(['status' => 201, 'message' => 'Something Went Wrong']);
}


public function showProductCat(Request $request){
    $searched_products = array();
    $count = 0 ;
    $id = array();
    $i=1;

    $checkId = Categories::where('id',$request->cat_id)->first();
    $send_id = $checkId->id;
    $id[0] = $checkId->id;
    if($id[0] == 0)
    {
        $products = Products::where('status','Active')->get();
        foreach($products as $pro){
            $searched_products[$count++] = $pro;
        }
    }
    else{
        $cat_id = Categories::where('categories_id',$id[0])->where('status','Active')->get();
        if($cat_id != null){
            foreach($cat_id as $cat){
                $id[$i] = $cat->id;
                $sub_id =Categories::where('categories_id',$id[$i])->where('status','Active')->get();
                $i++;
                if($sub_id != null){
                    foreach($sub_id as $sub){
                        $id[$i] = $sub->id;
                        $i++;
                    }
                }
            }
        }
        for($j=0 ; $j<$i ; $j++){
            $products= Products::where('categories_id',$id[$j])->where('status','Active')->get();
            foreach($products as $pro){
                $searched_products[$count++] = $pro;
            }
        }
    }
    shuffle($searched_products);
    return view('homeglare2.product_cat',compact('searched_products','send_id'));
}

    //Show Sorted Products on Search Category Page
public function sorting(Request $request){

    $searched_products = array();
    $min = $request['min_cost'];
    $max = $request['max_cost'];
    $products = "";
    $send_id = "";
    $i=0;
    $sort_type = $request->sort_type ?? '';
    $brand_id = $request->brand ?? '';
    $keyword = $request->keyword ?? '';

    if($request->cat_id == 'all'){

        if(!empty($keyword)){

            if($brand_id>0){
                $products = DB::table('products')
                ->leftjoin('products_description','products_description.products_id','products.products_id')
                ->leftjoin('images','images.id','products.products_image')
                ->leftjoin('image_categories','image_categories.image_id','images.id')
                ->where('products_description.products_name','LIKE','%'.$keyword.'%')
                ->where('products.products_brand',$brand_id)
                ->where('image_categories.image_type','ACTUAL')
                ->where('products.products_status',1)
                ->orderBy('products.created_at','DESC')
                ->select('products.*','products_description.*','images.id','image_categories.path')
                ->distinct('products.products_id')
                ->get();
            }
            else{

                $products = DB::table('products')
                ->leftjoin('products_description','products_description.products_id','products.products_id')
                ->leftjoin('images','images.id','products.products_image')
                ->leftjoin('image_categories','image_categories.image_id','images.id')
                ->where('products_description.products_name','LIKE','%'.$keyword.'%')
                ->where('image_categories.image_type','ACTUAL')
                ->where('products.products_status',1)
                ->orderBy('products.created_at','DESC')
                ->select('products.*','products_description.*','images.id','image_categories.path')
                ->distinct('products.products_id')
                ->get();
            }
        }
        else{
            if($brand_id>0){
                $products = DB::table('products')
                ->leftjoin('products_description','products_description.products_id','products.products_id')
                ->leftjoin('images','images.id','products.products_image')
                ->leftjoin('image_categories','image_categories.image_id','images.id')
                ->where('products.products_brand',$brand_id)
                ->where('image_categories.image_type','ACTUAL')
                ->where('products.products_status',1)
                ->orderBy('products.created_at','DESC')
                ->select('products.*','products_description.*','images.id','image_categories.path')
                ->distinct('products.products_id')
                ->get();
            }
            else{

                $products = DB::table('products')
                ->leftjoin('products_description','products_description.products_id','products.products_id')
                ->leftjoin('images','images.id','products.products_image')
                ->leftjoin('image_categories','image_categories.image_id','images.id')
                ->where('image_categories.image_type','ACTUAL')
                ->where('products.products_status',1)
                ->orderBy('products.created_at','DESC')
                ->select('products.*','products_description.*','images.id','image_categories.path')
                ->distinct('products.products_id')
                ->get();
            }
        }
    }
    else{

        $checkId = Categories::where('categories_slug',$request->cat_id)->first();
        $send_id = $checkId->categories_id;

        $category_ids = array();
        $j = 1;
        $category_ids[0] = $send_id;

        $cat = Categories::where('parent_id',$send_id)->get();

        if($cat != null){
            foreach($cat as $cid){
                $category_ids[$j++] = $cid->categories_id;
                $subcat = Categories::where('parent_id',$cid->categories_id)->get();
                if($subcat != null){
                    foreach($subcat as $sid){
                        $category_ids[$j++] = $sid->id;
                    }
                }
            }
        }

        if(!empty($keyword)){

            if($brand_id>0){
               $products = DB::table('products_to_categories')
               ->leftJoin('products','products.products_id','products_to_categories.products_id')
               ->leftjoin('products_description','products_description.products_id','products.products_id')
               ->leftjoin('images','images.id','products.products_image')
               ->leftjoin('image_categories','image_categories.image_id','images.id')
               ->where('products_description.products_name','LIKE','%'.$keyword.'%')
               ->where('products.products_brand',$brand_id)
               ->where('products_to_categories.categories_id',$category_ids[0])
               ->where('image_categories.image_type','ACTUAL')
               ->where('products.products_status',1)
               ->orderBy('products.created_at','DESC')
               ->select('products.*','products_description.*','images.id','image_categories.path')
               ->distinct('products.products_id')
               ->get();
           }
           else{
               $products = DB::table('products_to_categories')
               ->leftJoin('products','products.products_id','products_to_categories.products_id')
               ->leftjoin('products_description','products_description.products_id','products.products_id')
               ->leftjoin('images','images.id','products.products_image')
               ->leftjoin('image_categories','image_categories.image_id','images.id')
               ->where('products_description.products_name','LIKE','%'.$keyword.'%')
               ->where('products_to_categories.categories_id',$category_ids[0])
               ->where('image_categories.image_type','ACTUAL')
               ->where('products.products_status',1)
               ->orderBy('products.created_at','DESC')
               ->select('products.*','products_description.*','images.id','image_categories.path')
               ->distinct('products.products_id')
               ->get();
           }
       }
       else{
        if($brand_id>0){
           $products = DB::table('products_to_categories')
           ->leftJoin('products','products.products_id','products_to_categories.products_id')
           ->leftjoin('products_description','products_description.products_id','products.products_id')
           ->leftjoin('images','images.id','products.products_image')
           ->leftjoin('image_categories','image_categories.image_id','images.id')
           ->where('products.products_brand',$brand_id)
           ->where('products_to_categories.categories_id',$category_ids[0])
           ->where('image_categories.image_type','ACTUAL')
           ->where('products.products_status',1)
           ->orderBy('products.created_at','DESC')
           ->select('products.*','products_description.*','images.id','image_categories.path')
           ->distinct('products.products_id')
           ->get();
       }
       else{
           $products = DB::table('products_to_categories')
           ->leftJoin('products','products.products_id','products_to_categories.products_id')
           ->leftjoin('products_description','products_description.products_id','products.products_id')
           ->leftjoin('images','images.id','products.products_image')
           ->leftjoin('image_categories','image_categories.image_id','images.id')
           ->where('products_to_categories.categories_id',$category_ids[0])
           ->where('image_categories.image_type','ACTUAL')
           ->where('products.products_status',1)
           ->orderBy('products.created_at','DESC')
           ->select('products.*','products_description.*','images.id','image_categories.path')
           ->distinct('products.products_id')
           ->get();
       }
   }

}


if($products != null){
   $j=0;
   foreach($products as $product){
    $price = 0;
    $prices = DB::table('products_price')->where('products_id',$product->products_id)->where('products_sell_type','quantity_based')->first();
    if(!empty($prices)){
        $price = $prices->products_price + ($prices->products_price*$product->tax_percent)/100;
    }
    else{
        $price = $product->products_price + ($product->products_price*$product->tax_percent)/100;
    }
    if($price>=$min && $price<=$max){
        $searched_products[$j] = $product;
        $searched_products[$j]->products_price = $price;
        $j++;
    }
}
}


if($sort_type == 'name-asc'){
    $name = array_column($searched_products, 'products_name');
    array_multisort($name, SORT_ASC, $searched_products);
}

else if($sort_type == 'name-desc'){
    $created_at = array_column($searched_products, 'products_name');
    array_multisort($created_at, SORT_DESC, $searched_products);
}

else if($sort_type == 'created'){
    $created_at = array_column($searched_products, 'created_at');
    array_multisort($created_at, SORT_ASC, $searched_products);
}

else if($sort_type == 'low-high'){
    $sell_price = array_column($searched_products, 'products_price');
    array_multisort($sell_price, SORT_ASC, $searched_products);
}

else if($sort_type == 'high-low'){
    $sell_price = array_column($searched_products, 'products_price');
    array_multisort($sell_price, SORT_DESC, $searched_products);
}

    // var_dump(count($searched_products)); die;
return view('homeglare2.product_cat',compact('searched_products','send_id'));
}

    // getProductDetailsData
public function getProductDetailsData($productId)
{
    $products_images = [];
 // $productDetails = Products::where('products_id', $productId)->first();
    $productDetails = Products::leftjoin('products_description','products_description.products_id','products.products_id')
    ->where('products.products_id',$productId)
    ->orderBy('products.created_at','DESC')
    ->select('products.*','products_description.*')
    ->first();
    $brand = "";
    $stock = "";


    if(($productDetails->products_quantity-$productDetails->products_min_order)>0)
        $stock = "in";
    else 
        $stock = 'out';


    $p_id = $productDetails->products_id;
    if(!empty($productDetails->products_brand))
        $brand = ProductsBrand::where('id',$productDetails->products_brand)->first();

    $products_images[0] = Products::leftJoin('images','images.id','products.products_image')
    ->leftJoin('image_categories','image_categories.image_id','images.id')
    ->where('image_categories.image_type','ACTUAL')
    ->where('products.products_image',$productDetails->products_image)->first();


    $images = DB::table('products_images')
    ->leftJoin('images','images.id','products_images.image')
    ->leftJoin('image_categories','image_categories.image_id','images.id')
    ->where('image_categories.image_type','THUMBNAIL')
    ->where('products_images.products_id',$p_id)->get();


    if(!empty($images)){
        $j=1;
        foreach($images as $image){
            $products_images[$j++] = $image;
        }
    }

    
    $moq = 1; $carton = 0;
    $price = $productDetails->products_price ?? '0';
    $product_price = DB::table('products_price')->where('products_id',$p_id)->get();
    if(count($product_price)>0){
        foreach($product_price as $pro){
            if($pro->products_sell_type == 'quantity_based'){
                $moq = $pro->products_quantity;
            }
            if($pro->products_sell_type == 'carton'){
                $carton = $pro->products_quantity;
            }
            if($pro->products_sell_type == 'sample'){
                $price = $pro->products_price;
            }
        }
    }


    $price = ceil($price + ($price*$productDetails->tax_percent)/100);

    $category = DB::table('products_to_categories')
    ->leftjoin('categories','categories.categories_id','products_to_categories.categories_id')
    ->leftJoin('categories_description','categories_description.categories_id','categories.categories_id')
    ->where('products_to_categories.products_id',$p_id)
    ->select('categories_description.*')
    ->first(); 


    $discount = 0;

    return response()->json(['status' => 200, 'data' => $productDetails, 'category' => $category, 'discount' => $discount, 'products_images' => $products_images, 'price' => $price, 'moq' => $moq, 'brand' => $brand, 'carton' => $carton , 'stock'=> $stock]);
}
    // filter record by colored
public function colored(Request $request)
{

    $color_code = $request->color;
    $category_ids = array();
    $categories_id = "";
    $color_code = $request->color ?? '';

    $sort_type = $request->sort_type ?? '';

    $searched_products = array();
    $min = $request['min_cost'];
    $max = $request['max_cost'];
    $products = "";
    $send_id = "";
    $i=0;

    if($request->cat_id == 'all'){
        $products = DB::table('products')
        ->leftjoin('products_description','products_description.products_id','products.products_id')
        ->leftjoin('images','images.id','products.products_image')
        ->leftjoin('image_categories','image_categories.image_id','images.id')
        ->where('products.products_color',$color_code)
        ->where('image_categories.image_type','ACTUAL')
        ->where('products.products_status',1)
        ->orderBy('products.created_at','DESC')
        ->select('products.*','products_description.*','images.id','image_categories.path')
        ->distinct('products.products_id')
        ->get();
    }
    else{

        $checkId = Categories::where('categories_slug',$request->cat_id)->first();
        $send_id = $checkId->categories_id;

        $category_ids = array();
        $j = 1;
        $category_ids[0] = $send_id;

        $cat = Categories::where('parent_id',$send_id)->get();

        if($cat != null){
            foreach($cat as $cid){
                $category_ids[$j++] = $cid->categories_id;
                $subcat = Categories::where('parent_id',$cid->categories_id)->get();
                if($subcat != null){
                    foreach($subcat as $sid){
                        $category_ids[$j++] = $sid->id;
                    }
                }
            }
        }

        $products = DB::table('products_to_categories')
        ->leftJoin('products','products.products_id','products_to_categories.products_id')
        ->leftjoin('products_description','products_description.products_id','products.products_id')
        ->leftjoin('images','images.id','products.products_image')
        ->leftjoin('image_categories','image_categories.image_id','images.id')
        ->where('products_to_categories.categories_id',$category_ids[0])
        ->where('products.products_color',$color_code)
        ->where('image_categories.image_type','ACTUAL')
        ->where('products.products_status',1)
        ->orderBy('products.created_at','DESC')
        ->select('products.*','products_description.*','images.id','image_categories.path')
        ->distinct('products.products_id')
        ->get();

    }

    if($products != null){
       $j=0;
       foreach($products as $product){
        $price = 0;
        $prices = DB::table('products_price')->where('products_id',$product->products_id)->where('products_sell_type','quantity_based')->first();
        if(!empty($prices)){
            $price = $prices->products_price + ($prices->products_price*$product->tax_percent)/100;
        }
        else{
            $price = $product->products_price + ($product->products_price*$product->tax_percent)/100;
        }
        if($price>=$min && $price<=$max){
            $searched_products[$j] = $product;
            $searched_products[$j]->products_price = $price;
            $j++;
        }
    }
}

if($sort_type == 'name-asc'){
    $name = array_column($searched_products, 'products_name');
    array_multisort($name, SORT_ASC, $searched_products);
}

else if($sort_type == 'name-desc'){
    $created_at = array_column($searched_products, 'products_name');
    array_multisort($created_at, SORT_DESC, $searched_products);
}

else if($sort_type == 'created'){
    $created_at = array_column($searched_products, 'created_at');
    array_multisort($created_at, SORT_ASC, $searched_products);
}

else if($sort_type == 'low-high'){
    $sell_price = array_column($searched_products, 'products_price');
    array_multisort($sell_price, SORT_ASC, $searched_products);
}

else if($sort_type == 'high-low'){
    $sell_price = array_column($searched_products, 'products_price');
    array_multisort($sell_price, SORT_DESC, $searched_products);
}


return view('homeglare2.product_cat', compact('searched_products', 'categories_id', 'color_code', 'send_id'));
}



public function branded(Request $request)
{


    $color_code = $request->color ?? '';
    $brand_id = $request->brand ?? '';
    $keyword = $request->keyword ?? '';
    $category_ids = array();
    $categories_id = "";

    $sort_type = $request->sort_type ?? '';

    $searched_products = array();
    $min = $request['min_cost'];
    $max = $request['max_cost'];
    $products = "";
    $send_id = "";
    $i=0;

    if($request->cat_id == 'all'){
        if(!empty($keyword)){
            $products = DB::table('products')
            ->leftjoin('products_description','products_description.products_id','products.products_id')
            ->leftjoin('images','images.id','products.products_image')
            ->leftjoin('image_categories','image_categories.image_id','images.id')
            ->where('products.products_brand',$brand_id)
            ->where('products_description.products_name','LIKE','%'.$keyword.'%')
            ->where('image_categories.image_type','ACTUAL')
            ->where('products.products_status',1)
            ->orderBy('products.created_at','DESC')
            ->select('products.*','products_description.*','images.id','image_categories.path')
            ->distinct('products.products_id')
            ->get();
        }
        else{
            $products = DB::table('products')
            ->leftjoin('products_description','products_description.products_id','products.products_id')
            ->leftjoin('images','images.id','products.products_image')
            ->leftjoin('image_categories','image_categories.image_id','images.id')
            ->where('products.products_brand',$brand_id)
            ->where('image_categories.image_type','ACTUAL')
            ->where('products.products_status',1)
            ->orderBy('products.created_at','DESC')
            ->select('products.*','products_description.*','images.id','image_categories.path')
            ->distinct('products.products_id')
            ->get();
        }
    }
    else{

        $checkId = Categories::where('categories_slug',$request->cat_id)->first();
        $send_id = $checkId->categories_id;

        $category_ids = array();
        $j = 1;
        $category_ids[0] = $send_id;

        $cat = Categories::where('parent_id',$send_id)->get();

        if($cat != null){
            foreach($cat as $cid){
                $category_ids[$j++] = $cid->categories_id;
                $subcat = Categories::where('parent_id',$cid->categories_id)->get();
                if($subcat != null){
                    foreach($subcat as $sid){
                        $category_ids[$j++] = $sid->id;
                    }
                }
            }
        }

        if(!empty($keyword)){
           $products = DB::table('products_to_categories')
           ->leftJoin('products','products.products_id','products_to_categories.products_id')
           ->leftjoin('products_description','products_description.products_id','products.products_id')
           ->leftjoin('images','images.id','products.products_image')
           ->leftjoin('image_categories','image_categories.image_id','images.id')
           ->where('products_description.products_name','LIKE','%'.$keyword.'%')
           ->where('products_to_categories.categories_id',$category_ids[0])
           ->where('products.products_brand',$brand_id)
           ->where('image_categories.image_type','ACTUAL')
           ->where('products.products_status',1)
           ->orderBy('products.created_at','DESC')
           ->select('products.*','products_description.*','images.id','image_categories.path')
           ->distinct('products.products_id')
           ->get();
       }
       else{
        $products = DB::table('products_to_categories')
        ->leftJoin('products','products.products_id','products_to_categories.products_id')
        ->leftjoin('products_description','products_description.products_id','products.products_id')
        ->leftjoin('images','images.id','products.products_image')
        ->leftjoin('image_categories','image_categories.image_id','images.id')
        ->where('products_to_categories.categories_id',$category_ids[0])
        ->where('products.products_brand',$brand_id)
        ->where('image_categories.image_type','ACTUAL')
        ->where('products.products_status',1)
        ->orderBy('products.created_at','DESC')
        ->select('products.*','products_description.*','images.id','image_categories.path')
        ->distinct('products.products_id')
        ->get();
    }

}

if($products != null){
   $j=0;
   foreach($products as $product){
    $price = 0;
    $prices = DB::table('products_price')->where('products_id',$product->products_id)->where('products_sell_type','quantity_based')->first();
    if(!empty($prices)){
        $price = $prices->products_price + ($prices->products_price*$product->tax_percent)/100;
    }
    else{
        $price = $product->products_price + ($product->products_price*$product->tax_percent)/100;
    }
    if($price>=$min && $price<=$max){
        $searched_products[$j] = $product;
        $searched_products[$j]->products_price = $price;
        $j++;
    }
}
}

if($sort_type == 'name-asc'){
    $name = array_column($searched_products, 'products_name');
    array_multisort($name, SORT_ASC, $searched_products);
}

else if($sort_type == 'name-desc'){
    $created_at = array_column($searched_products, 'products_name');
    array_multisort($created_at, SORT_DESC, $searched_products);
}

else if($sort_type == 'created'){
    $created_at = array_column($searched_products, 'created_at');
    array_multisort($created_at, SORT_ASC, $searched_products);
}

else if($sort_type == 'low-high'){
    $sell_price = array_column($searched_products, 'products_price');
    array_multisort($sell_price, SORT_ASC, $searched_products);
}

else if($sort_type == 'high-low'){
    $sell_price = array_column($searched_products, 'products_price');
    array_multisort($sell_price, SORT_DESC, $searched_products);
}

return view('homeglare2.product_cat', compact('searched_products', 'categories_id', 'color_code', 'send_id'));
}

    // filter record by price
public function price(Request $request)
{
  $category_ids = array();
  $categories_id = "";
  $color_code = $request->color ?? '';
  $keyword = $request->keyword ?? '';
  
  $sort_type = $request->sort_type ?? '';
  $brand_id = $request->brand ?? '';

  $searched_products = array();
  $min = $request['min_cost'];
  $max = $request['max_cost'];
  $products = "";
  $send_id = "";
  $i=0;

  if($request->cat_id == 'all'){

    if(!empty($keyword)){

        if($brand_id>0){
            $products = DB::table('products')
            ->leftjoin('products_description','products_description.products_id','products.products_id')
            ->leftjoin('images','images.id','products.products_image')
            ->leftjoin('image_categories','image_categories.image_id','images.id')
            ->where('products_description.products_name','LIKE','%'.$keyword.'%')
            ->where('products.products_brand','>=',$brand_id)
            ->where('products.products_price','>=',$min)
            ->where('products.products_price','<=',$max)
            ->where('image_categories.image_type','ACTUAL')
            ->where('products.products_status',1)
            ->orderBy('products.created_at','DESC')
            ->select('products.*','products_description.*','images.id','image_categories.path')
            ->distinct('products.products_id')
            ->get();
        }
        else{
            $products = DB::table('products')
            ->leftjoin('products_description','products_description.products_id','products.products_id')
            ->leftjoin('images','images.id','products.products_image')
            ->leftjoin('image_categories','image_categories.image_id','images.id')
            ->where('products_description.products_name','LIKE','%'.$keyword.'%')
            ->where('products.products_price','>=',$min)
            ->where('products.products_price','<=',$max)
            ->where('image_categories.image_type','ACTUAL')
            ->where('products.products_status',1)
            ->orderBy('products.created_at','DESC')
            ->select('products.*','products_description.*','images.id','image_categories.path')
            ->distinct('products.products_id')
            ->get();
        }
    }
    else{

        if($brand_id>0){
            $products = DB::table('products')
            ->leftjoin('products_description','products_description.products_id','products.products_id')
            ->leftjoin('images','images.id','products.products_image')
            ->leftjoin('image_categories','image_categories.image_id','images.id')
            ->where('products.products_brand','>=',$brand_id)
            ->where('products.products_price','>=',$min)
            ->where('products.products_price','<=',$max)
            ->where('image_categories.image_type','ACTUAL')
            ->where('products.products_status',1)
            ->orderBy('products.created_at','DESC')
            ->select('products.*','products_description.*','images.id','image_categories.path')
            ->distinct('products.products_id')
            ->get();
        }
        else{
            $products = DB::table('products')
            ->leftjoin('products_description','products_description.products_id','products.products_id')
            ->leftjoin('images','images.id','products.products_image')
            ->leftjoin('image_categories','image_categories.image_id','images.id')
            ->where('products.products_price','>=',$min)
            ->where('products.products_price','<=',$max)
            ->where('image_categories.image_type','ACTUAL')
            ->where('products.products_status',1)
            ->orderBy('products.created_at','DESC')
            ->select('products.*','products_description.*','images.id','image_categories.path')
            ->distinct('products.products_id')
            ->get();
        }
    }

}
else{

    $checkId = Categories::where('categories_slug',$request->cat_id)->first();
    $send_id = $checkId->categories_id;

    $category_ids = array();
    $j = 1;
    $category_ids[0] = $send_id;

    $cat = Categories::where('parent_id',$send_id)->get();

    if($cat != null){
        foreach($cat as $cid){
            $category_ids[$j++] = $cid->categories_id;
            $subcat = Categories::where('parent_id',$cid->categories_id)->get();
            if($subcat != null){
                foreach($subcat as $sid){
                    $category_ids[$j++] = $sid->id;
                }
            }
        }
    }

    if(!empty($keyword)){

        if($brand_id>0){

           $products = DB::table('products_to_categories')
           ->leftJoin('products','products.products_id','products_to_categories.products_id')
           ->leftjoin('products_description','products_description.products_id','products.products_id')
           ->leftjoin('images','images.id','products.products_image')
           ->leftjoin('image_categories','image_categories.image_id','images.id')
           ->where('products_description.products_name','LIKE','%'.$keyword.'%')
           ->where('products.products_brand',$brand_id)
           ->where('products_to_categories.categories_id',$category_ids[0])
           ->where('image_categories.image_type','ACTUAL')
           ->where('products.products_status',1)
           ->orderBy('products.created_at','DESC')
           ->select('products.*','products_description.*','images.id','image_categories.path')
           ->distinct('products.products_id')
           ->get();
       }
       else{

           $products = DB::table('products_to_categories')
           ->leftJoin('products','products.products_id','products_to_categories.products_id')
           ->leftjoin('products_description','products_description.products_id','products.products_id')
           ->leftjoin('images','images.id','products.products_image')
           ->leftjoin('image_categories','image_categories.image_id','images.id')
           ->where('products_description.products_name','LIKE','%'.$keyword.'%')
           ->where('products_to_categories.categories_id',$category_ids[0])
           ->where('image_categories.image_type','ACTUAL')
           ->where('products.products_status',1)
           ->orderBy('products.created_at','DESC')
           ->select('products.*','products_description.*','images.id','image_categories.path')
           ->distinct('products.products_id')
           ->get();
       }

   }
   else{
    if($brand_id>0){

       $products = DB::table('products_to_categories')
       ->leftJoin('products','products.products_id','products_to_categories.products_id')
       ->leftjoin('products_description','products_description.products_id','products.products_id')
       ->leftjoin('images','images.id','products.products_image')
       ->leftjoin('image_categories','image_categories.image_id','images.id')
       ->where('products.products_brand',$brand_id)
       ->where('products_to_categories.categories_id',$category_ids[0])
       ->where('image_categories.image_type','ACTUAL')
       ->where('products.products_status',1)
       ->orderBy('products.created_at','DESC')
       ->select('products.*','products_description.*','images.id','image_categories.path')
       ->distinct('products.products_id')
       ->get();
   }
   else{

       $products = DB::table('products_to_categories')
       ->leftJoin('products','products.products_id','products_to_categories.products_id')
       ->leftjoin('products_description','products_description.products_id','products.products_id')
       ->leftjoin('images','images.id','products.products_image')
       ->leftjoin('image_categories','image_categories.image_id','images.id')
       ->where('products_to_categories.categories_id',$category_ids[0])
       ->where('image_categories.image_type','ACTUAL')
       ->where('products.products_status',1)
       ->orderBy('products.created_at','DESC')
       ->select('products.*','products_description.*','images.id','image_categories.path')
       ->distinct('products.products_id')
       ->get();
   }
}

}

if($products != null){
   $j=0;
   foreach($products as $product){
    $price = 0;
    $prices = DB::table('products_price')->where('products_id',$product->products_id)->where('products_sell_type','quantity_based')->first();
    if(!empty($prices)){
        $price = $prices->products_price + ($prices->products_price*$product->tax_percent)/100;
    }
    else{
        $price = $product->products_price + ($product->products_price*$product->tax_percent)/100;
    }
    if($price>=$min && $price<=$max){
        $searched_products[$j] = $product;
        $searched_products[$j]->products_price = $price;
        $j++;
    }
}
}

if($sort_type == 'name-asc'){
    $name = array_column($searched_products, 'products_name');
    array_multisort($name, SORT_ASC, $searched_products);
}

else if($sort_type == 'name-desc'){
    $created_at = array_column($searched_products, 'products_name');
    array_multisort($created_at, SORT_DESC, $searched_products);
}

else if($sort_type == 'created'){
    $created_at = array_column($searched_products, 'created_at');
    array_multisort($created_at, SORT_ASC, $searched_products);
}

else if($sort_type == 'low-high'){
    $sell_price = array_column($searched_products, 'products_price');
    array_multisort($sell_price, SORT_ASC, $searched_products);
}

else if($sort_type == 'high-low'){
    $sell_price = array_column($searched_products, 'products_price');
    array_multisort($sell_price, SORT_DESC, $searched_products);
}

return view('homeglare2.product_cat', compact('searched_products', 'categories_id', 'color_code', 'send_id'));
}


    // filter record by colored
public function rating(Request $request)
{
  $send_id = $request->cat_id;
  $searched_products = DB::table('categories')
  ->join('products', 'products.categories_id', '=', 'categories.id')
  ->where('categories.slug', $request->cat_id)
  ->where('products.product_color', $request->color)
  ->get();
  return view('homeglare2.product_cat', compact('searched_products', 'send_id'));
}



public function updatePassword (Request $request ){
    $data = $request->all();
    if($data['new-password'] == $data['confirm-password']){
        $user = Auth::user()->email;
        User::where('email',$user)->update(['password'=> Hash::make($data['new-password'])]);
        return redirect()->back()->with('message','Password Updated Successfully');
        
    }
    else{
        return redirect()->back()->with('message','Password did not match');

    }
}

public function updateProfile (Request $request ){
    $data = $request->all();
    $user = Auth::user();

    $user->email = $data['email'];
    $user->first_name = $data['first_name'];
    $user->last_name = $data['last_name'];
    $user->phone = $data['phone'];
    $user->update();
    
    $address=DB::table('address_book')->where('customers_id',$user->id)->first();
    if($address){

        $addess = DB::table('address_book')->where('customers_id',$user->id)->update(
            array(
                'entry_street_address' => $data['entry_street_address'],
                'entry_state' => $data['entry_state'],
                'entry_city' => $data['entry_city'],
                'entry_postcode' => $data['entry_postcode'],
                'entry_firstname' => $data['first_name'],
                'entry_lastname' => $data['last_name'],
                'entry_country_id' => '99'
            )
        );    
    }
    else{
        $addess = DB::table('address_book')->insert(
            array(
                'customers_id' => $user->id,
                'entry_street_address' => $data['entry_street_address'],
                'entry_state' => $data['entry_state'],
                'entry_city' => $data['entry_city'],
                'entry_postcode' => $data['entry_postcode'],
                'entry_firstname' => $data['first_name'],
                'entry_lastname' => $data['last_name'],
                'entry_country_id' => '99'
            )
        );       
        
    }

    return redirect()->back()->with('message','Profile  Updated Successfully');

}

public function checkEmail(Request $request){
    $flag = 1;
    $check = User::where('email',$request->email)->first();
    if(!empty($check)){
        $flag = 0;
    }
    echo $flag;
}

public function checkLogin(Request $request){
    $flag = 1;
    $check = User::where('email',$request->email)->first();

    if(Hash::check($request->password, $check->password)){
        $flag = 0;
    }

    echo $flag;
}

public function checkCoupon(Request $request){


    $code = $request->code;
    $total = $request->total;
    $check = Coupon::whereRaw("BINARY `code` = '".$request->code."'")->where('expiry_date','>=',date('y-m-d'))->first();
    if(!empty(session()->get('coupon'))){
        return 2;
    }
    else{
        if($check){

            if($check->minimum_amount>$total || $check->maximum_amount<$total){
                $status = 3;
                $min = $check->minimum_amount;
                $max = $check->maximum_amount;
                return response()->json(['min'=>$min , 'max'=>$max , 'status'=>$status]);
            }

            else{
                $item = new StdClass;
                $coupon_code = $code;
                $coupon_amount = $check->amount ?? '0';
                $item->coupon_code = $code;
                $item->coupon_amount = $coupon_amount;
                $item->status = '1';
                session()->put('coupon',$item);

                return response()->json(['couponcode'=>$code , 'amount'=>$coupon_amount , 'status'=>'1']);
            }

        }
        else{
            return 0;
        }
    }
}

public function getPrice(Request $request){
    $data = $request->all();
    if($data['category']=='moq'){
        $data['category']='quantity_based';
    }

    // var_dump($data['category']); die;
    $products = DB::table('products')->where('products_id',$data['pid'])->first();
    $tax = $products->tax_percent;
    $product = DB::table('products_price')->where('products_id',$data['pid'])->where('products_sell_type',$data['category'])->first();
    if(!empty($product)){
        if ($products->product_gst_inclusion)
            $price = ceil(($product->products_price) + (($product->products_price*$tax)/100));
        else
            $price = $product->products_price. " + GST";
        // $price = $product->products_quantity*$price;
    }
    else{
        if ($products->product_gst_inclusion)
            $price = ceil($products->products_price + (($products->products_price*$tax)/100));
        else
            $price = ceil($products->products_price). " + GST";
        
    }
    return $price;
}


}
