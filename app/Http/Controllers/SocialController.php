<?php
 namespace App\Http\Controllers;
 use Illuminate\Http\Request;
 use Validator,Redirect,Response,File;
 use Socialite;
 use App\User;
 use Log;
 
 class SocialController extends Controller
 {
 
 public function __construct()
    {
       $user = \Auth::user();
    }
 
  public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

 
 
 function createUser($getInfo, $provider){
 $user = User::where('provider_id', $getInfo->id)->first();
 
 if (!$user) {
       // check for email exist
                $userExist = User::where('email', $getInfo->email)->first();
                
                if($userExist){
                    User::where('id',$userExist->id)->update(['provider' => 'facebook',
                                                        'provider_id' => $getInfo->id]);
                    return $userExist;
                } else{
                    $user = new User;
                    $password = 'asd12345';
         $user->first_name  = $getInfo->name;
         $user->email    = $getInfo->email;
         $user->role_id = 2;
         $user->avatar = $getInfo->avatar_original;
         $user->status = '1';
         $user->is_seen = '0';
         $user->provider = 'facebook';
         $user->provider_id = $getInfo->id;
         $user->password = \Hash::make($password);
     $user->save();

$user = User::where('email',$getInfo->email)->first();

					DB::table('address_book')->insert(
						array(
							'entry_firstname' => $user->first_name ?? '',
							'entry_lastname' => $user->last_name ?? '',
							'customers_id' => $user->id,
							'entry_country_id' => 99,
							'user_id' => $user->id

						));			


   return $user;
                }

   }
   else{
       return $user;
   }
 }
 
 
 public function callback($provider = 'facebook')
 {
   $getInfo = Socialite::driver('facebook')->user();
    
   $user = $this->createUser($getInfo, 'facebook');
  
   auth()->login($user);
   
   return redirect()->to('/');
 }
 
 
 }
