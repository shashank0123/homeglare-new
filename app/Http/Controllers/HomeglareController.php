<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Core\Products;
use App\Models\Core\ProductsBrand;
use App\Models\Core\Categories;
// use App\Models\Core\Address;
use App\Models\Core\Order;
use App\Models\Core\Coupon;
// use App\Models\Core\OrderItem;
use App\Models\Core\Reviews;
// use App\Wishlist;
// use App\Models\Core\Offer;
use App\Models\Core\Banner;
use App\Models\Core\Setting;
// use App\Models\Core\Query;
// use App\Models\Core\Faq;
use App\Models\Core\Newsletter;
use App\Models\Core\Help;
// use App\Models\Core\ProductsBrand;
use App\User;
use App\Pincode;
use App\SellerQuery;

use DB;
use Auth;
use StdClass;
use Mail;
use Hash;

class HomeglareController extends Controller
{
	public function getHome(){
		$slider_banners = [];
		$medium_banners = [];
		$companyInfo = Setting::all();

		$maincategory = Categories::leftjoin('categories_description','categories_description.categories_id','categories.categories_id')
		->where('categories.categories_status','1')
		->where('categories.parent_id',0)
		->get();
		
		$slider_cat = Categories::leftjoin('categories_description','categories_description.categories_id','categories.categories_id')
		->where('categories.categories_status','1')
		->where('categories.parent_id',0)
		->orderBy('created_at','ASC')
		->limit(5)
		->get();	
		
		$slider_banners[0] = 'http://homeglare.com/storage/banners/8ec3dfa8989755f8a3d1232ada4ea9f4.jpg';
		$slider_banners[1] = 'http://homeglare.com/storage/banners/8e238d2c6f000160734f46eb12ae8bac.jpg';
		$slider_banners[2] = 'http://homeglare.com/storage/banners/346811ae5248a48e5e6a27da22908512.jpg';
		$medium_banners[0] = 'http://homeglare.com/storage/banners/3ff45504b249d0730f117b638522a99c.png';
		$medium_banners[1] = 'http://homeglare.com/storage/banners/39e971cf53732b0f416eec0939078cb3.jpeg';
		$medium_banners[2] = 'http://homeglare.com/storage/banners/c7287ebceccf45243da8cbb409298e91.png';

		$featured_products = Products::leftjoin('products_description','products_description.products_id','products.products_id')
		->leftjoin('images','images.id','products.products_image')
		->leftjoin('image_categories','image_categories.image_id','images.id')
		->where('image_categories.image_type','ACTUAL')
		->where('products.is_feature','1')
		->where('products.products_status','1')
		->orderBy('products.created_at','DESC')
		->select('products.*','products_description.*','images.id','image_categories.path')
		->limit(20)
		->get();
		
		$special_deals = DB::table('specials')
		->leftJoin('products','specials.products_id','products.products_id')
		->leftjoin('products_description','products_description.products_id','products.products_id')
		->leftjoin('images','images.id','products.products_image')
		->leftjoin('image_categories','image_categories.image_id','images.id')
		->where('products.products_status','1')
		->where('image_categories.image_type','ACTUAL')
		->orderBy('products.created_at','DESC')
		->limit(3)
		->get();

		$new_products = Products::leftjoin('products_description','products_description.products_id','products.products_id')
		->leftjoin('images','images.id','products.products_image')
		->leftjoin('image_categories','image_categories.image_id','images.id')
		->where('image_categories.image_type','ACTUAL')
		->where('products.products_status','1')
		->orderBy('products_description.products_name','ASC')
		->orderBy('products.created_at','DESC')
		->limit(20)
		->get();

		$trending_products = Products::leftjoin('products_description','products_description.products_id','products.products_id')
		->leftjoin('images','images.id','products.products_image')
		->leftjoin('image_categories','image_categories.image_id','images.id')
		->where('products.products_status','1')
		->where('image_categories.image_type','ACTUAL')
		->orderBy('products.created_at','DESC')
		->limit(3)
		->get();

		$brands = ProductsBrand::orderBy('brand_name','ASC')->get();
		
		// $offers = Coupon::where('status','1')->orderBy('created_at','DESC')->limit(3)->get();
		
		return view('homeglare2.home', compact('companyInfo','maincategory','slider_banners', 'medium_banners','featured_products','new_products','slider_cat','trending_products','special_deals','brands'));
	}

	public function getAbout(){
		return view('homeglare2.about-us');
	}

	public function getCart(){
		return view('homeglare2.cart');
	}

	public function getCheckout($id = false){

		$productDetails = array();
		$totalAmount = 0;
		$tax = 0;
		$check_sample = 0;
		$count_cart = 0;
		$type_cart = "";
		$type = 'normal';
		session()->put('shipped','moq');

		if (!isset(Auth::user()->id)){
			return redirect('/customer/login');
		}

		
		if ($id){
			$ids = explode('-', $id);
			$id = $ids[0];
			$product = Products::leftjoin('products_description','products_description.products_id','products.products_id')
			->where('products.products_id',$id)
			->orderBy('products.created_at','DESC')
			->select('products.*','products_description.*')
			->first();

			$product['quantity'] = $ids[1];
				// echo $ids[1]; die;
			$category = $ids[2];

			$unitprice = 0;
			$pro = "";
			if($category == 'moq'){
				session()->put('category','moq');

				// echo "hi";die;
				$pro = DB::table('products_price')->where('products_id',$id)->where('products_sell_type','quantity_based')->first();


				if(isset($pro)){
					if($product['quantity'] >= $pro->products_quantity){

						$unitprice = $pro->products_price;
					}
					else{
						$pro = Products::where('products_id',$id)->first();
						$unitprice = $pro->products_price ?? '';
					}
				}
				else{
					$pro = Products::where('products_id',$id)->first();
					$unitprice = $pro->products_price;
				}

			}
			elseif($category == 'carton'){
				$type="carton";
				session()->put('category','carton');
				session()->put('shipped','carton');
				$pro = DB::table('products_price')->where('products_id',$id)->where('products_sell_type','carton')->first();

				if(isset($pro)){
					
					$product['quantity'] = $product['quantity']*$pro->products_quantity;
					if($product['quantity'] >= $pro->products_quantity){

						$unitprice = $pro->products_price;
					}
					else{
						$pro = Products::where('products_id',$id)->first();
						$unitprice = $pro->products_price ?? '';
					}
				}
				else{
					$pro = Products::where('products_id',$id)->first();
					$unitprice = $pro->products_price;
				}

			}

				// $product['type'] = $type;
			$product['total_price'] = $unitprice * $product['quantity'] ;
			$totalAmount += $product['total_price'] + ($product->tax_percent*$product['total_price'])/100;
			$tax = $tax + ($product->tax_percent*$product['total_price'])/100;
			array_push($productDetails, $product);
			$cart = new StdClass;
			$item = new StdClass;
            	$pro_id = $product->products_id;
                $item->product_id = $product->products_id."_".$type;
                $item->quantity = $product['quantity'];
                $item->type = $type;
                $item->category = $category;
                $item->unit_price = $unitprice;
                $item->tax = $tax;
                $item->total = $totalAmount;
                $cart->$pro_id = $item;
           	session()->put('cart',$cart);

		}
		elseif (session()->get('cart')) {
			$m=0; $c=0; $s=0;
			foreach (session()->get('cart') as $value) {

				$count_cart++;

				$product_id = explode('_',$value->product_id)[0];
				$type = explode('_',$value->product_id)[1];
				$type_cart = $type;

				$product = Products::leftjoin('products_description','products_description.products_id','products.products_id')
				->where('products.products_id',$product_id)
				->orderBy('products.created_at','DESC')
				->select('products.*','products_description.*')
				->first();
				

				$product['quantity'] = $value->quantity;

				$unitprice=0;

				if($value->type == 'sample'){
					$s++;
					$pro = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','sample')->first();
					$unitprice = $pro->products_price ?? '';
				}
				elseif($value->type == 'normal'){
					$m++;
					$pro = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','quantity_based')->first();
					if(isset($pro)){
						if($value->quantity >= $pro->products_quantity){

							$unitprice = $pro->products_price;
						}
						else{
							$pro = Products::where('products_id',$product_id)->first();
							$unitprice = $pro->products_price ?? '';
						}
					}
					else{
						$pro = Products::where('products_id',$product_id)->first();
						$unitprice = $pro->products_price;
					}
				}
				elseif($value->type == 'carton'){
					$c++;
					$pro = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','carton')->first();
					if(isset($pro)){
						if($value->quantity >= $pro->products_quantity){

							$unitprice = $pro->products_price;
						}
						else{
							$pro = Products::where('products_id',$product_id)->first();
							$unitprice = $pro->products_price ?? '';
						}
					}
					else{
						$pro = Products::where('products_id',$product_id)->first();
						$unitprice = $pro->products_price;
					}
				}

				$product['type'] = $type;
				$product['total_price'] = $unitprice * $value->quantity ;
				$totalAmount += $product['total_price'] + ($product->tax_percent*$product['total_price'])/100;
				$tax = $tax + ($product->tax_percent*$product['total_price'])/100;
				array_push($productDetails, $product);
			}

			if($c>0){
				session()->put('shipped','carton');
			}
		} else {
			return redirect('/');
		}


		if($count_cart == 1 && $type_cart == 'sample') {
			$check_sample = 1;
		}

		// echo $check_sample; die;
		$success ='';
		
		$address = DB::table('address_book')->where('customers_id',\Auth::user()->id)->first();
		$user = User::where('id',\Auth::user()->id)->first();

		if(isset($address))
			$allAddress = Order::where('customers_id',\Auth::user()->id)->get();
		else
			$allAddress = "";

		// var_dump($productDetails); die;

		return view('homeglare2.checkout', compact('productDetails', 'totalAmount', 'success', 'address', 'allAddress','user','tax', 'check_sample'));
		
	}

	public function getContact(){
		$companyInfo = Setting::all();
		return view('homeglare2.contact',compact('companyInfo'));
	}

	public function getFaq(){
		return view('homeglare2.faq');
	}

	public function getLogin(){
		return view('homeglare2.login');
	}	

	public function getAccount(){
		$user = Auth::user();
		$billing_address = [];
		$delivery_address = [];

		$upd_add = DB::table('address_book')->where('customers_id', $user->id)->first();

		
		$orderDetails = Order::leftjoin('orders_status_history','orders_status_history.orders_id','orders.orders_id')->where('orders_status_history.orders_status_id' ,'>', '0')->where('orders.customers_id', $user->id)->orderBy('orders.orders_id','DESC')->orderBy('orders_status_history.orders_status_history_id','DESC')->get();

		// echo count($orderDetails);
		// die;
		
		if(count($orderDetails)>0){
			$i=0;
			foreach($orderDetails as $order){
				$billing_address[$i] = $order->billing_street_address.", ".$order->billing_city.", ".$order->billing_state.", ".$order->billing_country." (".$order->billing_postcode.") , Contact No. - ".$order->billing_phone;

				$delivery_address[$i] = $order->delivery_street_address.", ".$order->delivery_city.", ".$order->delivery_state.", ".$order->delivery_country." (".$order->delivery_postcode.") , Contact No. - ".$order->delivery_phone; 

				$i++;
			}
		}

		// var_dump($orderDetails); die;

		$help = Help::where('user_id', $user->id)->get();

		return view('homeglare2.my-account', compact('user', 'orderDetails','billing_address','delivery_address','upd_add','help'));
	}

	public function getProductDetails(Request $request,$slug){

		// session()->forget('cart');
		$product = Products::leftjoin('products_description','products_description.products_id','products.products_id')
		->where('products.products_slug',$slug)
		->orderBy('products.created_at','DESC')
		->select('products.*','products_description.*')
		->first();
		$review_status = 0;

		$p_id = $product->products_id;
		if(!empty(Auth::user()->id)){
			$check_status = DB::table('orders_products')->leftJoin('orders', 'orders.orders_id','orders_products.orders_id')->where('orders_products.products_id',$p_id)->where('orders.customers_id',Auth::user()->id)->first();

			if(!empty($check_status)){
				$status_history = DB::table('orders_status_history')->where('orders_id',$check_status->orders_id)->orderby('orders_status_history_id','DESC')->where('orders_status_id','5')->first();

				if(!empty($status_history)){
					$review_status = 1;
				}
			}

		}

		// var_dump($review_status); die

		$products_images = array();

		$products_images[0] = Products::leftJoin('images','images.id','products.products_image')
		->leftJoin('image_categories','image_categories.image_id','images.id')
		->where('image_categories.image_type','ACTUAL')
		->where('products.products_image',$product->products_image)->first();

		$images = DB::table('products_images')
		->leftJoin('images','images.id','products_images.image')
		->leftJoin('image_categories','image_categories.image_id','images.id')
		->where('image_categories.image_type','ACTUAL')
		->where('products_images.products_id',$p_id)->get();
		
		if(!empty($images)){
			$j=1;
			foreach($images as $image){
				$products_images[$j++] = $image;
			}
		}
		
		$product_price = DB::table('products_price')->where('products_id',$p_id)->get();

		$category = DB::table('products_to_categories')
		->leftjoin('categories','categories.categories_id','products_to_categories.categories_id')
		->leftJoin('categories_description','categories_description.categories_id','categories.categories_id')
		->where('products_to_categories.products_id',$p_id)
		->select('categories_description.*','categories.categories_slug')
		->first();

		$related_products = DB::table('products_to_categories')
		->leftJoin('products','products.products_id','products_to_categories.products_id')
		->leftjoin('products_description','products_description.products_id','products.products_id')
		->leftjoin('images','images.id','products.products_image')
		->leftjoin('image_categories','image_categories.image_id','images.id')
		->where('products_to_categories.categories_id',$category->categories_id)
		->where('image_categories.image_type','ACTUAL')
		->where('products.products_status','1')
		->orderBy('products.created_at','DESC')
		->select('products.*','products_description.*','images.id','image_categories.path')
		->limit(20)
		->get();

		$productReview = Reviews::leftJoin('reviews_description','reviews_description.review_id','reviews.reviews_id')
		->where('reviews.products_id', $p_id)->avg('reviews.reviews_rating');

		$countReview = Reviews::leftJoin('reviews_description','reviews_description.review_id','reviews.reviews_id')
		->where('reviews.products_id', $p_id)->count();

		$productReview = floor($productReview);

		$reviews = Reviews::leftJoin('reviews_description','reviews_description.review_id','reviews.reviews_id')
		->where('reviews.products_id', $p_id)
		->orderBy('reviews.created_at','DESC')
		->get();
		
		return view('homeglare2.product-details',compact('product','category','related_products','productReview','countReview','reviews','products_images','product_price','review_status'));
	}

	public function getProducts($id,Request $request){

		$page = $request->page ?? '0';

		if($page>0){
			$page = $page-1;
		}

		$cartproducts = array();
		$color = array();
		$brand = array();
		$material = array();
		$size = array();
		$quantity = array();
		$cnt = 0;
		
		
		$searched_products = array();
		$cat_ids = array();

		$total = array();


		$lastproduct = Products::where('products_status','1')->orderBy('created_at','desc')->first();

		$max_price = Products::where('products_status','1')->max("products_price");

		$maincategory = Categories::where('parent_id',0)->where('categories_status','1')->get();

		if($id == 'all'){

			$cat_ids = Categories::leftjoin('categories_description','categories_description.categories_id','categories.categories_id')
			->where('categories.parent_id',0)->where('categories.categories_status','1')->get();

			$searched_products = DB::table('products')
			->leftjoin('products_description','products_description.products_id','products.products_id')
			->leftjoin('images','images.id','products.products_image')
			->leftjoin('image_categories','image_categories.image_id','images.id')
			->where('image_categories.image_type','ACTUAL')
			->where('products.products_status','1')
			->orderBy('products.created_at','DESC')
			->select('products.*','products_description.*','images.id','image_categories.path')
			->distinct('products.products_id')
			->paginate(52);

		}

		else{
			$checkId = Categories::leftjoin('categories_description','categories_description.categories_id','categories.categories_id')
			->where('categories.categories_slug',$id)
			->first();

			$subcat = $checkId->categories_id;


			if(session()->get('cart') != null){
				foreach(session()->get('cart') as $cart){
					$cartproducts[$cnt] = Products::where('products_id',$cart->product_id)->first();
					$quantity[$cnt] = $cart->quantity;
					$cnt++;
				}
			}
			else{
				$cartproducts = null;
				$quantity = 0;
			}


			foreach($maincategory as $row){
				$count=0;
				$count=DB::table('products_to_categories')->where('categories_id',$row->categories_id)->count();
				$category = Categories::where('parent_id',$row->categories_id)->where('categories_status','1')->get();
				if($category){
					foreach($category as $col){
						$count+=DB::table('products_to_categories')->where('categories_id',$col->categories_id)->count();
						$subcategory = Categories::where('parent_id',$col->categories_id)->where('categories_status','1')->get();
						if($subcategory){
							foreach($subcategory as $set){
								$count+=DB::table('products_to_categories')->where('categories_id',$col->categories_id)->count();
							}
						}
					}
				}
			}


			$i=0;
			$sec = Categories::leftjoin('categories_description','categories_description.categories_id','categories.categories_id')
			->where('categories.categories_slug',$id)
			->first();

			$cat_ids[0] = Categories::leftjoin('categories_description','categories_description.categories_id','categories.categories_id')
			->where('categories.categories_id',$sec->categories_id)
			->first();

			$cid = 1;
			$cnt_pro = 0;
			$count_catpro = 0;
			$idss = Categories::leftjoin('categories_description','categories_description.categories_id','categories.categories_id')
			->where('categories.parent_id',$sec->categories_id)
			->get();

			if($idss != null){
				foreach($idss as $ids){
					$count_catpro = 0;
					$cat_ids[$cid++] = $ids;
					$idsss = Categories::leftjoin('categories_description','categories_description.categories_id','categories.categories_id')
					->where('categories.parent_id',$ids->categories_id)
					->get();

					$count_catpro+=DB::table('products_to_categories')->where('categories_id',$ids->categories_id)->count();
					if($idsss != null){
						foreach($idsss as $ds){
							$cat_ids[$cid++] = $ds;
							$count_catpro+=DB::table('products_to_categories')->where('categories_id',$ds->categories_id)->count();
						}
						if($count==0)
							$total[$i++] = 0;
						else
							$total[$i++] = $count_catpro;
					}
				}
			}


			$searched_products = DB::table('products_to_categories')
			->leftJoin('products','products.products_id','products_to_categories.products_id')
			->leftjoin('products_description','products_description.products_id','products.products_id')
			->leftjoin('images','images.id','products.products_image')
			->leftjoin('image_categories','image_categories.image_id','images.id')
			->where('products_to_categories.categories_id',$checkId->categories_id)
			->where('image_categories.image_type','ACTUAL')
			->where('products.products_status','1')
			->orderBy('products.created_at','DESC')
			->select('products.*','products_description.*','images.id','image_categories.path')
			->distinct('products.products_id')
			->paginate(52);

			$cnt_pro++;
			
		}

		$brands = ProductsBrand::where('status','1')->orderBy('brand_name','ASC')->get();

		$color = ""; $brand = ""; $material = "";

		return view('homeglare2.product',compact('maincategory','searched_products','cat_ids','total','lastproduct','brands','max_price','cartproducts','quantity','id','color','brand','material','page'));
	}
	
	
	public function getBrandProducts($brand_slug){
	    	// echo $brand; die;
		$page = $request->page ?? '0';

		if($page>0){
			$page = $page-1;
		}

		$cartproducts = array();
		$color = array();
		$brand = array();
		$material = array();
		$size = array();
		$quantity = array();
		$cnt = 0;
		
		$brand = ProductsBrand::where('brand_slug',$brand_slug)->first();
		
		$searched_products = array();
		$cat_ids = array();

		$total = array();

		$lastproduct = Products::where('products_status','1')->orderBy('created_at','desc')->first();

		$max_price = Products::where('products_status','1')->max("products_price");

		$maincategory = Categories::where('parent_id',0)->where('categories_status','1')->get();

		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Products::where('products_id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}

		$i=0;

		$cat_ids = Categories::leftjoin('categories_description','categories_description.categories_id','categories.categories_id')
		->where('categories.parent_id',0)
		->get();

		$cid = 1;
		$cnt_pro = 0;
		$count_catpro = 0; 

		

		$searched_products = ProductsBrand::leftJoin('products','products.products_brand','products_brands.id')
		->leftjoin('products_description','products_description.products_id','products.products_id')
		->leftjoin('images','images.id','products.products_image')
		->leftjoin('image_categories','image_categories.image_id','images.id')
		->where('products_brands.id',$brand->id)
		->where('image_categories.image_type','ACTUAL')
		->where('products.products_status','1')
		->orderBy('products.created_at','DESC')
		->select('products.*','products_description.*','images.id','image_categories.path','products_brands.brand_name')
		->distinct('products.products_id')
		->paginate(52);

// 			var_dump($searched_products); die;

		$id = 'all';

		$brands = ProductsBrand::where('status','1')->get();
// 			
		$color = "";  $material = "";

		return view('homeglare2.product',compact('maincategory','searched_products','cat_ids','total','lastproduct','max_price','cartproducts','brands','quantity','id','color','brand','material','page'));
	}

	public function postRegister(Request $request){
		$data = $request->all();
		// dd($data);

		$check = User::where('email',$data['email'])->get();
		if(count($check)>0){
			return redirect()->back()->withErrors([
				'credentials' => 'This email is already registered. Try another.'
			]);
		}
		else{
			if($data['password'] == $data['password_confirmation'] ){
				if(strlen($data['password'])>=8){
					
					$user = new User;
					$user->role_id = 2;
					$user->first_name = $data['first_name'];
					$user->last_name = $data['last_name'];
					$user->email = $data['email'];
					$user->gst = $data['gst'];
					$user->pincode = $data['pincode'];
					$user->state = $data['state'];
					$user->city = $data['city'];
					$user->phone = $data['phone'];
					$user->password = Hash::make($data['password']);
					$user->status = '1';

					$user->save();

					$user = User::where('email',$data['email'])->first();

					DB::table('address_book')->insert(
						array(
							'entry_firstname' => $data['first_name'],
							'entry_lastname' => $data['last_name'],
							'customers_id' => $user->id,
							'entry_street_address' => $data['address'],
							'entry_city' => $data['city'],
							'entry_state' => $data['state'],
							'entry_postcode' => $data['pincode'],
							'entry_country_id' => 99,
							'user_id' => $user->id

						));			

					$company = 'Homeglare';

					if($user){
						try {
							$name = $data['first_name']." ".$data['last_name'];
							
							$data->admin_email = 'support@homeglare.com';
							$data->admin_name = 'Homeglare';

	            //To Admin
							Mail::send('mail.register-admin', ['request' => $data], function ($m) use ($company, $data) {
								$m->from('info@homeglare.com', 'Homeglare');
								$m->subject('New User Registered to '.env('APP_NAME'));
								$m->to($data['admin_email'], $data['admin_name']);
							});

	           //To User
							Mail::send('mail.register-user', ['request' => $data], function ($m) use ($company, $data) {
								$m->from('info@homeglare.com', 'Homeglare');
								$m->subject('Welcome to '.env('APP_NAME'));
								$m->to($data['email'],$data['first_name']);
							});
						}
						catch(Exception $e)
						{

						}
						
						if ( \Auth::attempt([
							'email' => $request->email,
							'password' => $request->password
						],$request->has('remember'))){
							if(session()->has('url.intended'))
							{
								echo $next = session()->get('url.intended');
							}
							
							return redirect('/');
							return redirect($next);
						}
						else{
							
							return redirect('customer/login')->withErrors([
								'credentials' => 'Please, check your credentials'
							]);
						}

					}	
				}
				else{
					return redirect()->back()->with('message','Password must be 8 characters');
				}
			}

			else{
				return redirect()->back()->with('message','Password did not match');
			}
		}

		return redirect('/');
	}

	public function postlogin(Request $request){
		$next = session()->get('url.intended');
		
		$data = $request->all();

		if ( \Auth::attempt([
			'email' => $request->email,
			'password' => $request->password
		],$request->has('remember'))){
			if(session()->has('url.intended'))
			{
				echo $next = session()->get('url.intended');
			}
			
			return redirect('/');
			return redirect($next);
		}
		else{

			return redirect('customer/login')->with(
				'fail', '1'
			);
		}
	}

	public function getWishlist(Request $request, $id){
		// $wishlist = Wishlist::where('user_id',$id)->get();
		$wishlist = DB::table('liked_products')->where('liked_customers_id',$id)->get();

		return view('homeglare2.wishlist',compact('wishlist'));
		
	}
	
	public function getSeller(){
		// $offers = Offer::where('status','1')->orderBy('created_at','DESC')->get();
		return view('homeglare2.seller');
		// return view('homeglare2.seller',compact('offers'));
	}

	public function getOfferDetail(Request $request){
		$id = $request->id;
		$offer = Offer::find($id);
		return response()->json(['offer'=>$offer]);
	}

	public function checkepincode(Request $request)
	{
		$response = new StdClass;
		$response->status = 200;
		$pincode = Pincode::where('pincode', $request->pincode)->first();
		if ($pincode){
			$response->state = $pincode->state;
			$response->city = $pincode->district;

		}
		else{
			$response->state = "";
			$response->city = "";
		}
		return response()->json($response);
	}

	public function storesellquery(Request $request)
	{
		$data = $request->all();


		$savedata = new SellerQuery;
		$savedata->name			= $request->name;
		$savedata->email			= $request->email;
		$savedata->mobile			= $request->mobile;
		$savedata->company			= $request->company;
		$savedata->address			= $request->address;
		$savedata->gst			= $request->gst;
		$savedata->pincode			= $request->pincode;
		$savedata->city			= $request->city;
		$savedata->state			= $request->state;
		$savedata->message_content			= $request->message_content;
		$savedata->save();
		
		Mail::send('mail.sellus', ['request' => $data], function ($m) use ($data) {
			$m->from('info@homeglare.com', 'Homeglare');
			$m->subject('Sell With Homeglare');
			$m->to("support@homeglare.com", 'Admin');
		});

		return redirect()->back()->withErrors([

			'success' => 'Query received Successfully'
		]);


		// dd($data);

	}

}
