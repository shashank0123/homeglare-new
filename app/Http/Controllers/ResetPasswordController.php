<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    public function getResetPassword(){
    	return view('reset-password');
    }
}
