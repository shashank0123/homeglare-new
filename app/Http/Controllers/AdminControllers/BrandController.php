<?php

namespace App\Http\Controllers\AdminControllers;

use App\Models\Core\Images;
use App\Models\Core\Setting;
use App\Models\Core\Languages;
use App\Models\Core\ProductsBrand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Config;
use App;


class BrandController extends Controller
{

  public function __construct(ProductsBrand $brand,Images $images)
  {

      $this->brand = $brand;
      $this->images = $images;

  }


    //languages
    public function display(Request $request){

            $title = array('pageTitle' => Lang::get("labels.ListingBrands"));
            $result = array();
            $brands = $this->brand->paginator();
            $result['brands'] = $brands;
        
            return view("admin.brands.index",$title)->with('result', $result);
        }


    //addLanguages
    public function add(Request $request){

        $allimage = $this->images->getimages();
        $title = array('pageTitle' => Lang::get("labels.AddBrand"));
        return view("admin.brands.add",$title)->with('allimage',$allimage);
    }

    //addNewLanguages
    public function insert(Request $request){

        $brands = $this->brand->getter();
        $brands = $this->brand->insert($request);
        $message = Lang::get("labels.brandAddedMessage");
        return redirect()->back()->withErrors([$message]);

    }

    //editOrderStatus
    public function edit(Request $request){

        $allimage = $this->images->getimages();
        $title = array('pageTitle' => Lang::get("labels.EditBrand"));
        $brands = $this->brand->edit($request);
        $result['brands'] = $brands;
        return view("admin.brands.edit",$title)->with('result', $result)->with('allimage',$allimage);
    }



    //updateLanguageStatus
    public function update(Request $request){

            $brands = $this->brand->getter();
            $this->brand->updateRecord($request);
            $message = Lang::get("labels.brandEditMessage");
            return redirect()->back()->withErrors([$message]);
    }

   //deletelanguage
    public function delete(Request $request){

        if ($request->id==1){
            return redirect()->back()->withErrors([Lang::get("labels.DefaultDeleteMessage")]);
        }else{
               $brand = $this->brand->getter();
               $deleteLang = $this->brand->deleteRecord($request);
               return redirect()->back()->withErrors([Lang::get("labels.brandDeleteMessage")]);
        }
    }

    //getsinglelanguages
    // public function getSingleLanguages($language_id){

    //     $languagesClass = new Languages();

    //     $languages = $languagesClass->getSingleLan();
    //     return $languages;
    // }

    // public function fetchlanguages(){
    //     $languagesClass = new Languages();
    //     $languages = $languagesClass->getSingleLan();
    //     return $languages;
    // }

    public function filter(Request $request){


        $filter = $request->FilterBy;
        $parameter = $request->parameter;

        $title = array('pageTitle' => Lang::get("labels.ListingBrands"));

        $result = array();

        $Brands = null;
        switch ( $filter ) {
            case 'brand_name':

                $Brands = ProductsBrand::sortable(['id'=>'desc'])->leftJoin('images','images.id', '=', 'products_brands.brand_logo')
                    ->leftJoin('image_categories','image_categories.image_id', '=', 'products_brands.brand_logo')
                    ->select('products_brands.*','image_categories.path')
                    ->where('products_brands.brand_name', 'LIKE', '%' .  $parameter . '%') ->where(function($query) {
                        $query->where('image_categories.image_type', '=',  'THUMBNAIL')

                            ->where('image_categories.image_type','!=',   'THUMBNAIL')
                            ->orWhere('image_categories.image_type','=',   'ACTUAL');


                    })
                    ->paginate(5);
                break;

            case 'contact_person':

                $Brands = ProductsBrand::sortable(['id'=>'desc'])->leftJoin('images','images.id', '=', 'products_brands.brand_logo')
                    ->leftJoin('image_categories','image_categories.image_id', '=', 'products_brands.brand_logo')
                    ->select('products_brands.*','image_categories.path')
                    ->where('products_brands.contact_person', 'LIKE', '%' .  $parameter . '%') ->where(function($query) {
                        $query->where('image_categories.image_type', '=',  'THUMBNAIL')

                            ->where('image_categories.image_type','!=',   'THUMBNAIL')
                            ->orWhere('image_categories.image_type','=',   'ACTUAL');


                    })
                    ->paginate(5);
                break;
            default:
                $Brands = ProductsBrand::sortable(['id'=>'desc'])->leftJoin('images','images.id', '=', 'products_brands.brand_logo')
                    ->leftJoin('image_categories','image_categories.image_id', '=', 'products_brands.brand_logo')
                    ->select('products_brands.*','image_categories.path')
                    ->where(function($query) {
                        $query->where('image_categories.image_type', '=',  'THUMBNAIL')

                            ->where('image_categories.image_type','!=',   'THUMBNAIL')
                            ->orWhere('image_categories.image_type','=',   'ACTUAL');


                    })->paginate(5);

                break;

        }

        $result['brands'] = $Brands;

        return view("admin.brands.index",$title)->with('result', $result)->with('filter', $filter)->with('parameter', $parameter);



         }

         public function default(Request $request){

           DB::table('products_brands')->where('status','=', 1)->update([
                 'status'	=>	0
               ]);
           DB::table('products_brands')->where('id','=', $request->id)->update([
                 'status'	=>	1
               ]);
               // $lang = DB::table('languages')->where('languages_id',$request->languages_id)->first();
               // session()->put('language_id', $lang->languages_id);
         		   // session()->put('direction', $lang->direction);
         			 // session()->put('locale', $lang->code);
         			 // session()->put('language_name', $lang->name);
       					// $request->session()->put('direction', $lang->direction);
       					// $locale = $lang->code;
       			   //  App::setLocale($locale);

         }


}
