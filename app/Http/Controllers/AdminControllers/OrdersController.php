<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\AdminControllers\SiteSettingController;
use App\Http\Controllers\Controller;
use App\Models\Core\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use StdClass,Mail;

class OrdersController extends Controller
{
    //
    public function __construct(Setting $setting)
    {
        $this->myVarsetting = new SiteSettingController($setting);

    }

    //add listingOrders
    public function display()
    {

        $title = array('pageTitle' => Lang::get("labels.ListingOrders"));
        //$language_id                            =   $request->language_id;
        $language_id = '1';

        $message = array();
        $errorMessage = array();

        $orders = DB::table('orders')->orderBy('created_at', 'DESC')
        ->where('customers_id', '!=', '')->paginate(40);

        $index = 0;
        $total_price = array();

        foreach ($orders as $orders_data) {
            $orders_products = DB::table('orders_products')->sum('final_price');

            $orders[$index]->total_price = $orders_products;

            $orders_status_history = DB::table('orders_status_history')
            ->LeftJoin('orders_status', 'orders_status.orders_status_id', '=', 'orders_status_history.orders_status_id')
            ->LeftJoin('orders_status_description', 'orders_status_description.orders_status_id', '=', 'orders_status.orders_status_id')
            ->select('orders_status_description.orders_status_name', 'orders_status_description.orders_status_id')
            ->where('orders_status_description.language_id', '=', $language_id)
            ->where('orders_id', '=', $orders_data->orders_id)
            ->where('role_id', '<=', 2)
            ->orderby('orders_status_history.orders_status_id', 'DESC')->limit(1)->get();

            $orders[$index]->orders_status_id = $orders_status_history[0]->orders_status_id;
            $orders[$index]->orders_status = $orders_status_history[0]->orders_status_name;
            $index++;

        }

        $ordersData['message'] = $message;
        $ordersData['errorMessage'] = $errorMessage;
        $ordersData['orders'] = $orders;
        $ordersData['currency'] = $this->myVarsetting->getSetting();
        return view("admin.Orders.index", $title)->with('listingOrders', $ordersData);
    }

    //view order detail
    public function vieworder(Request $request)
    {

        $title = array('pageTitle' => Lang::get("labels.ViewOrder"));
        $language_id = '1';
        $orders_id = $request->id;

        $message = array();
        $errorMessage = array();

        DB::table('orders')->where('orders_id', '=', $orders_id)
        ->where('customers_id', '!=', '')->update(['is_seen' => 1]);

        $order = DB::table('orders')
        ->LeftJoin('orders_status_history', 'orders_status_history.orders_id', '=', 'orders.orders_id')
        ->LeftJoin('orders_status', 'orders_status.orders_status_id', '=', 'orders_status_history.orders_status_id')
        ->LeftJoin('orders_status_description', 'orders_status_description.orders_status_id', '=', 'orders_status.orders_status_id')
        ->where('orders_status_description.language_id', '=', $language_id)
        ->where('role_id', '<=', 2)
        ->where('orders.orders_id', '=', $orders_id)->orderby('orders_status_history.orders_status_id', 'DESC')->get();

        foreach ($order as $data) {
            $orders_id = $data->orders_id;

            $orders_products = DB::table('orders_products')
            ->join('products', 'products.products_id', '=', 'orders_products.products_id')
            ->LeftJoin('image_categories', function ($join) {
                $join->on('image_categories.image_id', '=', 'products.products_image')
                ->where(function ($query) {
                    $query->where('image_categories.image_type', '=', 'THUMBNAIL')
                    ->where('image_categories.image_type', '!=', 'THUMBNAIL')
                    ->orWhere('image_categories.image_type', '=', 'ACTUAL');
                });
            })
            ->select('orders_products.*', 'image_categories.path as image')
            ->where('orders_products.orders_id', '=', $orders_id)->get();
            $i = 0;
            $total_price = 0;
            $total_tax = 0;
            $product = array();
            $subtotal = 0;
            foreach ($orders_products as $orders_products_data) {
                $product_attribute = DB::table('orders_products_attributes')
                ->where([
                    ['orders_products_id', '=', $orders_products_data->orders_products_id],
                    ['orders_id', '=', $orders_products_data->orders_id],
                ])
                ->get();

                $orders_products_data->attribute = $product_attribute;
                $product[$i] = $orders_products_data;
                $total_price = $total_price + $orders_products[$i]->final_price;

                $subtotal += $orders_products[$i]->final_price;

                $i++;
            }
            $data->data = $product;
            $orders_data[] = $data;
        }

        $orders_status_history = DB::table('orders_status_history')
        ->LeftJoin('orders_status', 'orders_status.orders_status_id', '=', 'orders_status_history.orders_status_id')
        ->LeftJoin('orders_status_description', 'orders_status_description.orders_status_id', '=', 'orders_status.orders_status_id')
        ->where('orders_status_description.language_id', '=', $language_id)
        ->where('role_id', '<=', 2)
        ->orderBy('orders_status_history.orders_status_history_id', 'desc')
        ->where('orders_id', '=', $orders_id)->get();

            // var_dump($orders_status_history); die;

        $orders_status = DB::table('orders_status')
        ->LeftJoin('orders_status_description', 'orders_status_description.orders_status_id', '=', 'orders_status.orders_status_id')
        ->where('orders_status_description.language_id', '=', $language_id)->where('role_id', '<=', 2)->get();

        $ordersData['message'] = $message;
        $ordersData['errorMessage'] = $errorMessage;
        $ordersData['orders_data'] = $orders_data;
        $ordersData['total_price'] = $total_price;
        $ordersData['orders_status'] = $orders_status;
        $ordersData['orders_status_history'] = $orders_status_history;
        $ordersData['subtotal'] = $subtotal;

        //get function from other controller
        $ordersData['currency'] = $this->myVarsetting->getSetting();

        // var_dump($ordersData);
        // die;

        return view("admin.Orders.vieworder", $title)->with('data', $ordersData);
    }

    //update order
    public function updateOrder(Request $request)
    {

        $orders_status = $request->orders_status;
        $comments = $request->comments;
        $orders_id = $request->orders_id;
        $old_orders_status = $request->old_orders_status;
        $date_added = date('Y-m-d h:i:s');


        $changed_order = DB::table('orders')->where('orders_id',$orders_id)->first();

        $mobile = $changed_order->billing_phone;
        $message = "";

        if($orders_status=='2'){
            $message ="Order%20No%20$orders_id%20is%20in%20processing%20at%20HOMEGLARE%2E%20Thank%20you%2E";
        }
        else if($orders_status == '3'){
            $message ="Order%20No%20$orders_id%20is%20preparing%20for%20shipping%20at%20HOMEGLARE%2E%20Thank%20you2E";
        }
        else if($orders_status == '4'){
            $message ="Order%20No%20$orders_id%20is%20ready%20to%20shipped%2E%20Thank%20you%2E";
        }
        else if($orders_status == '5'){
            $message ="Congratutions%2E%20Your%20order%20has%20been%20delivered%20successfully%20with%20Order%20No%2E%20$orders_id%2E%20Thank%20you%20for%20your%20order%20at%20HOMEGLARE%2E";
        }
        else if($orders_status == '6'){
            $message ="Your%20order%20has%20been%20cancelled%20with%20Order%20No%2E%20$orders_id%2E%20Thank%20you%20for%20your%20order%20at%20HOMEGLARE%2E";
        }
        else if($orders_status == '7'){
            $message ="Your%20order%20has%20been%20returned%20with%20Order%20No%2E%20$orders_id%2E%20Thank%20you%20for%20your%20order%20at%20HOMEGLARE%2E";
        }



            //get function from other controller

        $setting = $this->myVarsetting->getSetting();

        $status = DB::table('orders_status')->LeftJoin('orders_status_description', 'orders_status_description.orders_status_id', '=', 'orders_status.orders_status_id')
        ->where('orders_status_description.language_id', '=', 1)->where('role_id', '<=', 2)->where('orders_status_description.orders_status_id', '=', $orders_status)->get();

        if ($old_orders_status == $orders_status) {
            return redirect()->back()->with('error', Lang::get("labels.StatusChangeError"));
        } else {


            $last_id = DB::table('orders_status_history')->get()->last();
            $last_inv_id = DB::table('inventory')->get()->last();
                // var_dump($last_id); die;
                //orders status history
            $orders_history_id = DB::table('orders_status_history')->insertGetId(
                ['orders_id' => $orders_id,
                'orders_status_history_id' => $last_id->orders_status_history_id+1,
                'orders_status_id' => $orders_status,
                'date_added' => $date_added,
                'customer_notified' => '1',
                'comments' => $comments,
            ]);

            if ($orders_status == '5') {

                $orders_products = DB::table('orders_products')->where('orders_id', '=', $orders_id)->get();

                foreach ($orders_products as $products_data) {
                    DB::table('products')->where('products_id', $products_data->products_id)->update([
                        'products_quantity' => DB::raw('products_quantity - "' . $products_data->products_quantity . '"'),
                        'products_ordered' => DB::raw('products_ordered + 1'),
                    ]);
                }
            }

            if ($orders_status == '6') {

                $orders_products = DB::table('orders_products')->where('orders_id', '=', $orders_id)->get();

                foreach ($orders_products as $products_data) {

                  $product_detail = DB::table('products')->where('products_id',$products_data->products_id)->first();

                      //dd($product_detail);

                  DB::table('products')->where('products_id', $products_data->products_id)->update([
                    'products_quantity' => DB::raw('products_quantity + "' . $products_data->products_quantity . '"'),
                    'products_ordered' => DB::raw('products_ordered + 1'),
                ]);


                  $date_added   = date('Y-m-d h:i:s');
                  $inventory_ref_id = DB::table('inventory')->insertGetId([
                    'inventory_ref_id' => $last_inv_id->inventory_ref_id+1,
                    'products_id' => $products_data->products_id,
                    'stock' => $products_data->products_quantity,
                    'admin_id' => auth()->user()->id,
                    'created_at' => $date_added,
                    'stock_type'  			=>   'in'

                ]);
                      //dd($product_detail);
                  if($product_detail->products_type==1){
                      $product_attribute = DB::table('orders_products_attributes')
                      ->where([
                          ['orders_products_id', '=', $products_data->orders_products_id],
                          ['orders_id', '=', $products_data->orders_id],
                      ])
                      ->get();

                      foreach($product_attribute as $attribute){
                                //dd($attribute->products_options,$attribute->products_options_values);
                        $prodocuts_attributes = DB::table('products_attributes')
                        ->join('products_options_descriptions', 'products_options_descriptions.products_options_id', '=', 'products_attributes.options_id')
                        ->join('products_options_values_descriptions', 'products_options_values_descriptions.products_options_values_id', '=', 'options_values_id')
                        ->where('products_options_values_descriptions.options_values_name', $attribute->products_options_values)
                        ->where('products_options_descriptions.options_name', $attribute->products_options)
                        ->select('products_attributes.products_attributes_id')
                        ->first();
                                  //dd($prodocuts_attributes);

                        DB::table('inventory_detail')->insert([
                          'inventory_ref_id' => $inventory_ref_id,
                          'products_id' => $products_data->products_id,
                          'attribute_id' => $prodocuts_attributes->products_attributes_id,
                      ]);

                    }


                }
            }
        }

        $products = DB::table('orders_products')->leftjoin('products_description','products_description.products_id','orders_products.products_id')->where('orders_id',$orders_id)->select('products_description.products_name','orders_products.*')->get();

        $orders = DB::table('orders')->where('orders_id', '=', $orders_id)
        ->where('customers_id', '!=', '')->get();

        $data = array();
        $data['customers_id'] = $orders[0]->customers_id;
        $data['orders_id'] = $orders_id;
        $data['status'] = $status[0]->orders_status_name;


        $url = "http://smpp.webtechsolution.co/http-tokenkeyapi.php?authentic-key=3934486f6d65676c6172653030313531391585654410&senderid=HOMGLR&route=1&number=$mobile&message=$message";

        $c = curl_init();
        curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($c,CURLOPT_HTTPGET ,1);

        curl_setopt($c, CURLOPT_URL, $url);
        $contents = curl_exec($c);
        if (curl_errno($c)) {
         echo 'Curl error: ' . curl_error($c);
     }else{
         curl_close($c);
     }


     $orderUpdate = DB::table('orders')->where('orders_id',$orders_id)->update(array(
        'order_action' => 'cancelled'
    ));

     $user = DB::table('users')->where('id',$changed_order->customers_id)->first();


     Mail::send('mail.orderActionResponse', ['action' => $orders_status, 'orderId' => $orders_id, 'order' => $changed_order, 'products' => $products],
       function ($n) use ($user) {
         $n->from('sales@homeglare.com', 'Homeglare' );

         $n->to($user->email, $user->first_name)->subject('Homeglare Shopping');
     });

     return redirect()->back()->with('message', Lang::get("labels.OrderStatusChangedMessage"));
 }



}

    //deleteorders
public function deleteOrder(Request $request)
{
    DB::table('orders')->where('orders_id', $request->orders_id)->delete();
    DB::table('orders_products')->where('orders_id', $request->orders_id)->delete();
    return redirect()->back()->withErrors([Lang::get("labels.OrderDeletedMessage")]);
}

    //view order detail
public function invoiceprint(Request $request)
{

    $title = array('pageTitle' => Lang::get("labels.ViewOrder"));
    $language_id = '1';
    $orders_id = $request->id;

    $message = array();
    $errorMessage = array();



    $company = new StdClass;
    $address = "";
    $res = Setting::where('name','contact_us_email')->first();
    $company->support_email = $res->value ?? '';
    $res = Setting::where('name','phone_no')->first();
    $company->contact_no = $res->value ?? '';
    $res = Setting::where('name','address')->first();
    $company->address = $res->value.", " ?? '';
    $res = Setting::where('name','state')->first();
    $company->state = $res->value.", " ?? '';
    $res = Setting::where('name','city')->first();
    $company->city = $res->value.", " ?? '';
    $res = Setting::where('name','country')->first();
    $company->country = $res->value.", " ?? '';
    $res = Setting::where('name','zip')->first();
    $company->zip = $res->value ?? '';

    DB::table('orders')->where('orders_id', '=', $orders_id)
    ->where('customers_id', '!=', '')->update(['is_seen' => 1]);

    $order = DB::table('orders')
    ->LeftJoin('orders_status_history', 'orders_status_history.orders_id', '=', 'orders.orders_id')
    ->LeftJoin('orders_status', 'orders_status.orders_status_id', '=', 'orders_status_history.orders_status_id')
    ->LeftJoin('orders_status_description', 'orders_status_description.orders_status_id', '=', 'orders_status.orders_status_id')
    ->where('orders_status_description.language_id', '=', $language_id)->where('role_id', '<=', 2)
    ->where('orders.orders_id', '=', $orders_id)->orderby('orders_status_history.date_added', 'DESC')->get();

    foreach ($order as $data) {
        $orders_id = $data->orders_id;

        $orders_products = DB::table('orders_products')
        ->join('products', 'products.products_id', '=', 'orders_products.products_id')
        ->join('products_price', 'products.products_id', '=', 'products_price.products_id')
        ->select('orders_products.*', 'products.products_image as image','products.tax_percent','products.products_tax','products_price.products_quantity as quantity')
        ->where('orders_products.orders_id', '=', $orders_id)
        ->where('products_price.products_sell_type','quantity_based')->get();
        $i = 0;
        $total_price = 0;
        $total_tax = 0;
        $product = array();
        $subtotal = 0;
        foreach ($orders_products as $orders_products_data) {

                //categories
            $categories = DB::table('products_to_categories')
            ->leftjoin('categories', 'categories.categories_id', 'products_to_categories.categories_id')
            ->leftjoin('categories_description', 'categories_description.categories_id', 'products_to_categories.categories_id')
            ->select('categories.categories_id', 'categories_description.categories_name', 'categories.categories_image', 'categories.categories_icon', 'categories.parent_id')
            ->where('products_id', '=', $orders_products_data->orders_products_id)
            ->where('categories_description.language_id', '=', $language_id)->get();

            $orders_products_data->categories = $categories;

            $product_attribute = DB::table('orders_products_attributes')
            ->where([
                ['orders_products_id', '=', $orders_products_data->orders_products_id],
                ['orders_id', '=', $orders_products_data->orders_id],
            ])
            ->get();

            $orders_products_data->attribute = $product_attribute;
            $product[$i] = $orders_products_data;
            $total_price = $total_price + $orders_products[$i]->final_price;

            $subtotal += $orders_products[$i]->final_price;

            $i++;
        }
        $data->data = $product;
        $orders_data[] = $data;
    }

    $orders_status_history = DB::table('orders_status_history')
    ->LeftJoin('orders_status', 'orders_status.orders_status_id', '=', 'orders_status_history.orders_status_id')
    ->LeftJoin('orders_status_description', 'orders_status_description.orders_status_id', '=', 'orders_status.orders_status_id')
    ->where('orders_status_description.language_id', '=', $language_id)->where('role_id', '<=', 2)
    ->orderBy('orders_status_history.date_added', 'desc')
    ->where('orders_id', '=', $orders_id)->get();

    $orders_status = DB::table('orders_status')->LeftJoin('orders_status_description', 'orders_status_description.orders_status_id', '=', 'orders_status.orders_status_id')
    ->where('orders_status_description.language_id', '=', $language_id)->where('role_id', '<=', 2)->get();



    $total_price_word = $this->numberTowords($total_price);
    
    $ordersData['message'] = $message;
    $ordersData['errorMessage'] = $errorMessage;
    $ordersData['orders_data'] = $orders_data;
    $ordersData['total_price'] = $total_price;
    $ordersData['total_price_word'] = $total_price_word;
    $ordersData['orders_status'] = $orders_status;
    $ordersData['orders_status_history'] = $orders_status_history;
    $ordersData['subtotal'] = $subtotal;

    $ordersData['company'] = $company;

        // var_dump($ordersData['orders_data']); die;
        //get function from other controller

    $ordersData['currency'] = $this->myVarsetting->getSetting();

    return view("admin.Orders.invoiceprint", $title)->with('data', $ordersData);

}


public function numberTowords($num)
{ 


    $ones = array(
        0 =>"ZERO",
        1 => "ONE",
        2 => "TWO",
        3 => "THREE",
        4 => "FOUR",
        5 => "FIVE",
        6 => "SIX",
        7 => "SEVEN",
        8 => "EIGHT",
        9 => "NINE",
        10 => "TEN",
        11 => "ELEVEN",
        12 => "TWELVE",
        13 => "THIRTEEN",
        14 => "FOURTEEN",
        15 => "FIFTEEN",
        16 => "SIXTEEN",
        17 => "SEVENTEEN",
        18 => "EIGHTEEN",
        19 => "NINETEEN",
        "014" => "FOURTEEN"
    );
    $tens = array( 
        0 => "ZERO",
        1 => "TEN",
        2 => "TWENTY",
        3 => "THIRTY", 
        4 => "FORTY", 
        5 => "FIFTY", 
        6 => "SIXTY", 
        7 => "SEVENTY", 
        8 => "EIGHTY", 
        9 => "NINETY" 
    ); 
    $hundreds = array( 
        "HUNDRED", 
        "THOUSAND", 
        "MILLION", 
        "BILLION", 
        "TRILLION", 
        "QUARDRILLION" 
    ); /*limit t quadrillion */
    $num = number_format($num,2,".",","); 
    $num_arr = explode(".",$num); 
    $wholenum = $num_arr[0]; 
    $decnum = $num_arr[1]; 
    $whole_arr = array_reverse(explode(",",$wholenum)); 
    krsort($whole_arr,1); 
    $rettxt = ""; 
    foreach($whole_arr as $key => $i){

        while(substr($i,0,1)=="0")
          $i=substr($i,1,5);
      if($i < 20){ 
        /* echo "getting:".$i; */
        $rettxt .= $ones[$i]; 
    }elseif($i < 100){ 
        if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)]; 
        if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)]; 
    }else{ 
        if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
        if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
        if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
    } 
    if($key > 0){ 
        $rettxt .= " ".$hundreds[$key]." "; 
    }
} 
if($decnum > 0){
    $rettxt .= " and ";
    if($decnum < 20){
        $rettxt .= $ones[$decnum];
    }elseif($decnum < 100){
        $rettxt .= $tens[substr($decnum,0,1)];
        $rettxt .= " ".$ones[substr($decnum,1,1)];
    }
}
return $rettxt; 
} 


public function editOrder($orderId){
        // $orderItemDetails = OrderItem::where('order_id', $orderId)->get();
  $orderItemDetails = DB::table('orders_products')
  ->where('orders_id',$orderId)
  ->orderBy('orders_products_id','DESC')
  ->get();
  $order_tran = DB::table('orders')->where('orders_id',$orderId)->first();
  $orderstatus = DB::table('orders_products')
  ->where('orders_id',$orderId)
  ->where('orders_action','cancelled')
  ->orderBy('orders_products_id','DESC')
  ->get();
  $countCheck = count($orderItemDetails)-count($orderstatus);

  return view('admin.Orders.editOrder', compact('orderItemDetails','order_tran','orderId','countCheck'));
}

public function changeOrder($action,$orderId,$productId){
 $orders_status = 6;
 if($action == 'cancel'){
    $action = 'cancelled';
    $orders_status = 6;
}
if($action == 'return'){
    $action = 'returned';
    $orders_status = 7;
}
$orders = DB::table('orders')->where('orders_id',$orderId)->first();
$order_info = json_decode($orders->order_information);


        $date_added   = date('Y-m-d h:i:s');
    $last_id = DB::table('orders_status_history')->get()->last();
    $last_inv_id = DB::table('inventory')->get()->last();

if($productId == 'all'){
    $i=0;
    foreach($order_info as $pro){
        echo $i++;
        $pro_id = explode('_',$pro->product_id)[0];
        $total = ceil($pro->total);

        DB::table('orders_products')->where('orders_id',$orderId)->where('products_id',$pro_id)->update(
            array(
                'orders_status' => 6,
                'orders_action' => $action, 
                'cancelled_amount' => $pro->total
            ));

        DB::table('products')->where('products_id', $pro_id)->update([
            'products_quantity' => DB::raw('products_quantity + "' . $pro->quantity . '"'),
            'products_ordered' => DB::raw('products_ordered - 1'),
        ]);


        $inventory_ref_id = DB::table('inventory')->insertGetId([
            'inventory_ref_id' => $last_inv_id->inventory_ref_id+1,
            'products_id' => $pro_id,
            'stock' => $pro->quantity,
            'admin_id' => auth()->user()->id,
            'created_at' => $date_added,
            'stock_type'            =>   'in'

        ]);

    }

                // var_dump($last_id); die;
                //orders status history
    $orders_history_id = DB::table('orders_status_history')->insertGetId(
        ['orders_id' => $orderId,
        'orders_status_history_id' => $last_id->orders_status_history_id+1,
        'orders_status_id' => $orders_status,
        'date_added' => $date_added,
        'customer_notified' => '1',
    ]);


}
else{
    foreach($order_info as $pro){
        if($pro->product_id == $productId){
           $pro_id = explode('_',$pro->product_id)[0];
           $total = ceil($pro->total);
           DB::table('orders_products')->where('orders_id',$orderId)->where('products_id',$productId)->update(
            array(
                'orders_status' => 6,
                'orders_action' => $action,
                'cancelled_amount' => $total
            ));

           DB::table('products')->where('products_id', $pro_id)->update([
            'products_quantity' => DB::raw('products_quantity + "' . $pro->quantity . '"'),
            'products_ordered' => DB::raw('products_ordered - 1'),
        ]);


        $inventory_ref_id = DB::table('inventory')->insertGetId([
            'inventory_ref_id' => $last_inv_id->inventory_ref_id+1,
            'products_id' => $pro_id,
            'stock' => $pro->quantity,
            'admin_id' => auth()->user()->id,
            'created_at' => $date_added,
            'stock_type'            =>   'in'

        ]);


       }
   }
}


if($orders_status == '6'){
            $message ="Your%20order%20has%20been%20cancelled%20with%20Order%20No%2E%20$orderId%2E%20Thank%20you%20for%20your%20order%20at%20HOMEGLARE%2E";
        }
        else if($orders_status == '7'){
            $message ="Your%20order%20has%20been%20returned%20with%20Order%20No%2E%20$orderId%2E%20Thank%20you%20for%20your%20order%20at%20HOMEGLARE%2E";
        }

return redirect()->back()->with('success','Order Updated Successfully');
}


}
