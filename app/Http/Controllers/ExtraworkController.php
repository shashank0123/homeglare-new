<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pincode;

class ExtraworkController extends Controller
{
    public function storepincode(Request $request)
    {
    	$file = public_path().'/Pincode_30052019.csv';
    	    //turn into array
	    $file = file($file);
	    foreach ($file as $key => $value) {
	    	if ($key == 0){
	    		continue;
	    	}
	    	else{
	    		$data = explode(',', $value);
	    		// dd($data);
	    		$dbcheck = Pincode::where('pincode', $data[4])->first();
	    		$pincode = new Pincode;
	    		$pincode->circle = $data[0];
	    		$pincode->region = $data[1];
	    		$pincode->division = $data[2];
	    		$pincode->office = $data[3];
	    		$pincode->pincode = $data[4];
	    		$pincode->office_type = $data[5];
	    		$pincode->district = $data[7];
	    		$pincode->state = $data[8];
	    		if (!$dbcheck)
		    		$pincode->save();

	    	}
	    }


    	dd($file);
    }
}
