<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Kyslik\ColumnSortable\Sortable;

class ProductsBrand extends Model
{

    use Sortable;

    public function images(){
        return $this->belongsTo('App\Images');
    }

    public $sortable = ['id', 'brand_name', 'contact_person','status','created_at','updated_at'];


        public function paginator(){
          $brands = ProductsBrand::sortable(['id'=>'desc'])
              ->select('products_brands.*','image_categories.path')
              ->LeftJoin('image_categories', function ($join) {
                  $join->on('image_categories.image_id', '=', 'products_brands.brand_logo')
                      ->where(function ($query) {
                          $query->where('image_categories.image_type', '=', 'THUMBNAIL')
                              ->where('image_categories.image_type', '!=', 'THUMBNAIL')
                              ->orWhere('image_categories.image_type', '=', 'ACTUAL');
                      });
              })
              ->groupby('products_brands.id')
              ->paginate(5);
             return $brands;
        }

        public function getter(){
          $brands = ProductsBrand::sortable(['id'=>'desc'])->leftJoin('images','images.id', '=', 'products_brands.brand_logo')
              ->leftJoin('image_categories','image_categories.image_id', '=', 'products_brands.brand_logo')
              ->select('products_brands.*','image_categories.path')
              ->where(function($query) {
                  $query->where('image_categories.image_type', '=',  'THUMBNAIL')
                      ->where('image_categories.image_type','!=',   'THUMBNAIL')
                      ->orWhere('image_categories.image_type','=',   'ACTUAL');
                    })->groupby('products_brands.id')->get();
             return $brands;
        }

    public function insert($request){

        // $idSort = DB::table('products_brands')->select('id')->get()->last();
        // $sortId = $idSort->id+1;
        
        $slug = str_replace(' ','-',$request->brand_name);
        $slug = str_replace('/','-',$slug);
        $slug = str_replace('?','-',$slug);
        
      $brand =  DB::table('products_brands')->insertGetId([
            'brand_name'			=>	$request->brand_name ?? '',
            'brand_description'			=>	$request->brand_description ?? '',
            'brand_logo'			=>	$request->brand_logo,
            'contact_person'		=>	$request->contact_person ?? '',
            'contact_number'		=>	$request->contact_number ?? '',
            'contact_address'		=>	$request->contact_address ?? '',
            'status' => $request->status,
            'brand_slug' => $slug
        ]);



    }

    public function updateRecord($request){


        if($request->brand_logo !== null){

            $uploadImage = $request->brand_logo;

        }	else{
            $uploadImage = $request->oldImage;
        }
        
         $slug = str_replace(' ','-',$request->brand_name);
        $slug = str_replace('/','-',$slug);
        $slug = str_replace('?','-',$slug);

        $orders_status = DB::table('products_brands')->where('id','=', $request->id)->update([
            'brand_name'			=>	$request->brand_name ?? '',
            'brand_description'			=>	$request->brand_description ?? '',
            'brand_logo'			=>	$uploadImage,
            'contact_person'		=>	$request->contact_person ?? '',
            'contact_number'		=>	$request->contact_number ?? '',
            'contact_address'		=>	$request->contact_address ?? '',
            'status' => $request->status,
            'brand_slug' => $slug,
            
        ]);


        return 'success';
    }

    public function deleteRecord($request){
        DB::table('products_brands')->where('id', $request->id)->delete();
        
        return 'success';
    }
    
     public function edit($request){

         $images = new Images;
         $allimage = $images->getimages();

         $brands = DB::table('products_brands')
             ->leftJoin('images','images.id', '=', 'products_brands.brand_logo')
             ->leftJoin('image_categories','image_categories.image_id', '=', 'products_brands.brand_logo')
             ->select('products_brands.*','image_categories.path')
             ->where('products_brands.id','=', $request->id)->get();


         return $brands;

     }
     public function getSingleLan(){

         $brands = DB::table('products_brands')->get();

         return $brands;

     }


}
