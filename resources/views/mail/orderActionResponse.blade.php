<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome To Homeglare</title>
<style>
.box
{
        height: 63px;
    border: 1px solid #000;
    background: black;
    color: #fff;
    line-height: 32px;
    margin-left: 15%;
    margin-right: 15%;
    padding-left: 11%;
    border-radius: 10px
}
.small-box
{
    height: 29px;
    border: 1px solid #000;
    background: black;
    color: #fff;
    line-height: 28px;
    margin-left: 42%;
    margin-right: 15%;
    padding-left: 5%;
    border-radius: 10px;
    width: 44%;
}
.snapBox
{
        height: 100px;
    border: 1px solid #000;
    width: 45%;
}
</style>
</head>

<body>
<!-- <div width="512" style="text-align:center;">
    <a href="#">Can't see our images? Click here.</a>
</div> -->
<table align="center" border="0" cellspacing="0" cellpadding="0" width="512" style="border:1px solid #e8e8e8;">
    <tbody>
        <tr><td colspan="3" height="10"></td></tr>
        <tr>
            <td width="10"></td>
            <td>
                <table border="0" cellspacing="0" cellpadding="0" width="490">
                    <tbody>
                        <tr>
                          <td colspan="3" height="70" align="center" style="background-color:#CFD9EB">
                            <a href="{{ url('/') }}"> <img src="{{ asset('/homeglare-new/images/logo/logo.png') }}" width="100" height="65"></a>
                          </td>
                        </tr>
                        
                        <tr>
                            <td width="10"></td>
                            <td>
                                <br>
                                @if($action==2)
                                Dear User, Your order is in processing. You order details are : 
                                @endif

                                @if($action==3)
                                Dear User, Your order is preparing fo shipping , having details : 
                                @endif 
                                
                                @if($action==4)
                                Dear User, Your order is ready to shipped. Your order details as are : 
                                @endif 

                                @if($action==5)
                                Congratulations, Your order has been delivered successfully to your address. Your order details as are : 
                                @endif 

                                @if($action==6)
                                Dear User, Your request to cancel order is accepted. Your cancelled order details are : 
                                @endif

                                @if($action==7)
                                Dear User, Your order is in processing. You order details as are : 
                                @endif

                                <h2>Order Details </h2>
                                <p>Order Id : {{$orderId ?? ''}} </p>
                                <br>
                            </td>
                            <td width="10"></td>
                        </tr>
                        
                        <tr>
                            <td width="10"></td>
                            <td>
                              <table style="width:100%">
                               <caption><h3>Product Details</h3></caption>
                               <tr>
                                 <th>Product Name</th>
                                 <th>Quantity</th>
                                 <th>Price</th>
                               </tr>
                               @foreach($products as $product)
                               <tr>
                                 <td>{{$product->products_name ?? ''}}</td>
                                 <td>{{$product->products_quantity ?? ''}}</td>
                                 <td>Rs. {{$product->final_price ?? ''}}</td>
                               </tr>
                               @endforeach
                               <tr>
                                <br>
                                   <b>Sub total</b> : Rs {{$order->order_price-$order->total_tax}}<br>
                                   <b>GST</b> : Rs. {{$order->total_tax}}<br>
                                   <b>Total Price</b> : Rs. {{$order->order_price}} 
                               </tr>
                              </table>
                            </td>
                            <td width="10"></td>
                        </tr>
                        <tr><td colspan="3" height="10"></td></tr>
                        <tr><td colspan="3" height="30" style="background-color:#e06f47;"></td></tr>
                    </tbody>
                </table>
            </td>
            <td width="10"></td>
        </tr>
        <tr><td colspan="3" height="10"></td></tr>
    </tbody>
</table>
</body>
</html>
