<?php



?>

@extends('admin.layout')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> {{ trans('labels.price') }} <small>{{ trans('labels.price') }}...</small> </h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard/this_month') }}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
            <li><a href="{{ URL::to('admin/products/display') }}"><i class="fa fa-database"></i> {{ trans('labels.ListingAllProducts') }}</a></li>

            <li class="active">{{ trans('labels.price') }}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->

        <!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{ trans('labels.addprice') }} </h3>

                    </div>
                    <div class="box-body">

                        <div class="row">
                            <div class="col-xs-12">
                                @if (count($errors) > 0)
                                @if($errors->any())
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{$errors->first()}}
                                </div>
                                @endif
                                @endif

                                @if (!empty(session()->get('success')))
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <p>Data udated successfully</p>
                                </div>
                                @endif
                            </div>

                        </div>

                        <div class="row">
                            <form action="/admin/products/price/add<?php if(isset($result['product_id']))echo '/'.$result['product_id'];?>" method="POST">
                                @csrf
                                <div class="col-xs-12">
                                    <div class="box box-info">
                                        <!-- form start -->
                                        <div class="box-body">

                                            <div class="row">
                                                <!-- Left col -->
                                                <div class="col-md-10">
                                                    <!-- MAP & BOX PANE -->
                                                    <div class="box-body">

                                                        <div class="form-group">
                                                            <label for="name" class="col-sm-2 col-md-4 control-label">{{ trans('labels.Products') }}<span style="color:red;">*</span> </label>
                                                            <div class="col-sm-10 col-md-8">
                                                                <select class="form-control field-validate product-type" name="products_id" id="products_price" required="">
                                                                    <option value="">{{ trans('labels.Choose Product') }}</option>
                                                                    @foreach ($result['products'] as $pro)
                                                                    <option value="{{$pro->products_id}}" @if(isset($result['product_id']) && ($result['product_id']==$pro->products_id)) {{'selected'}} @endif>{{$pro->products_name}}</option>
                                                                    @endforeach
                                                                </select><span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                                                {{ trans('labels.Product Type Text') }}.</span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group" style="display: none">
                                                            <label for="name" class="col-sm-2 col-md-4 control-label">
                                                                {{ trans('labels.Single Quantiy Price') }}<span style="color:red;">*</span>
                                                            </label>
                                                            <div class="col-sm-10 col-md-8">
                                                             <input type="text" name="single_product_price" id="single_product_price" value="@if(isset($result['single_product_price'])){{$result['single_product_price']}} @endif" class="form-control number-validate">
                                                             <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                                             {{ trans('labels.Single Quantiy Price') }}</span>

                                                         </div>
                                                     </div>
                                                     
                                                     {{-- </div> --}}
                                                     {{-- <br>    --}}
                                                     <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-4 control-label">{{ trans('labels.Product Quantity') }}<span style="color:red;">*</span></label>
                                                        <div class="col-sm-10 col-md-8">
                                                            <input type="text" name="product_quantity" id="product_quantity" value="@if(isset($result['product_quantity'])){{$result['product_quantity']}} @endif" class="form-control number-validate">
                                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                                            {{ trans('labels.Product Quantity Text') }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-4 control-label">{{ trans('labels.Quantity Based Price') }}<span style="color:red;">*</span></label>
                                                        <div class="col-sm-10 col-md-8">
                                                            <input type="text" name="quantity_based_price" id="quantity_based_price" value="@if(isset($result['quantity_based_price'])){{$result['quantity_based_price']}} @endif" class="form-control number-validate">
                                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                                            {{ trans('labels.Quantity Based Price Text') }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-4 control-label">{{ trans('labels.Sample Product Price') }}<span style="color:red;">*</span></label>
                                                        <div class="col-sm-10 col-md-8">
                                                            <input type="text" name="sample_product_price" id="sample_product_price" value="@if(isset($result['sample_product_price'])){{$result['sample_product_price']}} @endif" class="form-control number-validate">
                                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                                            {{ trans('labels.Sample Product Price Text') }}</span>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-4 control-label">{{ trans('labels.Carton Quantity') }}<span style="color:red;">*</span></label>
                                                        <div class="col-sm-10 col-md-8">
                                                            <input type="text" name="carton_quantity" id="carton_quantity" value="@if(isset($result['carton_quantity'])){{$result['carton_quantity']}} @endif" class="form-control number-validate">
                                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                                            {{ trans('labels.Carton Quantity Text') }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="name" class="col-sm-2 col-md-4 control-label">{{ trans('labels.Carton Price') }}<span style="color:red;">*</span></label>
                                                        <div class="col-sm-10 col-md-8">
                                                            <input type="text" name="carton_price" id="carton_price" value="@if(isset($result['carton_price'])){{$result['carton_price']}} @endif" class="form-control number-validate">
                                                            <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                                            {{ trans('labels.Carton Price Text') }}</span>
                                                        </div>
                                                    </div>

                                                    <!-- /.users-list -->
                                                </div>

                                            
                                            <div class="form-group" >
                                                            <label for="name" class="col-sm-2 col-md-4 control-label">
                                                                Gst Included<span style="color:red;">*</span>
                                                            </label>
                                                            <div class="col-sm-10 col-md-8">
                                                             <input type="checkbox" @if ($result['product_gst_inclusion'] == 1) checked @endif id="gst_inclusion" name="gst_inclusion" value="1">
                                                             

                                                             </div>
                                                         </div>
                                            </div>

                                            <div class="box-footer col-xs-12">
                                                <button type="submit" class="btn btn-default">{{ trans('labels.addprice') }}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>

</section>
<!-- /.row -->

<!-- Main row -->
</div>

<!-- /.row -->

@endsection


