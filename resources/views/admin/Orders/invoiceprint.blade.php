<?php
// var_dump($data['orders_data'][0]); die;
$gst_amount = 0;
?>


@extends('admin.layout')

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <!-- Popper JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  <style>
   .table-bordered{
   border:2px solid #000000 !important;
  }
.wrapper.wrapper2{
	display: block;
}
.wrapper{
	display: none;
}
.skin-blue .wrapper, .skin-blue .main-sidebar, .skin-blue .left-side {
    background-color: #fff !important;
}
</style>
<body onload="window.print();">
<div class="wrapper wrapper2">
  <table class="table-bordered table" style="font-weight: bold;">
    <tbody>

      <!-- Row 1 -->
      <tr>
        <td colspan="10">

          HOMEGLARE<br>
          {{$data['company']->address ?? ''}},<br>
          {{$data['company']->city??''}}, {{$data['company']->state??''}} - {{$data['company']->zip ?? ''}}  <br>
          MO : {{$data['company']->contact_no ?? ''}}<br> 
          E-mail : {{$data['company']->support_email ?? ''}}<br>
          GSTIN/UIN :07AAFCH1906C1Z3


        </td>
        <td colspan="7" style="text-align: center">
          <img src="{{ asset('/homeglare-new/images/logo/logo.png') }}" style="width: 150px; height: auto; margin: auto;">
        </td>
      </tr>

      <!-- Row 2 -->
      <tr>
        <td colspan="10"></td>
        <td colspan="7">
          <p style="font-style:bold";>Mfrs : HOMEGLARE</p>
        </td>
      </tr>

      <!-- Row 3 -->
      <tr>
        <td colspan="7">{{$data['orders_data'][0]->billing_name ?? ''}}<br>
        {{$data['orders_data'][0]->billing_street_address ?? ''}},
        {{$data['orders_data'][0]->billing_city ?? ''}}<br>
        {{$data['orders_data'][0]->billing_state ?? ''}}, 
        {{$data['orders_data'][0]->billing_country ?? ''}} - 
        {{$data['orders_data'][0]->billing_postcode ?? ''}} <br>
        MO. {{$data['orders_data'][0]->billing_phone ?? ''}} <br>
          
          <!--MO. 8826361066<br>-->
        </td>
        <td colspan="10">Invoice No: HG/20-21/{{$data['orders_data'][0]->orders_id ?? ''}} <br>Date: {{date('d/m/Y',strtotime(explode(' ',$data['orders_data'][0]->date_purchased ?? '')[0]))}} <br>
          Dispatch Through:<br>
          
         {{$data['orders_data'][0]->shipping_type ?? ''}} {{$data['orders_data'][0]->transportation_name ?? ''}} <br>
        </td>
      </tr>

      <!-- Row 4 -->
      <tr>
        <td colspan="1">No</td>
        <td colspan="5">Particulars</td>
        <td colspan="2">HSN No.</td>
        <td colspan="2">Qty</td>
        <td colspan="2">Rate</td>
        <td colspan="2">GST</td>
        <td colspan="3">Amount</td>
      </tr>

      <!-- Row 5 -->
      @php $i = 1; @endphp
      @foreach($data['orders_data'][0]->data as $key => $order)
      <tr>
        <td colspan="1">{{$i++}}</td>
        <td colspan="5">{{$order->products_name ?? ''}}<br>
         {{$order->quantity ?? '1'}} X {{$order->products_quantity/$order->quantity ?? '1'}}</td>
        <td colspan="2">39241090</td>
        <td colspan="2">{{$order->products_quantity ?? ''}} NOS.</td>
        <td colspan="2">{{$order->products_price ?? ''}}</td>
        <td colspan="2">{{$order->tax_percent ?? '0'}}%</td>
        <td colspan="3">{{$order->final_price + $order->products_quantity * $order->products_tax ?? '1'}}</td>
      </tr>
      @endforeach

      

      <!-- Row Bottom -->
      <tr>
        <td colspan="10">
          <table class="table">
              
              <?php
                $products = DB::table('orders_products')
                ->leftJoin('products','products.products_id','orders_products.products_id')
                ->where('orders_id',$data['orders_data'][0]->orders_id)->select('products.tax_percent','orders_products.final_price')->distinct('products.products_tax')->get();
                // var_dump($products); die;
                
               ?>
              @if($data['orders_data'][0]->billing_state == 'Delhi' || $data['orders_data'][0]->billing_state == 'New Delhi' || $data['orders_data'][0]->billing_state == 'delhi' || $data['orders_data'][0]->billing_state == 'DELHI')
            <tbody>             
        
              <tr>
                <th  colspan="2">GST Summary</th>
                <th colspan="2">Taxable Amnt</th>
                <th colspan="2"> CGST /SGST AMT</th>
                <th colspan="2"> Total GST</th>
              </tr>
              @foreach($products as $pro)
              @php $gst_amount = $gst_amount + ($pro->final_price/$pro->tax_percent) @endphp
              <tr>
                <td colspan="2">GST @ <i>{{$pro->tax_percent ?? ''}}</i></td>
                <td colspan="2">{{$pro->final_price}}</td>
                <td colspan="2">{{$pro->tax_percent/2}}/{{$pro->tax_percent/2}}</td>
                <td colspan="2">{{$pro->final_price + ($pro->final_price/$pro->tax_percent)}}</td>
              </tr>
              @endforeach
            </tbody>
            
            
            @else
            
            <tbody>             
        
              <tr>
                <th  colspan="2">GST Summary</th>
                <th colspan="2">Taxable Amnt</th>
                <th colspan="2"> IGST AMT</th>
                <th colspan="2"> Total GST</th>
              </tr>
              @foreach($products as $pro)
              @php $gst_amount = $gst_amount + ($pro->final_price/$pro->tax_percent) @endphp
              <tr>
                <td colspan="2">GST @ <i>{{$pro->tax_percent ?? ''}}</i></td>
                <td colspan="2">{{$pro->final_price}}</td>
                <td colspan="2">{{$pro->tax_percent}}</td>
                <td colspan="2">{{$pro->final_price + ($pro->final_price/$pro->tax_percent)}}</td>
              </tr>
              @endforeach
            </tbody>
            
            
            
            
            @endif
          </table>
          
        </td>
        <td colspan="7">
          <table class="table">
            <tbody>
              <tr>
                <th colspan="2">DISCOUNT%</th> 
                <th colspan="2"> -0.00% </th>
                <th colspan="2">-0</th>
              </tr>
              <tr>
                <td colspan="2">Sub Total</td>
                <td colspan="2">        </td>
                <td colspan="2">{{$data['total_price']}}</td>
              </tr>
              <tr>
                <td colspan="2">GST</td>
                <td colspan="2">        </td>
                <td colspan="2">{{$gst_amount}}</td>
              </tr>
              <tr>
                <td colspan="2">GRAND TOTAL</td>
                <td colspan="2">        </td>
                <td colspan="2">{{$data['total_price']+$gst_amount}}</td>
              </tr>
            </tbody>
            </table>
            </td>
          </tr>




          <!-- Row Last -->
          <tr>
            <td colspan="17">Rs. : {{$data['total_price_word'] ?? ''}}</td>
          </tr>





        </tbody>
      </table>




<!-- /.content -->
</div>
<!-- ./wrapper -->
</body>

