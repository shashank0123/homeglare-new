@extends('admin.layout')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> {{ trans('labels.EditOrder') }} <small>{{ trans('labels.EditOrderDetails') }}...</small> </h1>
        <ol class="breadcrumb">
            <li><a href="{{ URL::to('admin/dashboard/this_month') }}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
            <li class="active">{{ trans('labels.Orders') }}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->

        <!-- /.row -->

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                       <h3 class="page-header" style="padding-bottom: 25px; margin-top:0;">
                        <i class="fa fa-globe"></i> {{ trans('labels.EditOrderDetails') }}
                        <span style="font-size: 14px; font-weight: normal;">#Order ID - {{$orderId}}</span>
                        <small style="display: inline-block">/{{ trans('labels.OrderedDate') }}: {{ date('m/d/Y', strtotime($order_tran->date_purchased)) }}</small>

                        @if($countCheck > 0)
                        &nbsp;&nbsp; <a href="{{ URL::to('admin/orders/editorder/cancel/'.$order_tran->orders_id.'/all')}}"  class="btn btn-success pull-right" style="margin-right: 15px"><i class="fa fa-close"></i> {{ trans('labels.CancelOrder') }}</a>
                        @endif
                    </h3>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">

                            @if (count($errors) > 0)
                            @if($errors->any())
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{$errors->first()}}
                            </div>
                            @endif
                            @endif

                            @if (!empty(session()->get('success')))
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <p>Order Updated Successfully</p>
                                </div>
                                @endif

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>{{ trans('labels.ID') }}</th>
                                        <th>{{ trans('labels.OrderedProductName') }}</th>
                                        <th>{{ trans('labels.OrderedProductQuantity') }}</th>
                                        <th>{{ trans('labels.ProductUnitPrice') }}</th>
                                        <th>{{ trans('labels.SubTotalPrice') }} </th>
                                        <th>{{ trans('labels.Status') }}</th>
                                        <th>{{ trans('labels.Action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($orderItemDetails as $order)
                                    <?php
                                    $slug = DB::table('products')->leftjoin('products_description','products_description.products_id','products.products_id')
                                    ->leftjoin('images','images.id','products.products_image')
                                    ->leftjoin('image_categories','image_categories.image_id','images.id')
                                    ->where('image_categories.image_type','ACTUAL')
                                    ->where('products.products_id',$order->products_id)
                                    ->orderBy('products.created_at','DESC')
                                    ->select('products.products_slug','image_categories.path','products.products_sku_code')
                                    ->first(); 
                                    ?>
                                    <tr>
                                        <td><a href="/product-detail/{{$slug->products_slug}}"><img src="{{asset($slug->path)}}" alt="{{$order->products_name}}" title="{{$order->products_name}}" style="max-width: 100px; max-height: 150px;"></a></td>
                                        <td><a href="/product-detail/{{$slug->products_slug}}" class="view-detail-product">{{$order->products_name}}</a><br>

                                           <h6 style="font-size: 10px; font-weight: bold; color: #999"> SKU - <span style="color: #999">{{$slug->products_sku_code ?? ''}}</span></h6>
                                       </td>
                                       <td>{{$order->products_quantity}}</td>
                                       <td>Rs. {{$order->products_price}}</td>
                                       <td>Rs. {{$order->products_quantity * $order->products_price}}  </td>
                                       <td>
                                           @if($order->orders_action == 'cancel' && $order->orders_status == '1')
                                           <span style="color: red" >Requested To Cancel</span>
                                           @elseif($order->orders_status == '5')
                                           <span style="color: #00ff00" >Delivered</span>
                                           @elseif($order->orders_action == 'cancelled')
                                           <span style=" color: #00ff00" >Cancelled</span>
                                           @endif
                                       </td>
                                       <td>
                                           @if($order->orders_action == 'cancel' && $order->orders_status == '1')
                                           <a href="{{url('admin/orders/editorder/cancel/'.$order_tran->orders_id.'/'.$order->products_id)}}" class="alert alert-success" style="padding: 10px; margin-top: 5px" >Cancel</a>
                                           @endif
                                       </td>
                                   </tr>
                                   @endforeach

                                   <tr>
                                      <td colspan="4">
                                        @if(!empty($order_tran))
                                        <p><b>Payment Method -</b> {{str_replace('_',' ',$order_tran->payment_method)}}</p>
                                        <p><b>Shipping Method -</b> {{ucfirst($order_tran->shipping_type ?? '')}}
                                          @if($order_tran->shipping_type == 'transport')
                                          <br><b> Transportation Details : </b><br>
                                          <b>Name : </b>{{ucfirst($order_tran->transportation_name ?? '')}}<br>
                                          <b>Email : </b>{{ucfirst($order_tran->transportation_email ?? '')}},
                                          <b>Contact : </b>{{ucfirst($order_tran->transportation_mobile ?? '')}}<br>
                                          <b>Address : </b>{{ucfirst($order_tran->transportation_address ?? '')}}</p>
                                          @endif
                                          @endif
                                      </td>
                                      <td colspan="3">
                                          <p><b>Sub Total</b> &nbsp;: Rs. {{floor($order_tran->order_price-$order_tran->total_tax)}}</p>
                                          <p><b>GST</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Rs. {{$order_tran->total_tax}}</p>
                                          <p><b>Total</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Rs. {{$order_tran->order_price}}</p>
                                      </td>
                                  </tr>

                              </tbody>
                          </table>
                          <div class="col-xs-12 text-right">

                          </div>
                      </div>
                  </div>
              </div>
              <!-- /.box-body -->
          </div>
          <!-- /.box -->
      </div>
      <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- deleteModal -->
  <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="deleteModalLabel">{{ trans('labels.DeleteOrder') }}</h4>
            </div>
            {!! Form::open(array('url' =>'admin/orders/deleteOrder', 'name'=>'deleteOrder', 'id'=>'deleteOrder', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
            {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
            {!! Form::hidden('orders_id',  '', array('class'=>'form-control', 'id'=>'orders_id')) !!}
            <div class="modal-body">
                <p>{{ trans('labels.DeleteOrderText') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('labels.Close') }}</button>
                <button type="submit" class="btn btn-primary" id="deleteOrder">{{ trans('labels.Delete') }}</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<!-- Main row -->

<!-- /.row -->
</section>
<!-- /.content -->
</div>
@endsection
