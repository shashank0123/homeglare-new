<?php
use App\Models\Core\Products;



$date1 = explode(' ',$order_tran->date_purchased)[0];
$date2 = date('Y-m-d');
$date1=date_create($date1);
$date2=date_create($date2);
$diff=date_diff($date1,$date2);

$date_diff = $diff->format("%a");
$date_diff2 = 99;
$date_diff2 = $diff->format("%a");
$delivered = DB::table('orders_status_history')->where('orders_status_id',5)->where('orders_id',$orderId)->get()->last(); 
if(!empty($delivered)){
  $date1 = explode(' ',$delivered->date_added)[0];
  $date2 = date('Y-m-d');
  $date1=date_create($date1);
  $date2=date_create($date2);
  $diff=date_diff($date1,$date2);

  $date_diff2 = $diff->format("%a");
  
}

$Orders = DB::table('orders')->where('orders_id',$orderId)->first();
$check =  DB::table('orders_products')->where('orders_id',$orderId)->where('orders_action','!=','cancel')->count();
// var_dump($check) ; die;
?>
@extends('layouts/homeglare')

@section('content')
<style type="text/css">
tr p {
  font-size: 14px
}
#productsall, .selectall{
  height: 15px; width: 15px;
}
</style>
<br><br>
<main class="page-content" onload="checkAction()">
  <div class="account-page-area">
    <div class="container">


      <div class="myaccount-orders">
        <h4 class="small-title"> ORDER DETAILS</h4>
        <div class="row">
          <div class="col-sm-6">
            <h5>Order ID - {{$orderId}}</h5>
          </div>
          <div class="col-sm-6" style="text-align: right;padding-bottom: 20px">



            <a href="{{ url('account/'.Auth::user()->id) }}" class="btn btn-primary">Back</a>





            
          </div>
        </div>
        
        <div class="table-responsive">
          @if($orderItemDetails)
          <form method="GET" action="{{ url('ordered-item/'.$orderId.'/cancel') }}">   
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th colspan="6">
                    <input type="checkbox" class="selectedId" name="product" id="productsall" vaule="all" @if($check==0){{'disabled'}}@endif> <span style="margin-left: 25px; font-size: 18px" >Check All</span> 
                  </th>
                  <th style="text-align: right;">



                    <?php
                    $orders_status = DB::table('orders_status_history')->where('orders_id',$Orders->orders_id)->orderby('orders_status_history_id','DESC')->first();
                    ?>

                    @if($date_diff <= 10)
                    <button type="submit" class="btn btn-primary" style="background-color: #d20e14; border: 1px solid #d20e14"><span>Cancel</span></a>
                      <!-- @if($Orders->order_action == 'cancel' &&  $orders_status->orders_status_id < 5) -->
                      <!-- <span class="alert alert-primary" style="padding: 10px" >Request Under Process</span> -->
                      <!-- @elseif( $orders_status->orders_status_id == 6) -->

                      <!-- <span class="alert alert-success" style="padding: 10px" >Cancelled</span> -->
                      <!-- @elseif($orders_status->orders_status_id == 5) -->

                      <!-- <span class="alert alert-success" style="padding: 10px" >Delivered</span> -->

                      <!-- @else -->
                      <!-- @endif -->


                      @else

                      @if($orders_status->orders_status_id == 5)

                      <span class="alert alert-success" style="padding: 10px" >Delivered</span>
                      @endif

                      @endif






                    </th>
                  </tr>
                  <tr>
                    <th>#</th>
                    <th>Item Image</th>
                    <th>Item Name</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>
                    <th>Total Amount</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($orderItemDetails as $order)
                  <?php
                  $slug = Products::leftjoin('products_description','products_description.products_id','products.products_id')
                  ->leftjoin('images','images.id','products.products_image')
                  ->leftjoin('image_categories','image_categories.image_id','images.id')
                  ->where('image_categories.image_type','ACTUAL')
                  ->where('products.products_id',$order->products_id)
                  ->orderBy('products.created_at','DESC')
                  ->select('products.products_slug','image_categories.path','products.products_sku_code')
                  ->first(); 
                  ?>
                  <tr>
                    <td>
                      <input type="checkbox" name="products[]" id="products{{$slug->products_id}}" value="{{$order->orders_products_id}}" class="selectall" @if($order->orders_action == 'cancelled') {{'disabled'}} @endif>
                    </td>
                    <td><a href="/product-detail/{{$slug->products_slug}}"><img src="{{asset($slug->path)}}" alt="{{$order->products_name}}" title="{{$order->products_name}}" style="max-width: 100px; max-height: 150px;"></a></td>
                    <td><a href="/product-detail/{{$slug->products_slug}}" class="view-detail-product">{{$order->products_name}}</a><br>

                     <h6 style="font-size: 10px; font-weight: bold; color: #999"> SKU - <span style="color: #999">{{$slug->products_sku_code ?? ''}}</span></h6>
                   </td>
                   <td>{{$order->products_quantity}}</td>
                   <td>Rs. {{$order->products_price}}</td>
                   <td>Rs. {{$order->products_quantity * $order->products_price}}  </td>
                   <td>
                    @if($order->orders_action == 'cancel' && $order->orders_status == '1')
                    <span style="color: red" >Requested To Cancel</span>
                    @elseif($order->orders_status == '5')
                    <span style="color: #00ff00" >Delivered</span>
                    @elseif($order->orders_action == 'cancelled')
                    <span style=" color: #00ff00" >Cancelled</span>
                    @endif

                  </td>
                </tr>
                @endforeach

                <tr>
                  <td colspan="4">
                    @if(!empty($order_tran))
                    <p><b>Payment Method -</b> {{str_replace('_',' ',$order_tran->payment_method)}}</p>
                    <p><b>Shipping Method -</b> {{ucfirst($order_tran->shipping_type ?? '')}}
                      @if($order_tran->shipping_type == 'transport')
                      <br><b> Transportation Details : </b><br>
                      <b>Name : </b>{{ucfirst($order_tran->transportation_name ?? '')}}<br>
                      <b>Email : </b>{{ucfirst($order_tran->transportation_email ?? '')}},
                      <b>Contact : </b>{{ucfirst($order_tran->transportation_mobile ?? '')}}<br>
                      <b>Address : </b>{{ucfirst($order_tran->transportation_address ?? '')}}</p>
                      @endif
                      @endif
                    </td>
                    <td colspan="3">
                      <p><b>Sub Total</b> &nbsp;: Rs. {{ceil($Orders->order_price-$Orders->total_tax)}}</p>
                      <p><b>GST</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Rs. {{$Orders->total_tax}}</p>
                      <p><b>Total</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Rs. {{$Orders->order_price}}</p>
                      @if($deduct_amount>0)
                      <p><b>Deduct Amount</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Rs. {{$deduct_amount}}</p>
                      <h4><b>Grand Total</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Rs. {{$order_tran->order_price-$deduct_amount}}</h4>
                      @endif
                    </td>

                  </tr>
                </tbody>
              </table>

            </form>
            @endif


            <a href="{{url('/')}}" class="btn btn-primary">Continue Shoping</a>

          </div>
        </div>
      </div>
    </div>
  </main>

  <br><br>
  @endsection


  @section('script')

  <script type="text/javascript">

    @if(!empty(session()->get('requestCancel')))

    @if(session()->get('requestCancel')==1)
    Swal('Your request to cancel this selected products is sent to Homeglare successfully.');
    @else
    Swal('No product selected to cancel.');
    @endif
    @endif


  </script>

  <script>
    $(document).ready(function () {
      $('#productsall').click(function () {
        $('.selectall').prop('checked', this.checked);
      });

      $('.selectall').change(function () {
        var check = ($('.selectall').filter(":checked").length == $('.selectall').length);
        $('#productsall').prop("checked", check);
      });
    });
  </script>

  @endsection
