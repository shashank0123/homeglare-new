<?php
use App\Models\Core\Categories;
use App\Models\Core\Products;
$products_price = 0;
?>
@extends('layouts.homeglare')

@section('content')

@if(!empty($slider_banners))
<div class="slider-area">
   <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
         <?php $i=0; $j=0;?>
         @foreach($slider_banners as $banner)
         <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}" class="@if($i==0){{'active'}}@endif"></li>
         <?php $i++; ?>
         @endforeach         
         
      </ol>
      <div class="carousel-inner">
         @foreach($slider_banners as $banner)
         <div class="carousel-item @if($j==0){{'active'}}@endif">
            <img class="d-block w-100" src="{{$banner}}" alt="First slide">
            
         </div>
         <?php $j++; ?>
         @endforeach
         
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
         <span class="carousel-control-prev-icon" aria-hidden="true"></span>
         <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
         <span class="carousel-control-next-icon" aria-hidden="true"></span>
         <span class="sr-only">Next</span>
      </a>
   </div>
</div>
@endif
<style type="text/css">
.bg-color {
   background-color: #d20e14!important;
}
</style>

@if(!empty($medium_banners))
<div class="banner-area pt-20 pb-100">
   <div class="banner-slider-active">
      @foreach($medium_banners as $banner)
      <div class="banner-wrap">         
         <a href="#"><img src="{{$banner}}" alt="banner"></a>                    
      </div>
      @endforeach

   </div>
</div>
@endif

<div class="product-area">
   <div class="container-fluid">
      <div class="row">
         <div class="col-lg-3  col-md-12 " style="background-color: #00003314">
            <div class="category-menu-wrap" id="mobile-view">

               <button type="button" class="btn btn-block bg-color  mb-3 btn-lg">
                  <h3 class="showcat">
                     <a href="#">
                        <img class="category-menu-non-stick" src="/homeglare-new/images/icon-img/category-menu.png" alt="icon">
                        <img class="category-menu-stick" src="/homeglare-new/images/icon-img/category-menu-stick.png" alt="icon">
                        All Category <i class="la la-angle-down"></i>
                     </a>
                  </h3>
               </button>
               <div class="category-menu mobile-category-menu hidecat"style="display: none;" >
                  <nav>
                     <ul>
                       <li><a href="/product/all">All <span class="la la-angle-right"></span></a></li>
                       @if(!empty($maincategory))
                       @foreach($maincategory as $cat1)

                       <li class="cr-dropdown">
                        <a href="/product/{{$cat1->categories_slug}}">{{$cat1->categories_name}} <span class="la la-angle-down"></span></a>
                        <?php
                        $cate = Categories::leftjoin('categories_description','categories_description.categories_id','categories.categories_id')
                        ->where('categories.parent_id',$cat1->categories_id)
                        ->get();

                        ?>
                        @if(!empty($cate))
                        <ul class="cr-menu-desktop-none">
                           @foreach($cate as $cat2)
                           <li class="cr-sub-dropdown sub-style">
                              <a href="/product/{{$cat1->categories_slug}}">{{$cat2->categories_name}} <i class="la la-angle-down"></i></a>

                              <?php
                              $sub_cat = Categories::leftjoin('categories_description','categories_description.categories_id','categories.categories_id')
                              ->where('categories.parent_id',$cat2->categories_id)
                              ->get();

                              ?>
                              @if(!empty($sub_cat))
                              <ul style="display: none;">
                                 @foreach($sub_cat as $cat3)
                                 <li><a href="/product/{{$cat3->categories_slug}}">{{$cat3->categories_name}}</a></li>
                                 @endforeach
                              </ul>
                              @endif
                           </li>
                           @endforeach                              
                        </ul>
                        @endif
                     </li>
                     @endforeach
                     @endif                       
                  </ul>
               </nav>
            </div>

            <button type="button" class="btn btn-block bg-color  mb-3 btn-lg" style="margin-top: 25px">
               <h3 class="showbrand">
                  <a href="#">
                     <img class="category-menu-non-stick" src="/homeglare-new/images/icon-img/category-menu.png" alt="icon">
                     <img class="category-menu-stick" src="/homeglare-new/images/icon-img/category-menu-stick.png" alt="icon">
                     All Brands <i class="la la-angle-down"></i>
                  </a>
               </h3>
            </button>
            <div class="category-menu mobile-category-menu hidebrand" style="display: none;" >
               <nav>
                  <ul>
                    <!-- <li><a href="/products/all">All <span class="la la-angle-right"></span></a></li> -->
                    @if(!empty($brands))
                    @foreach($brands as $brand)

                    <li class="cr-dropdown">
                     <a href="/products/{{$brand->brand_slug}}">{{$brand->brand_name}} <span class="la la-angle-right"></span></a>

                  </li>
                  @endforeach
                  @endif                       
               </ul>
            </nav>
         </div>

      </div>

      <!-- For Desktop View -->
      <div class="category-menu-wrap" id="desktop-view">
         <!-- For All Categories -->
         <button type="button" class="btn bg-color btn-block btn-lg">
            <h3 class="showcat">
               <a href="#">
                  <img class="category-menu-non-stick" src="/homeglare-new/images/icon-img/category-menu.png" alt="icon">
                  <img class="category-menu-stick" src="/homeglare-new/images/icon-img/category-menu-stick.png" alt="icon">
                  All Category <i class="la la-angle-down"></i>
               </a>
            </h3>
         </button>
         <div class="category-menu category-menu-desktop hidecat" >
            <nav>
               @if(!empty($maincategory))
               <ul>
                 <li><a href="/product/all">All <span class="la la-angle-right"></span></a></li>
                 @foreach($maincategory as $cat1)
                 <li class="cr-dropdown">
                  <a href="/product/{{$cat1->categories_slug}}">{{$cat1->categories_name}} <span class="la la-angle-right"></span></a>
                  <?php
                  $cate = Categories::leftjoin('categories_description','categories_description.categories_id','categories.categories_id')
                  ->where('categories.parent_id',$cat1->categories_id)
                  ->get();
                  ?>
                  @if(!empty($cate))
                  <div class="category-menu-dropdown ct-menu-res-height-1">
                     @foreach($cate as $cat2)
                     <div class="single-category-menu ct-menu-mrg-bottom category-menu-border">
                        <h4><a href="/product/{{$cat2->categories_slug}}">{{$cat2->categories_name}}</a></h4>
                        <?php
                        $sub_cat = Categories::leftjoin('categories_description','categories_description.categories_id','categories.categories_id')
                        ->where('categories.parent_id',$cat2->categories_id)
                        ->get();
                        ?> 
                        @if(!empty($sub_cat))
                        <ul>
                           @foreach($sub_cat as $cat3)
                           <li><a href="/product/{{$cat3->categories_slug}}">{{$cat3->categories_name}}</a></li>
                           @endforeach
                        </ul>
                        @endif
                     </div>
                     @endforeach                             

                  </div>
                  @endif
               </li>
               @endforeach

            </ul>
            @endif
         </nav>
      </div>

      <!-- For All Brands -->
      <button type="button" class="btn bg-color btn-block btn-lg" style="margin-top: 25px">
         <h3 class="showbrand">
            <a href="#">
               <img class="category-menu-non-stick" src="/homeglare-new/images/icon-img/category-menu.png" alt="icon">
               <img class="category-menu-stick" src="/homeglare-new/images/icon-img/category-menu-stick.png" alt="icon">
               Shop By Brands <i class="la la-angle-down"></i>
            </a>
         </h3>
      </button>
      <div class="category-menu category-menu-desktop hidebrand" >
         <nav>
            @if(!empty($brands))
            <ul>
              <!-- <li><a href="/product/all">All <span class="la la-angle-right"></span></a></li> -->
              @foreach($brands as $brand)
              <li class="cr-dropdown">
               <a href="/products/{{$brand->brand_slug}}">{{$brand->brand_name}}
                 <span class="la la-angle-right"></span>
              </a>
           </li>
           @endforeach

        </ul>
        @endif
     </nav>
  </div>


</div>

@if(!empty($trending_products))
<div class="offers-img-sec left-side" id="desktop-view">
   <button class="btn bg-color btn-block text-light mb-3"> Trending Products </button>
   @foreach($trending_products as $product)

   <?php
   if(($product->products_quantity-$product->products_min_order)>0)
        $stock = "in";
         else
            $stock = 'out';

?>


<div class="my-2" id="image-pop">  <a href="/product-detail/{{$product->products_slug}}" >   
   <img lt="responsive image" class="img-fluid" style="width: auto; height: 200px" src="{{$product->path}}"></a>
   <div class="product-action-side">
<input type="hidden" id="stock{{$product->products_id}}" value="{{$stock}}">
      <a data-toggle="modal" data-target="#exampleModal" title="Quick View" href="#" data-myid="{{$product->products_id}}"><i class="la la-search"></i></a>
      <a title="Add To Cart" onclick="addToCart({{$product->products_id}},'normal','moq' @if(!empty(Auth::user()->id)) {{','.Auth::user()->id}} @endif)"><i class="la la-cart-plus"></i></a>
      <a title="Wishlist" onclick="addToWishlist({{$product->products_id}})"><i class="la la-heart-o"></i></a>
   </div>
</div>
@endforeach
</div>
@endif

@if(!empty($special_deals))
<div class="offers-img-sec left-side" id="desktop-view">
   <button class="btn bg-color btn-block text-light mb-3"> Special Deals </button>
   @foreach($special_deals as $product)

   <?php
   if(($product->products_quantity-$product->products_min_order)>0)
        $stock = "in";
         else
            $stock = 'out';

?>

   <div class="my-2" id="image-pop">  <a href="/product-detail/{{$product->products_slug}}" >   
      <img lt="responsive image" class="img-fluid" style="width: auto; height: 200px" src="{{$product->path}}"></a>
      <div class="product-action-side">
<input type="hidden" id="stock{{$product->products_id}}" value="{{$stock}}">
         <a data-toggle="modal" data-target="#exampleModal" title="Quick View" href="#" data-myid="{{$product->products_id}}"><i class="la la-search"></i></a>
         <a title="Add To Cart" onclick="addToCart({{$product->products_id}},'normal','moq' @if(!empty(Auth::user()->id)) {{','.Auth::user()->id}} @endif)"><i class="la la-cart-plus"></i></a>
         <a title="Wishlist" onclick="addToWishlist({{$product->products_id}})"><i class="la la-heart-o"></i></a>
      </div>
   </div>
   @endforeach
</div>
@endif

@if(!empty($offers))
<div class="offers-img-sec" id="desktop-view" style="margin-top: 80px">
   <button class="btn bg-color btn-block text-light mb-3"> OFFERS </button>
   @foreach($offers as $offer)
   <div class="my-2">      
      <img lt="responsive image" class="img-fluid" src="{{$offer->path}}">
   </div>
   @endforeach

</div>
@endif
</div>
<div class="col-lg-9 col-md-12">
   <div class="section-title-2 text-center">
      <h2>Featured products</h2>
      <img src="/homeglare-new/images/icon-img/title-shape.png" alt="icon-img">
   </div>
   <div class=" box-slider-active owl-carousel">
      @foreach($featured_products as $product)
      <?php
      $product_price = DB::table('products_price')->where('products_id',$product->products_id)->where('products_sell_type','sample')->first();
      $products_price = $product_price->products_price ?? $product->products_price;

      $products_price = $products_price + ($product->tax_percent*$products_price)/100;
      $products_price = number_format((float)$products_price, 2, '.', '');

   if(($product->products_quantity-$product->products_min_order)>0)
        $stock = "in";
         else
            $stock = 'out';

?>

      <div class="product-wrap product-border-1 product-img-zoom">
         <div class="product-img">
            <a href="/product-detail/{{$product->products_slug}}"><img src="{{url('/')}}/{{$product->path}}" alt="product" class="img-response"></a>

         </div>
         <div class="product-content product-content-padding">
<input type="hidden" id="stock{{$product->products_id}}" value="{{$stock}}">
            <h5 style="margin-bottom:0!important"><a href="/product-detail/{{$product->products_slug}}">{{substr(ucfirst(strtolower($product->products_name)),0,35) }}</a></h5>
            <h6 style="font-size: 10px; font-weight: bold; color: #999"> SKU - <span style="color: #999">{{$product->products_sku_code ?? ''}}</span></h6>
            <div class="price-addtocart">
               <div class="product-price">
                  <span>₹{{ceil($products_price)}}</span>
               </div>
            </div>
         </div>
         <div class="product-action-2">
            <a data-toggle="modal" data-target="#exampleModal" title="Quick View" data-myid="{{$product->products_id}}"><i class="la la-search"></i></a>
            <a title="Add To Cart" onclick="addToCart({{$product->products_id}},'normal','moq' @if(!empty(Auth::user()->id)) {{','.Auth::user()->id}} @endif)"><i class="la la-cart-plus"></i></a>
            <a title="Wishlist" onclick="addToWishlist({{$product->products_id}})"><i class="la la-heart-o"></i></a>
         </div>
      </div>
      @endforeach               
   </div>
   <div class="section-title-2 text-center mt-4">
      <h2>New Arrivals</h2>
      <img src="/homeglare-new/images/icon-img/title-shape.png" alt="icon-img">
   </div>
   <div class="box-slider-active owl-carousel">
      @if(!empty($new_products))
      @foreach($new_products as $product)
      <?php
      $product_price = DB::table('products_price')->where('products_id',$product->products_id)->where('products_sell_type','sample')->first();
      $products_price = $product_price->products_price ?? $product->products_price;
      $products_price = $products_price + ($product->tax_percent*$products_price)/100;
      $products_price = number_format((float)$products_price, 2, '.', '');

  if(($product->products_quantity-$product->products_min_order)>0)
        $stock = "in";
         else
            $stock = 'out';
?>

      <div class="product-wrap product-border-1 product-img-zoom">
         <div class="product-img">
            <a href="/product-detail/{{$product->products_slug}}"><img src="{{url('/')}}/{{$product->path}}" alt="product" class="img-response"></a>
         </div>
         <div class="product-content product-content-padding">
<input type="hidden" id="stock{{$product->products_id}}" value="{{$stock}}">
            <h5 style="margin-bottom:0!important"><a href="/product-detail/{{$product->products_slug}}">{{substr(ucfirst(strtolower($product->products_name)),0,35) }}</a></h5>
            <h6 style="font-size: 10px; font-weight: bold; color: #999"> SKU - <span style="color: #999">{{$product->products_sku_code ?? ''}}</span></h6>
            <div class="price-addtocart">
               <div class="product-price">
                  <span>₹{{ceil($products_price)}}</span>
               </div>
            </div>
         </div>
         <div class="product-action-2">
            <a data-toggle="modal" data-target="#exampleModal" title="Quick View" data-myid="{{$product->products_id}}"><i class="la la-search"></i></a>
            <a title="Add To Cart" onclick="addToCart({{$product->products_id}},'normal','moq' @if(!empty(Auth::user()->id)) {{','.Auth::user()->id}} @endif)"><i class="la la-cart-plus"></i></a>
            <a title="Wishlist" onclick="addToWishlist({{$product->products_id}})"><i class="la la-heart-o"></i></a>
         </div>
      </div>
      @endforeach
      @endif
   </div>

   @if(!empty($slider_cat))

   @foreach($slider_cat as $category)

   <?php

   $products = [];
   $cate = [];
   $cate[0] = $category->categories_id;

   $check = Categories::leftjoin('categories_description','categories_description.categories_id','categories.categories_id')
   ->where('categories.categories_status','1')
   ->where('categories.parent_id',$category->categories_id)
   ->get();

   if(!empty($check)){
      foreach($check as $cat){
         $cate[]=$cat->categories_id;

         $checks = Categories::leftjoin('categories_description','categories_description.categories_id','categories.categories_id')
         ->where('categories.categories_status','1')
         ->where('categories.parent_id',$cat->categories_id)
         ->get();
         if(!empty($checks)){
            foreach($checks as $ch){
               $cate[]=$ch->categories_id;
            }
         }
      }
   }

   if(!empty($cate)){
      foreach($cate as $cat){

         $prod = DB::table('products_to_categories')
         ->leftJoin('products','products.products_id','products_to_categories.products_id')
         ->leftjoin('products_description','products_description.products_id','products.products_id')
         ->leftjoin('images','images.id','products.products_image')
         ->leftjoin('image_categories','image_categories.image_id','images.id')
         ->where('products_to_categories.categories_id',$cat)
         ->where('image_categories.image_type','ACTUAL')
         ->orderBy('products.created_at','DESC')
         ->select('products.*','products_description.*','images.id','image_categories.path')
         ->limit(20)
         ->get();

         if(!empty($prod)){

            foreach($prod as $pro){
               $products[] = $pro;
            }
         }
      }
   }
   ?>
   <div class="">
      <div class="section-title-2 my-4 text-center">
         <h2>{{$category->categories_name}}</h2>
         <img src="/homeglare-new/images/icon-img/title-shape.png" alt="icon-img">
      </div>
      <div class=" box-slider-active owl-carousel">
         @if($products)
         @foreach($products as $product)
         <?php
         $product_price = DB::table('products_price')->where('products_id',$product->products_id)->where('products_sell_type','sample')->first();
         $products_price = $product_price->products_price ?? $product->products_price;
         $products_price = $products_price + ($product->tax_percent*$products_price)/100;
         $products_price = number_format((float)$products_price, 2, '.', '');

   if(($product->products_quantity-$product->products_min_order)>0)
        $stock = "in";
         else
            $stock = 'out';

?>

         <div class="product-wrap product-border-1 product-img-zoom">
            <div class="product-img">
               <a href="/product-detail/{{$product->products_slug}}"><img src="{{url('/')}}/{{$product->path}}" alt="product" class="img-response"></a>
            </div>
            <div class="product-content product-content-padding">
<input type="hidden" id="stock{{$product->products_id}}" value="{{$stock}}">
               <h5 style="margin-bottom:0!important"><a href="/product-detail/{{$product->products_slug}}">{{substr(ucfirst(strtolower($product->products_name)),0,35) }}</a></h5>
               <h6 style="font-size: 10px; font-weight: bold; color: #999"> SKU - <span style="color: #999">{{$product->products_sku_code ?? ''}}</span></h6>
               <div class="price-addtocart">
                  <div class="product-price">
                     <span>₹{{ceil($products_price)}}</span>
                  </div>
               </div>
            </div>
            <div class="product-action-2">
               <a data-toggle="modal" data-target="#exampleModal" title="Quick View" data-myid="{{$product->products_id}}"><i class="la la-search"></i></a>
               <a title="Add To Cart" onclick="addToCart({{$product->products_id}},'normal','moq' @if(!empty(Auth::user()->id)) {{','.Auth::user()->id}} @endif)"><i class="la la-cart-plus"></i></a>
               <a title="Wishlist" onclick="addToWishlist({{$product->products_id}})"><i class="la la-heart-o"></i></a>
            </div>
         </div>
         @endforeach               
         @endif
      </div>
   </div>
   @endforeach
   @endif
   <br>

</div>
</div>

</div>
</div>

<a data-toggle="modal" id="show-alerthome" data-target="#exampleModal2" href="#" style="display: none;">Product Stock</a>

@endsection

@section('script')

<script>
     // Add to Cart
     function addToCart(id,type,category){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        // Swal(id);
        var quantity = 1;
        var stock = $('#stock'+id).val();

        if(stock=='in'){
         $.ajax({
         url: '/add-to-cart',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, quantity: quantity, type:type, category: category},
         success: function (data) {
           // Swal(data.cartItem);
           $('#cartItem').html(data.cartItem);
           $('#cartItem2').html(data.cartItem);


         $("#messageBox2").hide().slideDown();
   setTimeout(function(){
      $("#messageBox2").hide();        
  }, 3000);
   
        },
        failure: function (data) {
          Swal(data.message);
       }
    });
        }
        else{
            $('.alert-msg').text('Product out of stock');
    $('#show-alerthome').click();
        }

        
     }

   // Add to Wishlist
   function addToWishlist(id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      userId = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id;} else {echo 0;}?> ;

      if(userId == 0){
       Swal('Please Login First');
    }
    else{
        // Swal(id);

        $.ajax({
         url: '/add-to-wishlist',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, userId : userId},
         success: function (data) {
           // Swal(data.wishItem);
           $('#wishItem').html(data.wishItem);
           $('#wishItem2').html(data.wishItem);
        },
        failure: function (data) {
          Swal(data);
       }
    });
     }
  }
</script>

@endsection