@extends('layouts.homeglare')

@section('content')

<!-- <div class="breadcrumb-area bg-img" style="background-image:url(/homeglare-new/images/bg/breadcrumb.jpg);"> -->
  <div class="bg-img">
    <div class="container">
      <div class="breadcrumb-content text-center">
        <h2>My account page</h2>
        <ul>
          <li>
            <a href="/">Home</a>
          </li>
          <li class="active">My account </li>
        </ul>
      </div>
    </div>
  </div>
  <!-- my account wrapper start -->
  <div class="my-account-wrapper pt-100 pb-100">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <!-- My Account Page Start -->
          <div class="myaccount-page-wrapper">

            @if(session('message'))
            <div class="alert alert-success" id="message" style="margin-top: 10px; text-align: center;">
              {{ session('message') }}
            </div>
            @endif

            <!-- My Account Tab Menu Start -->
            <div class="row">
              <div class="col-lg-3 col-md-4">
                <div class="myaccount-tab-menu nav" role="tablist">
                  <a href="#dashboad" class="active" data-toggle="tab"><i class="fa fa-dashboard"></i>
                  Dashboard</a>
                  <a href="#account-info" data-toggle="tab"><i class="fa fa-user"></i> My Account Details</a>    
                  <a href="#orders" data-toggle="tab"><i class="fa fa-cart-arrow-down"></i> Orders</a>
                  <a href="#download" data-toggle="tab"><i class="fa fa-cloud-download"></i> Help & Support</a>
                  {{-- <a href="#payment-method" data-toggle="tab"><i class="fa fa-credit-card"></i> Payment Method</a> --}}
                  <a href="#address-edit" data-toggle="tab"><i class="fa fa-map-marker"></i> Address</a>
                  <a id="account-logout-tab" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();" role="tab" aria-selected="false"><i class="fa fa-sign-out"></i>Logout</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                  </form>

                </div>
              </div>
              <!-- My Account Tab Menu End -->
              <!-- My Account Tab Content Start -->
              <div class="col-lg-9 col-md-8">
                <div class="tab-content" id="myaccountContent">
                  <!-- Single Tab Content Start -->
                  <div class="tab-pane fade show active" id="dashboad" role="tabpanel">
                    <div class="myaccount-content">
                      <h3>Dashboard</h3>
                      <div class="welcome">
                        <p>Hello, <strong>{{ucfirst($user->first_name ?? '')}} {{ucfirst($user->last_name ?? '')}}</strong> {{-- (If Not <strong>Tuntuni !</strong><a href="{{url('customer/login')}}" class="logout"> Logout</a>) --}}</p>
                      </div>

                      <p class="mb-0">From your account dashboard. you can easily check & view your recent orders, manage your shipping and billing addresses and edit your password and account details.</p>
                    </div>
                  </div>
                  <!-- Single Tab Content End -->
                  <!-- Single Tab Content Start -->
                  <div class="tab-pane fade" id="orders" role="tabpanel">
                    <div class="myaccount-content">
                      <h3>Orders</h3>
                      <div class="myaccount-table table-responsive text-center">
                        <table class="table table-bordered">
                          <thead class="thead-light">
                            <tr>
                              <th>ORDER ID</th>
                              <th>DATE</th>
                              <th>ADDRESS</th>
                              <th>PAYMENT MODE</th>
                              <th>STATUS</th>
                              <th>TOTAL</th>
                              <th colspan="3">Action</th>
                            </tr>
                          </thead>
                          @if($orderDetails != Null)
                          <tbody>
                            @php $orderCheck = 0; @endphp
                            @foreach($orderDetails as $order)
                            <?php 
                            $status = DB::table('orders_status_history')->leftJoin('orders_status_description','orders_status_history.orders_status_id','orders_status_description.orders_status_id')->where('orders_status_history.orders_id',$order->orders_id)->get()->last();
                            $quantity = DB::table('orders_products')->where('orders_id',$order->orders_id)->count();
                            
                            ?>
                            @if($orderCheck != $order->orders_id)
                            <tr>
                              <td><a class="account-order-id" href="javascript:void(0)">{{$order->orders_id}}</a></td>
                              <td>{{$date = explode(' ',$order->date_purchased)[0]}}</td>
                              <td>
                                <?php echo $order->delivery_street_address.",<br> ".$order->delivery_city.", ".$order->delivery_state.",<br> ".$order->delivery_country." (".$order->delivery_postcode.") <br>Contact No. - ".$order->delivery_phone;
                                ?>
                              </td>
                              <td>{{$order->payment_method}}</td>
                              <td>{{$status->orders_status_name}}</td>
                              <td>Rs. {{$order->order_price}} <!-- for {{$quantity}} items --></td>
                              <td>
                                <a href="/ordered-item-details/{{$order->orders_id}}" class="view-detail"><span><u>View Detail</u></span></a>
                              </td>
                              <td>
                                <a href="/ordered-item-track/{{$order->orders_id}}" class="track-order"><span><u>Track</u></span></a>

                              </td>
                              <?php
                              
                              
                              $date1 = $date;
                              $date2 = date('Y-m-d');
                              $date1=date_create($date1);
                              $date2=date_create($date2);
                              $diff=date_diff($date1,$date2);

                              $date_diff = $diff->format("%a");
                              $date_diff2 = 99;
                              $date_diff2 = $diff->format("%a");
                              $delivered = DB::table('orders_status_history')->where('orders_status_id',5)->where('orders_id',$order->orders_id)->get()->last(); 
                              if(!empty($delivered)){
                                $date1 = explode(' ',$delivered->date_added)[0];
                              $date2 = date('Y-m-d');
                              $date1=date_create($date1);
                              $date2=date_create($date2);
                              $diff=date_diff($date1,$date2);

                              $date_diff2 = $diff->format("%a");
                              
                              }

                              ?>

                              <!-- @if($date_diff2<=10 || $date_diff<=10) -->
                              <td style="text-align: left;">

                                @if($date_diff2<=10)
                                <!-- <a href="/ordered-item/{{$order->orders_id}}/return" class="btn btn-primary" style="margin-bottom: 5px" ><span>Return</span></a> -->
                                @endif

                                <br>



                              
                                <?php
            $orders_status = DB::table('orders_status_history')->where('orders_id',$order->orders_id)->orderby('orders_status_history_id','DESC')->first();
            ?>

            @if($date_diff <= 10 && $orders_status->orders_status_id < 5)
            
            <a href="/ordered-item/{{$order->orders_id}}" class="btn btn-primary" style="background-color: #d20e14; border: 1px solid #d20e14"><span>Cancel</span></a>
            


            @else

            @if($orders_status->orders_status_id == 5)

            <span class="alert alert-success" style="padding: 10px" >Delivered</span>
            @elseif($orders_status->orders_status_id == 6)
            <span class="alert alert-success" style="padding: 10px" >Cancelled</span>
            @elseif($orders_status->orders_status_id == 7)
            <span class="alert alert-success" style="padding: 10px" >Returned</span>
            @endif

            @endif

                                
                              </td>
                              <!-- @endif -->

                            </tr>
                            @endif
                            @php $orderCheck = $order->orders_id; @endphp
                            @endforeach

                          </tbody>
                          @endif
                        </table>
                      </div>
                    </div>
                  </div>
                  <!-- Single Tab Content End -->
                  <!-- Single Tab Content Start -->
                  <div class="tab-pane fade" id="download" role="tabpanel">
                    <div class="myaccount-content">
                      <a href="/help-support/create" class="btn btn-primary" style="background-color: #000033; margin-bottom: 20px"><span>Add Query</span></a>
                      <h3>Help & Support</h3>
                      <div class="myaccount-table table-responsive text-center">
                        @if(!empty($help))
                        <table class="table table-bordered">
                          <thead class="thead-light">
                            <tr>
                              <th>Token</th>
                              <th>Subject</th>
                              <th>Message</th>
                              <th>Date</th>
                              <th>Status</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($help as $item)
                            <tr>
                              <td><a class="account-help-token" href="javascript:void(0)">{{$item->token}}</a></td>
                              <td>{{$item->subject}}</td>
                              <td>{{$item->message}}</td>
                              <td>{{$item->created_at->format('d-m-Y') }}</td>
                              <td>{{$item->status}}</td>
                              <td>
                                <a href="/reply?token={{$item->token}}" class="btn btn-primary"><span>Reply</span></a>
                              </td>
                            </tr>
                            @endforeach

                          </tbody>
                        </table>
                        @endif
                      </div>
                    </div>
                  </div>
                  <!-- Single Tab Content End -->
                  <!-- Single Tab Content Start -->
                  <div class="tab-pane fade" id="payment-method" role="tabpanel">
                    <div class="myaccount-content">
                      <h3>Payment Method</h3>
                      <p class="saved-message">You Can't Saved Your Payment Method yet.</p>
                    </div>
                  </div>
                  <!-- Single Tab Content End -->
                  <!-- Single Tab Content Start -->
                  <div class="tab-pane fade" id="address-edit" role="tabpanel">
                    <div class="myaccount-content row">
                      <div class="col-sm-6">
                        <h3>Billing Address</h3>
                        <address>
                          <p><strong>{{$user->name}}</strong></p>
                          <p>
                            @if(!empty($billing_address))         
                            @foreach($billing_address as $address)
                            {{$address}}
                            @endforeach
                            @endif
                          </p>
                        </address>
                        {{-- <a href="#" class="check-btn sqr-btn "><i class="fa fa-edit"></i> Edit Address</a> --}}
                      </div>
                      <div class="col-sm-6">
                        <h3>Shipping Address</h3>
                        <address>
                          <p><strong>{{$user->name}}</strong></p>
                          <p><ul>
                           @if(!empty($delivery_address))         
                           @foreach($delivery_address as $address)
                           <li>{{$address}}</li>
                           @endforeach
                           @endif
                           <ul>                                  
                           </p>
                         </address>
                         {{-- <a href="#" class="check-btn sqr-btn "><i class="fa fa-edit"></i> Edit Address</a> --}}
                       </div>

                     </div>
                   </div>
                   <!-- Single Tab Content End -->
                   <!-- Single Tab Content Start -->
                   <div class="tab-pane fade" id="account-info" role="tabpanel">
                    <div class="myaccount-content">
                      <h3>Account Details</h3>
                      <div class="account-details-form">
                        <form action="/update-profile" method="POST">
                          @csrf
                          
                          <div class="row">
                            <div class="col-lg-6">
                              <div class="single-input-item">
                                <label for="first_name" class="required">First Name</label>
                                <input type="text" class="form-control" id="first_name" name="first_name" value="{{$user->first_name ?? ''}}" />
                              </div>
                            </div>
                            <div class="col-lg-6">
                              <div class="single-input-item">
                                <label for="last_name" class="required">Last Name</label>
                                <input type="text" class="form-control" id="last_name" name="last_name" value="{{$user->last_name ?? ''}}" />
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="single-input-item">
                                <label for="email" class="required">Email Address</label>
                                <input type="email" id="email" name="email" value="{{$user->email}}" class="form-control"/>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-lg-12">
                              <div class="single-input-item">
                                <label for="entry_street_address" class="required">Address</label>
                                <input type="text" class="form-control" name="entry_street_address" id="entry_street_address" value="{{$upd_add->entry_street_address ?? ''}}" />
                              </div>
                            </div>

                            <div class="col-lg-6">
                              <div class="single-input-item">
                                <label for="entry_state" class="required">State</label>
                                <input type="text" id="entry_state" name="entry_state" value="{{$upd_add->entry_state ?? ''}}" class="form-control"/>
                              </div>
                            </div>

                            <div class="col-lg-6">
                              <div class="single-input-item">
                                <label for="entry_city" class="required">City</label>
                                <input type="text" id="entry_city" name="entry_city" value="@if(!empty($upd_add->entry_city)){{$upd_add->entry_city}}@endif" class="form-control"/>
                              </div>
                            </div>

                            <div class="col-lg-6">
                              <div class="single-input-item">
                                <label for="entry_postcode" class="required">PIN Code</label>
                                <input type="text" id="entry_postcode" name="entry_postcode" value="@if(!empty($upd_add->entry_postcode)){{$upd_add->entry_postcode}}@endif" class="form-control"/>
                              </div>
                            </div>

                            <div class="col-lg-6">
                              <div class="single-input-item">
                                <label for="phone" class="required">Phone</label>
                                <input type="text" id="phone" name="phone" value="@if(!empty($user->phone)){{$user->phone}}@endif" class="form-control"/>
                              </div>
                            </div>

                            <div class="single-input-item">
                              <button stye="submit" name="edit" class="check-btn sqr-btn ">Update Profile</button>
                            </div>
                          </div>
                        </form>


                        <form action="/update-password" method="POST">
                          @csrf
                          <fieldset>
                            <legend>Password change</legend>
                            <div class="single-input-item">
                              <label for="current-password" class="required">Current Password</label>
                              <input type="password" class="form-control" id="current-password" name="current-password" />
                            </div>
                            <div class="row">
                              <div class="col-lg-6">
                                <div class="single-input-item">
                                  <label for="new-password" class="required">New Password</label>
                                  <input type="password" class="form-control" name="new-password" id="new-password" />
                                </div>
                              </div>
                              <div class="col-lg-6">
                                <div class="single-input-item">
                                  <label for="confirm-password" class="required">Confirm Password</label>
                                  <input type="password" class="form-control" name="confirm-password" id="confirm-password" />
                                </div>
                              </div>
                            </div>
                          </fieldset>
                          <div class="single-input-item">
                            <button stye="submit" name="edit" class="check-btn sqr-btn ">Save Changes</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div> <!-- Single Tab Content End -->
                </div>
              </div> <!-- My Account Tab Content End -->
            </div>
          </div> <!-- My Account Page End -->
        </div>
      </div>
    </div>
  </div>

  @endsection

  
@section('script')

<script type="text/javascript">
  
  @if(!empty(session()->get('requestCancel')))
  Swal('Your request to cancel this order is sent to Homeglare successfully.');
  @endif

   
</script>

@endsection