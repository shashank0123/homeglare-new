@extends('layouts.homeglare')

@section('content')
<style>
    p{
        color:#000;
    }
</style>

 <!-- <div class="breadcrumb-area bg-img" style="background-image:url('/homeglare-new/images/bg/breadcrumb.jpg);">
            <div class="container">
                <div class="breadcrumb-content text-center">
                    <h2>About us page</h2>
                    <ul>
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li class="active">About us </li>
                    </ul>
                </div>
            </div>
        </div> -->
        <div class="about-us-area pt-90 pb-90">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="about-us-img text-center">
                            <a href="#">
                                <img src="{{url('/homeglare-new/images/banner/about-us.jpg')}}" alt ="">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 align-self-center">
                        <div class="about-us-content">
                            <h2>Welcome To <span>Homeglare</span> Store !</h2>
                            <p class="">Homeglare is a Business-to-Business-to-Customer (B2B2C) E-commerce Marketplace curated to solve all of your household needs. Be it resetting your living room, or changing apparatuses in your kitchen, we have it all. With a variety of products offered on our platform, one would get ample of choices to choose from. We cater to all type of customers, so one would find a range of items that would suit their pocket. 
Our expertise is not only limited to Manufacturing, but we have also ventured out to Importing and Exporting. We offer these services on a single platform, collaborating Importers and Exporters together. We take great pride in catering to major Modern Retailers such as Vishal Mega Mart and Walmart.
At Homeglare, one would find varying products, ranging from home furnishing, home improvements, and home decor to cleaning supplies. Our major focus is to provide top notch quality products to our customers. At Homeglare, we are a firm believer that the satisfaction of our customers is our top priority. What makes Homeglare stand apart is our distribution network, where we provide door to door delivery services through our trusted and renowned logistics partners such as FedEx, Delhivery, Bluedart, etc.</p>
<h2>Our <span>Story</span></h2>
                            <p>

                             An ISO Certified company: Homeglare E-Commerce Pvt. Ltd is a registered startup recognized by DIPP Govt. of India, also registered with MSME brings for you functional and fun kitchen and home tools to make the daily chores interesting not tiring. Along with the everyday great prices, Homeglare is a registered brand that brings the best offers on all favorite products. Get quality products, exclusive designs, and timeless style all at http://homeglare.com/Showcasing useful products that range from kitchenware to homeware, Homeglare is one of the great B2B e-commerce marketplace that prepares you to be spoilt for choice!</p>

                            <br>

                            <p class="peragraph-blog">There are four primary reasons our clients adore us;</p>

                            <p>
                                <b>Service:</b>The administration we offer you is everything to us. Our expert site makes picking your apparatus as simple as could be expected under the circumstances. Putting in your request online is sheltered with our 100% verified site, so you can feel loose and certain your subtleties are completely secured

                                
                                <br><br>
                                <b>Price:</b> We checks each value, every day to ensure we can bring our clients the most recent offers and extraordinary low costs.

                                <br><br>

                                <b>Conveyance:</b>  We pride ourselves on our conveyance administration where our group will convey your machine into a room of your decision. We guarantee clarifies our whole conveyance process and what makes us stand apart from the group.</p>
                           <!--  <div class="about-us-btn btn-hover hover-border-none">
                                <a class="btn-color-white btn-color-theme-bg black-color" href="/productw">Shop now!</a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="feature-area pb-90 section-padding-3">-->
        <!--    <div class="container">-->
        <!--        <div class="feature-border feature-border-about">-->
        <!--            <div class="row">-->
        <!--                <div class="col-lg-3 col-md-6 col-sm-6">-->
        <!--                    <div class="feature-wrap mb-30 text-center">-->
        <!--                        <img src="{{url('/homeglare-new/images/icon-img/feature-icon-1.png')}}" alt="">-->
        <!--                        <h5>Best Product</h5>-->
        <!--                        <span>Best Queality Products</span>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--                <div class="col-lg-3 col-md-6 col-sm-6">-->
        <!--                    <div class="feature-wrap mb-30 text-center">-->
        <!--                        <img src="{{url('/homeglare-new/images/icon-img/feature-icon-2.png')}}" alt="">-->
        <!--                        <h5>100% fresh</h5>-->
        <!--                        <span>Best Queality Products</span>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--                <div class="col-lg-3 col-md-6 col-sm-6">-->
        <!--                    <div class="feature-wrap mb-30 text-center">-->
        <!--                        <img src="{{url('/homeglare-new/images/icon-img/feature-icon-3.png')}}" alt="">-->
        <!--                        <h5>Secure Payment</h5>-->
        <!--                        <span>Best Queality Products</span>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--                <div class="col-lg-3 col-md-6 col-sm-6">-->
        <!--                    <div class="feature-wrap mb-30 text-center">-->
        <!--                        <img src="{{url('/homeglare-new/images/icon-img/feature-icon-4.png')}}" alt="">-->
        <!--                        <h5>Best Wood</h5>-->
        <!--                        <span>Best Queality Products</span>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--    </div>-->
        <!--</div>-->
        <!-- <div class="banner-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="banner-wrap mb-30">
                            <a href="/product-detailw">
                                <img src="{{url('/homeglare-new/images/banner/banner-12.png')}}" alt="banner">
                            </a>
                            <div class="banner-content-8">
                                <h2>Best LED TV</h2>
                                <h5>G432xx</h5>
                                <h3><span>$</span>1990.00</h3>
                                <h4>0% <span>EMI </span> available</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="banner-wrap mb-30">
                            <a href="/product-detailw">
                                <img src="{{url('/homeglare-new/images/banner/banner-13.png')}}" alt="banner">
                            </a>
                            <div class="banner-content-9">
                                <h3>Beots <br>Superb</h3>
                                <p>Lorem Ipsum is simply dummy text</p>
                                <a href="/product-detailw">Shop Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="banner-wrap mb-30">
                            <a href="/product-detailw">
                                <img src="{{url('/homeglare-new/images/banner/banner-14.png')}}" alt="banner">
                            </a>
                            <div class="banner-content-9">
                                <h1>Beots</h1>
                                <h4>Sup<span>erb</span></h4>
                                <p>Lorem Ipsum is simply dummy text</p>
                                <a href="/product-detailw">Shop Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- <div class="team-area pt-60 pb-60">
            <div class="container">
                <div class="section-title-2 text-center">
                    <h2>Team Members</h2>
                    <img src="{{url('/homeglare-new/images/icon-img/title-shape.png')}} " alt="icon-img">
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="team-wrapper mb-30">
                            <div class="team-img">
                                <a href="#">
                                    <img src="{{url('/homeglare-new/images/team/team-1.jpg')}}" alt=""> 
                                </a>
                                <div class="team-action">
                                    <a class="facebook" href="#">
                                        <i class="ti-facebook"></i>
                                    </a>
                                    <a class="twitter" href="#">
                                        <i class="ti-twitter-alt"></i>
                                    </a>
                                    <a class="instagram" href="#">
                                        <i class="ti-instagram"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="team-content text-center">
                                <h4>Mr.Mike Banding</h4>
                                <span>Manager </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="team-wrapper mb-30">
                            <div class="team-img">
                                <a href="#">
                                    <img src="{{url('/homeglare-new/images/team/team-2.jpg')}}" alt=""> 
                                </a>
                                <div class="team-action">
                                    <a class="facebook" href="#">
                                        <i class="ti-facebook"></i>
                                    </a>
                                    <a class="twitter" href="#">
                                        <i class="ti-twitter-alt"></i>
                                    </a>
                                    <a class="instagram" href="#">
                                        <i class="ti-instagram"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="team-content text-center">
                                <h4>Mr.Peter Pan</h4>
                                <span>Developer </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="team-wrapper mb-30">
                            <div class="team-img">
                                <a href="#">
                                    <img src="{{url('/homeglare-new/images/team/team-3.jpg')}}" alt=""> 
                                </a>
                                <div class="team-action">
                                    <a class="facebook" href="#">
                                        <i class="ti-facebook"></i>
                                    </a>
                                    <a class="twitter" href="#">
                                        <i class="ti-twitter-alt"></i>
                                    </a>
                                    <a class="instagram" href="#">
                                        <i class="ti-instagram"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="team-content text-center">
                                <h4>Ms.Sophia</h4>
                                <span>Designer </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="team-wrapper mb-30">
                            <div class="team-img">
                                <a href="#">
                                    <img src="{{url('/homeglare-new/images/team/team-4.jpg')}}" alt=""> 
                                </a>
                                <div class="team-action">
                                    <a class="facebook" href="#">
                                        <i class="ti-facebook"></i>
                                    </a>
                                    <a class="twitter" href="#">
                                        <i class="ti-twitter-alt"></i>
                                    </a>
                                    <a class="instagram" href="#">
                                        <i class="ti-instagram"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="team-content text-center">
                                <h4>Mr.John Lee</h4>
                                <span>Chairmen </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
       
        {{-- <div class="testimonial-area pt-60 pb-80">
            <div class="container">
                <div class="section-title-2 text-center">
                    <h2>Testimonials</h2>
                    <img src="{{url('/homeglare-new/images/icon-img/title-shape.png')}} " alt="icon-img">
                </div>
                <div class="testimonial-active owl-carousel">
                    <div class="sin-testimonial">
                        <div class="client-content">
                            <p>“ Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, laboris consequat. ”</p>
                        </div>
                        <div class="client-info">
                            <img alt="" src="{{url('/homeglare-new/images/testimonial/client-1.png')}}">
                            <h5>Anna Miller</h5>
                            <span>Deginer</span>
                        </div>
                    </div>
                    <div class="sin-testimonial">
                        <div class="client-content">
                            <p>“ Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, laboris consequat. ”</p>
                        </div>
                        <div class="client-info">
                            <img alt="" src="{{url('/homeglare-new/images/testimonial/client-2.png')}}">
                            <h5>Kevin Walker</h5>
                            <span>Developer</span>
                        </div>
                    </div>
                    <div class="sin-testimonial">
                        <div class="client-content">
                            <p>“ Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, laboris consequat. ”</p>
                        </div>
                        <div class="client-info">
                            <img alt="" src="{{url('/homeglare-new/images/testimonial/client-3.png')}}">
                            <h5>Ruth Pierce</h5>
                            <span>Customer</span>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        



        @endsection