<?php
use App\Models\Core\Product;
use App\Models\Core\Reviews;
$product_quantity = 1;
$fix_quantity = 1;

$products_price = $product->products_price;
if(!empty($product_price))
    foreach($product_price as $price){
        if($price->products_sell_type == 'quantity_based'){
            $products_price = $price->products_price;
        }
    }

    ?>
    @extends('layouts.homeglare')

    <!-- seo and social share content -->
    @if($product)
    @section('title', $product->products_name)
    @endif

    @if($product)
    @section('social_title', $product->products_name)
    @section('social_description',$product->products_description)

    @section('social_image',  env('APP_URL').'/'.$products_images[0]->path )
    @endif

    @section('content')  
    <style>
    label { color: #000033 !important; }
    .star s:hover,
    .star s.active { color: #000033; }
    .star-rtl s:hover,
    .star-rtl s.active { color: #37bc9b; }
    .star s, .star-rtl s { color: black; font-size: 50px; cursor: default; text-decoration: none; line-height: 50px; }
    .star { padding: 2px; }
    .star-rtl { background: #555; display: inline-block; border: 2px solid #444; }
    .star-rtl s { color: #000033; }
    .star s:hover:before, .star s.rated:before, .star s.active:before { content: "\2605";
    font-size: 32px; }
    .star s:before { content: "\2606"; font-size: 32px; }
    .star-rtl s:hover:after, .star-rtl s.rated:after, .star-rtl s.active:after { content: "\2605"; font-size: 32px; }
    .star-rtl s:after { content: "\2606"; }
    .active { color: #000033  !important; }
    .fa-star { color: #ccc ; }
    [class^="ti-"], [class*=" ti-"] {
        line-height: 2 !important;
    }
</style>            

<div class="product-details-area pt-45 pb-90">
    <div class="container-fluid" style="padding: 0 25px">
       <h6>
        &nbsp;&nbsp;&nbsp;<a href="{{ url('/') }}" style="color: #d20e14;">Home</a>&nbsp;<i class="la la-angle-right"></i>&nbsp;<a href="{{ url('product/'.$category->categories_slug ?? '') }}" style="color: #d20e14;">{{$category->categories_name ?? ''}}</a>&nbsp;<i class="la la-angle-right"></i>&nbsp;{{$product->products_name ?? ''}}</h6>

        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="product-details-img" style="width: 100%; height: auto">
                    <div class="zoompro-border zoompro-span" style="width: 100%; height: 400px; display: flex; border: 1px solid #efefef; text-align: center">
                        @if(!empty($products_images) && isset($products_images[0]))
                        <img class="zoompro" src="{{asset($products_images[0]->path)}}" data-zoom-image="{{asset($products_images[0]->path)}}" alt=""  style="max-height: 380px; margin: auto; display: flex"/> 
                        @endif
                    </div>
                    <div id="gallery" class="mt-20 product-dec-slider" style="text-align: center; border: 1px solid #f1f1f1">
                        @if(!empty($products_images))
                        @foreach($products_images as $image)
                        <a data-image="{{asset($image->path)}}" data-zoom-image="{{asset($image->path)}}">
                            <img src="{{asset($image->path)}}" alt="" style="height: 75px; width: auto;" style="border: 1px solid #efefef">
                        </a>
                        @endforeach
                        @endif

                    </div>
                </div>
            </div>

            <div class="col-lg-5 col-md-5">

                <div class="product-details-content pro-details-content-modify">
                    <br>

                    <h3 style="font-size: 24px; margin-bottom: 0!important">{{ucfirst(strtolower($product->products_name))}}</h3>
                    <h6 style="font-size: 14px; font-weight: bold; color: #999"> SKU - <span style="color: #999">{{$product->products_sku_code ?? ''}}</span></h6>
                    <div class="product-ratting-review">
                        <div class="product-ratting">
                            @for($i=0;$i<5;$i++)
                            @if($productReview-- >0)
                            <i class="la la-star"></i>
                            @else
                            <i class="la la-star-o"></i>
                            @endif
                            @endfor
                        </div>
                        <div class="product-review">
                            <span>{{$countReview}} @if($countReview==1){{'Review'}}@else{{'Reviews'}}@endif</span>
                        </div>
                    </div>

                    <div class="pro-details-price-wrap">
                        <div class="product-price">
                            <?php

                            $products_price = $products_price + ($product->tax_percent*$products_price)/100;
                            $products_price = number_format((float)$products_price, 2, '.', '');
                            ?>
                            <span> ₹{{$products_price}}</span>
                            <!-- <span class="old"></span> -->
                        </div>

                    </div>
                    <div >             
                      <span class="socialleft">
                          Share On : 
                          <a style="font-size: 30px" href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}" data-toggle="tooltip" target="_blank" title="Facebook" class="facebook">
                            <!--<i class=" ti-facebook"></i>-->
                            <img src="{{asset('/logo/facebook.png')}}" style="width: 30px; height: auto;padding-bottom: 5px;">
                        </a>

                        &nbsp;
                        <a style="font-size: 30px" class="instagram" href="https://www.instagram.com/home_glare/" data-toggle="tooltip" target="_blank" title="Instagram">
                           <!--<i class="ti-instagram"></i>-->
                           <img src="{{asset('/logo/instagram.png')}}" style="width: 20px; height: auto;padding-bottom: 5px;">
                       </a>
                       &nbsp;
                       <a style="font-size: 30px" class="instagram" href="http://www.twitter.com/intent/tweet?url={{url()->current()}}" data-toggle="tooltip" target="_blank" title="twitter">
                           <!--<i class="ti-twitter-alt"></i>-->
                           <img src="{{asset('/logo/twitter.webp')}}" style="width: 25px; height: auto;padding-bottom: 5px;">
                       </a>

                       &nbsp;
                       <a href="https://telegram.me/share/url?url={{url()->current()}}" style="padding-right: 1%" target="blank">
                          <img src="{{asset('/logo/telegram.png')}}" style="width: 25px; height: auto;padding-bottom: 10px;">&nbsp;&nbsp;
                      </a>
                      <a href="https://api.whatsapp.com/send?text={{url()->current()}}" target="blank">
                          <img src="{{asset('/logo/icon.png')}}" style="width: 25px; height: auto;padding-bottom: 10px;">
                      </a>  


                  </span>
              </div>
              @if(isset($product->product_brand))
              <div class="pro-details-price-wrap">
                <div class="product-price">
                   <b>Brands</b> : {{strtoupper($product->product_brand)}}
               </div>
           </div>
           @endif

           <div class="pro-details-price-wrap">
            <div class="product-price">
               <b>Availability</b> : <?php if($product->products_max_stock > '0'){echo 'In Stock';} else{echo 'Out of Stock';}?>
           </div>
       </div>


       @if(count($product_price) > 0)
       <div class="pro-details-price-wrap">
        <div class="product-price" style="padding-bottom: 20px">
            @foreach($product_price as $price)

            @if($price->products_sell_type == 'quantity_based')
            <br><span style="font-size: 18px;">Minimum Order Qty &nbsp;&nbsp;&nbsp;&nbsp;:  {{$price->products_quantity}} Pieces</span>
            <?php $product_quantity = $price->products_quantity; $fix_quantity = $price->products_quantity; ?>
            @endif
            @if($price->products_sell_type == 'carton')
            <br><span style="font-size: 18px;">Carton Qty &nbsp;: {{$price->products_quantity}} Pieces</span>
            @endif        

            @endforeach
        </div>
    </div>

    @endif




    <input type ="hidden" id="fixed_quantity" name="fixed_quantity" value="{{$fix_quantity ?? '1'}}">
    <input type ="hidden" id="product_tax" name="product_tax" value="{{$product->tax_percent ?? '0'}}">
    <input type ="hidden" id="send_tax" name="send_tax" value="{{$product->tax_percent ?? '0'}}">






    <div class="pro-details-quality" style="margin-top: 0px">
        <div class="row">

           <div class="col-sm-4">
               <a onclick="addToCart({{$product->products_id}},'sample' @if(!empty(Auth::user()->id)) {{','.Auth::user()->id}} @endif)" class="btn sample-product">Order Sample Products</a>
           </div>

           <div class="col-sm-4">
            <a class="show-moq show-active" onclick="showMOQ()" >Order Min Order Qty</a>
        </div>

        <div class="col-sm-4">
            <a class="show-carton" onclick="showCarton()" style="">Order Carton Qty</a>
        </div>

    </div>


<div class="row" style="padding: 0">
    <div class="col-sm-4 moq-qty" style="padding: 0">
        <label>Select Qty: </label>
        <div class="cart-plus-minus" id="moq-quantity">
            <input class="cart-plus-minus-box" type="text" name="qtybutton" value="{{$product_quantity}}" id="quantity{{$product->products_id}}" style="width: 100%">
        </div>
    </div>


    <div class="col-sm-3 moq-qty" style="padding: 0">
        <div class="pro-details-buy-now btn-hover btn-hover-radious" >

            <a class="add-cart" class="btn "  onclick="addToCart({{$product->products_id}},'normal','moq' @if(!empty(Auth::user()->id)) {{','.Auth::user()->id}} @endif)">Add To Cart</a>
        </div>
    </div>




    <div class="col-sm-4 carton-qty" style="padding: 0">
        <!-- <label>&nbsp;&nbsp;Carton</label> -->
        <div class="cart-plus-minus" id="cartoon-qty">
            <input class="cart-plus-minus-box" type="text" name="qtybutton1" value="1" id="carton{{$product->products_id}}" style="width: 100%">
        </div>
    </div>

    <div class="col-sm-3 carton-qty" style="padding: 0">
        <div class="pro-details-buy-now btn-hover btn-hover-radious" >

            <a style="background: #d20e14; color:white; margin-right: 0; margin-left: 10%; padding: 12px 9px; width: 100%" class="btn "  onclick="addToCart({{$product->products_id}},'normal','carton' @if(!empty(Auth::user()->id)) {{','.Auth::user()->id}} @endif)">Add To Cart</a>
        </div>
    </div>

    <div class="col-sm-3">
        <div class=" pro-details-buy-now btn-hover btn-hover-radious">
            <a onclick="BuyNow({{$product->products_id}})" style="background: #d20e14; color:white; width: 100%;padding: 12px 5px;" class="btn " title="Buy Now">
                <i class="la la-cart-plus"></i> Buy Now
            </a>
        </div>
    </div>
    <div class="col-sm-2">
        <div class=" pro-details-buy-now btn-hover btn-hover-radious" id="wishlist">
            <a title="Add To Wishlist" onclick="addToWishlist({{$product->products_id}})" style=" width: 100%">
                <i class="la la-heart-o"></i>
            </a>
        </div>
    </div>




</div>









</div>



<div class="pro-details-quality" style="margin-top: 15px" id="quantity-based">
    <div class="row">

        <div class="col-sm-8">
            <div class="dec-review-topbar nav">
                <a class="active" data-toggle="tab" href="#moqDiv" style="width: 47%; margin-right: 2.5%;font-size: 14px">Order Min Order Qty</a>
                @if(count($product_price) > 0 )
                @foreach($product_price as $pro)
                @if($pro->products_sell_type == 'carton')
                <a data-toggle="tab" href="#cartonDiv" style="width: 47%; margin-left: 2.5%; margin-right: 0;font-size: 14px">Order Carton Qty</a>
                @endif
                @endforeach
                @endif
            </div>
            <div class="tab-content dec-review-bottom"  style="padding: 10px 0 15px">
                <div id="moqDiv" class="tab-pane active" style="padding: 0 5px">

                    <p style="font-size: 14px; margin-bottom: 7px"> <b><span style="margin-left: 0px">{{$fix_quantity}} Pieces / Order</span> </b> </p>
                    <div class="row" style="padding: 0">
                        <div class="col-sm-6" style="padding: 0">
                            <!-- <label>&nbsp;&nbsp;MOQ</label> -->
                            <div class="cart-plus-minus" id="moq-quantity">
                                <input class="cart-plus-minus-box" type="text" name="qtybutton" value="{{$product_quantity}}" id="quantity{{$product->products_id}}" style="width: 100%">
                            </div>
                        </div>


                        <div class="col-sm-6" style="padding: 0">
                            <div class="pro-details-buy-now btn-hover btn-hover-radious" >

                                <a style="background: #d20e14; color:white; margin-right: 0; margin-left: 8%; padding: 12px 9px; width: 100%" class="btn "  onclick="addToCart({{$product->products_id}},'normal','moq' @if(!empty(Auth::user()->id)) {{','.Auth::user()->id}} @endif)">Add To Cart</a>
                            </div>
                        </div>
                    </div> 

                </div>
                <div id="cartonDiv" class="tab-pane"  style="padding: 0 5px">

                    @if(count($product_price) > 0)
                    @foreach($product_price as $price)

                    @if($price->products_sell_type == 'carton')
                    <p style="font-size: 14px; margin-bottom: 7px"> <b>&nbsp;&nbsp; <span style="margin-left: 5px"> {{$price->products_quantity}} Pieces / Carton </span> </b> </p>
                    @endif      

                    @endforeach
                    @endif


                    <div class="row" style="padding: 0">

                        <div class="col-sm-6" style="padding: 0">
                            <!-- <label>&nbsp;&nbsp;Carton</label> -->
                            <div class="cart-plus-minus" id="cartoon-qty">
                                <input class="cart-plus-minus-box" type="text" name="qtybutton1" value="1" id="carton{{$product->products_id}}" style="width: 100%">
                            </div>
                        </div>

                        <div class="col-sm-6" style="padding: 0">
                            <div class="pro-details-buy-now btn-hover btn-hover-radious" >

                                <a style="background: #d20e14; color:white; margin-right: 0; margin-left: 10%; padding: 12px 9px; width: 100%" class="btn "  onclick="addToCart({{$product->products_id}},'normal','carton' @if(!empty(Auth::user()->id)) {{','.Auth::user()->id}} @endif)">Add To Cart</a>
                            </div>
                        </div>
                    </div> 


                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <a onclick="addToCart({{$product->products_id}},'sample' @if(!empty(Auth::user()->id)) {{','.Auth::user()->id}} @endif)" class="btn " style="background: #d20e14; width: 100%; color:white;cursor: pointer; border-radius: 25px;font-size: 14px; padding:auto 0 !important">Order Sample Products</a>

            <div class="row" style="margin-top: 40px; text-align: center;">
                <div class="col-sm-6">
                    <div class=" pro-details-buy-now btn-hover btn-hover-radious">
                        <a onclick="BuyNow({{$product->products_id}})" style="background: #d20e14; color:white; width: 100%;padding: 12px 5px;" class="btn " title="Buy Now">
                            <i class="la la-cart-plus"></i> Buy Now
                        </a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class=" pro-details-buy-now btn-hover btn-hover-radious" id="wishlist">
                        <a title="Add To Wishlist" onclick="addToWishlist({{$product->products_id}})" style=" width: 100%">
                            <i class="la la-heart-o"></i>
                        </a>
                    </div>
                </div>
            </div>


        </div>


    </div>   
</div>

<div class="pro-details-compare-wishlist">
    <div id="messageBox" class="alert alert-success" style="display: none;">Product added successfully.</div>
</div>

</div>
</div>

<div class="col-sm-3 col-lg-3">
    <div class="description-review-wrapper pt-45" style="padding-bottom: 45px">
        <div class="container-fluid">
            <p>
                Specifications:
                <table class="table table-bordered" style="width: 100%">
                    @if(!empty($product->products_brand))

                    @php 
                    $brand = DB::table('products_brands')->where('id',$product->products_brand)->first();
                    @endphp

                    @if(!empty($brand))
                    <tr>
                        <th>Brand</th>
                        <td><a href="{{url('products/'.$brand->brand_slug)}}" style="color:red ">{{ucfirst($brand->brand_name ?? '')}}</a></td>
                    </tr>
                    @endif
                    @endif
                    @if(!empty($product->products_model))
                    <tr>
                        <th>Model</th>
                        <td>{{$product->products_model ?? ''}}</td>
                    </tr>
                    @endif
                    @if(!empty($products->products_color))
                    <tr>
                        <th>Color</th>
                        <td>{{$product->products_color ?? ''}}</td>
                    </tr>
                    @endif
                    @if(!empty($product->products_weight))
                    <tr>
                        <th>Weight</th>
                        <td>{{$product->products_weight ?? ''}} {{$product->products_weight_unit ?? ''}}</td>
                    </tr>
                    @endif
                </table>
            </p>
            <div class="row">
                <div class="ml-auto mr-auto col-lg-12">
                    <style>
                    .dec-review-topbar a{
                        padding: 5px 10px;
                        font-size: 18px;
                        color : #555;
                    }
                    .dec-review-topbar .active{
                        border: 1px solid #efefef;
                        background-color: #efefef;
                    }
                </style>
                <div class="dec-review-topbar nav">
                    <a class="active" data-toggle="tab" href="#des-details1" style="width: 50%">Description</a>
                    <!--  <a class="active" data-toggle="tab" href="#des-details2">Specification</a> -->
                    <a data-toggle="tab" href="#des-details3" style="width: 50%">Reviews</a>
                </div>
                <div class="tab-content dec-review-bottom"  style="padding: 0 15px">
                    <div id="des-details1" class="tab-pane active" style="padding: 0 5px">
                        <style>
                        .description-wrap ul { list-style-type: disc; }
                        .description-wrap ul li{ font-size: 16px; }
                    </style>
                    <div class="description-wrap">
                        <p>
                            <!--Features:-->
                            <br>

                            <?php echo $product->products_description; ?></p>
                            <br>

                        </div>
                    </div>
                    <div id="des-details3" class="tab-pane"  style="padding: 0 5px">
                        @if(!empty($reviews))
                        <?php $i=1; ?>
                        @foreach($reviews as $review)
                        <?php $rate = $review->reviews_rating;
                        $date = explode(' ',$review->created_at)[0];
                        ?>
                        <div class="dec-review-wrap" style="margin-bottom: 20px">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12">
                                    <div class="dec-review-img-wrap">
                                        <div class="dec-review-img" style="color: #000033">
                                            {{$i++}}
                                            {{-- <img src="/homeglare-new/images/product-details/review-1.png" alt="review"> --}}

                                        </div>
                                        <div class="dec-client-name">
                                            <h4>{{ucfirst($review->customers_name)}}</h4>
                                            <!--<h6>{{'Email'}}</h6>-->
                                            <div class="dec-client-rating">
                                                @for($j=0;$j<=4;$j++)
                                                @if($rate-- >0)
                                                <i class="la la-star"></i>
                                                @else
                                                <i class="la la-star-o"></i>
                                                @endif
                                                @endfor                    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12">
                                    <div class="dec-review-content">
                                        <p>
                                            @if($review->reviews_text !=null){{$review->reviews_text}}@endif</p>
                                            <div class="review-content-bottom">
                                                <div class="review-like">
                                                    {{-- <span><i class="la la-heart-o"></i> 24 Likes</span> --}}
                                                </div>
                                                <div class="review-date">
                                                    <span>{{$date}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endif

                            <div class="row">

                                <div class="review-sec"  style="width: 100% ; padding: 25px; border:1px solid #eee; ">
                                    <h3>Write a review</h3>
                                    <form id="form-review" action="{{route('give-product-rating')}}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <input type="text" name="product_id" hidden value="{{$product->producs_id}}">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Your Name </label>
                                            <input type="text" class="form-control" name="customers_name" value="{{ucfirst(Auth::user()->first_name ?? '')}} {{ucfirst(Auth::user()->last_name ?? '')}}" id="exampleFormControlInput1" placeholder="" required="required">
                                        </div>

                                      <!--   <div class="form-group">
                                            <label for="exampleFormControlInput1">Email address</label>
                                            <input type="email" class="form-control" name="email" id="exampleFormControlInput1" placeholder="name@example.com">
                                        </div>
                                    -->

                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Share your opinion</label>
                                        <textarea class="form-control" name="review" id="exampleFormControlTextarea1" rows="3" required="required"></textarea>
                                    </div>

                                    <div class="rarting">
                                      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

                                      <script>
                                        $(function() {
                                            $("div.star > s").on("click", function(e) {

                                                            // remove all active classes first, needed if user clicks multiple times
                                                            $(this).closest('div').find('.active').removeClass('active');

                                                            $(e.target).parentsUntil("div").addClass('active'); // all elements up from the clicked one excluding self
                                                            $(e.target).addClass('active');  // the element user has clicked on

                                                            var numStars = $(e.target).parentsUntil("div").length+1;
                                                            $('.show-result input').val(numStars );
                                                        });
                                        });
                                    </script>

                                    <div class="star"><s><s><s><s><s></s></s></s></s></s></div>
                                    <div class="show-result">
                                        <input type="hidden" name="reviews_rating" id="rating" value="0">
                                    </div>
                                </div>

                                <button type="submit" class="btn" style="background: #d20e14; color:white;">Submit</button>
                            </form>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
</div>
</div>
</div>




<div class="mb-50 mt-40">
    <div class="section-title-2 text-center mt-4 ">
       <h2>Related products</h2>
       <img src="/homeglare-new/images/icon-img/title-shape.png" alt="icon-img">
   </div>
   <div class="box-slider-active owl-carousel" style="padding: 0 50px">
    @if($related_products !=null)
    @foreach($related_products as $product)

    <?php
    $product_price = DB::table('products_price')->where('products_id',$product->products_id)->where('products_sell_type','quantity_based')->first();
    $products_price = $product_price->products_price ?? $product->products_price;
    $products_price = $products_price + ($product->tax_percent*$products_price)/100;
    $products_price = number_format((float)$products_price, 2, '.', '');
    ?>
    <div class="product-wrap product-border-1 product-img-zoom" style="margin-right: 10px">
      <div class="product-img">
       <a href="/product-detail/{{$product->products_slug}}"><img src="{{url('/')}}/{{$product->path}}" alt="product" class="img-response"></a>

   </div>
   <div class="product-content product-content-padding">
       <h5 style="margin-bottom:0!important"><a href="/product-detail/{{$product->products_slug}}">{{substr(ucfirst(strtolower($product->products_name)),0,35) }}</a></h5>
                     <h6 style="font-size: 10px; font-weight: bold; color: #999"> SKU - <span style="color: #999">{{$product->products_sku_code ?? ''}}</span></h6>
       <div class="price-addtocart">
        <div class="product-price">
         <span>₹{{$products_price}}</span>
     </div>
 </div>
</div>
<div class="product-action-2">
    <a data-toggle="modal" data-target="#exampleModal" title="Quick View" data-myid="{{$product->products_id}}"><i class="la la-search"></i></a>
    <a title="Add To Cart" onclick="addToCart({{$product->products_id}},'normal','moq')"><i class="la la-cart-plus"></i></a>
    <a title="Wishlist" onclick="addToWishlist({{$product->products_id}})"><i class="la la-heart-o"></i></a>
</div>
</div>
@endforeach
@endif

</div>
</div>

@endsection


@section ('script')
<script>

    function addToCart(id,type,category){
        var quantity1 = $('#quantity'+id).val();
        var quantity2 = $('#carton'+id).val();

    // alert(quantity2)

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var tax = $('#send_tax').val();

    $.ajax({
     url: '/add-to-cart',
     type: 'POST',
     data: {_token: CSRF_TOKEN, id: id, quantity_moq: quantity1, quantity_carton: quantity2, type:type, tax: tax, category: category},
     success: function (data) {
   // Swal(data.cartItem);
   $('#cartItem').html(data.cartItem)
   $('#cartItem2').html(data.cartItem)

   $("#messageBox").hide().slideDown();
   setTimeout(function(){
      $("#messageBox").hide();        
  }, 3000);

},
failure: function (data) {
   Swal(data.message);
}
});
}

function addToWishlist(id){
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  userId = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id;} else {echo 0;}?> ;

  if(userId == 0){
    Swal('Please Login First');
}
else{
// Swal(id);

$.ajax({
 url: '/add-to-wishlist',
 type: 'POST',
 data: {_token: CSRF_TOKEN, id: id, userId : userId},
 success: function (data) {
   // Swal(data.wishItem);
   $('#wishItem').html(data.wishItem);
   $('#wishItem2').html(data.wishItem);
},
failure: function (data) {
   Swal(data);
}
});
}
}


function BuyNow(id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    userId = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id;} else {echo 0;}?> ;
    var quantity = $('#quantity'+id).val();
    window.location.href = '/checkout/single/'+id+'-'+quantity;
}
</script>

@endsection