@extends('layouts/homeglare')

@section('content')
<br><br>
  <div class="container-fluid">
    <div class="card">
      <div class="card-header">
        Order Booked
      </div>
      <div class="card-body">
        <h5 class="card-title">Your Order Booked Successfully</h5>
        <!-- <p class="card-text">With supporting text below as a natural lead-in to additional content.</p> -->
        <a href="{{url('/')}}" class="btn btn-primary">Continue Shoping</a>
      </div>
    </div>

  </div>
<br><br>

@endsection
