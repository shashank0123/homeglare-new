@extends('layouts/homeglare')

@section('content')

<div class="about-us-area pt-50 pb-90">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				
				<!-- <h2>Welcome To <span>HOMEGLARE</span></h2> -->

				<div class="short_desc">
					
					<h2><span>Privacy Policy</span></h2><br>

					<h4>Last updated January 29, 2020</h4>

					<p>Thank you for choosing to be part of our community at Homeglare E-commerce Pvt Ltd, doing business as Homeglare (“<b>Homeglare</b>”, “<b>we</b>”, “<b>us</b>”, or “<b>our</b>”). We are committed to protecting your personal information and your right to privacy. If you have any questions or concerns about our policy, or our practices with regards to your personal information, please contact us at <a href="mailto:support@homeglare.com">support@homeglare.com</a>.</p>

					<p>When you visit our website <a href="http://www.homeglare.com/" target='blank'>http://www.homeglare.com/</a>, mobile application, and use our services, you trust us with your personal information. We take your privacy very seriously. In this privacy policy, we seek to explain to you in the clearest way possible what information we collect, how we use it and what rights you have in relation to it. We hope you take some time to read through it carefully, as it is important. If there are any terms in this privacy policy that you do not agree with, please discontinue use of our Sites or Apps and our services.</p>

					<p>This privacy policy applies to all information collected through our website (such as http://www.homeglare.com/), mobile application, ("<b>Apps</b>"), and/or any related services, sales, marketing or events (we refer to them collectively in this privacy policy as the <b>"Services"</b>).</p>

					<p><b>Please read this privacy policy carefully as it will help you make informed decisions about sharing your personal information with us.</b></p>

					<h5>TABLE OF CONTENTS</h5>
					<ul>
						<li><a href="#1">1. WHAT INFORMATION DO WE COLLECT?</a></li>
						<li><a href="#2">2. HOW DO WE USE YOUR INFORMATION?</a></li>
						<li><a href="#3">3. WILL YOUR INFORMATION BE SHARED WITH ANYONE?</a></li>
						<li><a href="#4">4. DO WE USE COOKIES AND OTHER TRACKING TECHNOLOGIES?</a></li>
						<li><a href="#5">5. HOW DO WE HANDLE YOUR SOCIAL LOGINS?</a></li>
						<li><a href="#6">6. HOW LONG DO WE KEEP YOUR INFORMATION?</a></li>
						<li><a href="#7">7. HOW DO WE KEEP YOUR INFORMATION SAFE?</a></li>
						<li><a href="#8">8. DO WE COLLECT INFORMATION FROM MINORS?</a></li>
						<li><a href="#9">9. WHAT ARE YOUR PRIVACY RIGHTS?</a></li>
						<li><a href="#10">10. CONTROLS FOR DO-NOT-TRACK FEATURES</a></li>
						<li><a href="#11">11. DO CALIFORNIA RESIDENTS HAVE SPECIFIC PRIVACY RIGHTS?</a></li>
						<li><a href="#12">12. DO WE MAKE UPDATES TO THIS POLICY?</a></li>
						<li><a href="#13">13. HOW CAN YOU CONTACT US ABOUT THIS POLICY?</a></li>

						<div id="1">
							<h5>1. WHAT INFORMATION DO WE COLLECT?</h5>

							<p><b>Personal information you disclose to us</b></p>

							<p><i><b>In Short:</b>  We collect personal information that you provide to us such as name, address, contact information, passwords and security data, payment information, and social media login data.</i></p>

							<p>We collect personal information that you voluntarily provide to us when registering at the Services or Apps, expressing an interest in obtaining information about us or our products and services, when participating in activities on the Services or Apps or otherwise contacting us.</p>

							<p>The personal information that we collect depends on the context of your interactions with us and the Services or Apps, the choices you make and the products and features you use. The personal information we collect can include the following:
							</p>

							<p><b>Publicly Available Personal Information.</b> We collect first name, maiden name, last name, and nickname; phone numbers; email addresses; business email; business phone number; social media; and other similar data. </p>

							<p><b>Personal Information Provided by You.</b> We collect app usage; data collected from surveys; CV and other job application data such as background checks; purchase history; and other similar data.</p>

							<p><b>Credentials.</b> We collect passwords, password hints, and similar security information used for authentication and account access.</p>

							<p><b>Payment Data.</b> We collect data necessary to process your payment if you make purchases, such as your payment instrument number (such as a credit card number), and the security code associated with your payment instrument. </p>

							<p><b>Social Media Login Data.</b> We provide you with the option to register using social media account details, like your Facebook, Twitter or other social media account. If you choose to register in this way, we will collect the Information described in the section called "<u>HOW DO WE HANDLE YOUR SOCIAL LOGINS</u>" below.</p>

							<p>All personal information that you provide to us must be true, complete and accurate, and you must notify us of any changes to such personal information.</p>

							<h5>Information collected through our Apps</h5>

							<p><i><b>In Short:</b>   We may collect information regarding your geo-location, mobile device, push notifications, when you use our apps.</i></p>

							<p>If you use our Apps, we may also collect the following information:</p>

							<ul>
								<li>Geo-Location Information. We may request access or permission to and track location-based information from your mobile device, either continuously or while you are using our mobile application, to provide location-based services. If you wish to change our access or permissions, you may do so in your device's settings.</li>

								<li>Mobile Device Access. We may request access or permission to certain features from your mobile device, including your mobile device's camera, contacts, sensors, sms messages, social media accounts, microphone, and other features. If you wish to change our access or permissions, you may do so in your device's settings.</li>

								<li>Push Notifications. We may request to send you push notifications regarding your account or the mobile application. If you wish to opt-out from receiving these types of communications, you may turn them off in your device's settings.</li>
							</ul>
						</div>

						<div id="2">
							<h5>2. HOW DO WE USE YOUR INFORMATION?</h5>

							<p><i><b>In Short:</b>  We process your information for purposes based on legitimate business interests, the fulfillment of our contract with you, compliance with our legal obligations, and/or your consent.</i>

								<p>We use personal information collected via our Services or Apps for a variety of business purposes described below. We process your personal information for these purposes in reliance on our legitimate business interests, in order to enter into or perform a contract with you, with your consent, and/or for compliance with our legal obligations. We indicate the specific processing grounds we rely on next to each purpose listed below.</p>

								<p>We use the information we collect or receive:</p>

								<ul>
									<li><b>To facilitate account creation and logon process.</b> If you choose to link your account with us to a third party account (such as your Google or Facebook account), we use the information you allowed us to collect from those third parties to facilitate account creation and logon process for the performance of the contract. See the section below headed "<u>HOW DO WE HANDLE YOUR SOCIAL LOGINS</u>" for further information.</li>

									<li><b>To send you marketing and promotional communications.</b> We and/or our third party marketing partners may use the personal information you send to us for our marketing purposes, if this is in accordance with your marketing preferences. You can opt-out of our marketing emails at any time (see the "<u>WHAT ARE YOUR PRIVACY RIGHTS</u>" below).</li>

									<li><b>Fulfill and manage your orders.</b> We may use your information to fulfill and manage your orders, payments, returns, and exchanges made through the Services or Apps.</li>

									<li><b>To post testimonials.</b> We post testimonials on our Services or Apps that may contain personal information. Prior to posting a testimonial, we will obtain your consent to use your name and testimonial. If you wish to update, or delete your testimonial, please contact us at <a href="user@homeglare.com" target="blank">user@homeglare.com</a> and be sure to include your name, testimonial location, and contact information.</li>

									<li><b>Deliver targeted advertising to you.</b> We may use your information to develop and display content and advertising (and work with third parties who do so) tailored to your interests and/or location and to measure its effectiveness.</li>

									<li><b>Administer prize draws and competitions.</b> We may use your information to administer prize draws and competitions when you elect to participate in competitions.</li>

									<li><b>Request Feedback.</b> We may use your information to request feedback and to contact you about your use of our Services or Apps.</li>

									<li><b>To protect our Services.</b> We may use your information as part of our efforts to keep our Services or Apps safe and secure (for example, for fraud monitoring and prevention).</li>

									<li><b>To enable user-to-user communications.</b> We may use your information in order to enable user-to-user communications with each user's consent.</li>

									<li>To enforce our terms, conditions and policies for Business Purposes, Legal Reasons and Contractual.</b>

										<li><b>To respond to legal requests and prevent harm.</b> If we receive a subpoena or other legal request, we may need to inspect the data we hold to determine how to respond.</li>

										<li><b>To manage user accounts.</b> We may use your information for the purposes of managing our account and keeping it in working order.</li>
										
										<li><b>To deliver services to the user.</b> We may use your information to provide you with the requested service.</li>

										<li><b>To respond to user inquiries/offer support to users.</b> We may use your information to respond to your inquiries and solve any potential issues you might have with the use of our Services.</li>

										<li><b>For other Business Purposes.</b> We may use your information for other Business Purposes, such as data analysis, identifying usage trends, determining the effectiveness of our promotional campaigns and to evaluate and improve our Services or Apps, products, marketing and your experience. We may use and store this information in aggregated and anonymized form so that it is not associated with individual end users and does not include personal information. We will not use identifiable personal information without your consent.</li>
									</ul>

								</div>

								<div id="3">
									<h5>3. WILL YOUR INFORMATION BE SHARED WITH ANYONE?</h5>

									<p><i><b>In Short:</b>  We only share information with your consent, to comply with laws, to provide you with services, to protect your rights, or to fulfill business obligations.</i></p>

									<p>We may process or share data based on the following legal basis:</p>
									<ul>
										<li><b>Consent:</b> We may process your data if you have given us specific consent to use your personal information in a specific purpose.</li>


										<li><b>Legitimate Interests:</b> We may process your data when it is reasonably necessary to achieve our legitimate business interests.</li>


										<li><b>Performance of a Contract:</b> Where we have entered into a contract with you, we may process your personal information to fulfill the terms of our contract.</li>


										<li><b>Legal Obligations:</b> We may disclose your information where we are legally required to do so in order to comply with applicable law, governmental requests, a judicial proceeding, court order, or legal process, such as in response to a court order or a subpoena (including in response to public authorities to meet national security or law enforcement requirements).</li>


										<li><b>Vital Interests:</b> We may disclose your information where we believe it is necessary to investigate, prevent, or take action regarding potential violations of our policies, suspected fraud, situations involving potential threats to the safety of any person and illegal activities, or as evidence in litigation in which we are involved.</li>
									</ul>

									<p>More specifically, we may need to process your data or share your personal information in the following situations:</p>

									<ul>
										<li><b>Vendors, Consultants and Other Third-Party Service Providers.</b> We may share your data with third party vendors, service providers, contractors or agents who perform services for us or on our behalf and require access to such information to do that work. Examples include: payment processing, data analysis, email delivery, hosting services, customer service and marketing efforts. We may allow selected third parties to use tracking technology on the Services or Apps, which will enable them to collect data about how you interact with the Services or Apps over time. This information may be used to, among other things, analyze and track data, determine the popularity of certain content and better understand online activity. Unless described in this Policy, we do not share, sell, rent or trade any of your information with third parties for their promotional purposes.</li>

										<li><b>Business Transfers.</b> We may share or transfer your information in connection with, or during negotiations of, any merger, sale of company assets, financing, or acquisition of all or a portion of our business to another company.</li>

										<li><b>Third-Party Advertisers.</b> We may use third-party advertising companies to serve ads when you visit the Services or Apps. These companies may use information about your visits to our Website(s) and other websites that are contained in web cookies and other tracking technologies in order to provide advertisements about goods and services of interest to you.</li>

										<li><b>Offer Wall.</b> Our Apps may display a third-party hosted “offer wall.” Such an offer wall allows third-party advertisers to offer virtual currency, gifts, or other items to users in return for acceptance and completion of an advertisement offer. Such an offer wall may appear in our mobile application and be displayed to you based on certain data, such as your geographic area or demographic information. When you click on an offer wall, you will leave our mobile application. A unique identifier, such as your user ID, will be shared with the offer wall provider in order to prevent fraud and properly credit your account.</li>
									</ul>
								</div>

								<div id="4">
									<h5>4. DO WE USE COOKIES AND OTHER TRACKING TECHNOLOGIES?</h5>
									<p><i><b>In Short:</b>  We may use cookies and other tracking technologies to collect and store your information.</i></p>

									<p>We may use cookies and similar tracking technologies (like web beacons and pixels) to access or store information. Specific information about how we use such technologies and how you can refuse certain cookies is set out in our Cookie Policy.</p>
								</div>

								<div id="5">
									<h5>5. HOW DO WE HANDLE YOUR SOCIAL LOGINS?</h5>

									<p><i><b>In Short:</b>  If you choose to register or log in to our services using a social media account, we may have access to certain information about you.</i></p>

									<p>Our Services or Apps offer you the ability to register and login using your third party social media account details (like your Facebook or Twitter logins). Where you choose to do this, we will receive certain profile information about you from your social media provider. The profile Information we receive may vary depending on the social media provider concerned, but will often include your name, e-mail address, friends list, profile picture as well as other information you choose to make public.</p>

									<p>We will use the information we receive only for the purposes that are described in this privacy policy or that are otherwise made clear to you on the Services or Apps. Please note that we do not control, and are not responsible for, other uses of your personal information by your third party social media provider. We recommend that you review their privacy policy to understand how they collect, use and share your personal information, and how you can set your privacy preferences on their sites and apps.</p>
								</div>

								<div id="6">
									<h5>6. HOW LONG DO WE KEEP YOUR INFORMATION?</h5>
									<p><i><b>In Short:</b>  We keep your information for as long as necessary to fulfill the purposes outlined in this privacy policy unless otherwise required by law.</i></p>

									<p>We will only keep your personal information for as long as it is necessary for the purposes set out in this privacy policy, unless a longer retention period is required or permitted by law (such as tax, accounting or other legal requirements). No purpose in this policy will require us keeping your personal information for longer than 6 months past the termination of the user's account.</p>

									<p>When we have no ongoing legitimate business need to process your personal information, we will either delete or anonymize it, or, if this is not possible (for example, because your personal information has been stored in backup archives), then we will securely store your personal information and isolate it from any further processing until deletion is possible.</p>
								</div>

								<div id="7">
									<h5>7. HOW DO WE KEEP YOUR INFORMATION SAFE?</h5>

									<p><i><b>In Short:</b>  We aim to protect your personal information through a system of organizational and technical security measures.</i></p>

									<p>We have implemented appropriate technical and organizational security measures designed to protect the security of any personal information we process. However, please also remember that we cannot guarantee that the internet itself is 100% secure. Although we will do our best to protect your personal information, transmission of personal information to and from our Services or Apps is at your own risk. You should only access the services within a secure environment.</p>
								</div>

								<div id="8">
									<h5>8. DO WE COLLECT INFORMATION FROM MINORS?</h5>

									<p><i><b>In Short:</b>  We do not knowingly collect data from or market to children under 18 years of age.</i></p>

									<p>We do not knowingly solicit data from or market to children under 18 years of age. By using the Services or Apps, you represent that you are at least 18 or that you are the parent or guardian of such a minor and consent to such minor dependent’s use of the Services or Apps. If we learn that personal information from users less than 18 years of age has been collected, we will deactivate the account and take reasonable measures to promptly delete such data from our records. If you become aware of any data we have collected from children under age 18, please contact us at user@homeglare.com.</p>


									<p><b>Account Information</b></p>
									<p>If you would at any time like to review or change the information in your account or terminate your account, you can:</p>
									<ul>
										<li><p>Contact us using the contact information provided.</p>

											<p>Upon your request to terminate your account, we will deactivate or delete your account and information from our active databases. However, some information may be retained in our files to prevent fraud, troubleshoot problems, assist with any investigations, enforce our Terms of Use and/or comply with legal requirements.</p>

											<p><b><u>Cookies and similar technologies:</u></b> Most Web browsers are set to accept cookies by default. If you prefer, you can usually choose to set your browser to remove cookies and to reject cookies. If you choose to remove cookies or reject cookies, this could affect certain features or services of our Services or Apps. To opt-out of interest-based advertising by advertisers on our Services or Apps visit http://www.aboutads.info/choices/.</p>

											<p><b><u>Opting out of email marketing:</u></b> You can unsubscribe from our marketing email list at any time by clicking on the unsubscribe link in the emails that we send or by contacting us using the details provided below. You will then be removed from the marketing email list – however, we will still need to send you service-related emails that are necessary for the administration and use of your account. To otherwise opt-out, you may:</p>
										</li>

										<li>Note your preferences when you register an account with the site.</li>
									</ul>
								</div>

								<div id="10">
									<h5>10. CONTROLS FOR DO-NOT-TRACK FEATURES</h5>
									<p>Most web browsers and some mobile operating systems and mobile applications include a Do-Not-Track (“DNT”) feature or setting you can activate to signal your privacy preference not to have data about your online browsing activities monitored and collected. No uniform technology standard for recognizing and implementing DNT signals has been finalized. As such, we do not currently respond to DNT browser signals or any other mechanism that automatically communicates your choice not to be tracked online. If a standard for online tracking is adopted that we must follow in the future, we will inform you about that practice in a revised version of this privacy policy.</p>
								</div>

								<div id="12">
									<h5>12. DO WE MAKE UPDATES TO THIS POLICY?</h5>
									<p><i><b>In Short:</b>  Yes, we will update this policy as necessary to stay compliant with relevant laws.</i></p>
									<p>We may update this privacy policy from time to time. The updated version will be indicated by an updated “Revised” date and the updated version will be effective as soon as it is accessible. If we make material changes to this privacy policy, we may notify you either by prominently posting a notice of such changes or by directly sending you a notification. We encourage you to review this privacy policy frequently to be informed of how we are protecting your information.</p>
								</div>

								<div id="13">
									<h5>13. HOW CAN YOU CONTACT US ABOUT THIS POLICY?</h5>
									<p>If you have questions or comments about this policy, you may email us at user@homeglare.com or by post to:</p>
									<p>Homeglare E-commerce Pvt Ltd <br>
										Khasra No 915, Nala Road<br>
										Rithala<br>
										NEW DELHI, Next to Guleria House 110085<br>
									India</p>

									<h5>HOW CAN YOU REVIEW, UPDATE, OR DELETE THE DATA WE COLLECT FROM YOU?</h5>
									<p>Based on the laws of some countries, you may have the right to request access to the personal information we collect from you, change that information, or delete it in some circumstances. To request to review, update, or delete your personal information, please submit a request form by clicking here. We will respond to your request within 30 days.</p>

								</div>
								
							</div>
						</div>
					</div>
				</div>

				@endsection
