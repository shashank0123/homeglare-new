<?php
use App\Models\Core\Products;
use App\Models\Core\Reviews;
$bill = 0;
$i=0;
$cnt = 0;
$product = "";
$price = 0;
$stock = 0;
$tax = 0;
$total = 0;
$total_price = 0;
$fix_quant = 0;
$cartonqty = array();
$prices = array();
$taxs = array();
?>
@extends('layouts.homeglare')

@section('content')

<!-- <div class="breadcrumb-area bg-img" style="background-image:url(/homeglare-new/images/bg/breadcrumb.jpg);"> -->
    <div class="bg-img" >
        <div class="container">
            <div class="breadcrumb-content text-center">
                <h2>cart page</h2>
                <ul>
                    <li>
                        <a href="/">Home</a>
                    </li>
                    <li class="active">cart </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="cart-main-area pt-85 pb-90">
        <div class="container">
            <h3 class="cart-page-title">Your cart items</h3>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <form action="#">
                        @if(session()->get('cart') != null)

                        <?php

                        $ids = array();
                        $cat_id = array();
                        $quantities = array();
                        $category = array();

                        $cartonqty[$i] = 1;


                        foreach(session()->get('cart') as $data)
                        {
                            $ids[$i] = $data->product_id;
                            $quantities[$i] = $data->quantity;
                            $taxs[$i] = $data->tax;
                            $category[$i] = $data->category;
                            $cartonqty[$i] = 1;


                            $product_id = explode('_',$data->product_id)[0];


                            if($data->type == 'sample'){

                                $product = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','sample')->first();
                                $price = $product->products_price ?? '';
                                $fix_quant = 1;
                            }
                            elseif($data->type == 'normal'){
                                $product = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','quantity_based')->first();
                                if(isset($product)){
                                    $fix_quant = $product->products_quantity;
                                    if($data->quantity >= $product->products_quantity){
                                        $price = $product->products_price;
                                    }
                                    else{
                                        $product = Products::where('products_id',$product_id)->first();
                                        $price = $product->products_price ?? '';
                                    }
                                }
                            }

                            elseif($data->type == 'carton'){

                                $product = DB::table('products_price')->where('products_id',$product_id)->where('products_sell_type','carton')->first();
                                if(isset($product)){
                                    $fix_quant = $product->products_quantity;
                                    if($data->quantity >= $product->products_quantity){
                                        $price = $product->products_price;
                                        $cartonqty[$i] = $fix_quant;
                                    }
                                    else{
                                        $product = Products::where('products_id',$product_id)->first();
                                        $price = $product->products_price ?? '';
                                    }
                                }
                            }


                            else{
                                $product = Products::where('products_id',$product_id)->first();
                                $price = $product->products_price ?? '';
                                $fix_quant = 1;
                            }
                            
                            ?>


                            <input type='hidden' id="fix_quant{{$ids[$i]}}" value="{{$fix_quant}}" name="fix_quant{{$ids[$i]}}">


                            <?php
                            $prices[$i] = $data->unit_price;

                            $i++;
                        }
                        ?>
                        <div class="table-content table-responsive cart-table-content">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Product Name</th>
                                        <!-- <th>Type</th> -->
                                        <th>Unit Price</th>
                                        <th>Qty</th>
                                        <!-- <th>Total QTY</th> -->
                                        <th>Subtotal</th>
                                        <th>GST</th>
                                        <th>action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @for($j=0 ; $j<$i ; $j++ )
                                    <?php

                                    $cnt++;
                                    $pro_id = explode('_',$ids[$j])[0];
                                    $type = explode('_',$ids[$j])[1];
                                // $product = Product::where('id',$ids[$j])->first();
                                    $product = Products::leftjoin('products_description','products_description.products_id','products.products_id')
                                    ->leftjoin('images','images.id','products.products_image')
                                    ->leftjoin('image_categories','image_categories.image_id','images.id')
                                    ->where('products.products_id',$pro_id)
                                    ->where('image_categories.image_type','ACTUAL')   
                                    ->first();

                                    $stock = $product->products_quantity;                                        

                                    ?>


                                    <input type='hidden' id="stock{{$ids[$j]}}" value="{{$stock}}" name="stock{{$ids[$j]}}">
                                    <input type='hidden' id="tax{{$ids[$j]}}" value="{{$product->tax_percent}}" name="tax{{$ids[$j]}}">
                                    <input type='hidden' id="unit_price{{$ids[$j]}}" value="{{$prices[$j]}}" name="unit_price{{$ids[$j]}}">
                                    <input type='hidden' id="category{{$ids[$j]}}" value="{{$category[$j]}}" name="category{{$ids[$j]}}">
                                    <input type='hidden' id="cartonqty{{$ids[$j]}}" value="{{$cartonqty[$j]}}" name="cartonqty{{$ids[$j]}}">

                                    <?php

                                    if($product != null)
                                    {
                                        $cat_id[$j] = $product->category_id;
                                    }
                                    else
                                    {
                                        $cat_id[$j] = 0;
                                    }

                                    ?>
                                    <tr id="cart{{$ids[$j]}}">
                                        <td class="product-thumbnail">
                                            <a href="/product-detail/{{$product->products_slug}}"><img src="{{url('/')}}/{{$product->path}}" alt="" style="width: 120px; height: 120px"></a>
                                        </td>


                                        <td class="product-name"><a href="/product-detail/{{$product->products_slug}}">{{strtoupper($product->products_name)}}
                                            <br>
                                            <h6 style="font-size: 10px; font-weight: bold; color: #999"> SKU - <span style="color: #999">{{$product->products_sku_code ?? ''}}</span></h6>
                                            
                                            @if($product->products_weight>0)<b>Weight</b> : {{$product->products_weight}}gm/Piece @endif
                                            <br>
                                            @if($type == 'sample'){{'(Sample)'}}<br>@endif
                                            @if($category[$j]=='carton') <b>1 Carton = {{$cartonqty[$j]}} Pieces</b> @endif</a></td>

                                        <td class="product-price-cart"><span class="amount" id="unitprice{{$ids[$j]}}">₹{{$prices[$j]}}
                                        </span><br>GST({{$product->tax_percent}})%</td>
                                        <td class="product-quantity" style="width: 350px">
                                            <div class="">

                                                <i class="ti-minus" style="border: 1px solid #afafaf; padding: 5px 5px 8px; cursor: pointer; @if($type == 'sample') display: none @endif" onclick="decrement('{{$ids[$j]}}');" ></i>

                                                <input style="width: 50px; text-align: center;" type="text" name="quantity" id="quantity{{$ids[$j]}}" value="@if($category[$j]=='carton'){{$quantities[$j]/$cartonqty[$j]}}@else{{$quantities[$j]}}@endif" disabled="disabled ">

                                                <i class="ti-plus" style="border: 1px solid #afafaf; padding: 5px 5px 7px; cursor: pointer; @if($type == 'sample') display: none @endif"  onclick="increment('{{$ids[$j]}}')"></i>
                                            </div>
                                            <div style="text-align: center;">@if($category[$j]!='sample'){{ucfirst($category[$j])}}@endif</div>

                                        </td>
                                        <!-- <td class="product-subtotal" id="totalqty{{$ids[$j]}}">{{$quantities[$j]}}</td> -->
                                        <?php
                                        
                                        $total = $quantities[$j]*$prices[$j];
                                        $tax = $tax+ ($product->tax_percent*$total)/100;
                                        $bill = $bill +  $total + $tax;
                                        $total_price = $total_price + $total;

                                        ?>
                                        <td class="product-subtotal" id="amount{{$ids[$j]}}">₹{{$total}}</td>
                                        <td class="product-subtotal" id="tax_applic{{$ids[$j]}}">₹{{($product->tax_percent*$total)/100}}</td>
                                        <td class="product-remove">
                                            <a onclick="deleteCartProduct('{{$ids[$j]}}')"><i class="la la-close"></i></a>
                                        </td>
                                    </tr>
                                    @endfor
                                </tbody>
                            </table>
                        </div>
                        @endif

                    </form>
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="cart-shiping-update-wrapper">
                                <div class="cart-shiping-update">
                                    <a href="/">Continue Shopping</a>
                                </div>
                                {{-- <div class="cart-clear">
                                    <button>Update Shopping Cart</button>
                                    <a href="#">Clear Shopping Cart</a>
                                </div> --}}
                            </div>
                            {{-- <div class="cart-tax">
                                <div class="title-wrap">
                                    <h4 class="cart-bottom-title section-bg-gray">Estimate Shipping And Tax</h4>
                                </div>
                                <div class="tax-wrapper">
                                    <p>Enter your destination to get a shipping estimate.</p>
                                    <div class="tax-select-wrapper">
                                        <div class="tax-select">
                                            <label>
                                                * Country
                                            </label>
                                            <select class="email s-email s-wid">
                                                <option>India</option>
                                                <option>Nepal</option>
                                                <option>Åland Islands</option>
                                                <option>Afghanistan</option>
                                                <option>Belgium</option>
                                            </select>
                                        </div>
                                        <div class="tax-select">
                                            <label>
                                                * Region / State
                                            </label>
                                            <select class="email s-email s-wid">
                                                <option>Delhi</option>
                                                <option>Punjab</option>
                                                <option>U.P</option>
                                                <option>Bihar</option>
                                                <option>Himachal Pradesh</option>
                                            </select>
                                        </div>
                                        <div class="tax-select">
                                            <label>
                                                * Zip/Postal Code
                                            </label>
                                            <input type="text">
                                        </div>
                                        <button class="cart-btn-2" type="submit">Get A Quote</button>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                        {{-- <div class="col-lg- col-md-6"> --}}
                            {{-- <div class="discount-code-wrapper">
                                <div class="title-wrap">
                                    <h4 class="cart-bottom-title section-bg-gray">Use Coupon Code</h4>
                                </div>
                                <div class="discount-code">
                                    <p>Enter your coupon code if you have one.</p>
                                    <form>
                                        <input type="text" required="" name="name">
                                        <button class="cart-btn-2" type="submit">Apply Coupon</button>
                                    </form>
                                </div>
                            </div> --}}
                        {{-- </div> --}}
                        <div class="col-lg-6 col-md-12">
                            <div class="grand-totall">
                                <div class="title-wrap">
                                    <h4 class="cart-bottom-title section-bg-gary-cart">Cart Total</h4>
                                </div>
                                <h5>Sub Total<span id="total_price">₹{{$total_price}}</span></h5>
                                <h5>GST<span id="total_tax">₹{{$tax}}</span></h5>
                                <hr>
                                <h5>Total <span id="decimal_total">₹{{($total_price+$tax)}}</span></h5>
                                {{-- <div class="total-shipping">
                                    <h5>Total shipping</h5>
                                    <ul>
                                        <li><input type="checkbox"> Standard <span>₹20.00</span></li>
                                        <li><input type="checkbox"> Express <span>₹30.00</span></li>
                                    </ul>
                                </div> --}}
                                
                                 

                                <h4 class="grand-totall-title">Grand Total <span id="grand_total">₹{{ceil($total_price+$tax)}}</span></h4>
                                <?php
                                $total = $total_price+$tax;
                                ?>

                                @if($total < 1000 && (!empty(session()->get('category') && session()->get('category') != 'sample')))
                                <a class="black-color"  onclick="Swal('Book orders of min Rs. 1000 to continue')">Proceed to Checkout</a>
                                
                                @else
                                <a href="<?php if(!empty(Auth::user()->id)){echo '/checkout';} else{ echo '/customer/login'; }?>">Proceed to Checkout</a>
                                @endif
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<a data-toggle="modal" id="show-alertcart" data-target="#exampleModal2" href="#" style="display: none;">Order Quantity Exceeds</a>

    @endsection


    @section('script')

    <script>


        // var count=<?php echo session()->get('count'); ?>;

        function deleteCartProduct(id){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    // count--;

    $.ajax({
        /* the route pointing to the post function */
        url: '/cart/delete-product/'+id,
        type: 'POST',
        datatype: 'JSON',
        /* send the csrf-token and the input to the controller */
        data: {_token: CSRF_TOKEN, id: id},
        success: function (data) {
            $('#cart'+id).hide();
            $('#cartItem').html(data.cartItem);
            $("#total_price").empty();
            $("#total_price").append("₹" + data.total);
            $("#total_tax").html("₹" + data.total_tax);
           $("#decimal_total").html("₹" + data.decimal_bill);
            $("#grand_total").html("₹" + data.bill);

            if(data.bill <= 0){
              $("#checkoutLink").attr("href", "/");
              $("#checkoutLink").html('Continue Shopping');
          }
      }
  });
}


function decrement(id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    
    var action = 'minus' ;
    var quantity = $('#quantity'+id).val();
    var tax = $('#tax'+id).val();
    var unit_price = $('#unit_price'+id).val();
    var fix_quant = $('#fix_quant'+id).val();
    var category = $('#category'+id).val();
    var cartonqty = $('#cartonqty'+id).val();
    
    // alert(quantity + " " +fix_quant)
    if(parseInt(quantity)>(parseInt(fix_quant)/parseInt(cartonqty))){
        // alert('yes')
        quantity = parseInt(quantity)-(parseInt(fix_quant)/parseInt(cartonqty));
        // alert(quantity);

    // quantity = quantity-fix_quant;

    $('#quantity'+id).val(quantity);
    $.ajax({
       url: '/update-cart',
       type: 'POST',
       data: {_token: CSRF_TOKEN, id: id, action: action, tax: tax, fix_quant: fix_quant, unit_price: unit_price},
       success: function (data) {

           $("#total_price").empty();
           $("#total_price").append("₹" + (data.total_price));
           $("#total_tax").html("₹" + data.total_tax);
           $("#grand_total").html("₹" + data.bill);
           $("#decimal_total").html("₹" + data.decimal_bill);
           $("#amount"+id).html("₹" + data.total);
           $("#tax_applic"+id).html("₹" + data.product_tax);
           $("#unitprice"+id).html("₹" + data.unitprice);


       },
       failure: function (data) {
         alert(data.message);
     }
 });
}
}   


function increment(id){

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    
    var quantity = $('#quantity'+id).val();
    // alert(quantity)
    var action = 'plus' ;
    var tax = $('#tax'+id).val();
    var unit_price = $('#unit_price'+id).val();
    var fix_quant = $('#fix_quant'+id).val();
    var category = $('#category'+id).val();
    var cartonqty = $('#cartonqty'+id).val();
    var stock = $('#stock'+id).val();

    
    quantity = parseInt(quantity)+(parseInt(fix_quant)/parseInt(cartonqty));

        $.ajax({
         url: '/update-cart',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, action:action, tax: tax, fix_quant: fix_quant, unit_price: unit_price},
         success: function (data) {
            
            if(data.status == 0){
        Swal('Order quantity exceeds the stock limit. You cannot add more quantity of this product in cart.');
    }
    else{
           $("#total_price").empty();
           $("#total_price").append("₹" + (data.total_price));
           $("#total_tax").html("₹" + data.total_tax);
           $("#decimal_total").html("₹" + data.decimal_bill);
           $("#grand_total").html("₹" + data.bill);
           $("#amount"+id).html("₹" + data.total );
           $("#tax_applic"+id).html("₹" + data.product_tax );
           $("#unitprice"+id).html("₹" + data.unitprice);
        $('#quantity'+id).val(quantity);
    }
       },
       failure: function (data) {
           alert('Something went wrong');
       }
   });
    }


</script>

@endsection