@extends('layouts/homeglare')

<?php
$name="";
$email="";
if(!empty(Auth::user())){
    $name = Auth::user()->first_name ?? ''." ".Auth::user()->last_name;
    $email = Auth::user()->email ?? '';
}

?>

@section('content')
<style>
label { color: #000033 !important; }
.star s:hover,
.star s.active { color: #000033; }
.star-rtl s:hover,
.star-rtl s.active { color: #37bc9b; }
.star s, .star-rtl s { color: black; font-size: 50px; cursor: default; text-decoration: none; line-height: 50px; }
.star { padding: 2px; }
.star-rtl { background: #555; display: inline-block; border: 2px solid #444; }
.star-rtl s { color: #000033; }
.star s:hover:before, .star s.rated:before, .star s.active:before { content: "\2605";
font-size: 32px; }
.star s:before { content: "\2606"; font-size: 32px; }
.star-rtl s:hover:after, .star-rtl s.rated:after, .star-rtl s.active:after { content: "\2605"; font-size: 32px; }
.star-rtl s:after { content: "\2606"; }
.active { color: #000033  !important; }
.fa-star { color: #ccc ; }
</style>

    @if(Session::has('message'))
    <div id="messageBox" class="alert alert-success" style=" text-align: center; width: 100%">{{Session::get('message')}}</div>
    @endif

<div class="about-us-area pt-50 pb-50">
    
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12">                

				<div class="short_desc">
				<h2><span>Feedback</span></h2><br/>
				<div class="row" style="width: 68%; margin: 2% 16%">
				<p>Hii {{$name ?? 'user'}},<br>We're curious how you felt about HOMEGLARE. Feel free to share your thoughts with us. Your opinion is highly appreciated as it will help us to serve you better. </p></div>
<div class="row">

                                        <div class="review-sec"  style="width: 68% ; margin: 2% 16%; padding: 25px; border:1px solid #eee; ">
                                            
                                            <form id="form-review" class="contact-from" action="{{url('feedback')}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                
                                                <input type="hidden" value="@if(!empty(Auth::user()->id)){{Auth::user()->id}}@else{{'0'}}@endif" name="customers_id">

                                                <div class="form-group">
                                                    <!--<label for="exampleFormControlInput1">Your Name </label>-->
                                                    <input type="text" class="input-field" value="{{$name ?? ''}}" name="customers_name" id="exampleFormControlInput1" placeholder="Your Name" required="required">
                                                </div>

                                                 <div class="form-group">
                                                    <!--<label for="exampleFormControlInput1">Email address</label>-->
                                                    <input type="email" class="input-field" name="customers_email" value="{{$email ?? ''}}" id="exampleFormControlInput1" placeholder="Your Email" required="required">
                                                </div>
                                            

                                            <div class="form-group">
                                                <!--<label for="exampleFormControlTextarea1">Share your opinion</label>-->
                                                <textarea class="input-field" name="customers_feedback" id="exampleFormControlTextarea1" placeholder="Share Your Opinion" rows="3" required="required"></textarea>
                                            </div>

                                            <div class="rating">
                                              <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

                                              <script>
                                                $(function() {
                                                    $("div.star > s").on("click", function(e) {

                                                                    // remove all active classes first, needed if user clicks multiple times
                                                                    $(this).closest('div').find('.active').removeClass('active');

                                                                    $(e.target).parentsUntil("div").addClass('active'); // all elements up from the clicked one excluding self
                                                                    $(e.target).addClass('active');  // the element user has clicked on

                                                                    var numStars = $(e.target).parentsUntil("div").length+1;
                                                                    $('.show-result input').val(numStars );
                                                                });
                                                });
                                            </script>
                                            <label>Rate your experience</label>
                                            <div class="star"><s><s><s><s><s></s></s></s></s></s></div>
                                            <div class="show-result">
                                                <input type="hidden" name="customers_rating" id="rating" value="0">
                                            </div>
                                        </div>

                                        <button type="submit" class="submit">Submit</button>
                                    </form>
                                </div>



                            </div>
					
						</div>
					</div>
				</div>
			</div>
			</div>
			

			@endsection
			
			@section('script')
			
			
			
			@endsection
