<?php
$name = "";

if(!empty($address)){
    $name = $address->entry_firstname." ".$address->entry_lastname ;
}
else{
    $first_name = $user->first_name?$user->first_name: " ";
    $last_name = $user->last_name?$user->last_name: " ";
    $name = $first_name." ".$last_name;
}
$phone = $user->phone;


?>
@extends('layouts.homeglare')

@section('content')

<!-- <div class="breadcrumb-area bg-img" style="background-image:url(/homeglare-new/images/bg/breadcrumb.jpg);"> -->
    <div class="bg-img">
        <div class="container">
            <div class="breadcrumb-content text-center">
                <h2>checkout page</h2>
                <ul>
                    <li>
                        <a href="/">Home</a>
                    </li>
                    <li class="active">checkout </li>
                </ul>
            </div>
        </div>
    </div>

    @if(session('error'))
    <div class="alert alert-success">
        {{ session('error') }}
    </div>
    @endif
    <form action="{{route('order-product')}}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="uid" id="uid" value="{{$user->id}}">
        <div class="checkout-main-area pt-45 pb-90">
            <div class="container">
                @if(Auth::user())
                <div class="customer-zone mb-20">

                    <div class="checkout-login-info">
                        <p>If you have shopped with us before, please enter your details in the boxes below. If you are a new customer, please proceed to the Billing & Shipping section.</p>
                        <form action="#">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="sin-checkout-login">
                                        <label>Username or email address <span>*</span></label>
                                        <input type="text" name="user-name">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="sin-checkout-login">
                                        <label>Passwords <span>*</span></label>
                                        <input type="password" name="user-password">
                                    </div>
                                </div>
                            </div>
                            <div class="button-remember-wrap">
                                <button class="button" type="submit">Login</button>
                                <div class="checkout-login-toggle-btn">
                                    <input type="checkbox">
                                    <label>Remember me</label>
                                </div>
                            </div>
                            <div class="lost-password">
                                <a href="#">Lost your password?</a>
                            </div>
                        </form>
                        <div class="checkout-login-social">
                            <span>Login with:</span>
                            <ul>
                                <li><a href="#">Facebook</a></li>
                                <li><a href="#">Twitter</a></li>
                                <li><a href="#">Google</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--<div class="customer-zone mb-20">
                    <p class="cart-page-title">Have a coupon? <a class="checkout-click3" href="#">Click here to enter your code</a></p>
                    <div class="checkout-login-info3">
                        <form >
                             <input type="text" name="" id="coupon_code" placeholder="Coupon code">
                            <!-- <span id="apply" value="Apply Coupon" onclick="checkCoupon()">Apply Coupon</span>
                            <input type="submit" value="Apply Coupon" hidden>
                        </form>
                    </div>
                </div>  -->
                <div class="checkout-wrap pt-30">
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="billing-info-wrap mr-50">
                             <input type="hidden" name="add_id" value="@if(!empty($address)){{$address->address_book_id}}@endif">
                             <h3>Billing Details</h3>
                             <div class="row">

                                <div class="col-lg-12 col-md-12">
                                    <div class="billing-info mb-20">
                                        <label>Full Name <abbr class="input-field required" title="required" required="required">*</abbr></label>
                                        <input type="text" class="input-field" name="billing_name" value="{{ucfirst($name)}}" id="billing_name">
                                    </div>
                                    @error('billing_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="col-lg-12">
                                    <div class="billing-info mb-20">
                                     <label>Country<abbr class="required" title="required">*</abbr></label>
                                     <input type="text" class="input-field @error('billing_country') is-invalid @enderror" value="India"  name="billing_country" id="billing_country" required="required" readonly>
                                 </div>
                                 @error('billing_country')
                                 <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="col-lg-12">
                                <div class="billing-info mb-20">
                                    <label>Address <abbr class="required" title="required">*</abbr></label>
                                    <input class="input-field billing-address @error('address') is-invalid @enderror" placeholder="Complete address" name="billing_street_address" type="text" value="{{ $address ? $address->entry_street_address: old('entry_street_address') }}"  required="required">             
                                </div>
                                @error('billing_street_address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="col-lg-12">
                                <div class="billing-info mb-20">                          
                                    <label>Town / City <abbr class="required" title="required">*</abbr></label>
                                    <input class="input-field @error('city') is-invalid @enderror" value="{{ $address ? $address->entry_city: old('entry_city') }}" id="billing_city" type="text" name="billing_city" pattern="^[a-zA-Z\s]*$"  onkeyup="checkInputValue(this)" required="required">

                                </div>
                                @error('billing_city')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="billing-info mb-20">
                                    <label>State <abbr class="required" title="required">*</abbr></label>
                                    <input class="input-field @error('state') is-invalid @enderror" value="{{$address ? $address->entry_state: old('entry_state') }}" onkeyup="checkInputValue(this)" pattern="^[a-zA-Z\s]*$"  type="text" id="billing_state" name="billing_state" required="required">

                                </div>
                                @error('billing_state')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="billing-info mb-20">
                                    <label>Postcode / ZIP <abbr class="required" title="required">*</abbr></label>
                                    <input class="input-field @error('pin_code') is-invalid @enderror" value="{{$address ? $address->entry_postcode: old('entry_postcode') }}" type="text" pattern="^\d+$" name="billing_postcode" id="billing_postcode" onkeyup="checkInputValue(this)" maxlength="6"  required="required" />
                                    {{-- <input type="text"> --}}
                                </div>
                                @error('billing_postcode')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="billing-info mb-20">
                                    <label>Phone <abbr class="required" title="required">*</abbr></label>
                                    <input class="input-field @error('phone') is-invalid @enderror" value="{{$phone}}" type="text" pattern="^\d+$" name="billing_phone" id="billing_phone" onkeyup="checkInputValue(this)" maxlength="10"  required="required"/>

                                </div>
                                @error('billing_phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            @if(empty($user->gst))
                            <div class="col-lg-12 col-md-12">
                                <div class="billing-info mb-20">
                                    <label>GST</label>
                                    <input class="input-field @error('gst') is-invalid @enderror" value="{{$user->gst ?? ''}}" type="text" name="user_gst" id="user_gst" onkeyup="checkInputValue(this)" />

                                </div>
                                @error('user_gst')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            @endif
                        </div>

                        <div class="checkout-account mt-25">
                            <input class="checkout-toggle @error('ship-box') is-invalid @enderror" value="different" id="ship-box" type="checkbox" name="ship_to_different_address">
                            <span>Ship to a different address?</span>
                        </div>
                        <div class="different-address open-toggle mt-30">
                            <div class="">
                                @if(!empty($allAddress))
                                @foreach($allAddress as $address)
                                <input type="radio" name="shipped"  value="{{$address->id}}">
                                {{$address->delivery_street_address}}, {{$address->delivery_city}}, {{$address->delivery_state}}, {{$address->delivery_country}} - {{$address->delivery_postcode}}<br>
                                Contact - {{$address->delivery_phone}}<br><br>   
                                @endforeach
                                @endif
                            </div>

                            <br><br>

                            <h3>or New Address</h3>
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="billing-info mb-20">
                                        <label>Full Name </label>
                                        <input class="input-field @error('delivery_name') is-invalid @enderror"  placeholder="Full Name" type="text"name="delivery_name" id="delivery_name">
                                    </div>
                                    @error('delivery_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-lg-12">
                                    <div class="billing-select mb-20">
                                        <label>Country </label>
                                        <input class="input-field @error('delivery_country') is-invalid @enderror" value="India" name="delivery_country" id="delivery_country" readonly>
                                    </div>
                                    @error('delivery_country')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-lg-12">
                                    <div class="billing-info mb-20">
                                        <label>Address</label>
                                        <input class="input-field @error('delivery_street_address') is-invalid @enderror"  placeholder="Complete address" type="text"name="delivery_street_address" id="delivery_street_address">     
                                    </div>
                                    @error('delivery_street_address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-lg-12">
                                    <div class="billing-info mb-20">
                                        <label>Town / City</label>
                                        <input class="input-field @error('delivery_city') is-invalid @enderror"  id="delivery_city" type="text" name="delivery_city" pattern="^[a-zA-Z\s]*$"  onkeyup="checkInputValue(this)" />              
                                    </div>
                                    @error('delivery_city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="billing-info mb-20">
                                        <label>State </label>
                                        <input class="input-field @error('delivery_state') is-invalid @enderror"  onkeyup="checkInputValue(this)" pattern="^[a-zA-Z\s]*$"  type="text" id="delivery_state" name="delivery_state" />
                                    </div>
                                    @error('delivery_state')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="billing-info mb-20">
                                        <label>Postcode / ZIP</label>
                                        <input class="input-field @error('delivery_postcode') is-invalid @enderror"  type="text" pattern="^\d+$" name="delivery_postcode" id="delivery_postcode" onkeyup="checkInputValue(this)" maxlength="6" />
                                    </div>
                                    @error('delivery_postcode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="billing-info mb-20">
                                        <label>Phone</label>
                                        <input class="input-field @error('delivery_phone') is-invalid @enderror"  type="text" pattern="^\d+$" name="delivery_phone" id="delivery_phone" onkeyup="checkInputValue(this)" maxlength="10" />
                                    </div>
                                    @error('delivery_phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>
                        </div>

                        {{-- <div class="additional-info-wrap">
                            <label>Order notes</label>
                            <textarea placeholder="Notes about your order, e.g. special notes for delivery. " name="message"></textarea>
                        </div> --}}

                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="your-order-area">
                        <h3>Your order</h3>
                        <div class="your-order-wrap gray-bg-4">
                            <div class="your-order-info-wrap">
                                <div class="your-order-info">
                                    <ul>
                                        <li>Product <span>Total</span></li>
                                    </ul>
                                </div>
                                <div class="your-order-middle">

                                    <ul>
                                        @if(!empty($productDetails))
                                        @foreach($productDetails as $product)
                                        <li>{{substr($product->products_name,0,35)}}... X {{$product->quantity}}
                                            @if($product->type=='sample') {{'(Sample)'}}@endif <span>₹{{$product->total_price}}.00 </span></li>
                                            @endforeach
                                            @endif

                                        </ul>
                                    </div>

                                    <div class="your-order-info order-subtotal">
                                        <ul>
                                            <li >
                                                 <li>Subtotal <span>₹{{$totalAmount-$tax}} </span></li>
                                                @if(!empty(session()->get('coupon')))
                                                <a class="btn" style="background-color: #27e027; color: #fff" disabled>{{'Coupon Applied'}}</a>
                                                @else
                                                <!-- <a class="btn" style=" color: #fff; background-color: #d20e14" onclick="openCoupon()">{{'Apply Coupon'}}</a> -->
                                                <div style="display: block; padding-top: 10px" id="showCoupon">
                                                    <input type="text" id="coupon_code" placeholder="Enter Coupon Code Here" class="form-control"><a  onclick="checkCoupon()" class="btn" style=" color: #fff; background-color: #d20e14; margin-top: 10px">Apply Coupon</a>
                                                </div>
                                                @endif
                                                <!-- <span>₹{{$totalAmount-$tax}} </span></li> -->
                                               
                                            </ul>
                                        </div>

                                        <div class="your-order-info order-subtotal">
                                            <ul>
                                                <li>GST <span>₹ {{$tax}}</span></li>
                                                <!-- <li>GST <span>₹ @if(!empty($user->gst)){{'0'}}@else{{$tax}}@endif </span></li> -->
                                            </ul>
                                        </div>

                                        @if(!empty(session()->get('coupon')))
                                        <div class="your-order-info order-subtotal">
                                            <ul>
                                                <li>Discount <span>- ₹ {{session()->get('coupon')->coupon_amount}} </span></li>
                                            </ul>
                                        </div>
                                        @endif

                                        <?php
                                        $grand_total = 0;
                                    //     if(!empty($user->gst) && !empty(session()->get('coupon'))){
                                    //      $grand_total = $totalAmount - $tax - session()->get('coupon')->coupon_amount;
                                    //      $tax = 0;
                                    //  }
                                    //  else if(!empty($user->gst)){
                                    //     $grand_total = $totalAmount -$tax ;
                                    //     $tax=0;
                                    // }
                                    // else{
                                    //     $grand_total = $totalAmount;
                                    // }


                                        if(!empty(session()->get('coupon'))){
                                         $grand_total = ceil($totalAmount - session()->get('coupon')->coupon_amount);
                                     }
                                    else{
                                        $grand_total = ceil($totalAmount);
                                    }
                                    ?>
                                    <input type="text" name="totalAmount" id="totalAmount" hidden value="{{$grand_total}} ">
                                    <input type="text" name="gst_tax" hidden value="{{$tax}}">
                                    <input type="text" name="coupon_amount" hidden value="@if(!empty(session()->get('coupon'))){{session()->get('coupon')->coupon_amount}}@else{{'0'}}@endif">
                                    <input type="text" name="coupon_code" hidden value="@if(!empty(session()->get('coupon'))){{session()->get('coupon')->coupon_code}}@endif">
                                    <div class="your-order-info order-total">
                                        <ul>
                                            <li>Total <span>₹ {{$grand_total}}</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="shipping-method">
                                    <br>
                                    <h4>Select Shipping Method</h4>
                                @if(!empty(session()->get('shipped')) && session()->get('shipped') =='carton' )
                                    <input  type="radio" id="transport" name="shipping_method" class="input-radio" value="transport" checked onclick="getTransport()">

                                    <label for="shipping__method_1" style="font-size: 18px; font-weight: bold;"> If you want to avail transport facility </label>
                                    <p style="margin-left: 14px; font-weight: bold;">
                                    Please fill out below details : </p>
                                    <div class="row" id="transport-form">
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-10">
                                            <input type="text" name="transportation_name" id="transportation_name" placeholder="Transport Name *" class="form-control" style="margin-bottom: 10px" >
                                            <input type="text" name="transportation_email" id="transportation_email" placeholder="Marka (If any)" class="form-control" style="margin-bottom: 10px">
                                            <input type="text" name="transportation_mobile" id="transportation_mobile" placeholder="Transport Mobile " class="form-control" style="margin-bottom: 10px" >
                                            <input type="text" name="transportation_address" id="transportation_address" placeholder="Transport Address" class="form-control" style="margin-bottom: 10px">
                                        </div>
                                        <div class="col-sm-1"></div>
                                    </div>
                                    
                                    <br>
                                @endif
                                    <input  type="radio" id="courier" name="shipping_method" value="courier" class="input-radio" checked="checked">

                                    <label for="shipping_method-2" style="font-size: 17px; font-weight: bold;"> Courier </label>                
                                    
                                </div>
                                <div class="payment-method">
                                    <h4>Select Payment Method</h4>

                                    <div class="pay-top sin-payment">
                                        <input  type="radio" id="pay_by_card" name="payment_method" value="prepaid" class="input-radio" checked="checked">
                                        <label for="payment-method-2" style="font-size: 16px; font-weight: bold;">Prepaid </label>                
                                    </div>
                                    @if ($grand_total<5001)
                                    <div class="pay-top sin-payment">
                                        <input  type="radio" id="cash_on_delivery" name="payment_method" class="input-radio" value="cash_on_delivery" @if(!empty(session()->get('category')) && session()->get('category')!='sample' && session()->get('category')!='moq' && $grand_total>5000) {{'disabled'}}@endif>
                                        <label for="payment_method_1" style="font-size: 16px; font-weight: bold;"> Cash On Delivery @if(!empty(session()->get('category')) && session()->get('category')!='sample' && session()->get('category')!='moq' ) {{'(Not available)'}}@endif</label>
                                    </div> 
                                    @endif                                   
                                    
                                </div>
                            </div>
                            <div class="Place-order mt-40">




                             @if($totalAmount < 1000 && (!empty(session()->get('category') && session()->get('category') != 'sample')))
                             <a  onclick="Swal('Book orders of min Rs. 1000 to continue')">Place Order</a>
                             <a data-toggle="modal" id="show-alertcheckout" data-target="#exampleModal2" href="#" style="display: none;">Continue to Checkout</a>
                             @else
                             <a onclick="formSubmit()">Place Order</a>
                             <input value="Place order" id="checkoutForm" class="btn-submit" type="submit" style="display: none;">
                             @endif







                         </div>
                     </div>
                 </div>
             </div>
         </div>
         @endif
     </div>
 </div>

</form>
<a data-toggle="modal" id="show-alertcheckout" data-target="#exampleModal2" href="#" style="display: none;">

    @endsection


    @section('script')

    <script type="text/javascript">

        /* Start input check regex */
        function checkInputValue(field) {
            var value = field.value;
            var fieldId = field.id;
            var pattern = field.pattern;
            var pattern = new RegExp(field.pattern);
            var re = pattern.test(value);
            // var re = value.match(pattern);
            if(!re) {
                $('#' + fieldId + '_error').removeClass('d-none');
                if (document.getElementById(fieldId + '_error')) {
                  $('#' + fieldId + '_error').removeClass('d-none');
                  $('.btn-submit').prop('disabled', true);
              } else {
                $('#' + fieldId).after('<small id="' +fieldId+'_error" class="text-danger">This value is not valid</small>');
                $('.btn-submit').prop('disabled', true);
            }
        } else {
            $('#' + fieldId + '_error').addClass('d-none');
            $('.btn-submit').prop('disabled', false)
        }
    };
    /* End input check regex*/

    function formSubmit(){
        $('#checkoutForm').click();
    }

    function getTransport(){

        var data = $('input[name="shipping_method"]:checked').val();

        if(data == 'transport'){
            $('#transport-form').show();
            $('#transportation_name').prop('required', true);
            $('#transportation_mobile').prop('required', true);
        }
        else if(data == 'courier'){
            $('#transport-form').hide();
            $('#transportation_name').prop('required', false);
            $('#transportation_mobile').prop('required', false);
        }
    }

    function checkCoupon(){
        var code = $('#coupon_code').val();
        var total = $('#totalAmount').val();

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
           url: '/checkCoupon',
           type: 'POST',
           data: {_token: CSRF_TOKEN, code: code, total: total},
           success: function (data) {
            if(data==0){
                Swal('Invalid Coupon Code');
                
            }
            else if(data == 2){
               Swal('One Coupon already applied. You cannot apply another coupon.');
                
            }
            else if(data.status == 3){
                Swal('This coupon is applicable on order between Rs.'+data.min+' - '+data.max);
                
            }
            else if(data.status == 1){
                var coupon_amount = data.amount;
                Swal('Coupon applied successfully.Rs. '+coupon_amount+' get reduced from your total order amount.');
                // Swal('<h3>Coupon applied successfully.</h2><h5>Rs. '+coupon_amount+' get reduced from your total order amount.</h5>');
                // 

                setTimeout(function(){
                 window.location.reload(1);
             }, 5000);
            }
        }
    });
        
    }

    function openCoupon(){
        $('#showCoupon').toggle()
    }


    
</script>

@endsection