<?php
use App\Review;
$total = 0;
$products_price = 0;
?>


<div class="shopping-cart-top">
  <h4>Your Cart</h4>
  <a class="cart-close" href="#"><i class="la la-close"></i></a>
</div>
@if($cartproducts != null)
<?php $i=0;  ?>
<ul>
 @foreach($cartproducts as $product)
 <?php
 $products_price = $prices[$i] + (($product->tax_percent*$prices[$i])/100);
 $products_price = number_format((float)$products_price, 2, '.', '');
 ?>
 <li class="single-shopping-cart" id="listhide{{$pro_id[$i]}}">
  <div class="shopping-cart-img">
    <a href="/product-detail/{{$product->products_slug}}"><img alt="" src="{{url('/')}}/{{$product->path}}"></a>
    <div class="item-close">
      <a onclick="deleteProduct({{$product->products_id}})"><i class="sli sli-close"></i></a>
    </div>
  </div>
  <div class="shopping-cart-title">
    <h4><a href="/product-detail/{{$product->products_slug}}">{{substr($product->products_name,0,18)}}.</a></h4>
    <span>₹{{$products_price}} * {{$quantity[$i]}}</span>
  </div>
  <div class="shopping-cart-delete">
    <a onclick="deleteProduct('{{$pro_id[$i]}}')"><i class="la la-trash"></i></a>
  </div>
</li>
<?php
$total  =$total + $products_price * $quantity[$i];
$i++;
?>
@endforeach

</ul>
@endif
<div class="shopping-cart-bottom">
  <div class="shopping-cart-total">
    <h4>Subtotal <span class="shop-total" >₹<span id="total-bill">{{$total}}</span></span></h4>
  </div>
  <div class="shopping-cart-btn btn-hover default-btn text-center">
    <a href="<?php if(!empty(Auth::user()->id)){echo '/cart/'.Auth::user()->id;} else{ echo '/customer/login'; } ?>" class="black-color" id="gotoCart" >Cart</a>
  </div>
  <div class="shopping-cart-btn btn-hover default-btn text-center">
    @if($total < 1000 && (!empty(session()->get('category') && session()->get('category') != 'sample')))
    <a class="black-color"  onclick="Swal('Book orders of min Rs. 1000 to continue')">Continue to Checkout</a>
    <a data-toggle="modal" id="show-alertminicart" data-target="#exampleModal2" href="#" style="display: none;">Continue to Checkout</a>
    @else
    <a class="black-color" href="<?php if(!empty(Auth::user()->id)){echo '/checkout/';} else{ echo '/customer/login'; } ?>">Continue to Checkout</a>
    @endif
  </div>
</div>
