<?php
use App\Models\Core\Products;
$count = 0;
?>
@extends('layouts.homeglare')

@section('content')
<style>
#img-response{ width: 100px; height: 100px }
</style>

<!-- /*<div class="breadcrumb-area bg-img" style="background-image:url(/homeglare-new/images/bg/breadcrumb.jpg);">*/ -->
  <div class="bg-img">
    <div class="container">
      <div class="breadcrumb-content text-center">
        <h2>Wishlist page</h2>
        <ul>
          <li>
            <a href="/">Home</a>
          </li>
          <li class="active">Wishlist </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="cart-main-area pt-85 pb-90">
    <div class="container">
      <h3 class="cart-page-title">Your Wishlist items</h3>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
          <form action="#">
            <div class="table-content table-responsive cart-table-content">
              <table>
                <thead>
                  <tr>
                    <th>Image</th>
                    <th>Product Name</th>
                    <th>Until Price</th>
                    <th>Quantity</th>
                    <th>Stock Status</th>
                    <th>Add To Cart</th>
                    <th>Remove</th>
                  </tr>
                </thead>
                <tbody>
                  <tr colspan="7">
                    <div class="pro-details-compare-wishlist">
    <div id="messageBox" class="alert alert-success" style="display: none;">Product added successfully.</div>
</div>
                  </tr>
                  @if(!empty($wishlist))
                  @foreach($wishlist as $list)

                  @php $count++; @endphp
                  <?php 
                  $product_quantity = 1;
                  $item = Products::leftjoin('products_description','products_description.products_id','products.products_id')
                  ->leftjoin('images','images.id','products.products_image')
                  ->leftjoin('image_categories','image_categories.image_id','images.id')
                  ->where('products.products_id',$list->liked_products_id)
                  ->where('image_categories.image_type','ACTUAL')   
                  ->first();
                  $price = $item->products_price + ($item->products_price*$item->tax_percent)/100;
                  $products_quantity = DB::table('products_price')->where('products_id',$list->liked_products_id)->where('products_sell_type','quantity_based')->first();
                  

                  if(!empty($products_quantity)){
                    $product_quantity = $products_quantity->products_quantity;
                    $price = $products_quantity->products_price + ceil(($products_quantity->products_price*$item->tax_percent)/100);
                  }
                  ?>
                  <tr id="div-list{{$list->like_id}}">
                    <td class="product-thumbnail">
                      <a href="/product-detail/{{$item->products_slug}}"><img src="{{url('/')}}/{{$item->path}}" alt="" id="img-response"></a>
                    </td>
                    <td class="product-name">
                      <a href="/product-detail/{{$item->products_slug}}">{{$item->products_name}}</a>
                      <h6 style="font-size: 10px; font-weight: bold; color: #999"> SKU - <span style="color: #999">{{$product->products_sku_code ?? ''}}</span></h6>
                    </td>
                    <td class="product-price-cart"><span class="amount">₹{{$price}}</span></td>
                    <td class="product-quantity">
                      <div class="">
                        <input type="hidden" id="fix_quant{{$list->liked_products_id}}" value="{{$product_quantity ?? '1'}}">
                        <input type="hidden" id="stock{{$list->liked_products_id}}" value="@if($item->products_quantity-$item->products_min_order>0){{'in'}}@else{{'out'}}@endif">
                        <i class="ti-minus" style="border: 1px solid #afafaf; padding: 5px 5px 8px; cursor: pointer;" onclick="decrement('{{$list->liked_products_id}}')" ></i>

                        <input style="width: 70px; text-align: center;" type="text" name="quantity" id="quantity{{$list->liked_products_id}}" value="{{$product_quantity}}" disabled="disabled ">

                        <i class="ti-plus" style="border: 1px solid #afafaf; padding: 5px 5px 7px; cursor: pointer; "  onclick="increment('{{$list->liked_products_id}}')"></i>

                        <div class="pro-details-compare-wishlist">
    <div id="messageBox" class="alert alert-success" style="display: none;">Product added successfully.</div>
</div>

                      </div>
                    </td>

                    <td class="product-subtotal">@if($item->products_quantity>$item->products_min_order){{'In Stock'}}@else{{'Out Of Stock'}}@endif</td>
                    <td class="product-wishlist-cart">
                      <a onclick="addToCart('{{$item->products_id}}','normal' ,'moq')">add to cart</a>
                    </td>
                    <td><a onclick="deleteWishlistItem('{{$list->like_id}}')"><b><i class="la la-close"></i></b></a></td>
                  </tr>    
                  @endforeach
                  @endif                                    
                </tbody>
              </table>


              @if($count==0)
              <div style="text-align: center;padding: 50px">No result found</div>
              @endif

            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


  @endsection

  @section('script')

  <script>

    function addToCart(id,type, category){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var quantity = $('#quantity'+id).val();
      var stock = $('#stock'+id).val();

      var type = 'normal';
      if(stock == 'in'){
      $.ajax({
       url: '/add-to-cart',
       type: 'POST',
       data: {_token: CSRF_TOKEN, id: id, quantity_moq: quantity, type: type, category: 'moq' },
       success: function (data) {
        $('#cartItem').html(data.cartItem);

        $("#messageBox").hide().slideDown();
                    setTimeout(function(){
                      $("#messageBox").hide();        
                  }, 3000);

      }
    });
    }
    else{
        Swal('Product out of stock');      
    }
    }

    function deleteWishlistItem(id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      user_id = <?php echo Auth::user()->id; ?>

      $.ajax({
       url: '/delete-wishlist-item',
       type: 'POST',
       data: {_token: CSRF_TOKEN, id: id, user_id : user_id},
       success: function (data) {
         $('#cartItem').html(data.cartItem);
         $('#wishItem').html(data.wishItem);

         $('#div-list'+id).hide();
       },
       failure: function (data) {
       }
     });
    }

    function decrement(id){

      var fix_quant = $('#fix_quant'+id).val();
      var quantity = $('#quantity'+id).val();

    if(parseInt(quantity)>parseInt(fix_quant)){

      quantity = parseInt(quantity)-parseInt(fix_quant) ;
      
        $('#quantity'+id).val(quantity);
      }
    }   


    function increment(id){

     var fix_quant = $('#fix_quant'+id).val();
     var quantity = $('#quantity'+id).val();

    quantity = parseInt(quantity)+parseInt(fix_quant) ;
        $('#quantity'+id).val(quantity);
      }

    </script>

    @endsection