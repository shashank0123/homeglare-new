@extends('layouts/homeglare')

@section('content')

<div class="about-us-area pt-50 pb-90">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				
				<!-- <h2>Welcome To <span>HOMEGLARE</span></h2> -->

				<div class="short_desc">
					
					<h2><span>Return Policy</span></h2><br>

					<h4>Last updated January 29, 2020</h4>


					<p>Thank you for your purchase. We hope you are happy with your purchase. However, if you are not completely satisfied with your purchase for any reason, you may return it to us for a full refund, store credit, or an exchange. Please see below for more information on our return policy.</p>
					
					<h5>RETURNS</h5>
					
					<p>All returns must be postmarked within 7 days of the purchase date. All returned items must be in new and unused condition, with all original tags and labels attached.</p>
					
					<h5>RETURN PROCESS</h5>
					
					<p>To return an item, place the item securely in its original packaging and Purchase Invoice, and mail your return to the following address:</p>

					<h5>Homeglare E-commerce Pvt Ltd<br>
						Khasra No 915, Nala Road,<br>
						Next to Guleria House, Rithala<br>
					Delhi - 110085</h5>
					

					<h5>REFUNDS</h5>
					
					<p>After receiving your return and inspecting the condition of your item, we will process your return. Please allow at least 15 days from the receipt of your item to process your return.</p>
					
					<h5>EXCEPTIONS</h5>    

					<p>For defective or damaged products, please contact us at the customer service number below to arrange a refund or exchange. </p>
					

					<h5>QUESTIONS</h5>

					<p>If you have any questions concerning our return policy, please contact us at: 
						<a href="mailto:support@homeglare.com">support@homeglare.com</a></p>

					</div>
					
				</div>
			</div>
		</div>
	</div>

	@endsection
