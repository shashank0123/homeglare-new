<?php
$contact = explode(',',$companyInfo->contact_no);
?>

@extends('layouts.homeglare')

@section('content')
@if($message = Session::get('status'))
      <div class="alert alert-success message" style="text-align: center; width: 100%">{{$message}}</div>
      @endif
<div class="breadcrumb-area bg-img" style="background-image:url(/homeglare-new/images/bg/breadcrumb.jpg);">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <h2>contact page</h2>
            <ul>
                <li>
                    <a href="/">Home</a>
                </li>
                <li class="active">contact us </li>
            </ul>
        </div>
    </div>
</div>
<div class="contact-area pt-25 pb-90">
    <div class="container">
        <div class="contact-info-wrap mb-50">
           @if($companyInfo)
           <h3>contact info</h3>
           
           <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="single-contact-info text-center mb-30">
                    <i class="ti-location-pin"></i>
                    <h4>our address</h4>
                    <p><?php echo $companyInfo->address ?>.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="single-contact-info extra-contact-info text-center mb-30">
                    <i class="ti-mobile"></i>
                    <h4>Phone </h4>
                    <ul>
                     
                        <li><i class="ti-mobile"></i>
                            @if(isset($contact))
                            @foreach($contact as $con)
                            <a href="tel:{{$con}}">(+91) {{$con}}</a> <br>
                            @endforeach
                            @endif
                        </li>
                        
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="single-contact-info text-center mb-30">
                    <i class=" ti-email"></i>
                    <h4>Mail </h4>
                    <ul>
                        <li>
                            <i class="ti-email"></i> <a href="mailto:<?php echo $companyInfo->support_email; ?>"> {{$companyInfo->support_email}}</a>
                            <br>
                            <!-- <i class="ti-email"></i> <a href="mailto:<?php echo $companyInfo->support_email; ?>"> {{$companyInfo->support_email}}</a> -->
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        @endif
    </div>
    <div class="get-in-touch-wrap">
        <h3>Get In Touch</h3>
        <div class="contact-from contact-shadow">
            <form id="contact-form" method="post" action="{{ route('contact-us') }}">
             {{ csrf_field() }}
             <div class="row">
                <div class="col-lg-6">
                    <input name="contact_name" id="contact_name" class="input-field" type="text" placeholder="Name">
                </div>
                <div class="col-lg-6">
                    <input name="contact_email" id="contact_email" class="input-field" type="email" placeholder="Email">
                </div>
                <div class="col-lg-12">
                    <select name="contact_subject" id="contact_subject" class="input-field" placeholder="Subject">
                        <option> -- Select Subject --  </option>
                        <option value="Feedback Suggestion">Feedback Suggestion</option>
                        <option value="Become Partner">Become Partner</option>
                        <option value="Sell on Homeglare">Sell on Homeglare</option>
                        <option value="Sponsorship">Sponsorship</option>
                    </select>
                </div>
                <div class="col-lg-12">
                    <textarea name="contact_message" id="contact_message" class="input-field" placeholder="Your Message"></textarea>
                </div>
                <div class="col-lg-12">
                    <button class="submit" type="submit">Send Message</button>
                </div>
            </div>
        </form>
        <p class="form-messege"></p>
    </div>
</div>
<div class="contact-map pt-90">
   <div class="embed-responsive  embed-responsive-21by9">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3498.961877376643!2d77.1050124145617!3d28.720685286837693!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d06b350000001%3A0x784e61ccea5aae77!2sArihant%20Steels!5e0!3m2!1sen!2sin!4v1577452205207!5m2!1sen!2sin" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
  </div>
  

</div>
</div>
</div>


@endsection