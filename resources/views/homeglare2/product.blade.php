@extends('layouts.homeglare')

@section('content')

<style type="text/css">
.bg-color { background-color: #000033!important; }

@media screen and (min-width: 991px)  { 
  #desktop-view{ display: block!important; }
  #mobile-view{ display: none!important; }
}

@media screen and (max-width: 991px)  {
  #desktop-view{ display: none!important; }
  #mobile-view{ display: block!important; }
} 

#brand-filter .active{ background-color: #f1f1f1; padding-top: 5px; }
</style>

<div class="shop-area pt-90 pb-90">
 <div class="container-fluid">
  <div class="row ">

   <div class="col-lg-3">

     <input type="text" name="category" hidden id="category" value="{{$id ?? ''}}">
     <input type="text" name="pro-color" hidden id="pro-color" value="">
     <input type="text" name="pro-rating" hidden id="pro-rating" value="">
     <input type="text" name="min-price" hidden id="min-price" value="0">
     <input type="text" name="max-price" hidden id="max-price" value="2000">
     <input type="text" name="filter-brand" hidden id="filter-brand" value="{{$brand->id ?? '0'}}">
     <input type="text" name="filter-search" hidden id="filter-search" value="{{$keyword ?? ''}}">
     
     <div class="category-menu-wrap" id="mobile-view"  >
       <button type="button" class="btn btn-block bg-color  mb-3 btn-lg">
        <h3 class="showcat">
         <a href="#">
           <img class="category-menu-non-stick" src="/homeglare-new/images/icon-img/category-menu.png" alt="icon">
           <img class="category-menu-stick" src="/homeglare-new/images/icon-img/category-menu-stick.png" alt="icon">
           FILTER <i class="la la-angle-down"></i>
         </a>
       </h3>
     </button>
     <div class="category-menu mobile-category-menu mobile-view-filter hidecat"style="display: none; height: 100%" >
      <h5>Shop By Category</h5>
      <nav>
       <ul><?php $c = 0; ?>
       @if(!empty($cat_ids))
       @foreach($cat_ids as $cat)

       <li>
         <a data-toggle="collapse" data-parent="#faq" href="/product/{{$cat->categories_slug}}">{{$cat->categories_name}} </a>
         
       </li>
       @endforeach
       @endif

     </ul>
   </nav>



   <div class="shop-price-filter mt-35 shop-sidebar-border pt-40 sidebar-widget">
    <h4 class="sidebar-title">Price Filter</h4>
    <div class="price-filter mt-20">
      <div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" >
        <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 66.6667%;"></div>
        <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span>
        <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 66.6667%;"></span>
      </div>
      <div class="price-slider-amount">
        <div class="label-input">
          <input type="text" id="amount" name="price" placeholder="Add Your Price" >
        </div>
        <button type="button" onclick="getRange()">Filter</button>
      </div>
    </div>
  </div>


  @if(!empty($brands))
  <div class="sidebar-widget shop-sidebar-border pt-40 mt-40">
    <h4 class="sidebar-title">Shop By Brand </h4>
    <div class="sidebar-widget-list mt-20" id="brand-filter" style="max-height: 300px;  overflow-y: auto">  
     <ul>
      @foreach($brands as $brnd)
      <li class="@if(!empty($brand) && $brand->id == $brnd->id){{'active'}}@endif">
       <a onclick="getBrandFilter({{$brnd->id}})" ><input type="hidden" value="{{$brnd->id}}" id="brandFilter{{$brnd->id}}">{{ucfirst($brnd->brand_name ?? '')}}</a>
     </li>
     @endforeach
   </ul>
 </div>
</div>
@endif



@if(!empty($colors))
<div class="sidebar-widget shop-sidebar-border pt-40 mt-40">
  <h4 class="sidebar-title">Shop By Brand </h4>
  <div class="sidebar-widget-list mt-20" id="brand-filter" style="max-height: 300px;  overflow-y: auto">  
   <ul>
    @foreach($colors as $brnd)
    <li class="@if(!empty($brand) && $brand->id == $brnd->id){{'active'}}@endif">
     <a onclick="getBrandFilter({{$brnd->id}})" ><input type="hidden" value="{{$brnd->id}}" id="colorFilter{{$brnd->id}}">{{ucfirst($brnd->brand_name ?? '')}}</a>
   </li>
   @endforeach
 </ul>
</div>
</div>
@endif


@if(!empty($materials))
<div class="sidebar-widget shop-sidebar-border pt-40 mt-40">
  <h4 class="sidebar-title">Shop By Brand </h4>
  <div class="sidebar-widget-list mt-20" id="brand-filter" style="max-height: 300px;  overflow-y: auto">  
   <ul>
    @foreach($materials as $brnd)
    <li class="@if(!empty($brand) && $brand->id == $brnd->id){{'active'}}@endif">
     <a onclick="getBrandFilter({{$brnd->id}})" ><input type="hidden" value="{{$brnd->id}}" id="materialFilter{{$brnd->id}}">{{ucfirst($brnd->brand_name ?? '')}}</a>
   </li>
   @endforeach
 </ul>
</div>
</div>
@endif



</div>

</div>


<div class="sidebar-wrapper" id="desktop-view">
  {{-- <div class="sidebar-widget">
    <h4 class="sidebar-title">Search </h4>
    <div class="sidebar-search mb-40 mt-20">
      <form class="sidebar-search-form" action="#">
        <input type="text" placeholder="Search here...">
        <button>
          <i class="la la-search"></i>
        </button>
      </form>
    </div>
  </div> --}}
  <div class="sidebar-widget shop-sidebar-border pt-40">
    <h4 class="sidebar-title">Shop By Categories</h4>
    <div class="shop-catigory mt-20" style="max-height: 300px; overflow: auto">
      <ul id="faq">
        <?php $c = 0; ?>
        @if(!empty($cat_ids))
        @foreach($cat_ids as $cat)

        <li>
         <a href="/product/{{$cat->categories_slug}}">{{$cat->categories_name}}{{--  <i class="la la-angle-down"></i> --}}</a>
         {{-- <a data-toggle="collapse" data-parent="#faq" href="#shop-catigory-1">{{$cat->categories_name}} <i class="la la-angle-down"></i></a> --}}
         
       </li>
       @endforeach
       @endif

     </ul>
   </div>
 </div>

 <div class="shop-price-filter mt-35 shop-sidebar-border pt-40 sidebar-widget">
  <h4 class="sidebar-title">Price Filter</h4>
  <div class="price-filter1 mt-20">
    <div id="slider-range1" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" >
      <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 66.6667%;"></div>
      <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span>
      <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 66.6667%;"></span>
    </div>
    <div class="price-slider-amount1">
      <div class="label-input">
        <input type="text" id="amount1" name="price" placeholder="Add Your Price" >
      </div>
      <button type="button" onclick="getRange2()">Filter</button>
    </div>
  </div>
</div>


@if(!empty($brands))
<div class="sidebar-widget shop-sidebar-border pt-40 mt-40">
  <h4 class="sidebar-title">Shop By Brand</h4>
  <div class="sidebar-widget-list mt-20" id="brand-filter" style="max-height: 300px;  overflow-y: auto">  
   <ul>
    @foreach($brands as $brnd)
    <li class="@if(!empty($brand) && $brand->id == $brnd->id){{'active'}}@endif">
     <a onclick="getBrandFilter({{$brnd->id}})" ><input type="hidden" value="{{$brnd->id}}" id="brandFilter{{$brnd->id}}">{{ucfirst($brnd->brand_name ?? '')}}</a>
   </li>
   @endforeach
 </ul>
</div>
</div>
@endif
@if(!empty($material))
<div class="sidebar-widget shop-sidebar-border pt-40 mt-40">
  <h4 class="sidebar-title">Material </h4>
  <div class="sidebar-widget-list mt-20" style="max-height: 300px; overflow-y: auto">
   <ul>
    @foreach($material as $mate)
    <li>
     <a href="javascript:void(0)">{{$mate}}</a>
   </li>
   @endforeach
 </ul>
</div>
</div>
@endif
@if(!empty($color))        
<div class="sidebar-widget pt-40 mt-40 shop-sidebar-border">
  <h4 class="sidebar-title">Colour </h4>
  <div class="sidebar-widget-list mt-20" style="max-height: 300px; overflow-y: auto">
   <ul>
    @foreach($color as $col)
    <li>
      <a href="javascript:void(0)" class="colorOnClick" id="{{$col}}">{{$col}}</a>
    </li>
    @endforeach                       
  </ul>
</div>
</div>
@endif

</div>

<div class="sidebar-wrapper" id="desktop-view"  >






</div>
</div>
<div class="col-lg-9">
  <div class="shop-topbar-wrapper">
   <div class="shop-topbar-left">
    <div class="view-mode nav">
     <a class="active" href="#shop-1" data-toggle="tab"><i class="la la-th"></i></a>
     <a href="#shop-2" data-toggle="tab"><i class="la la-list-ul"></i></a>
   </div>
   
 </div>
 <div class="product-sorting-wrapper">
  <div class="product-shorting shorting-style">

  </div>
  <div class="product-show shorting-style">
   <label>Sort by:</label>
   <select id="sort">
    <option value="created">Relevance</option>
    <option value="name-asc">Name, A to Z</option>
    <option value="name-desc">Name, Z to A</option>
    <option value="low-high">Price, low to high</option>
    <option value="high-low">Price, high to low</option>
  </select>
</div>
</div>
</div>
<div class="shop-bottom-area">
  <div class="row">
    <div class="col-sm-6">
      @if(!empty($keyword))
      <h4>Searched results for <i>'{{$keyword}}'</i></h4>
      @endif
    </div>
    <div class="col-sm-6" style="text-align: right; padding-bottom: 10px">
      <a href="" class="btn" style="background-color: #d20e14; color: #ffffff; margin-right: 10px;">Reset</a>
    </div>
  </div>

  <div class="tab-content jump" id="productCat">
    <div id="shop-1" class="tab-pane active">
     <div class="row">
      <?php $count = 0; ?>
      @if($searched_products !=null)
      @foreach($searched_products as $product)
      <?php
      $count++;
      $discount = $product->products_price;

      if($discount>0)
       $discount = $discount;
     ?>
     <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">


      <?php
      $product_price = DB::table('products_price')->where('products_id',$product->products_id)->where('products_sell_type','sample')->first();
      $products_price = $product_price->products_price ?? $product->products_price;
      $products_price = $products_price + ceil(($product->tax_percent*$products_price)/100);
      // $products_price = number_format((float)$products_price, 2, '.', '');
      if(($product->products_quantity-$product->products_min_order)>0)
        $stock = "in";
      else
        $stock = 'out';

      ?>



      <div class="product-wrap product-border-1 product-img-zoom">
       <div class="product-img">
        <a href="/product-detail/{{$product->products_slug}}"><img src="{{url('/')}}/{{$product->path}}" alt="product" class="img-response"></a>
      </div>
      <div class="product-content product-content-padding">
        <input type="hidden" id="stock{{$product->products_id}}" value="{{$stock}}">
        <h5 style="margin-bottom:0!important"><a href="/product-detail/{{$product->products_slug}}">{{substr(ucfirst(strtolower($product->products_name)),0,35) }}</a></h5>
        <h6 style="font-size: 10px; font-weight: bold; color: #999"> SKU - <span style="color: #999">{{$product->products_sku_code ?? ''}}</span></h6>
        <div class="price-addtocart">
         <div class="product-price">
          <span>₹{{$products_price}}</span>
        </div>
      </div>
    </div>
    <div class="product-action-2">
     <a data-toggle="modal" data-target="#exampleModal" title="Quick View" data-myid="{{$product->products_id}}"><i class="la la-search"></i></a>
     <a title="Add To Cart" onclick="addToCart({{$product->products_id}},'normal','moq' @if(!empty(Auth::user()->id)) {{','.Auth::user()->id}} @endif)"><i class="la la-cart-plus"></i></a>
     <a title="Wishlist" onclick="addToWishlist({{$product->products_id}})"><i class="la la-heart-o"></i></a>
   </div>
 </div>



 <br>
 
</div>
@endforeach


@endif                        
</div>

</div>
<div id="shop-2" class="tab-pane container">

 <?php $count=0; ?>
 @if($searched_products)
 @foreach($searched_products as $product)
 <?php $count++; ?>
 <?php
 $product_price = DB::table('products_price')->where('products_id',$product->products_id)->where('products_sell_type','sample')->first();
 $products_price = $product_price->products_price ?? $product->products_price;
 $products_price = $products_price + ceil(($product->tax_percent*$products_price)/100);
 // $products_price = number_format((float)$products_price, 2, '.', '');
 if(($product->products_quantity-$product->products_min_order)>0)
  $stock = "in";
else
  $stock = 'out';

?>

<div class="shop-list-wrap mb-30">
  <div class="row" style="border:1px solid #f1f1f1">
   <div class="col-xl-4 col-lg-5 col-md-6 col-sm-6" style="border: 1px solid #f1f1f1">
    <div class="product-list-img">
     <a href="/product-detail/{{$product->products_slug}}">
      <img src="{{asset($product->path)}}" alt="Product Style">
    </a>
    <div class="product-list-quickview">
      <a data-toggle="modal" data-target="#exampleModal" title="Quick View" data-myid="{{$product->products_id}}"><i class="la la-plus"></i></a>
    </div>
  </div>
</div>
<div class="col-xl-8 col-lg-7 col-md-6 col-sm-6">
  <div class="shop-list-content">
   <input type="hidden" id="stock{{$product->products_id}}" value="{{$stock}}">
   <h5 style="margin-bottom:0!important"><a href="/product-detail/{{$product->products_slug}}">{{substr(ucfirst(strtolower($product->products_name)),0,35) }}</a></h5>
   <h6 style="font-size: 10px; font-weight: bold; color: #999"> SKU - <span style="color: #999">{{$product->products_sku_code ?? ''}}</span></h6>
   
   <div class="pro-list-price">
    <span>₹{{$products_price}}</span>
    <!-- <span class="old-price">₹0</span> -->
  </div>
  <p><?php if($product->products_description!=null) echo ucfirst($product->products_description);  ?></p>
  <div class="product-list-action">
    <a title="Wishlist" onclick="addToWishlist({{$product->products_id}})"><i class="la la-heart-o"></i></a>
    <a title="Add To Cart" onclick="addToCart({{$product->products_id}},'normal','moq' @if(!empty(Auth::user()->id)) {{','.Auth::user()->id}} @endif)"><i class="la la-shopping-cart"></i></a>
  </div>
</div>
</div>
</div>
</div>

@endforeach
@endif

</div>
<div style="text-align: right">
  {{$searched_products->links()}}
</div>

</div>
</div>
</div>

</div>
</div>
</div>

@endsection

@section('script')
<script>

                // Add to Cart
                function addToCart(id,type,category){
                  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                  var quantity = 1;
                  var stock = $('#stock'+id).val();

                  if(stock == 'in'){

                    $.ajax({
                     url: '/add-to-cart',
                     type: 'POST',
                     data: {_token: CSRF_TOKEN, id: id, quantity: quantity, type: type, category: cateogry},
                     success: function (data) {
                      $('#cartItem').html(data.cartItem)
                      $('#cartItem2').html(data.cartItem)

                      $("#messageBox2").hide().slideDown();
                      setTimeout(function(){
                        $("#messageBox2").hide();        
                      }, 3000);
                      
                    },
                    failure: function (data) {
                      Swal(data.message);
                    }
                  });
                  }
                  else{
                    Swal('Product out of stock');
                  }
                }

   // Add to Wishlist
   function addToWishlist(id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    userId = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id;} else {echo 0;}?> ;

    if(userId == 0){
      Swal('Please Login First');
    }quantity     
    else{

     $.ajax({
       url: '/add-to-wishlist',
       type: 'POST',
       data: {_token: CSRF_TOKEN, id: id, userId : userId},
       success: function (data) {
           // alert(data.wishItem);
           $('#wishItem').html(data.wishItem);
           $('#wishItem2').html(data.wishItem);
         },
         failure: function (data) {
           Swal(data.message);
         }
       });
   }
 }

 $('.mainCategory').click(function(){
  var categoryId = $(this).attr("id");
  var categoryArray = categoryId.split("_");
  var cat = categoryArray[1];
  var min = $('#min-price').val();
  var max = $('#max-price').val();
  $.ajax({
   type: 'get',
   data_type: 'html',
   url: '/search-product/productsCat',
   data:{ cat_id:cat},

   success:function(response){
    $('#productCat').html(response);
    $('#category').empty();

  }
});
});

 $('#sort').on('change',(function(){
  var sort = $('#sort').val();
  var brand = $('#filter-brand').val();
  var keyword = $('#filter-search').val();
  var category_id = $('#category').val();
  var min = $('#min-price').val();
  var max = $('#max-price').val();
  var color_code = $('#pro-color').val();
    // var size_code = $('#pro-size').val();
    $.ajax({
     type: 'get',
     data_type: 'html',
     url: '/search-product/sort',
     data:{ sort_type:sort, cat_id :category_id,color: color_code, min_cost: min, max_cost: max, brand: brand, keyword: keyword},

     success:function(response){
      $('#productCat').html(response);
      $('#min-price').val(min);
      $('#max-price').val(max);
      $('#pro-color').val(color_code);
    }
  });
  }));

 function getBrandFilter(b_id){
  var sort = $('#sort').val();
  var category_id = $('#category').val();
  var min = $('#min-price').val();
  var max = $('#max-price').val();
  var brand = $('#brandFilter'+b_id).val();
  var keyword = $('#filter-search').val();
  var color_code = $('#pro-color').val();
  
  $('#filter-brand').val(brand);
  
  $.ajax({
   type: 'get',
   data_type: 'html',
   url: '/search-product/brand',
   data:{ sort_type:sort, cat_id :category_id,color: color_code, min_cost: min, max_cost: max, brand: brand, keyword: keyword},

   success:function(response){
    $('#productCat').html(response);
    $('#min-price').val(min);
    $('#max-price').val(max);
    $('#pro-color').val(color_code);
  }
});
};

$('.colorOnClick').on('click',(function(){
  var sort = $('#sort').val();
  var category_id = $('#category').val();
  var min = $('#min-price').val();
  var max = $('#max-price').val();
  var brand = $('#filter-brand').val();
  var keyword = $('#filter-search').val();
  var color_code = $(this).attr("id");

  $.ajax({
   type: 'get',
   url: '/search-product/colored',
   data:{ max_cost:max , min_cost:min, cat_id: category_id,color: color_code, brand: brand, sort_type: sort, keyword: keyword},
   success:function(response){
    $('#productCat').html(response);            
    $('#pro-color').val(color_code);            
  }
});
}));

function priceFilter(){
  var sort = $('#sort').val();
  var category_id = $('#category').val();
  var min = $('#min-price').val();
  var max = $('#max-price').val();
  var brand = $('#filter-brand').val();
  var keyword = $('#filter-search').val();
  var color_code = $('#pro-color').val();
  
  $.ajax({
   type: 'get',
   data_type: 'html',
   url: '/search-product/price-filter',
   data:{ max_cost:max , min_cost:min, cat_id: category_id,color: color_code, brand: brand, sort: sort, keyword: keyword},

   success:function(response){
    $('#productCat').html(response);
    $('#cat_id').val(category_id);
         // $('#pro-size').val(size_code);
         $('#pro-color').val(color_code);
       }
     });
}

$('#size').on('change',(function(){
    // var size_code = $('#size').val();
    var min = $('#min-price').val();
    var max = $('#max-price').val();
    var color_code = $('#pro-color').val();
    var category_id = $('#category').val();

    $.ajax({
     type: 'get',
     data_type: 'html',
     url: '/search-product/size',
     data:{ cat_id: category_id, color: color_code, min_cost: min, max_cost: max},
     success:function(response){
      $('#productCat').html(response);
      $('#cat_id').val(category_id);
      // $('#pro-size').val(size_code);
      $('#pro-color').val(color_code);
      console.log(response);
    }
  });
  }));

function getRange(){
  var amount = $('#amount').val();
  var amountArray = amount.split(" - ");
  var str1 = amountArray[0];
  str1 = str1.substring(1, 6);
  $('#min-price').val(str1);
  
  var str2 = amountArray[1];
  str2 = str2.substring(1, 6);
  $('#max-price').val(str2);
  priceFilter();
}

function getRange2(){
  var amount = $('#amount1').val();
  var amountArray = amount.split(" - ");
  var str1 = amountArray[0];
  str1 = str1.substring(1, 6);
  $('#min-price').val(str1);
  
  var str2 = amountArray[1];
  str2 = str2.substring(1, 6);
  $('#max-price').val(str2);
  priceFilter();
}
</script>

@endsection
