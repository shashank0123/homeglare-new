@extends('layouts.homeglare')

@section('content')

 	<div class="sell-area bg-img" style="background-image:url(/homeglare-new/images/bg/breadcrumb.jpg);">
 		<div class="container">
 			<div class="sell-area-text text-center">

 				<h2> "Welcome To Homeglare Store !<br> Register Here your shop for selling your Product on Homeglare."</h2>


 				<!--<button type="button" class="btn btn-large my-4 sell-btn btn-danger">Start selling</button>-->

 				
 			</div>
 		</div>
 	</div>



 	<div class="section-title-2 my-4 text-center">
 		<h2>Start selling on Homeglare today</h2>
 		<img src="/homeglare-new/images/icon-img/title-shape.png" alt="icon-img">
 	</div>


 	<div class="container">
 		<div class="row">

 			<div class="col-lg-4">
 				<div class="sellsec-3">
 					<div class="sellsec-3-img">
 						<img src="/homeglare-new/images/icon-img/repairing-service.jpeg">
 					</div>
 					<h2>SERVICE</h2>
 					<p>The administration we offer you is everything to us. Our expert site makes picking your apparatus as simple as could be expected under the circumstances. Putting in your request online is sheltered with our 100% verified site, so you can feel loose and certain your subtleties are completely secured</p>
 					
 				</div>
 				
 			</div>

 			<div class="col-lg-4">
 				<div class="sellsec-3">
 					<div class="sellsec-3-img">
 						<img src="/homeglare-new/images/icon-img/hand.jpeg">
 					</div>
 					<h2>PRICE</h2>
 					<p> We checks each value, every day to ensure we can bring our clients the most recent offers and extraordinary low costs.</p>
 					
 				</div>
 				
 			</div>


 			<div class="col-lg-4">
 				<div class="sellsec-3">
 					<div class="sellsec-3-img">
 						<img src="/homeglare-new/images/icon-img/conv.jpeg">
 					</div>
 					<h2>CONVEYANCE</h2>
 					<p> We pride ourselves on our conveyance administration where our group will convey your machine into a room of your decision. We guarantee clarifies our whole conveyance process and what makes us stand apart from the group.</p>
 					
 				</div>
 				
 			</div>

 		</div>
 		
 	</div>
	<section style="margin: 50px 20px">
 	<div class=" container get-in-touch-wrap">
        <h3>Get In Touch</h3>
        @if($errors->any())
          <h4>{{$errors->first()}}</h4>
        @endif
        <div class="contact-from contact-shadow">
            <form id="contact-form" method="post" action="{{ url('sell_with_us') }}">
             {{ csrf_field() }}
             <div class="row">
                <div class="col-lg-6">
                    <input name="name" id="name" class="input-field" type="text" placeholder="Name">
                </div>
                <div class="col-lg-6">
                    <input name="email" id="email" class="input-field" type="email" placeholder="Email">
                </div>
                <div class="col-lg-6">
                    <input name="company_name" id="company_name" class="input-field" type="text" placeholder="Company Name">
                </div>
                <div class="col-lg-6">
                    <input name="mobile" id="mobile" class="input-field" type="text" placeholder="Mobile">
                </div>

                <div class="col-lg-6">
                    <input name="gst" id="gst" class="input-field" type="text" placeholder="GST">
                </div>
                <div class="col-lg-6">
                    <input name="pincode" id="pincode" class="input-field" type="text" placeholder="Pincode">
                </div>

                <div class="col-lg-6">
                    <input name="city" id="city" class="input-field" type="text" placeholder="City">
                </div>
                <div class="col-lg-6">
                    <input name="state" id="state" class="input-field" type="text" placeholder="State">
                </div>
                {{-- <div class="col-lg-12">
                    <select name="subject" id="subject" class="input-field" placeholder="Subject">
                        <option> -- Select Subject --  </option>
                        <option value="Feedback Suggestion">Feedback Suggestion</option>
                        <option value="Become Partner">Become Partner</option>
                        <option value="Sell on Homeglare">Sell on Homeglare</option>
                        <option value="Sponsorship">Sponsorship</option>
                    </select>
                </div> --}}
                <div class="col-lg-12">
                    <textarea name="message_content" id="message_content" class="input-field" placeholder="Your Message"></textarea>
                </div>
                <div class="col-lg-12">
                    <button class="submit" type="submit">Send Message</button>
                </div>
            </div>
        </form>
        <p class="form-messege"></p>
    </div>



 	</section>
 	

 	


@endsection