@extends('layouts.homeglare')

@section('content')

<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"></link>
<style type="text/css">
	#accordion { width: 100%; }
	[data-toggle="collapse"]:after {
display: inline-block;
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  content: "\f054";
  transform: rotate(90deg) ;
  transition: all linear 0.25s;
  float: right;
  }   
[data-toggle="collapse"].collapsed:after {
  transform: rotate(0deg) ;
}
</style>

<div class="breadcrumb-area bg-img" style="background-image:url(/homeglare-new/images/bg/breadcrumb.jpg);">
            <div class="container">
                <div class="breadcrumb-content text-center">
                    <h2>FAQ page</h2>
                    <ul>
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li class="active">FAQ </li>
                    </ul>
                </div>
            </div>
        </div>

<div class="container mt-40 mb-40">
	<div class="row">
		<div id="accordion" role="tablist">
       @if($faq)
                                  @foreach($faq as $index => $faqData)
  <div class="card">
    <div class="card-header" role="tab" id="heading{{$index}}">
      <h5 class="mb-0">
        <a data-toggle="collapse" href="#collapse{{$index}}" aria-expanded="@if($index==0){{'true'}}@else{{'false'}}@endif " aria-controls="collapseOne">can i have multiple product at a time</a>
      </h5>
</div>

<div id="collapse{{$index}}" class="collapse @if($index==0){{'show'}}@endif" role="tabpanel" aria-labelledby="heading{{$index}}" data-parent="#accordion">
      <div class="card-body">
        Yes You can buy multiple prooduct at a time.
      </div>
</div>

</div>
@endforeach
@endif

</div>
</div>
		
	</div>
	
</div>

@endsection





