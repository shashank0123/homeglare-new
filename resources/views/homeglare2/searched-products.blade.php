@extends('layouts.homeglare')

@section('content')

<style type="text/css">
.bg-color { background-color: #000033!important; }

@media screen and (min-width: 991px)  { 
  #desktop-view{ display: block!important; }
  #mobile-view{ display: none!important; }
}

@media screen and (max-width: 991px)  {
  #desktop-view{ display: none!important; }
  #mobile-view{ display: block!important; }
} 

</style>

<div class="shop-area pt-90 pb-90">
 <div class="container">
  <h3>Search result for <i><b>'{{ucfirst($keyword)}}'</b></i></h3>
  <div class="row ">

   <div class="col-lg-12">
    <div class="shop-bottom-area">
     <div class="tab-content jump">
      <div id="shop-1" class="tab-pane active">
       <div class="row">
        <?php $i=0; ?>
        @if($products !=null)
        @foreach($products as $product)
        <?php
        $i++;
        ?>
        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">


          <?php
          $product_price = DB::table('products_price')->where('products_id',$product->products_id)->where('products_sell_type','sample')->first();
          $products_price = $product_price->products_price ?? $product->products_price;
          $products_price = $products_price + ($product->tax_percent*$products_price)/100;
          $products_price = number_format((float)$products_price, 2, '.', '');
          if(($product->products_quantity-$product->products_min_order)>0)
        $stock = "in";
         else
            $stock = 'out';
          ?>




          <div class="product-wrap product-border-1 product-img-zoom">
           <div class="product-img">
            <a href="/product-detail/{{$product->products_slug}}"><img src="{{url('/')}}/{{$product->path}}" alt="product" class="img-response"></a>
          </div>
          <div class="product-content product-content-padding">
            <input type="hidden" id="stock{{$product->products_id}}" value="{{$stock}}">
            <h5 style="margin-bottom:0!important"><a href="/product-detail/{{$product->products_slug}}">{{substr(ucfirst(strtolower($product->products_name)),0,35) }}</a></h5>
            <h6 style="font-size: 10px; font-weight: bold; color: #999"> SKU - <span style="color: #999">{{$product->products_sku_code ?? ''}}</span></h6>
            <div class="price-addtocart">
             <div class="product-price">
              <span>₹{{$products_price}}</span>
            </div>
          </div>
        </div>
        <div class="product-action-2">
         <a data-toggle="modal" data-target="#exampleModal" title="Quick View" data-myid="{{$product->products_id}}"><i class="la la-search"></i></a>
         <a title="Add To Cart" onclick="addToCart({{$product->products_id}},'normal','moq' @if(!empty(Auth::user()->id)) {{','.Auth::user()->id}} @endif)"><i class="la la-cart-plus"></i></a>
         <a title="Wishlist" onclick="addToWishlist({{$product->products_id}})"><i class="la la-heart-o"></i></a>
       </div>
     </div>



     <br>

   </div>
   @endforeach


   @endif                        
 </div>
</div>

@if($i==0)

<div style="text-align: center; font-size: 42px; margin: 100px 0">
  No Result Found
</div>
@endif

</div>
</div>
</div>

</div>
</div>
</div>

<a data-toggle="modal" id="show-alertsearch" data-target="#exampleModal2" href="#" style="display: none;">Product Stock</a>

@endsection


@section('script')


<script>

  function addToCart(id,type,category){
    var quantity = $('#quantity'+id).val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        // alert(id+" / "+quantity );
        var stock = $('#stock'+id).val();

        if(stock == 'in'){

          $.ajax({
           url: '/add-to-cart',
           type: 'POST',
           data: {_token: CSRF_TOKEN, id: id, quantity: quantity,type: type, category: category},
           success: function (data) {
           // alert(data.cartItem);
           $('#cartItem').html(data.cartItem)
            $("#messageBox").hide().slideDown();
   setTimeout(function(){
      $("#messageBox").hide();        
  }, 3000);
         },
         failure: function (data) {
           alert(data.message);
         }
       });

        }
        else{
        Swal('Product out of stock');
          $('#show-alertsearch').click();
        }
      }

      function addToWishlist(id){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        userId = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id;} else {echo 0;}?> ;

        if(userId == 0){
          Swal('Please Login First');
        }
        else{
        // alert(id);

        $.ajax({
         url: '/add-to-wishlist',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, userId : userId},
         success: function (data) {
           // alert(data.wishItem);
           $('#wishItem').html(data.wishItem);
         },
         failure: function (data) {
           Swal(data);
         }
       });
      }
    }
  </script>

  @endsection
