@extends('layouts.homeglare')

@section('content')

<div class="breadcrumb-area bg-img" style="background-image:url(/homeglare-new/images/bg/breadcrumb.jpg);">
    <div class="container">
        <div class="breadcrumb-content text-center">
            <h2>We are Hiring</h2>
            <ul>
                <li>
                    <a href="/">Home</a>
                </li>
                <li class="active">Careers </li>
            </ul>
        </div>
    </div>
</div>
<div class="contact-area pt-45 pb-90">
    <div class="container" style="padding: 0 25px">
        <div class="contact-info-wrap mb-50">
           <p>We are looking for people who are want to work in an healthy environment and contribute to the society in the process of getting money.</p>
           
        </div>
    <div class="get-in-touch-wrap">
        <h3>Apply Here</h3>
        <div class="contact-from contact-shadow">
            <form id="contact-form" method="post" action="{{ url('career') }}">
             {{ csrf_field() }}
             <div class="row">
                <div class="col-lg-6">
                    <input name="name" id="name" class="input-field" type="text" placeholder="Name" required="required">
                </div>
                <div class="col-lg-6">
                    <input name="email" id="email" class="input-field" type="email" placeholder="Email" required="required">
                </div>
                <div class="col-lg-12">
                    <select name="role" id="role" class="input-field" placeholder="Subject" required="required">
                        <option> -- Select Job role --  </option>
                        <option value="Digital Marketing">Digital Marketing</option>
                        <option value="Become Partner">Become Partner</option>
                        <option value="UI/UX Designer">UI/UX Designer</option>
                        <option value="Brand Ambassador">Brand Ambassador</option>
                        <option value="Operations">Operations</option>
                        <option value="Sells & Marketing">Sells & Marketing</option>
                        <option value="Call Center">Call Center</option>
                        <option value="Branding">Branding</option>
                        <option value="Logistics/Supply Chain">Logistics/Supply Chain</option>
                        <option value="Import/Export">Import/Export</option>
                        <option value="Packaging/Warehousing">Packaging/Warehousing</option>
                        <option value="HR">HR</option>
                        <option value="IT">IT</option>
                        <option value="Admin">Admin</option>
                        <option value="Product Development">Product Development</option>
                        <option value="Others">Others</option>
                    </select>
                </div>
                <div class="col-lg-6">
                    <input name="experience" id="experience" class="input-field" type="text" placeholder="Experience" required="required">
                </div>
                <div class="col-lg-6">
                    <input name="skills" id="skills" class="input-field" type="text" placeholder="Skills" required="required">
                </div>
                <div class="col-lg-12">
                    <textarea name="message" id="message" class="input-field" placeholder="Your Message" required="required"></textarea>
                </div>
                <div class="col-lg-12">
                    <button class="submit" type="submit">Send Message</button>
                </div>
            </div>
        </form>
        <p class="form-messege"></p>
    </div>
</div>
<div class="contact-map pt-90">
   <div class="embed-responsive  embed-responsive-21by9">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3498.961877376643!2d77.1050124145617!3d28.720685286837693!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d06b350000001%3A0x784e61ccea5aae77!2sArihant%20Steels!5e0!3m2!1sen!2sin!4v1577452205207!5m2!1sen!2sin" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
  </div>
  

</div>
</div>
</div>


@endsection

@section('script')
<script type="text/javascript">
@if(!empty(session()->get('careerSuccess')))
Swal('Thank you for your request.')
@endif
</script>
@endsection