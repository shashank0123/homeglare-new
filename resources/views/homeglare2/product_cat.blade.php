<div class="tab-content jump" id="productCat">
 <div id="shop-1" class="tab-pane active">
  <div class="row">
   @if($searched_products !=null)
   <?php $count=0; ?>
   @foreach($searched_products as $product)
   <?php
   $count++;
   $discount = $product->products_price;

   if($discount>0)
     $discount = $discount;
   ?>
   <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">     

    <?php
    $product_price = DB::table('products_price')->where('products_id',$product->products_id)->where('products_sell_type','sample')->first();
    $products_price = $product_price->products_price ?? $product->products_price;
    $products_price = $products_price + ceil(($product->tax_percent*$products_price)/100);
    // $products_price = number_format((float)$products_price, 2, '.', '');
    if(($product->products_quantity-$product->products_min_order)>0)
        $stock = "in";
         else
            $stock = 'out';
    ?>



    <div class="product-wrap product-border-1 product-img-zoom">
     <div class="product-img">
      <a href="/product-detail/{{$product->products_slug}}"><img src="{{url('/')}}/{{$product->path}}" alt="product" class="img-response"></a>
    </div>
    <div class="product-content product-content-padding">
      <input type="hidden" id="stock{{$product->products_id}}" value="{{$stock}}">
      <h5 style="margin-bottom:0!important"><a href="/product-detail/{{$product->products_slug}}">{{substr(ucfirst(strtolower($product->products_name)),0,35) }}</a></h5>
      <h6 style="font-size: 10px; font-weight: bold; color: #999"> SKU - <span style="color: #999">{{$product->products_sku_code ?? ''}}</span></h6>
      <div class="price-addtocart">
       <div class="product-price">
        <span>₹{{$products_price}}</span>
      </div>
    </div>
  </div>
  <div class="product-action-2">
   <a data-toggle="modal" data-target="#exampleModal" title="Quick View" data-myid="{{$product->products_id}}"><i class="la la-search"></i></a>
   <a title="Add To Cart" onclick="addToCart({{$product->products_id}},'normal','moq' @if(!empty(Auth::user()->id)) {{','.Auth::user()->id}} @endif)"><i class="la la-cart-plus"></i></a>
   <a title="Wishlist" onclick="addToWishlist({{$product->products_id}})"><i class="la la-heart-o"></i></a>
 </div>
</div>



<br>

</div>
@endforeach
@if($count==0)
<div style="text-align: center;margin-top: 50px"><h2>No product found</h2></div>
@endif
@endif                        
</div>
</div>
<div id="shop-2" class="tab-pane">

  @if($searched_products)
  <?php $count = 0; ?>
  @foreach($searched_products as $product)
  <?php $count++; ?>
  <?php
  $product_price = DB::table('products_price')->where('products_id',$product->products_id)->where('products_sell_type','sample')->first();
  $products_price = $product_price->products_price ?? $product->products_price;
  $products_price = $products_price + ceil(($product->tax_percent*$products_price)/100);
  // $products_price = number_format((float)$products_price, 2, '.', '');
  if(($product->products_quantity-$product->products_min_order)>0)
        $stock = "in";
         else
            $stock = 'out';
  ?>

  <div class="shop-list-wrap mb-30">
   <div class="row">
    <div class="col-xl-4 col-lg-5 col-md-6 col-sm-6">
     <div class="product-list-img">
      <a href="/product-detail/{{$product->products_slug}}">
       <img src="{{url('/'.$product->path)}}" alt="Product Style">
     </a>
     <div class="product-list-quickview">
       <a data-toggle="modal" data-target="#exampleModal" title="Quick View" data-myid="{{$product->products_id}}"><i class="la la-plus"></i></a>
     </div>
   </div>
 </div>
 <div class="col-xl-8 col-lg-7 col-md-6 col-sm-6">
   <div class="shop-list-content">
    <input type="hidden" id="stock{{$product->products_id}}" value="{{$stock}}">
    <h5 style="margin-bottom:0!important"><a href="/product-detail/{{$product->products_slug}}">{{substr(ucfirst(strtolower($product->products_name)),0,35) }}</a></h5>
    <h6 style="font-size: 10px; font-weight: bold; color: #999"> SKU - <span style="color: #999">{{$product->products_sku_code ?? ''}}</span></h6>
    <div class="pro-list-price">
     <span>₹{{$products_price}}</span>
     <!-- <span class="old-price">₹200.00</span> -->
   </div>
   <p><?php if($product->products_description!=null) echo ucfirst($product->products_description);  ?></p>
   <div class="product-list-action">
     <a title="Wishlist" onclick="addToWishlist({{$product->products_id}})"><i class="la la-heart-o"></i></a>
     <a title="Add To Cart" onclick="addToCart({{$product->products_id}},'normal','moq')"><i class="la la-shopping-cart"></i></a>
   </div>
 </div>
</div>
</div>
</div>
@endforeach

@if($count==0)
<div style="text-align: center;margin-top: 50px"><h2>No product found</h2></div>
@endif
@endif
</div>   
</div>
