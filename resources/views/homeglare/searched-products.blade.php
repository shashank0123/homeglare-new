@extends('layouts/ecommerce')

@section('content')


        <!-- Begin Hiraola's Breadcrumb Area -->
        <!-- <div class="breadcrumb-area">
            <div class="container">
                <div class="breadcrumb-content">
                    <h2>Shop</h2>
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li class="active">Shop Left Sidebar</li>
                    </ul>
                </div>
            </div>
        </div> -->
        <!-- Hiraola's Breadcrumb Area End Here -->

        <!-- Begin Hiraola's Content Wrapper Area -->
          @if($product)
        <div class="container">
          <div class="col-lg-12 order-1 order-lg-2">
              <div class="shop-product-wrap grid gridview-4 row">
                @foreach($product as $productDetails)
                  <div class="col-lg-4">
                    <div class="slide-item">
                        <div class="single_product">
                            <div class="product-img">
                                <a href="/product-detail/{{$productDetails->slug}}">
                                    <img class="primary-img" src="{{url('/')}}/{{$productDetails->image1}}" alt="Homeglare Product Image" style="width: 438px; height: 238px">
                                    <img class="secondary-img" src="{{url('/')}}/{{$productDetails->image2}}" alt="Homeglare Product Image" style="width: 438px; height: 238px">
                                </a>
                                <span class="sticker">New</span>
                                <div class="add-actions">
                                    <ul>
                                        <li><a class="hiraola-add_cart"  onclick="addToCart({{$productDetails->id}})" data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="ion-bag"></i></a></li>
                                        <li class="quick-view-btn" data-toggle="modal" data-target="#exampleModalCenter" data-myid="{{$productDetails->id}}"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Quick View"><i class="ion-eye"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="hiraola-product_content">
                                <div class="product-desc_info">
                                    <h6><a class="product-name"  href="/product-detail/{{$productDetails->slug}}">{{str_limit($productDetails->name, 18)}}</a></h6>
                                    <div class="price-box">
                                        <span class="new-price">Rs. {{$productDetails->sell_price}}</span>
                                    </div>
                                    <div class="additional-add_action">
                                        <ul>
                                            <li><a class="hiraola-add_compare"  onclick="addToWishlist({{$productDetails->id}})" data-toggle="tooltip" data-placement="top" title="Add To Wishlist"><i class="ion-android-favorite-outline"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="rating-box">
                                      @if($productDetails->Rating)
                                        <span class="fa fa-star {{$productDetails->Rating >= 1 ? 'star-checked' : ''}}"></span>
                                        <span class="fa fa-star {{$productDetails->Rating >= 2 ? 'star-checked' : ''}}"></span>
                                        <span class="fa fa-star {{$productDetails->Rating >= 3 ? 'star-checked' : ''}}"></span>
                                        <span class="fa fa-star {{$productDetails->Rating >= 4 ? 'star-checked' : ''}}"></span>
                                        <span class="fa fa-star {{$productDetails->Rating >= 5 ? 'star-checked' : ''}}"></span>
                                        @else
                                        <span class="invisible">Unrated</span>
                                      @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                  @endforeach
              </div>
          </div>
        </div>
        @endif
        <!-- Hiraola's Content Wrapper Area End Here -->
@if(!$product)
<div class="container text-center">
    <h2 class="text-danger">No Data Found</h2>
</div>
@endif

    <script>

    function addToCart(id){
        var quantity = $('#quantity'+id).val();
         var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        // alert(id+" / "+quantity );

      $.ajax({
         url: '/add-to-cart',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, quantity: quantity},
         success: function (data) {
           // alert(data.cartItem);
           $('#cartItem').html(data.cartItem)
         },
         failure: function (data) {
           alert(data.message);
         }
      });
    }

    function addToWishlist(id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      userId = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id;} else {echo 0;}?> ;

      if(userId == 0){
        alert('Please Login First');
      }
      else{
        // alert(id);

      $.ajax({
         url: '/add-to-wishlist',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, userId : userId},
         success: function (data) {
           // alert(data.wishItem);
           $('#wishItem').html(data.wishItem);
         },
         failure: function (data) {
           alert(data);
         }
      });
      }
   }
    </script>

@endsection
