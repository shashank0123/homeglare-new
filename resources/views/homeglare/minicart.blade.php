<?php
use App\Review;
$total = 0;
?>
<!-- <div class="row">
  <div class="col-md-6">
    <div class="">
      <h4>Shopping Cart</h4>
    </div>
  </div>
  <div class="col-md-6">
    <a href="#" class="btn-close"><i class="ion-android-close"></i></a>
  </div>
</div> -->

<div class="offcanvas-minicart_wrapper">
          <div class="offcanvas-menu-inner">
            <a href="javascript:void(0);" id="minicartA" class="btn-close"><i class="ion-android-close"></i></a>
            <div class="minicart-content">
              <div class="minicart-heading">
                <h4>Shopping Cart</h4>
              </div>
              @if($cartproducts != null)
                <?php $i=0;  ?>
              <ul class="minicart-list">
                @foreach($cartproducts as $product)

                <li class="minicart-product" id="listhide{{$product->id}}">
                  <a class="product-item_remove" href="javascript:void(0)"  onclick="deleteProduct({{$product->id}})"><i class="ion-android-close"></i></a>
                  <div class="product-item_img">
                    <img src="{{url('/')}}/{{$product->image1}}" alt="Hiraola's Product Image">
                  </div>
                  <div class="product-item_content">
                    <a class="product-item_title" href="/product-detail/{{$product->slug}}">{{strtoupper($product->name)}}</a>
                    <span class="product-item_quantity">{{$quantity[$i]}} x Rs. {{$product->sell_price}}</span>
                  </div>
                </li>
                <?php
                      $total  =$total + $product->sell_price * $quantity[$i];
                      $i++;
                      ?>
                @endforeach
              </ul>
              @endif
            </div>
            <div class="minicart-item_total">
              <span>Subtotal</span>
              <span class="ammount">Rs. <p id="total-bill">{{$total}}</p></span>
            </div>
            <div class="minicart-btn_area">
              <a href="<?php if(!empty(Auth::user()->id)){echo '/cart/'.Auth::user()->id;} else{ echo '/customer/login'; } ?>" class="hiraola-btn hiraola-btn_dark hiraola-btn_fullwidth">Cart</a>
            </div>
            <div class="minicart-btn_area">
              <a href="<?php if(!empty(Auth::user()->id)){echo '/checkout/'.Auth::user()->id;} else{ echo '/customer/login'; } ?>" class="hiraola-btn hiraola-btn_dark hiraola-btn_fullwidth">Checkout</a>
            </div>
          </div>
        </div>



        <script type="text/javascript">
          $("#minicartA").on("click", function(){
            $("#miniCart").removeClass('open');
          });
        </script>
