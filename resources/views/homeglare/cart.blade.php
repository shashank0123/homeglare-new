<?php
use App\Models\Product;
use App\Review;
$bill = 0;
$i=0;
?>

@extends('layouts/ecommerce')

@section('content')


<!-- Begin Hiraola's Breadcrumb Area -->
<!-- <div class="breadcrumb-area">
    <div class="container">
        <div class="breadcrumb-content">
            <h2>Other</h2>
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">Cart</li>
            </ul>
        </div>
    </div>
</div> -->
<!-- Hiraola's Breadcrumb Area End Here -->



<!-- Begin Hiraola's Cart Area -->
<div class="hiraola-cart-area">
    <div class="container">
        <div class="row">
            <div class="col-12">

                @if(session()->get('cart') != null)

                <?php

                $ids = array();
                $cat_id = array();
                $quantities = array();


                foreach(session()->get('cart') as $data)
                {
                    $ids[$i] = $data->product_id;
                    $quantities[$i] = $data->quantity;
                    $i++;
                }
                ?>

                <div class="table-content table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="hiraola-product-remove">remove</th>
                                <th class="hiraola-product-thumbnail">images</th>
                                <th class="cart-product-name">Product</th>
                                <th class="hiraola-product-price">Unit Price</th>
                                <th class="hiraola-product-quantity">Quantity</th>
                                <th class="hiraola-product-subtotal">Total</th>
                            </tr>
                        </thead>
                        <tbody>

                            @for($j=0 ; $j<$i ; $j++ )
                            <?php
                            $product = Product::where('id',$ids[$j])->first();
                            if($product != null)
                            {
                                $cat_id[$j] = $product->category_id;
                            }
                            else
                            {
                                $cat_id[$j] = 0;
                            }
                            ?>

                            <tr id="cart{{$ids[$j]}}">
                                <td class="hiraola-product-remove"><a  onclick="deleteCartProduct({{$ids[$j]}})"><i class="fa fa-trash"
                                    title="Remove"></i></a></td>

                                    <td class="hiraola-product-thumbnail"><a href="javascript:void(0)"><img src="{{url('/')}}/{{$product->image1}}" alt="Hiraola's Cart Thumbnail" style="width: 120px; height: 120px"></a></td>

                                    <td class="hiraola-product-name"><a href="javascript:void(0)">{{strtoupper($product->name)}}</a></td>
                                    <td class="hiraola-product-price"><span class="amount">Rs. {{$product->sell_price}}</span></td>

                                    <td class="quantity">

                                        <span class="amount">
                                                <i class="fa fa-minus" onclick="decrement({{$product->id}})"></i>&nbsp;
                                                <input type="text" name="quantity" id="quantity{{$product->id}}" value="{{$quantities[$j]}}" style="width: 60px; text-align: center;" onkeyup="alert({{$product->id}})"/>&nbsp;
                                                <i class="fa fa-plus" onclick="increment({{$product->id}})"></i>
                                            </span>

                                        {{-- <div class="cart-plus-minus">
                                            <input class="cart-plus-minus-box" value="{{$quantities[$j]}}" type="text" id="quantity{{$ids[$j]}}">
                                            <div class="dec qtybutton"><i class="fa fa-angle-down"></i></div>
                                            <div class="inc qtybutton"><i class="fa fa-angle-up" onclick="increment({{$ids[$j]}})"></i></div>
                                        </div> --}}
                                    </td>
                                    <?php
                                    $total = $quantities[$j]*$product->sell_price;
                                    $bill =$bill +  $total;
                                    ?>
                                    <td class="product-subtotal"><span class="amount" id="amount{{$product->id}}">Rs. {{$total}}.00</span></td>

                                </tr>

                                @endfor

                            </tbody>
                        </table>
                    </div>
                            {{-- <div class="row">
                                <div class="col-12">
                                    <div class="coupon-all">
                                        <div class="coupon">
                                            <input id="coupon_code" class="input-text" name="coupon_code" value="" placeholder="Coupon code" type="text">
                                            <input class="button" name="apply_coupon" value="Apply coupon" type="submit">
                                        </div>
                                        <div class="coupon2">
                                            <input class="button" name="update_cart" value="Update cart" type="submit">
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                            <form action="javascript:void(0)">
                                <div class="row">
                                    <div class="col-md-5 ml-auto">
                                        <div class="cart-page-total">
                                            <h2>Cart totals</h2>
                                            <ul>
                                                <li>Subtotal <span id="total_price">Rs. {{$bill}}.00</span></li>
                                                <li>Total <span id="grand_total">Rs. {{$bill}}.00</span></li>
                                            </ul>
                                            <a id="checkoutLink" href="<?php if(!empty(Auth::user()->id)){echo '/checkout/'.Auth::user()->id;} else{ echo '/customer/login'; }?>">Proceed to checkout</a>

                                        </div>
                                    </div>
                                </div>
                            </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!-- Hiraola's Cart Area End Here -->

            @endsection


            @section('js-script')

            <script>
        // var count=<?php echo session()->get('count'); ?>;

        function deleteCartProduct(id){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    // count--;
    $('#cart'+id).hide();

    $.ajax({
        /* the route pointing to the post function */
        url: '/cart/delete-product',
        type: 'POST',
        datatype: 'JSON',
        /* send the csrf-token and the input to the controller */
        data: {_token: CSRF_TOKEN, id: id},
        success: function (data) {
            $('#cartItem').html(data.cartItem);
            $("#total_price").empty();
            $("#total_price").append("Rs. " + data.bill);
            $("#grand_total").html("Rs. " + data.bill);
            if(data.bill <= 0){
              $("#checkoutLink").attr("href", "/");
              $("#checkoutLink").html('Continue Shopping');
            }
                }
            });
}


        function decrement(id){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            // alert(id);
            var action = 'minus' ;
            var quantity = $('#quantity'+id).val();
            if(quantity>1){
                // alert(quantity);
            $('#quantity'+id).val(--quantity);
            $.ajax({
             url: '/update-cart',
             type: 'POST',
             data: {_token: CSRF_TOKEN, id: id, action: action},
             success: function (data) {
               // alert(data.bill);
               $("#total_price").empty();
            $("#total_price").append("Rs. " + data.bill);
            $("#grand_total").html("Rs. " + data.bill);
            $("#amount"+id).html("Rs. " + data.total);


           },
           failure: function (data) {
               alert(data.message);
           }
       });
        }
        }

        function increment(id){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            // alert(id);
            var quantity = $('#quantity'+id).val();
            var action = 'plus' ;

                // alert(quantity);

            $('#quantity'+id).val(++quantity);
            $.ajax({
             url: '/update-cart',
             type: 'POST',
             data: {_token: CSRF_TOKEN, id: id, action:action},
             success: function (data) {
               // alert('done');
            $("#total_price").empty();
            $("#total_price").append("Rs. " + data.bill);
            $("#grand_total").html("Rs. " + data.bill);
            $("#amount"+id).html("Rs. " + data.total);

           },
           failure: function (data) {
               alert('Something went wrong');
           }
       });
            }




</script>

@endsection
