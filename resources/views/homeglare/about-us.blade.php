@extends('layouts/ecommerce')

@section('content')
        <!-- Begin Hiraola's About Us Area -->
        <div class="about-us-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 d-flex align-items-center">
                        <div class="overview-content">
                            @if($aboutCompany)
                              <h2><span>About Us</span></h2>
                              <p class="short_desc"><?php echo $aboutCompany->about_us; ?> </p>
                            @else
                            <h2>Welcome To <span>{{env('APP_NAME')}}</span></h2>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
