@extends('layouts/ecommerce')

@section('content')
      <main class="page-content">
        <div class="account-page-area">
          <div class="container">
            @if($helpSupport)
            <div class="row">
              <div class="col-12">
                <h3 class="contact-page-title text-center mb-2">Subject : {{$helpSupport->subject}}</h3>
                @if($replyMessages)
                @foreach($replyMessages as $reply)
                <div class="row justify-content-end">
                  <div class="col offset-md-8">
                    <p class="d-block badge badge-pill {{$reply->reply_from == 'admin' ? 'float-left badge-success' : 'float-right badge-info'}}">{{$reply->message}}</p>
                  </div>
                </div>
                @endforeach

                @endif
                <form method="post" action="{{ route('reply.store') }}">
                     {{ csrf_field() }}
                     <input type="text" name="token" value="{{$helpSupport->token}}" hidden>
                     <div class="form-group row justify-content-end">
                       <label for="message_content" class="col-sm-2 offset-md-6 col-form-label"></label>
                       <div class="col-sm-4">
                         <textarea rows="4" cols="40" class="form-control @error('message_content') is-invalid @enderror" name="message_content" id="message_content" placeholder="Your Query" required>{{old('message_content')}}</textarea>
                         @error('message_content')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                       </div>
                     </div>

                  <div class="form-group row float-right">
                    <div class="col ">
                      <button type="submit" class="btn btn-primary">Send</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            @else
              <h4 class="badge badge-pill badge-danger">Something Went Wrong</h4>
            @endif
          </div>
        </div>
      </main>
@endsection
