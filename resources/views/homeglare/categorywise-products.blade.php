@extends('layouts/ecommerce')

@section('content')


<!-- Begin Hiraola's Breadcrumb Area -->
<!-- <div class="breadcrumb-area">
    <div class="container">
        <div class="breadcrumb-content">
            <h2>Shop</h2>
            <ul>
                <li><a href="/">Home</a></li>
                <li class="active">Shop Left Sidebar</li>
            </ul>
        </div>
    </div>
</div> -->
<!-- Hiraola's Breadcrumb Area End Here -->

<!-- Begin Hiraola's Content Wrapper Area -->
<div class="hiraola-content_wrapper">
    <input type="text" name="category" hidden id="category" value="{{$id}}">
    <input type="text" name="pro-color" hidden id="pro-color" value="">
    <input type="text" name="pro-rating" hidden id="pro-rating" value="">
    <input type="text" name="min-price" hidden id="min-price" value="20">
    <input type="text" name="max-price" hidden id="max-price" value="1000">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 order-2 order-lg-1">
                <div class="hiraola-sidebar-catagories_area">

                  <div class="category-module hiraola-sidebar_categories">
                      <div class="category-module_heading">
                          <h5>Categories</h5>
                      </div>
                      <div class="module-body" style="max-height: 300px; overflow: auto">
                          <ul class="module-list_item">
                              <?php $c = 0; ?>
                              @if(!empty($cat_ids))
                              @foreach($cat_ids as $cat)
                              <li>
                                  <a href="javascript:void(0)" class="mainCategory" id="category_{{$cat->id}}">{{$cat->category_name}} {{-- ({{$cat->ProductCount}}) --}}</a>
                              </li>
                              @endforeach
                              @endif
                          </ul>
                      </div>
                  </div>
                  
                    <div class="hiraola-sidebar_categories">
                        <div class="hiraola-categories_title">
                            <h5>Price</h5>
                        </div>
                        <div class="price-filter">
                            <div id="slider-range"></div>
                            <div class="price-slider-amount">
                                <div class="label-input">
                                    <label>price : </label>
                                    <input type="text" id="amount" name="price" placeholder="Add Your Price" />
                                </div>
                                <!-- <button type="button">Filter</button> -->
                            </div>
                        </div>
                    </div>

                    @if(!empty($brand))
                    <div class="hiraola-sidebar_categories">
                        <div class="hiraola-categories_title">
                            <h5>Brand</h5>
                        </div>
                        <ul class="sidebar-checkbox_list">
                            @foreach($brand as $bran)
                            <li>
                                <a href="javascript:void(0)">{{$bran}}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @endif


                    @if(!empty($material))
                    <div class="hiraola-sidebar_categories">
                        <div class="hiraola-categories_title">
                            <h5>Material</h5>
                        </div>
                        <ul class="sidebar-checkbox_list">
                            @foreach($material as $mate)
                            <li>
                                <a href="javascript:void(0)">{{$mate}}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    @if(!empty($color))
                    <div class="hiraola-sidebar_categories">
                        <div class="hiraola-categories_title">
                            <h5>Color</h5>
                        </div>
                        <ul class="sidebar-checkbox_list">
                            @foreach($color as $col)
                            <li>
                                <a href="javascript:void(0)" class="colorOnClick" id="{{$col}}">{{$col}}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <!-- <div class="hiraola-sidebar_categories">
                        <div class="hiraola-categories_title">
                            <h5>Rating</h5>
                        </div>
                        <ul class="sidebar-checkbox_list">
                            <li class="clearfix">
                                <a href="javascript:void(0)">
                                  <span class="float-left">1</span>
                                  <span class="float-right">
                                    <span class="fa fa-star star-checked"></span>
                                    <span class="fa fa-star "></span>
                                    <span class="fa fa-star "></span>
                                    <span class="fa fa-star "></span>
                                    <span class="fa fa-star "></span>
                                  </span>
                                </a>
                            </li>
                            <li class="clearfix">
                              <a href="javascript:void(0)">
                                <span class="float-left">2</span>
                                <span class="float-right">
                                  <span class="fa fa-star star-checked"></span>
                                  <span class="fa fa-star star-checked"></span>
                                  <span class="fa fa-star "></span>
                                  <span class="fa fa-star "></span>
                                  <span class="fa fa-star "></span>
                                </span>
                              </a>
                            </li>
                            <li class="clearfix">
                              <a href="javascript:void(0)">
                                <span class="float-left">3</span>
                                <span class="float-right">
                                  <span class="fa fa-star star-checked"></span>
                                  <span class="fa fa-star star-checked"></span>
                                  <span class="fa fa-star star-checked"></span>
                                  <span class="fa fa-star "></span>
                                  <span class="fa fa-star "></span>
                                </span>
                              </a>
                            </li>
                            <li class="clearfix">
                              <a href="javascript:void(0)">
                                <span class="float-left">4</span>
                                <span class="float-right">
                                  <span class="fa fa-star star-checked"></span>
                                  <span class="fa fa-star star-checked"></span>
                                  <span class="fa fa-star star-checked"></span>
                                  <span class="fa fa-star star-checked"></span>
                                  <span class="fa fa-star "></span>
                                </span>
                              </a>
                            </li>
                            <li class="clearfix">
                              <a href="javascript:void(0)">
                                <span class="float-left">5</span>
                                <span class="float-right">
                                  <span class="fa fa-star star-checked"></span>
                                  <span class="fa fa-star star-checked"></span>
                                  <span class="fa fa-star star-checked"></span>
                                  <span class="fa fa-star star-checked"></span>
                                  <span class="fa fa-star star-checked"></span>
                                </span>
                              </a>
                            </li>
                        </ul>
                    </div> -->
                </div>
            </div>
            <div class="col-lg-9 order-1 order-lg-2">
                <div class="shop-toolbar">
                    <div class="product-view-mode">
                        <a class="active grid-3" data-target="gridview-3" data-toggle="tooltip" data-placement="top" title="Grid View"><i class="fa fa-th"></i></a>
                        <a class="list" data-target="listview" data-toggle="tooltip" data-placement="top" title="List View"><i class="fa fa-th-list"></i></a>
                    </div>
                    <div class="product-item-selection_area">
                        <div class="product-short">
                            <label class="select-label">Short By:</label>
                            <select class="nice-select" id="sort">
                                <option value="created">Relevance</option>
                                <option value="name-asc">Name, A to Z</option>
                                <option value="name-desc">Name, Z to A</option>
                                <option value="low-high">Price, low to high</option>
                                <option value="high-low">Price, high to low</option>
                                {{-- <option value="5">Rating (Highest)</option>
                                <option value="5">Rating (Lowest)</option> --}}
                                {{-- <option value="5">Model (A - Z)</option>
                                <option value="5">Model (Z - A)</option> --}}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="shop-product-wrap grid gridview-3 row" id="productCat">
                    @if($searched_products != null)
                    @foreach($searched_products as $product)
                    <div class="col-lg-4">
                        <div class="slide-item">
                            <div class="single_product">
                                <div class="product-img">
                                    <a href="/product-detail/{{$product->slug}}">
                                        <img class="primary-img" src="{{url('/')}}/{{$product->image1}}" alt="Hiraola's Product Image" style="width: 438px; height: 238px;">
                                        <img class="secondary-img" src="{{url('/')}}/{{$product->image2}}" alt="Hiraola's Product Image" style="width: 438px; height: 238px;">
                                    </a>
                                    <div class="add-actions">
                                        <ul>
                                            <li><a class="hiraola-add_cart" data-toggle="tooltip" data-placement="top" title="Add To Cart" onclick="addToCart({{$product->id}})"><i class="ion-bag"></i></a>
                                            </li>
                                            {{-- <li><a class="hiraola-add_compare" href="" data-toggle="tooltip" data-placement="top" title="Compare This Product"><i
                                                class="ion-ios-shuffle-strong"></i></a>
                                            </li> --}}
                                            <li class="quick-view-btn" data-toggle="modal" data-target="#exampleModalCenter" data-myid="{{$product->id}}"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Quick View"><i
                                                class="ion-eye"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="hiraola-product_content">
                                        <div class="product-desc_info">
                                            <h6><a class="product-name" href="/product-detail/{{$product->slug}}">{{strtoupper(substr($product->name,0,18))}} <?php strlen($product->name)>18 ? "...":null; ?></a></h6>
                                            <div class="price-box">
                                                <span class="new-price">Rs. {{$product->sell_price}}</span>
                                            </div>
                                            <div class="additional-add_action">
                                                <ul>
                                                    <li><a class="hiraola-add_compare" data-toggle="tooltip" data-placement="top" title="Add To Wishlist" onclick="addToWishlist({{$product->id}})"><i
                                                        class="ion-android-favorite-outline"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="rating-box">
                                              @if($product->Rating)
                                                <span class="fa fa-star {{$product->Rating >= 1 ? 'star-checked' : ''}}"></span>
                                                <span class="fa fa-star {{$product->Rating >= 2 ? 'star-checked' : ''}}"></span>
                                                <span class="fa fa-star {{$product->Rating >= 3 ? 'star-checked' : ''}}"></span>
                                                <span class="fa fa-star {{$product->Rating >= 4 ? 'star-checked' : ''}}"></span>
                                                <span class="fa fa-star {{$product->Rating >= 5 ? 'star-checked' : ''}}"></span>
                                              @else
                                              <span class="invisible">Unrated</span>
                                              @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="list-slide_item">
                                <div class="single_product">
                                    <div class="product-img">
                                        <a href="/product-detail/{{$product->slug}}">
                                            <img class="primary-img" src="{{url('/')}}/{{$product->image1}}" alt="Hiraola's Product Image" style="width: 438px; height: 230px;">
                                            <img class="secondary-img" src="{{url('/')}}/{{$product->image2}}" alt="Hiraola's Product Image" style="width: 438px; height: 230px;">
                                        </a>
                                    </div>
                                    <div class="hiraola-product_content">
                                        <div class="product-desc_info">
                                            <h6><a class="product-name" href="/product-detail/{{$product->slug}}">{{strtoupper( substr($product->name,0,18))}} <?php strlen($product->name)>18 ? "...":null; ?></a></h6>
                                            <div class="rating-box">
                                              @if($product->Rating)
                                                <span class="fa fa-star {{$product->Rating >= 1 ? 'star-checked' : ''}}"></span>
                                                <span class="fa fa-star {{$product->Rating >= 2 ? 'star-checked' : ''}}"></span>
                                                <span class="fa fa-star {{$product->Rating >= 3 ? 'star-checked' : ''}}"></span>
                                                <span class="fa fa-star {{$product->Rating >= 4 ? 'star-checked' : ''}}"></span>
                                                <span class="fa fa-star {{$product->Rating >= 5 ? 'star-checked' : ''}}"></span>
                                              @endif
                                                <!-- <ul>
                                                    <li><i class="fa fa-star-of-david"></i></li>
                                                    <li><i class="fa fa-star-of-david"></i></li>
                                                    <li><i class="fa fa-star-of-david"></i></li>
                                                    <li><i class="fa fa-star-of-david"></i></li>
                                                    <li class="silver-color"><i class="fa fa-star-of-david"></i></li>
                                                </ul> -->
                                            </div>
                                            <div class="price-box">
                                                <span class="new-price">Rs. {{$product->sell_price}}</span>
                                            </div>
                                            <div class="product-short_desc">
                                                <p>{{$product->short_descriptions}}</p>
                                            </div>
                                        </div>
                                        <div class="add-actions">
                                            <ul>
                                                <li><a class="hiraola-add_cart" data-toggle="tooltip" data-placement="top" title="Add To Cart" onclick="addToCart({{$product->id}})">Add To Cart</a></li>
                                                {{-- <li><a class="hiraola-add_compare" href="" data-toggle="tooltip" data-placement="top" title="Compare This Product"><i
                                                    class="ion-ios-shuffle-strong"></i></a></li> --}}
                                                    <li class="quick-view-btn" data-toggle="modal" data-target="#exampleModalCenter" data-myid="{{$product->id}}"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Quick View"><i
                                                        class="ion-eye"></i></a></li>
                                                        <li><a class="hiraola-add_compare" data-toggle="tooltip" data-placement="top" title="Add To Wishlist" onclick="addToWishlist({{$product->id}})"><i
                                                            class="ion-android-favorite-outline"></i></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @endif
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Hiraola's Content Wrapper Area End Here -->

                @endsection

                @section('js-script')
                <script>

                // Add to Cart
    function addToCart(id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        // alert(id);
        var quantity = 1;

      $.ajax({
         url: '/add-to-cart',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, quantity: quantity},
         success: function (data) {
           // alert(data.cartItem);
           $('#cartItem').html(data.cartItem)
         },
         failure: function (data) {
           Swal(data.message);
         }
      });
   }

   // Add to Wishlist
   function addToWishlist(id){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      userId = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id;} else {echo 0;}?> ;

      if(userId == 0){
        Swal('Please Login First');
      }
      else{
        // alert(id);

      $.ajax({
         url: '/add-to-wishlist',
         type: 'POST',
         data: {_token: CSRF_TOKEN, id: id, userId : userId},
         success: function (data) {
           // alert(data.wishItem);
           $('#wishItem').html(data.wishItem);
         },
         failure: function (data) {
           Swal(data.message);
         }
      });
      }
   }

   $('.mainCategory').click(function(){
     var categoryId = $(this).attr("id");
      var categoryArray = categoryId.split("_");
    var cat = categoryArray[1];

    var min = $('#min-price').val();
    var max = $('#max-price').val();
    $.ajax({
        type: 'get',
        data_type: 'html',
        url: '/search-product/productsCat',
        data:{ cat_id:cat},

        success:function(response){
            $('#productCat').html(response);
            $('#category').empty();
            // $('#category').val(send_id);
            // console.log(response);
        }
    });
});
{{--
@foreach($maincategory as $row)
$('#cat{{$row->id}}').click(function(){
    var cat = {{$row->id}};


    $.ajax({
        type: 'get',
        data_type: 'html',
        url: '/search-product/productsCat',
        data:{ cat_id:cat},

        success:function(response){
            $('#productCat').html(response);
            $('#category').empty();
            // $('#category').val(send_id);
            console.log(response);
        }
    });
});
@endforeach
--}}

$('#sort').on('change',(function(){
    var sort = $('#sort').val();
    var category_id = $('#category').val();
    var min = $('#min-price').val();
    var max = $('#max-price').val();
    // var min = $("#slider-range").slider("option", "min");
    // var max = $("#slider-range").slider("option", "max");
    var color_code = $('#pro-color').val();
    var size_code = $('#pro-size').val();
    $.ajax({
        type: 'get',
        data_type: 'html',
        url: '/search-product/sort',
        data:{ sort_type:sort, cat_id :category_id,color: color_code, size: size_code, min_cost: min, max_cost: max},

        success:function(response){
            $('#productCat').html(response);
            // $('#category').val(category_id);
            $('#min-price').val(min);
            $('#max-price').val(max);
            $('#pro-color').val(color_code);
            // $('#pro-size').val(size_code);
            // console.log(response);
        }
    });
}));

$('.colorOnClick').on('click',(function(){
    var min = $('#min-price').val();
    var max = $('#max-price').val();
    // var min = $("#slider-range").slider("option", "min");
    // var max = $("#slider-range").slider("option", "max");
    var color_code = $(this).attr("id");
    var size_code = $('#pro-size').val();
    var category_id = $('#category').val();
    var val = $('#slider-range').slider("option", "value");

    $.ajax({
        type: 'get',
        data_type: 'html',
        url: '/search-product/colored',
        data:{ color: color_code, cat_id: category_id, min_cost: min, max_cost: max, size: size_code},
        success:function(response){
            $('#productCat').html(response);
            // $('#category').val(category_id);
            $('#pro-color').val(color_code);
            // $('#pro-size').val(size_code);
            // console.log(response);
        }
    });
}));

// $("#slider-range").on("change", function() {
//   priceFilter();
// });
function priceFilter(){
    var min = $('#min-price').val();
    var max = $('#max-price').val();
    // var min = $("#slider-range").slider("option", "min");
    // var max = $("#slider-range").slider("option", "max");
    var color_code = $('#pro-color').val();
    var size_code = $('#pro-size').val();
    var category_id = $('#category').val();
    $.ajax({
        type: 'get',
        data_type: 'html',
        url: '/search-product/price-filter',
        data:{ max_cost:max , min_cost:min, cat_id: category_id,color: color_code,size: size_code},

        success:function(response){
            $('#productCat').html(response);
            // $('#category').val(category_id);
            // $('#min-price').val(min);
            // $('#max-price').val(max);
            // $('#pro-color').val(color_code);
            // $('#pro-size').val(size_code);
            // console.log(response);
        }
    });
}

$('#size').on('change',(function(){
    var size_code = $('#size').val();
    var min = $('#min-price').val();
    var max = $('#max-price').val();
    var color_code = $('#pro-color').val();
    var category_id = $('#category').val();

    $.ajax({
        type: 'get',
        data_type: 'html',
        url: '/search-product/size',
        data:{ size: size_code, cat_id: category_id, color: color_code, min_cost: min, max_cost: max},
        success:function(response){
            $('#productCat').html(response);
            $('#cat_id').val(category_id);
            $('#pro-size').val(size_code);
            $('#pro-color').val(color_code);
            console.log(response);
        }
    });
}));

$('#sort').on('change',(function(){
      var sort = $('#sort').val();
      alert(sort);
      var size_code = $('#size').val();
    var min = $('#min-price').val();
    var max = $('#max-price').val();
    var color_code = $('#pro-color').val();
    var category_id = $('#category').val();

      $.ajax({
         type: 'get',
         data_type: 'html',
         url: '/search-product/sort',
        data:{ size: size_code, sort_type: sort, cat_id: category_id, color: color_code, min_cost: min, max_cost: max},
        success:function(response){
            $('#productCat').html(response);
            $('#cat_id').val(category_id);
            $('#pro-size').val(size_code);
            $('#pro-color').val(color_code);
            console.log(response);
          }
      });
   }));

</script>

                @endsection
