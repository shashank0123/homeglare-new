<?php 
// use App\Wishlist;
use App\Models\Core\Setting;
$company = new StdClass;
$address = "";
$res = Setting::where('name','contact_us_email')->first();
$company->support_email = $res->value ?? '';
$res = Setting::where('name','phone_no')->first();
$company->contact_no = $res->value ?? '';
$res = Setting::where('name','address')->first();
$address = $address.$res->value.", " ?? '';
$res = Setting::where('name','state')->first();
$address = $address.$res->value.", " ?? '';
$res = Setting::where('name','city')->first();
$address = $address.$res->value.", " ?? '';
$res = Setting::where('name','zip')->first();
$address = $address.$res->value ?? '';
$company->address = $address ?? '';

if(isset($company))
  $contact = explode(',',$company->contact_no);
$count = 0;

if(!empty(session()->get('cart'))){
  foreach(session()->get('cart') as $cart){
    $count++;
  }
}

?>
<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta name="keywords" content="HomeGlare-Ecommerce">
  
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Home Glare | @yield('page_title')</title>
  <!-- <meta name="description" content="@yield('page_discription')"> -->
  <link rel="canonical" href="@yield('page_URL')" />
  <!-- meta tags for social share -->
  <meta property="og:image" content="@yield('social_image')">
  <meta property="og:title" content="@yield('social_title')">
  <meta property="og:description" content="@yield('social_description')">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Favicon -->
  <link rel="shortcut icon" type="image/x-icon" href="/homeglare-new/images/logo/favicon.png">
    <!-- CSS
      ============================================ -->
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="/homeglare-new/css/vendor/bootstrap.min.css">
      <!-- Icon Font CSS -->
      <link rel="stylesheet" href="/homeglare-new/css/vendor/line-awesome.css">
      <link rel="stylesheet" href="/homeglare-new/css/vendor/themify.css">
      <!-- othres CSS -->
      <link rel="stylesheet" href="/homeglare-new/css/plugins/animate.css">
      <link rel="stylesheet" href="/homeglare-new/css/plugins/owl-carousel.css">
      <link rel="stylesheet" href="/homeglare-new/css/plugins/slick.css">
      <link rel="stylesheet" href="/homeglare-new/css/plugins/magnific-popup.css">
      <link rel="stylesheet" href="/homeglare-new/css/plugins/jquery-ui.css">
      <link rel="stylesheet" href="/homeglare-new/css/style.css">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&display=swap" rel="stylesheet">
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-154696872-2"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-154696872-2');
      </script>

      <style>
      .swal2-popup .swal2-styled.swal2-confirm { background-color: #d20e14 !important }
      .owl-nav .la { margin-top: 10px; }
      #pdDescription p {
        font-size: 14px !important;
        margin-bottom: 0 !important;
      }

      .swal2-title{
        font-size: 24px!important; padding: 20px 5px !important;
      }
    </style>
  </head>
  <body onload="checkCart()">
    <div class="main-wrapper">

      <header class="header-area bg-black sticky-bar header-padding-1">

        <div class="main-header-wrap">

          <div class="top-head" style="background-color: #ddd">
            <a href="https://t.me/Homeglare" style="padding-right: 1%" target="blank">
              <img src="{{asset('/logo/telegram.png')}}" style="width: 25px; height: auto">&nbsp;&nbsp;Homeglare
            </a>
            <a href="https://api.whatsapp.com/send?phone=8585900059&text=Hey" target="blank">
              <img src="{{asset('/logo/icon.png')}}" style="width: 25px; height: auto">
            8585900059</a>
          </div>

          <div class="container-fluid">
            <div class="row align-items-center">
              <div class="col-xl-2 col-lg-3 logo-border">
                <div class="logo ">
                  <a style="font-family: 'Azonix Regular'; color: white; font-size: 35px" href="/">{{-- <img src="/homeglare-new/images/logo/logo.png" alt="logo"> --}}HOMEGLARE</a>
                </div>
              </div>
              <div class="col-xl-8 col-lg-6 d-flex justify-content-center">
                <div class="main-menu menu-common-style menu-lh-2 menu-margin-2 menu-white">
                  <nav>
                    <ul>
                      <li><a href="/">Home</a></li>
                      <li><a href="/about">About us </a></li>
                      {{-- 
                        <li><a href="/productw">Products</a></li>
                        --}}
                        <li><a href="/contact">Contact Us</a></li>
                        <li><a href="/sell-on-homeglare">Sell with Homeglare</a></li>
                        <li><a href="/career">We are Hiring</a></li>
                        @if(!empty(Auth::user()->id))
                        <li class="angle-shape">
                          <a href="#">{{ucfirst(explode(" ",Auth::user()->first_name ?? '')[0])}}
                          </a>
                          <ul class="submenu">
                            <li><a href="/account/{{Auth::user()->id}}">Dashboard</a></li>
                            <li class="active">
                              <a href="{{ route('logout') }}"
                              onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                              {{'Logout'}}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                            </form>
                          </li>
                        </ul>
                      </li>
                      @else
                      <li><a href="{{url('customer/login')}}">Login</a></li>
                      {{-- 
                        <li><a href="{{url('customer/login')}}">Sign Up</a></li>
                        --}}
                        @endif
                        {{-- 
                          <li class="angle-shape">
                            <a href="shop.html">Shop </a>
                            <ul class="mega-menu">
                              <li>
                                <a class="menu-title" href="#">Shop Layout</a>
                                <ul>
                                  <li><a href="shop.html">standard style</a></li>
                                  <li><a href="shop-2.html">standard style 2</a></li>
                                  <li><a href="shop-2-col.html">shop 2 column</a></li>
                                  <li><a href="shop-no-sidebar.html">shop no sidebar</a></li>
                                  <li><a href="shop-fullwide.html">shop fullwide</a></li>
                                </ul>
                              </li>
                              <li>
                                <a class="menu-title" href="#">Shop Layout</a>
                                <ul>
                                  <li><a href="shop-fullwide-no-sidebar.html">fullwide no sidebar </a></li>
                                  <li><a href="shop-list.html">list style</a></li>
                                  <li><a href="shop-list-2col.html">list 2 column</a></li>
                                  <li><a href="shop-list-no-sidebar.html">list no sidebar</a></li>
                                </ul>
                              </li>
                              <li>
                                <a class="menu-title" href="#">Product Details</a>
                                <ul>
                                  <li><a href="product-details.html">standard style</a></li>
                                  <li><a href="product-details-2.html">standard style 2</a></li>
                                  <li><a href="product-details-tab1.html">tab style 1</a></li>
                                  <li><a href="product-details-tab2.html">tab style 2</a></li>
                                  <li><a href="product-details-tab3.html">tab style 3 </a></li>
                                </ul>
                              </li>
                              <li>
                                <a class="menu-title" href="#">Product Details</a>
                                <ul>
                                  <li><a href="product-details-gallery.html">gallery style </a></li>
                                  <li><a href="product-details-sticky.html">sticky style</a></li>
                                  <li><a href="product-details-slider.html">slider style</a></li>
                                  <li><a href="product-details-affiliate.html">Affiliate style</a></li>
                                </ul>
                              </li>
                            </ul>
                          </li>
                          --}}
                        </ul>
                      </nav>
                    </div>
                  </div>
                  <div class="col-xl-2 col-lg-3 header-right-border">
                    <div class="header-right-wrap header-right-white ">
                      <div class="search-wrap common-style mr-45">
                        <button class="search-active">
                          <i class="la la-search"></i>
                        </button>
                      </div>
                      <div class="cart-wrap common-style mr-15">
                        <button class="cart-active" onclick="getWishlist()">
                          <i class="la la-heart-o"></i>
                          <sup>
                            <p class="cartsup" id="wishItem"><?php
                            if(!empty(Auth::user()->id))
                            {
                              $wish_count = DB::table('liked_products')->where('liked_customers_id',Auth::user()->id)->count();
                              echo $wish_count;
                            }
                            else { echo 0; }
                            ?></p>
                          </sup>
                        </button>
                      </div>
                      <div class="cart-wrap common-style">
                        <button class="cart-active" onclick="showMiniCart()">
                          <i class="la la-shopping-cart"></i>
                          <sup>
                            <p class="cartsup" id="cartItem">0</p>
                          </sup>
                          <!--  <span class="count-style count-theme-color">2 Items</span> -->
                        </button>
                        <div class="shopping-cart-content" id="miniCart">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- main-search start -->
              <div class="main-search-active">

                <div class="sidebar-search-icon">
                  <button class="search-close"><span class="la la-close"></span></button>
                </div>
                
                <form action="/global-search-product" method="GET">
                  <div class="form-search">
                    <div class="row">
                    </div>
                    <div class="row">
                      <div class="col-sm-10">
                        <input id="search" class="input-text" value="" placeholder="Search Now" type="search" name="product_keyword">

                      </div>
                      <div class="col-sm-2">
                        <button class="web-search">
                          <i class="la la-search"></i>
                        </button>
                      </div>
                    </div>

                  </div>
                </form>
                
              </div>
            </div>
            <div class="top-head2" style="background-color: #ddd">
              <a href="https://t.me/Homeglare" style="padding-right: 1%" target="blank">
                <img src="{{asset('/logo/telegram.png')}}" style="width: 25px; height: auto">&nbsp;&nbsp;Homeglare
              </a>
              <a href="https://api.whatsapp.com/send?phone=8585900059&text=Hey" target="blank">
                <img src="{{asset('/logo/icon.png')}}" style="width: 25px; height: auto">
              8585900059</a>
            </div>
            <div class="header-small-mobile header-mobile-white">

              <div class="container-fluid">
                <div class="row align-items-center">

                  <div class="col-6">
                    <div class="mobile-logo">
                      <a style="font-family: 'Azonix Regular'; color: white; font-size: 20px" href="/">{{-- <img src="/homeglare-new/images/logo/logo.png" alt="logo"> --}}Homeglare</a>
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="header-right-wrap mr-10">
                      <div class="cart-wrap common-style">

                        <button class="cart-active" onclick="getWishlist()">
                          <i class="la la-heart-o"></i>
                          <sup>
                            <p class="cartsup" id="wishItem"><?php
                            if(!empty(Auth::user()->id))
                            {
                              $wish_count = DB::table('liked_products')->where('liked_customers_id',Auth::user()->id)->count();
                              echo $wish_count;
                            }
                            else { echo 0; }
                            ?></p>
                          </sup>
                        </button>
                        
                      </div>
                      <div class="cart-wrap common-style">
                        <button class="cart-active" onclick="showMiniCart()">
                          <i class="la la-shopping-cart"></i>
                          <!--  <span class="count-style">2 Items</span> -->
                          <sup>
                            <p class="cartsup" id="cartItem2">0</p>
                          </sup>
                        </button>
                        <div class="shopping-cart-content" id="miniCart2">
                        </div>

                      </div>
                      <div class="mobile-off-canvas">
                        <a class="mobile-aside-button" href="#"><i class="la la-navicon la-2x"></i></a>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </header>
          <div class="mobile-off-canvas-active">
            <a class="mobile-aside-close"><i class="la la-close"></i></a>
            <div class="header-mobile-aside-wrap">
              <div class="mobile-search">
                <form class="search-form" action="{{url('global-search-product')}}" method="GET">
                  <input id="search" class="input-text" value="" placeholder="Search Now" type="search" name="product_keyword">
                  <button class="button-search"><i class="la la-search"></i></button>
                </form>
              </div>
              <div class="mobile-menu-wrap">
                <!-- mobile menu start -->
                <div class="mobile-navigation">
                  <!-- mobile menu navigation start -->
                  <nav>
                    <ul class="mobile-menu">
                      <li><a href="/">Home</a></li>
                      <li><a href="/about">About us </a></li>
                      {{-- 
                        <li><a href="/productw">Products</a></li>
                        --}}
                        <li><a href="/contact">Contact Us</a></li>
                        <li><a href="/sell-on-homeglare">Sell on Homeglare</a></li>

                      </ul>
                    </nav>
                    <!-- mobile menu navigation end -->
                  </div>
                  <!-- mobile menu end -->
                </div>
                <div class="mobile-curr-lang-wrap">
                  <div class="single-mobile-curr-lang">
                    @if(!empty(Auth::user()->id))
                    <a class="mobile-account-active" href="/accountw">{{ucfirst(explode(" ",Auth::user()->first_name ?? '')[0])}} <i class="la la-angle-down"></i></a>
                    <div class="lang-curr-dropdown account-dropdown-active">
                      <ul>
                        <li><a href="{{url('account/'.Auth::user()->id)}}">Dashboard</a></li>
                        <li>
                          <a href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                          {{'Logout'}}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                        </form>
                      </li>
                    </ul>
                  </div>
                  @else

                  <a href="{{url('customer/login')}}">Login </a>
                  <a href="{{url('customer/login#lg2')}}">Register </a>

                  @endif
                </div>
              </div>
              <div class="mobile-social-wrap">
                <a href="https://www.facebook.com/homeglareonline" class="facebook" data-toggle="tooltip" target="_blank" title="Facebook"><i class=" ti-facebook"></i></a>
                <a class="twitter" href="https://www.twitter.com" data-toggle="tooltip" target="_blank" title="Twitter"><i class="ti-twitter-alt"></i></a>
                {{-- <a class="pinterest" href="#"><i class="ti-pinterest"></i></a> --}}
                {{-- <a class="google" href="https://www.plus.google.com/discover" data-toggle="tooltip" target="_blank" title="Google Plus"><i class="ti-google"></i></a> --}}
                <a class="instagram" href="https://www.instagram.com" data-toggle="tooltip" target="_blank" title="Instagram"><i class="ti-instagram"></i></a>
              </div>
            </div>
          </div>
          @yield('content')
          {{-- 
            <div class="subscribe-area pb-100">
              <div class="container">
                <div class="subscribe-bg bg-img pt-45 pb-50 pl-20 pr-20" style="background-image:url(/homeglare-new/images/bg/bg-3.jpg);">
                  <div class="row">
                    <div class="col-lg-6 ml-auto mr-auto">
                      <div class="subscribe-content-3 text-center">
                        <h2>Sign up &amp; Get all updates.</h2>
                        <div id="mc_embed_signup" class="subscribe-form-3 mt-20">
                          <form id="mc-embedded-subscribe-form" class="validate subscribe-form-style" novalidate="" name="mc-embedded-subscribe-form" method="post" action="http://devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&amp;id=05d85f18ef">
                            <div id="mc_embed_signup_scroll" class="mc-form">
                              <input class="email" type="email" required="" placeholder="Enter Your E-mail" name="EMAIL" value="">
                              <div class="mc-news" aria-hidden="true">
                                <input type="text" value="" tabindex="-1" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef">
                              </div>
                              <div class="clear">
                                <input id="mc-embedded-subscribe" class="button" type="submit" name="subscribe" value="">
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            --}}
            <footer class="footer-area">
              {{-- <div class="feature-area ">
                <div class="container-fluid">
                  <div class="feature-border feature-border-about">
                    <div class="row">
                      <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="feature-wrap mb-30 text-center">
                          <img src="/homeglare-new/images/icon-img/feature-icon-1.png" alt="">
                          <h5>Best Product</h5>
                          <span>Best Queality Products</span>
                        </div>
                      </div>
                      <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="feature-wrap mb-30 text-center">
                          <img src="/homeglare-new/images/icon-img/feature-icon-2.png" alt="">
                          <h5>100% fresh</h5>
                          <span>Best Queality Products</span>
                        </div>
                      </div>
                      <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="feature-wrap mb-30 text-center">
                          <img src="/homeglare-new/images/icon-img/feature-icon-3.png" alt="">
                          <h5>Secure Payment</h5>
                          <span>Best Quality Products</span>
                        </div>
                      </div>
                      <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="feature-wrap mb-30 text-center">
                          <img src="/homeglare-new/images/icon-img/feature-icon-4.png" alt="">
                          <h5>Winner Of 15 Awards</h5>
                          <span>Healthy food and drink 2019</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div> --}}
              <div class="footer-top pt-105 pb-20 default-overlay footer-overlay">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-3 col-md-6 col-12 col-sm-6">
                      <div class="footer-widget footer-contact-wrap-2 mb-40" style="margin-top: -40px">
                        <!-- <a href="#"><img src="/homeglare-new/images/logo/logo.png" alt="logo"></a> -->

                        <div class="footer-contact-wrap">
                          <p style="margin:35px">

                            <a href="#"><img width="100%" src="{{asset('logo/recog.jpeg')}}" style="height: auto"></a>&nbsp;&nbsp;
                            <a href="#"><img width="100%" src="{{asset('logo/recog3.jpeg')}}" alt="ISO"  style="height: auto"></a>&nbsp;&nbsp;<br>
                            <a href="#"><img src="{{asset('logo/iso.png')}}" alt="ISO"  style="width:60px; height: auto"></a>&nbsp;&nbsp;ISO 9001:2015 Certified<br>
                          </div>
                        </div>
                      </div>

                      <div  style="text-align: center" class="col-lg-3 col-md-6 col-12 col-sm-6">
                        <div class="footer-widget mb-30">
                          <div class="footer-title-2">
                            <h3>Information</h3>
                          </div>
                          <div class="footer-contact-wrap">
                            <ul class="">
                              <li><a href="{{url('/about')}}">About Us</a></li>
                              <li><a href="{{url('/disclaimer')}}">Disclaimer</a></li>
                              <li><a href="{{url('/terms&conditions')}}">Terms & Conditions</a></li>
                              <li><a href="{{url('/privacy-policy')}}">Privacy Policy</a></li>
                              <li><a href="{{url('/return-policy')}}">Return Policy</a></li>
                              <li><a href="{{url('/cookie-policy')}}">Cookie Policy</a></li>
                              <li><a href="{{url('/contact')}}">Contact us</a></li>
                              <li><a href="{{url('/career')}}">Careers</a></li>
                              <li><a href="{{url('/complaint')}}">Complaint</a></li>
                              <li><a href="{{url('/feedback')}}">Feedback & Suggestion</a></li>
                              <!-- <li><a href="{{url('/faq')}}">FAQ</a></li> -->
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div  style="text-align: center" class="col-lg-3 col-md-6 col-12 col-sm-6">
                        <div class="footer-widget mb-10">
                          <div class="footer-title-2">
                            <h3>Buy On Homeglare</h3>
                          </div>
                          <div class="footer-contact-wrap">
                            <ul class="">
                              <li>100% Purchase Product  </li>
                              <li>Easy Return</li>
                              <li>Genuine Product </li>
                              <li>Secure Payment & Safe Ordering</li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div  style="text-align: center" class="col-lg-3 col-md-6 col-12 col-sm-6">
                        <div class="footer-widget mb-10">
                          <div class="footer-title-2">
                            <h3>Contact Us</h3>
                          </div>
                          <div class="footer-contact-wrap">
                            @if($company)
                            <p><i class=" ti-home"></i>&nbsp;&nbsp;Address: </p>
                            <span > <?php echo $company->address ?> <br></span></p>
                            <p><i class=" ti-headphone-alt "></i>&nbsp;&nbsp;Call Us: @if(isset($contact))
                              @foreach($contact as $con)
                              <a href="tel://+91{{$con}}">+91 {{$con}}.&nbsp;&nbsp;</a> 
                              <br>
                              @endforeach
                              @endif
                            </p>
                            <p><i class=" ti-email"></i>&nbsp;&nbsp;Email: <a href="mailto:{{$company->support_email}}">{{$company->support_email}}</a> 
                              @endif
                            </div>
                          </div>
                        </div>


                      </div>
                      <div class="row ">

                        <div class="col-lg-4 col-md-6 col-12 col-sm-6">
                          <div class="footer-widget mb-30">
                            <div class="footer-title-2">
                              <h3>Logistic Partner</h3>
                            </div>
                            <div class="footer-list-2 mx-auto" >
                              <ul class="logistic-partners">
                                <li><a href="#"><img src="/logo/delhivery.png" style="width: 65px; height: auto"></a></li>
                                <li><a href="#"><img src="/logo/xpressbees.jpg" style="width: 65px; height: auto"></a></li>
                                <li><a href="#"><img src="/logo/fedex.webp" style="width: 65px; height: auto"></a></li>
                                <li><a href="#"><img src="/logo/blue_dart.png" style="width: 65px; height: auto"></a></li>
                                <!-- <li><a href="{{url('getshiprocket')}}"><img src="/logo/shiprocket.webp" style="width: 65px; height: auto"></a></li> -->
                              </ul>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-12 col-sm-6 text-center">
                          <div class="footer-widget mb-30">
                            <div class="footer-title-2">
                              <h3>Available On</h3>
                            </div>
                            <ul class="app-link-img">
                              <li> <img src="/app-code/playstore.png" class="google-play" style="margin-top: 0"> </li>
                              <li> <img  src="/app-code/qr.jpeg" class="qr-code"> </li>
                            </ul>
                          </div>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-lg-3 col-md-6 col-12 col-sm-6">
                          <div class="footer-widget mb-30">
                            <div class="footer-title-2">
                              <h3>Subscription</h3>
                            </div>
                            <div class="footer-contact-wrap">

                              <span>Subscribe to our newsletters now and stay up-to-date with new collections.</span>
                              <div id="mc_embed_signup" class="subscribe-form-2">
                                <form class="validate subscribe-form-style" name="mc-embedded-subscribe-form" method="POST" action="@if(Auth::user()){{ route('newsletter') }}@else{{  url('login') }}@endif">
                                  <div id="newsletterEmail" class="mc-form">
                                    @csrf
                                    <input class="email" type="email" required="" placeholder="Enter Your E-mail" name="email" value="">

                                    <div class="clear">
                                      <input id="mc-embedded-subscribe" class="button" type="submit" name="subscribe" value="">
                                    </div>
                                  </div>
                           <!--  @if(session('NewslatterMessege'))
                            <div class="alert alert-success" id="NewslatterMessege" style="margin-top: 10px">
                              {{ session('NewslatterMessege') }}
                            </div>
                            @endif -->
                          </form>
                        </div>
                        <div class="footer-social-hm5">
                          <span>Follow US</span>
                          <ul >
                            <li><a style="font-size: 30px" href="https://www.facebook.com/homeglareonline" data-toggle="tooltip" target="_blank" title="Facebook" class="facebook"><i class=" ti-facebook"></i></a></li>
                            <li><a style="font-size: 30px" class="youtube" href="https://www.youtube.com/channel/UCkysmqGqLvYxzGTCOz6LDVw" data-toggle="tooltip" target="_blank" title="Youtube"><i class="ti-youtube"></i></a></li>
                            {{-- 
                              <li><a class="pinterest" href="#"><i class="ti-pinterest"></i></a></li>
                              --}}
                              {{-- 
                                <li><a class="google" href="https://www.plus.google.com/discover" data-toggle="tooltip" target="_blank" title="Google Plus"><i class="ti-google"></i></a></li>
                                --}}
                                <li><a style="font-size: 30px" class="instagram" href="https://www.instagram.com/home_glare/" data-toggle="tooltip" target="_blank" title="Instagram"><i class="ti-instagram"></i></a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="footer-bottom bg-black-2 ptb-10">
                  <div class="container">
                    <div class="copyright copyright-2 text-center">
                      <p>Copyright © <a href="/">HomeGlare</a>. All Right Reserved</p>
                    </div>
                  </div>
                </div>
              </footer>


              <!--Start of Tawk.to Script-->
              <script type="text/javascript">
                var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
                (function(){
                  var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                  s1.async=true;
                  s1.src='https://embed.tawk.to/5e28277edaaca76c6fcf46aa/default';
                  s1.charset='UTF-8';
                  s1.setAttribute('crossorigin','*');
                  s0.parentNode.insertBefore(s1,s0);
                })();
              </script>
              <!--End of Tawk.to Script-->

              <!-- Modal -->
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-md-5 col-sm-6 col-xs-12">
                          <div class="tab-content quickview-big-img">
                            <div id="pro-1" class="tab-pane fade show active">
                              <img src="" alt="" id="pro1">
                            </div>
                            <div id="pro-2" class="tab-pane fade">
                              <img src="" alt="" id="pro2">
                            </div>
                         <!--  <div id="pro-3" class="tab-pane fade">
                            <img src="" alt="" id="pro3">
                          </div> -->
                          <div id="pro-4" class="tab-pane fade">
                            <img src="" alt="" id="pro4">
                          </div>
                        </div>
                        <!-- Thumbnail Large Image End -->
                        <!-- Thumbnail Image End -->
                        <div class="quickview-wrap mt-15">
                          <div class="quickview-slide-active owl-carousel nav nav-style-2" role="tablist">
                            <a class="active" data-toggle="tab" href="#pro-1"><img src="" alt="" id="pro5"></a>
                            <a data-toggle="tab" href="#pro-2"><img src="" alt="" id="pro6"></a>
                            <!-- <a data-toggle="tab" href="#pro-3"><img src="" alt="" id="pro7"></a> -->
                            <a data-toggle="tab" href="#pro-4"><img src="" alt="" id="pro8"></a>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-7 col-sm-6 col-xs-12">
                        <div class="product-details-content quickview-content">
                          <span id="pdCategory"></span>
                          <h3 id="pdName"></h3>
                          <h5 id="psku"></h5>
                          <div class="pro-details-price-wrap">
                            <div class="product-price">
                              <span id="pdPrice" style="font-size: 20px">₹</span>
                            </div>
                          </div>
                          <h5 id="pdAvailability"></h5>
                          <h4 id="pdMoq" style="margin-top: 15px;"></h4>
                          <h4 id="pdCarton" style="margin-top: 15px;"></h4>
                          <div class="description" style="margin-top: 25px;">
                            <p id="pdDescription" style="font-size: 12px"></p>
                          </div>


                          <div class="pro-details-compare-wishlist" id="modal-product">
                            <div class="pro-details-buy-now btn-hover btn-hover-radious">
                              <a class="btn sample-product show-active" id="sampleProduct" style="padding: 9px !important; margin-top: 20px ">Order Sample Products</a>
                            </div>
                            <div class="pro-details-buy-now btn-hover btn-hover-radious">
                              &nbsp;&nbsp;<a id="orderMoq" class="show-moq" onclick="showMOQModal('moq')">Order Min Order Qty</a>
                            </div>

                            <div class="pro-details-buy-now btn-hover btn-hover-radious " >
                              &nbsp;&nbsp;<a id="orderCarton" class="show-carton" onclick="showCartonModal('carton')">Order Carton Qty</a>
                            </div>

                            
                          </div>



                          <input type ="hidden" id="pid" name="pid" value="1">
                          <input type ="hidden" id="categoryModel" name="categoryModel" value="moq">
                          <input type ="hidden" id="stockcheck" name="stockcheck" value="">
                          <input type ="hidden" id="fixed_quantity" name="fixed_quantity" value="{{'1'}}">
                          <input type ="hidden" id="product_tax" name="product_tax" value="{{'0'}}">
                          <input type ="hidden" id="send_tax" name="send_tax" value="{{'0'}}">



                          <div class="row" style="padding: 0">
                            <div class="col-sm-4 moq-qty" style="padding: 0">
                              <label style="width: 50; position: absolute;margin-top: 8px">Select Qty: </label>
                              <div class="cart-plus-minus" id="moq-quantity">
                                <input class="cart-plus-minus-box" type="text" name="qtybutton" value="" id="quantitydyn" style="width: 100%">
                              </div>
                            </div>


                            <div class="col-sm-3 moq-qty" style="padding: 0">
                              <div class="pro-details-buy-now btn-hover btn-hover-radious" >

                                <a class="add-cart" class="btn " id="pdAddToCartMoq">Add To Cart</a>
                              </div>
                            </div>

                            <div class="col-sm-4 carton-qty" style="padding: 0">
                              <!-- <label>&nbsp;&nbsp;Carton</label> -->
                              <label style="width: 50; position: absolute;margin-top: 8px">Select Qty: </label>
                              <div class="cart-plus-minus" id="cartoon-qty">
                                <input class="cart-plus-minus-box" type="text" name="qtybutton1" value="1" id="cartoondyn" style="width: 100%">
                              </div>
                            </div>

                            <div class="col-sm-3 carton-qty" style="padding: 0">
                              <div class="pro-details-buy-now btn-hover btn-hover-radious" >

                                <a  class="btn add-cart" id="pdAddToCartCarton">Add To Cart</a>

                              </div>
                            </div>

                            <div class="col-sm-3">
                              <div class=" pro-details-buy-now btn-hover btn-hover-radious">
                                <a id="buyNow"  class="btn buy-now" title="Buy Now">
                                  <i class="la la-cart-plus"></i> Buy Now
                                </a>
                              </div>
                            </div>
                            <div class="col-sm-2">
                              <div class=" pro-details-buy-now btn-hover btn-hover-radious" id="wishlist">
                                <a title="Add To Wishlist" id="pdaddToWishlist" style=" width: 100%">
                                  <i class="la la-heart-o"></i>
                                </a>
                              </div>
                            </div>

                          </div>












                          <div class="pro-details-compare-wishlist">
                            <div id="messageBox2" class="alert alert-success" style="display: none;">Product added successfully.</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Modal end -->



              <!-- Alert Modal -->
              <div class="modal fade" id="exampleModalAlert" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document" style="margin: 15% auto; max-width: 600px; width: 575px;z-index: 999;">
                  <div class="modal-content">

                    <div class="modal-body" style="text-align: center; margin-top: 25px">
                      <div class="row" style="text-align: center;">
                        <div style="text-align: center; width: 100%">
                          <h3 class="alert-msg2" style="text-align: center;"></h3>
                          <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close" style="background-color: #d20e14; color: #fff; width: 100px; border: 1px solid #fff; margin-top: 10%; margin-bottom: 5%">Ok</button> 
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Alert Modal end -->

              <!-- Apply Coupon Modal -->
              <div class="modal fade" id="exampleModalCoupon" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document" style="margin: 15% auto; max-width: 600px; width: 575px;z-index: 999;">
                  <div class="modal-content">

                    <div class="modal-body" style="text-align: left; margin-top: 25px">
                      <div class="row" style="text-align: left;">
                        <div style="text-align: left; width: 80%; margin: auto;">
                          <h5 class="alert-msg" style="">Have a coupon code? Enter coupon code here .</h5>
                          <input type="text" name="coupon_code" placeholder="" style="border:none; border-bottom: 1px solid #afafaf; width: 90%;"><br>

                          <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close" style="background-color: #d20e14; color: #fff; width: 100px; border: 1px solid #fff; margin-top: 10%; margin-bottom: 5%">Apply</button> 
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Apply Coupon Modal end -->


            </div>
            <a href="javascript:void(0)" class="mypresta_scrollup hidden-phone open"><i class="ti-arrow-up"></i></a>
    <!-- JS
      ============================================ -->
      <!-- Modernizer JS -->
      <script src="/homeglare-new/js/vendor/modernizr-3.6.0.min.js"></script>
      <!-- Modernizer JS -->
      <script src="/homeglare-new/js/vendor/jquery-3.3.1.min.js"></script>
      <!-- Popper JS -->
      <script src="/homeglare-new/js/vendor/popper.js"></script>
      <!-- Bootstrap JS -->
      <script src="/homeglare-new/js/vendor/bootstrap.min.js"></script>
      <!-- Slick Slider JS -->
      <script src="/homeglare-new/js/plugins/countdown.js"></script>
      <script src="/homeglare-new/js/plugins/counterup.js"></script>
      <script src="/homeglare-new/js/plugins/images-loaded.js"></script>
      <script src="/homeglare-new/js/plugins/isotope.js"></script>
      <script src="/homeglare-new/js/plugins/instafeed.js"></script>
      <script src="/homeglare-new/js/plugins/jquery-ui.js"></script>
      <script src="/homeglare-new/js/plugins/jquery-ui-touch-punch.js"></script>
      <script src="/homeglare-new/js/plugins/magnific-popup.js"></script>
      <script src="/homeglare-new/js/plugins/owl-carousel.js"></script>
      <script src="/homeglare-new/js/plugins/scrollup.js"></script>
      <script src="/homeglare-new/js/plugins/waypoints.js"></script>
      <script src="/homeglare-new/js/plugins/slick.js"></script>
      <script src="/homeglare-new/js/plugins/wow.js"></script>
      <script src="/homeglare-new/js/plugins/textillate.js"></script>
      <script src="/homeglare-new/js/plugins/elevatezoom.js"></script>
      <script src="/homeglare-new/js/plugins/sticky-sidebar.js"></script>
      <script src="/homeglare-new/js/plugins/smoothscroll.js"></script>
      <!-- Main JS -->
      <script src="/homeglare-new/js/main.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.js"></script>
      @yield('script')


      <script type="text/javascript">
        function showMOQModal(category){
          var pid=$('#pid').val();
          $('#categoryModel').val('moq');
          $('.carton-qty').hide();
          $('.moq-qty').show();
          $('.show-carton').removeClass('show-active');
          $('.sample-product').removeClass('show-active');
          $('.show-moq').addClass('show-active');
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
          $.ajax({
           url: '/getPrice',
           type: 'POST',
           data: {_token: CSRF_TOKEN, pid: pid, category : category},
           success: function (data) {

            $('#pdPrice').text('₹'+data);
          },
          failure: function (data) {
           Swal(data);
         }
       });
        }


        function BuyNowModel(id){
          var stock = $('#stock'+id).val();
          var category = $('#categoryModel').val();
          if(stock == 'in'){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            userId = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id;} else {echo 0;}?> ;
            if(category == 'carton'){
              var quantity = $('#carton'+id).val();
            }
            else{
              var quantity = $('#quantity'+id).val();
            }
            window.location.href = '/checkout/single/'+id+'-'+quantity+'-'+category;
          }
          else{
            Swal('Product out of stock');
          }
        }


        function showCartonModal (category){
          var pid=$('#pid').val();
          $('#categoryModel').val('carton');
          $('.moq-qty').hide();
          $('.carton-qty').show();
          $('.show-moq').removeClass('show-active');
          $('.sample-product').removeClass('show-active');
          $('.show-carton').addClass('show-active');
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
          $.ajax({
           url: '/getPrice',
           type: 'POST',
           data: {_token: CSRF_TOKEN, pid: pid, category : category},
           success: function (data) {

            $('#pdPrice').text('₹'+data);
          },
          failure: function (data) {
           Swal(data);
         }
       });
        }
      </script>


      <script>



        function getWishlist(){
         var data = <?php if(!empty(Auth::user()->id)){echo Auth::user()->id ;} else {echo '0';}?>;
         if(data > 0){
          window.location.href = '/wishlist/'+data;
        }
        else{
          window.location.href = '/customer/login';

        }
      }
      
      function showMiniCart(){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
         url: '/show-minicart',
         type: 'GET',
         data: {_token: CSRF_TOKEN},
         success: function (data) {
          $('#miniCart').html(data);
          $("#total-bill").text(data.bill); 
          $('#miniCart2').html(data);
          $("#total-bill2").text(data.bill);
        }
      });
      }
      
      function deleteProduct(id){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      // var std = $("#show-total").html();
      // count_product--;
      // alert(id);
      
      $.ajax({
        /* the route pointing to the post function */
        url: '/cart/delete-product/'+id,
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        data: {_token: CSRF_TOKEN, id: id},
        success: function (data) {
          $('#listhide'+id).hide();
          $('#cart'+id).hide();
          $("#show-total").html(data.showcount);
          $("#total-bill").html(data.bill+".00");

          $('#cartItem').html(data.cartItem);
          $('#cartItem2').html(data.cartItem);   
          if(data.status == 0)        {
            setTimeout(function(){
             window.location.reload(1);
           }, 2000);
          } 
        }
      });
    }

    function checkCart(){
      var count = <?php echo $count;?>;
      // alert(count);
      $('#cartItem').html(count);
      $('#cartItem2').html(count);

      @if(!empty(session()->get('review-success')))
      Swal('Thanks for your feedback');
      @endif   
    }

    function showAlert(data){
      // $('#miniCart').hide();
      $('.alert-msg').text(data);

      $('#show-alert').click();
      
    }



    $('#exampleModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var productId = button.data('myid')
      var modal = $(this)
      // get product details data
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      
      $.ajax({
        url: '/get-product_details-data/'+productId,
        type: 'GET',
        data: {_token: CSRF_TOKEN},
        success: function (response) {
          // var responseObj = JSON.parse(data);
          data = response.data;
          category = response.category;
          price = response.price;
          moq = response.moq;
          stock = response.stock;
          carton = response.carton;
          brand = response.brand;
          images = response.products_images;

          $("#pdName").html(data.products_name);
          $("#pid").val(productId);
          
          $("#psku").html("<b style='color: #afafaf'>SKU - "+data.products_sku_code+"</b>");
          $("#pdDiscount").text('-'+response.discount+'%');
          $("#pdCry").text(category.categories_name);
          $("#pdDescription").html(data.products_description);
          $("#pdPrice").html('₹ '+price);
          $("#pdMoq").html("MOQ : "+moq+ " Pieces");
          $("#pdCarton").html("Carton : "+carton+ " Pieces");
          $("#pdName").attr('href', '/product-detail/'+data.products_slug);

          $("#buyNow").attr('onclick', 'BuyNowModel('+data.products_id+')');
          $("#pdAddToCartMoq").attr('onclick', 'addToCart('+data.products_id+',"normal","moq")');
          $("#pdAddToCartCarton").attr('onclick', 'addToCart('+data.products_id+',"carton","carton")');
          $("#sampleProduct").attr('onclick', 'addToCart('+data.products_id+',"sample")');
          $("#pdaddToWishlist").attr('onclick', 'addToWishlist('+data.products_id+')');
          $("#quantitydyn").val(moq);
          $("#cartoondyn").val(moq);
          $("#stockcheck").val(stock);
          $("#product_tax").val(data.tax_percent);
          $("#fixed_quantity").val(moq);
          if(parseInt(data.products_quantity) > parseInt(moq))
           $("#pdAvailability").html("Availability: In Stock");
         else
           $("#pdAvailability").html("Availability: Out of Stock");

          if(brand)
            $("#pdBrand").html('Brand: '+ brand.brand_name);

          $("#pro1").attr('src', '/'+response.products_images[0].path);
          $("#pro2").attr('src', '/'+response.products_images[1].path);
          $("#pro4").attr('src', '/'+response.products_images[2].path);

          $("#pro5").attr('src', '/'+response.products_images[0].path);
          $("#pro6").attr('src', '/'+response.products_images[1].path);
          $("#pro8").attr('src', '/'+response.products_images[2].path);
          // $("#pdMrp").html('MRP: ₹'+data.mrp);


          
         
          
       }
     });
    })

  </script>
</body>
</html>